msgid ""
msgstr ""
"Project-Id-Version: LearnDash Pro Panel v1.0.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2015-05-11 17:24:31+0000\n"
"Last-Translator: fassalab <julien@fassaha.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n>1;\n"
"X-Generator: CSL v1.x\n"
"X-Poedit-Language: French\n"
"X-Poedit-Country: FRANCE\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-SearchPath-0: .\n"
"X-Textdomain-Support: yes"

#: learndash-propanel.php:29
#@ default
msgid "LearnDash Pro Panel #2"
msgstr ""

#: learndash-propanel.php:35
#@ default
msgid "LearnDash Pro Panel #3"
msgstr ""

#: learndash-propanel.php:48
#@ ld_propanel
msgid "LearnDash Activity"
msgstr ""

#: learndash-propanel.php:53
#@ ld_propanel
msgid "Uploaded Assignments"
msgstr ""

#: tpl/activity_panel.php:5
#: tpl/assignments_panel.php:9
#@ ld_propanel
msgid "See user progress"
msgstr ""

#: tpl/activity_panel.php:12
#@ ld_propanel
msgid "Quiz"
msgstr ""

#: tpl/activity_panel.php:12
#@ ld_propanel
msgid "completed"
msgstr ""

#: tpl/activity_panel.php:13
#@ ld_propanel
msgid "Passed"
msgstr ""

#: tpl/activity_panel.php:13
#@ ld_propanel
msgid "Not Passed"
msgstr ""

#: tpl/activity_panel.php:14
#@ ld_propanel
msgid "Score"
msgstr ""

#: tpl/activity_panel.php:14
#: tpl/activity_panel.php:17
#@ ld_propanel
msgid "out of"
msgstr ""

#: tpl/activity_panel.php:16
#@ ld_propanel
msgid "Course"
msgstr ""

#: tpl/activity_panel.php:16
#@ ld_propanel
msgid "updated"
msgstr ""

#: tpl/activity_panel.php:17
#: tpl/assignments_panel.php:29
#: tpl/assignments_panel.php:55
#@ ld_propanel
msgid "Completed"
msgstr ""

#: tpl/activity_panel.php:17
#@ ld_propanel
msgid "steps"
msgstr ""

#: tpl/activity_panel.php:22
#@ ld_propanel
msgid "No activity found."
msgstr ""

#: tpl/activity_panel.php:26
#@ ld_propanel
msgid "Export Quiz Data"
msgstr ""

#: tpl/activity_panel.php:27
#@ ld_propanel
msgid "Export Course Data"
msgstr ""

#: tpl/assignments_panel.php:11
#@ ld_propanel
msgid "in"
msgstr ""

#: tpl/assignments_panel.php:13
#@ ld_propanel
msgid "See lesson assignments"
msgstr ""

#: tpl/assignments_panel.php:15
#@ ld_propanel
msgid "uploaded"
msgstr ""

#: tpl/assignments_panel.php:23
#@ ld_propanel
msgid "Download"
msgstr ""

#: tpl/assignments_panel.php:33
#@ ld_propanel
msgid "Mark as completed"
msgstr ""

#: tpl/assignments_panel.php:39
#@ ld_propanel
msgid "Delete"
msgstr ""

#: tpl/assignments_panel.php:43
#@ ld_propanel
msgid "Marking as completed ..."
msgstr ""

#: tpl/assignments_panel.php:45
#@ ld_propanel
msgid "Deleting ..."
msgstr ""

#: tpl/assignments_panel.php:51
#@ ld_propanel
msgid "No assigments found."
msgstr ""

#: tpl/assignments_panel.php:57
#@ ld_propanel
msgid "Are you sure you want to delete this assignment?"
msgstr ""

#: wp_autoupdate_sfwd_lms.php:204
#@ default
msgid "You do not have sufficient permissions to access this page."
msgstr ""

#: wp_autoupdate_sfwd_lms.php:227
#@ learndash
msgid "settings saved."
msgstr ""

#: wp_autoupdate_sfwd_lms.php:250
#@ learndash
msgid "Update License"
msgstr ""

