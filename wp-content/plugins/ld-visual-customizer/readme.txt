=== Learndash Visual Customizer ===
Contributors: Ross Johnson
Tags: learndash
Requires at least: 3.5.0
Tested up to: 4.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Customize the look and feel of Learndash with five custom skins, icons and color pickers. 

== Description ==

Learndash Visual Customizer gives you the ability to select from five custom skins each with their own unique look, feel and icons and customize colors using a set of custom color pickers. If you like the default Learndash appearance but want new colors the Learndash Visual Customizer lets you do that as well.

= Website =
http://fromhighorbit.com/downloads/learndash-visual-customizer/

= Documentation =
http://fromhighorbit.com/docs/learndash-visual-customizer/

= Bug Submission and Forum Support =
http://fromhighorbit.com/support

== Installation ==

1. Upload 'ld-visual-customizer' to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Under Learndash > Settings you will have a new tab called "Appearance"
4. Under appearance you can switch between selecting your base theme, customizing colors and managing your license

== Changelog ==

= 1.0 = 
* Initial Release!