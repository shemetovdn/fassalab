function process_test_checkbox() {
	if (jQuery('#sfwd-stripe_test_mode input[name="learndash_stripe_settings[test_mode]"]').is(':checked')) {

		jQuery('div#sfwd-stripe_secret_key_live').hide();
		jQuery('div#sfwd-stripe_publishable_key_live').hide();

		jQuery('div#sfwd-stripe_secret_key_test').show();
		jQuery('div#sfwd-stripe_publishable_key_test').show();
		
	} else {

		jQuery('div#sfwd-stripe_secret_key_test').hide();
		jQuery('div#sfwd-stripe_publishable_key_test').hide();

		jQuery('div#sfwd-stripe_secret_key_live').show();
		jQuery('div#sfwd-stripe_publishable_key_live').show();
	}
}

jQuery(document).ready(function($) {
	if (jQuery('#sfwd-stripe_test_mode input[name="learndash_stripe_settings[test_mode]"]').length) {
		process_test_checkbox();
		
		jQuery('#sfwd-stripe_test_mode input[name="learndash_stripe_settings[test_mode]"]').change(process_test_checkbox);
		
	}
});