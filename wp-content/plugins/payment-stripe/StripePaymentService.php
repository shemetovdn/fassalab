<?php
require __DIR__.'/stripeLib/vendor/autoload.php';

class StripePaymentService
{
    private $apiPublishableKey = 'pk_test_sQT1ge3NUSSeHTBR6aGNb1kr00vs5pkyw6';
    private $apiSecretKey = 'sk_test_BWr4kTRGsp6vJKm6VCn1seBf00TPVfkiRy';
    private $stripeConfig;

    public function __construct()
    {
        $this->stripeConfig = get_option('my_dashboard_stripe_options');
    }
    public function getStripeCheckout()
{
    $price = 50;
    if (!empty($_POST['branchId'])) {
        $price = $this->getPriceByBranchId($_POST['branchId']);
    }
    $orderId = bin2hex(openssl_random_pseudo_bytes(8));
        \Stripe\Stripe::setApiKey($this->stripeConfig['secret_key']);

        $session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
                'name' => 'Product_1',
                'description' => 'Test payment',
//                'images' => ['https://example.com/t-shirt.png'],
                'amount' => $price,
                'currency' => 'eur',
                'quantity' => 1,
            ]],
            'success_url' => 'https://fassalab.weeteam.net/wp-admin/admin.php?page=payment-stripe&payment-success&orderId=' . $orderId,
            'cancel_url' => 'https://fassalab.weeteam.net/wp-admin/admin.php?page=payment-stripe&payment-canceled&orderId=' . $orderId,
        ]);

        if(!empty($session->id) && !empty($_POST)) {
            $data = [
                'sessionId' => $session->id,
                'orderId' => $orderId,
                'userId' => $_POST['userId'],
                'branchId' => $_POST['branchId'],
            ];

           $this->savePayment($data);
            ?>
            <script src="https://js.stripe.com/v3/"></script>
            <script>
                var stripe = Stripe('<?php echo $this->stripeConfig['publishable_key'];?>');

                stripe.redirectToCheckout({

                    sessionId: '<?php echo $session->id;?>'
                }).then(function (result) {
                    console.log(result);
                });
            </script>
            <?php
        }
}

    public function savePayment($data = [], $insert = true)
    {

        global $wpdb;
        if ($insert) {
            $wpdb->get_results("
                        INSERT INTO
                            `buy_branches`
                            (
                                `session_id`,
                                `order_id`,
                                `user_id`,
                                `branch_id`
                            )
                            VALUES
                            (
                                '{$data['sessionId']}',
                                '{$data['orderId']}',
                                '{$data['userId']}',
                                '{$data['branchId']}'
                            )
                    ");
        } else {
            $wpdb->get_results("
                        UPDATE
                            `buy_branches`
                        SET
                            `status` = 1
                        WHERE
                            `order_id` = '{$data['orderId']}'
                    ");
        }

    }

    public function deletePayment($orderId)
    {

        global $wpdb;
        $wpdb->get_results("
                        DELETE FROM
                            `buy_branches`

                        WHERE
                            `order_id` = '{$orderId}'
                    ");

    }

    private function getPriceByBranchId($branchId)
{
    $price = 0;
    $branchTree = get_option('wtfa_decks_tree');
    foreach ($branchTree as $item) {
        if ($item['id'] == $branchId) {
            $price = $item['price'] * 100;
        }
    }

    return $price;
}
}