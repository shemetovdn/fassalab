<?php
/*
Plugin Name: strip payment
Plugin URI: http://weeteam.net
Description:
Version: 1.1
Author: WeeTeam
Author URI: http://weeteam.net
*/

require __DIR__.'/widget-config.php';
require __DIR__.'/StripePaymentService.php';
$stripePaymentService = new StripePaymentService();
if(isset($_GET['page']) && $_GET['page'] == 'payment-stripe') {
    if (isset($_GET['payment-create'])) {
         $stripePaymentService->getStripeCheckout();
    } elseif (isset($_GET['payment-success'])){
        file_put_contents(__DIR__ . '/response-stripe/get.txt', json_encode($_GET));
        file_put_contents(__DIR__ . '/response-stripe/post.txt', json_encode($_POST));
        $stripePaymentService->savePayment(['orderId' => $_GET['orderId']], false);
        echo 'it is OK';
    } elseif (isset($_GET['payment-canceled'])){
        $stripePaymentService->deletePayment($_GET['orderId']);
        echo 'it is Canseled';
    }

}