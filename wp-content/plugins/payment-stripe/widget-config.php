<?php
add_action( 'wp_dashboard_setup', 'prefix_add_dashboard_widget' );
function prefix_add_dashboard_widget() {
    wp_add_dashboard_widget(
        'my_dashboard_stripe_widget',
        'Stripe config',
        'prefix_dashboard_stripe_widget',
        'prefix_dashboard_stripe_widget_handle'
    );
    echo "<style>
#my_dashboard_stripe_widget .hndle .postbox-title-action > a{
opacity: 1;
}
</style>";
}

function prefix_dashboard_stripe_widget() {
    # получим сохраненые данные
    if( !$widget_options = get_option( 'my_dashboard_stripe_options' ) )
        $widget_options = array( );

    # вывод виджета
    $output = sprintf(
        '<h2 style="text-align:right">%s</h2>',
        __( 'Configure ☝' )
    );

    # проверим есть ли сохраненные данные
    $saved_secret_key = isset( $widget_options['secret_key'] )
        ? $widget_options['secret_key'] : false;
    $saved_publishable_key = isset( $widget_options['publishable_key'] )
        ? $widget_options['publishable_key'] : false;

    # произвольные контент сохраненный функцией, изменяет вывод
    if( $saved_secret_key ) {
        $post = get_post( $saved_secret_key );
        if( $post ) {
            $content = do_shortcode( html_entity_decode( $post->post_content ) );
            $output = "<h2>{$post->post_title}</h2><p>{$content}</p>";
        }
    }
    echo "<div class='secret_key_class_wrap'>
		<label style='background:#ccc;'>$output</label>
	</div>
	";
}

function prefix_dashboard_stripe_widget_handle(){
    # получим сохраненные данные
    if( !$widget_options = get_option( 'my_dashboard_stripe_options' ) )
        $widget_options = array( );

    # обновление
    if( 'POST' == $_SERVER['REQUEST_METHOD'] && isset( $_POST['my_dashboard_stripe_options'] ) ) {
        # проверка
        $widget_options['secret_key'] = $_POST['my_dashboard_stripe_options']['secret_key'] ;
        $widget_options['publishable_key'] = $_POST['my_dashboard_stripe_options']['publishable_key'] ;
        # сохранение данных
        update_option( 'my_dashboard_stripe_options', $widget_options );
    }

    # значения по умолчанию
    if(!isset( $widget_options['secret_key'] ))$widget_options['secret_key'] = '';
    if(!isset( $widget_options['secret_key'] ))$widget_options['publishable_key'] = '';

    echo "
	<div class='secret_key_class_wrap'>
		<br><label>Secret key</label></br>
		<input type='text' name='my_dashboard_stripe_options[secret_key]' value='" . $widget_options['secret_key'] . "'/></p>
				<label>Publishable key</label>
		<p><input type='text' name='my_dashboard_stripe_options[publishable_key]' value='" . $widget_options['publishable_key'] . "'/></p>
		";

    echo "</div>";
}