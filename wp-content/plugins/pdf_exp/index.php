<?php
/*
Plugin Name: pdf export
Plugin URI: http://weeteam.net
Description:
Version: 1.1
Author: WeeTeam
Author URI: http://weeteam.net
*/

require __DIR__.'/lib/html2pdf/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

if($_SERVER['REQUEST_METHOD'] == 'GET' && $_GET['page'] == 'pdf_exp') {

    $statistics = json_decode($_GET['statistics']);
    $html = "
            <ul>
            <li>a fait <span class=\"stepLerned\">" . $statistics->steps->stepLerned . "</span> exercices</li>
            <li>a débloqué <span class=\"wordLerned\">" . $statistics->words->wordLerned . "</span> mots de vocabulaire</li>
            <li>a débloqué la carte de compétence <span class=\"branchesUnlocked\">" . $statistics->branchesUnlocked . "</span></li>
        </ul>
    ";

    $html2pdf = new Html2Pdf();
    $html2pdf->writeHTML($html);
    $html2pdf->output('statistic.pdf', 'D');
}