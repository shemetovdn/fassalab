<?php
/*
Plugin Name: PMPro Customizations
Plugin URI: http://www.paidmembershipspro.com/wp/pmpro-customizations/
Description: Customizations for Paid Memberships Pro
Version: .1
Author: Stranger Studios
Author URI: http://www.strangerstudios.com
*/

<?php
/**
	* When registering, add the member to a specific membership level 
	* @param integer $user_id
**/
//Disables the pmpro redirect to levels page when user tries to register
add_filter("pmpro_login_redirect", "__return_false");
function my_pmpro_default_registration_level($user_id) {
	//Give all members who register membership level 1
	pmpro_changeMembershipLevel(8, $user_id);
}
add_action('user_register', 'my_pmpro_default_registration_level');
?>