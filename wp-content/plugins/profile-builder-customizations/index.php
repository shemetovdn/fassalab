<?php
/*
Plugin Name: Profile Builder Customizations
Plugin URI: 
Description: This is a plugin which you can use to run the code added via the filters existing in Profile Builder to customize aspect, functionality etc. This plugin was created to improve code readability.
Version: 1
Author: Reflection Media, Barina Gabriel
Author URI: http://www.reflectionmedia.ro
License: GPL2

== Copyright ==
Copyright 2011 Reflection Media (wwww.reflectionmedia.ro)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/


function custom_register_redirect( $oldMessage, $oldLink ){
$new_url = 'http://www.my_site.com/my_page'; // here you can specify your own url

 return '<font id="messageTextColor">' . sprintf(__( 'You will soon be redirected automatically. If you see this page for more than 3 seconds, please click %1$s.%2$s', 'profilebuilder'), '<a href="'.$new_url.'">'.__('here', 'profilebuilder').'</a>', '<meta http-equiv="Refresh" content="3;url='.$new_url.'" />') . '</font><br/><br/>';
}
add_filter ( 'wppb_register_redirect_after_creation1', 'custom_register_redirect', 10, 2 );