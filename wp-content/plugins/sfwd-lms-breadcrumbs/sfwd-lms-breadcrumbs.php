<?php
/*
Plugin Name: LearnDash LMS - Breadcrumbs
Description: Easily implement LearnDash LMS Breadcrumbs that supports courses, lessons, topics and quizzes.  Also supports custom post types with or without taxonomies & tags, pages and blog posts.  Can also be called by shortcode [sfwd-breadcrumbs].

Version: 1.0.0
Author: Uncanny Owl
Author URI: http://www.uncannyowl.com/

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * Finding ID of the post
 *
 */
if ( ! function_exists( 'lms_get_the_id' ) ) {
    function lms_get_the_id() {
        // If singular get_the_ID
        if ( is_singular() ) {
            return get_the_ID();
        }
        // Get ID of WooCommerce product archive
        elseif ( is_post_type_archive( 'product' ) && class_exists( 'Woocommerce' ) && function_exists( 'wc_get_page_id' ) ) {
            $shop_id = wc_get_page_id( 'shop' );
            if ( isset( $shop_id ) ) {
                return wc_get_page_id( 'shop' );
            }
        }
        // Posts page
        elseif( is_home() && $page_for_posts = get_option( 'page_for_posts' ) ) {
            return $page_for_posts;
        }
        // Return nothing
        else {
            return NULL;
        }
    }
}


/**
 * Finding Category of the post, custom post type
 *
 */
if ( ! function_exists( 'lms_post_taxonomy' ) ) {
    function lms_post_taxonomy($post_id, $taxonomy = 'category'){
        $terms = get_the_terms( $post_id, $taxonomy );
        $t = array();
        if($terms){
            foreach ( $terms as $term ) {
                $t[] = lms_build_anchor_links(get_term_link($term->slug, $taxonomy), $term->name);
            }        
            return implode( " / ", $t );
        }else{
            return false;
        }
    }
}

/**
 * Finding Taxonomies of the given post type
 *
 */
if ( ! function_exists( 'lms_get_taxonomy' ) ) {
    function lms_get_taxonomy($post_id, $post_type){
        $taxonomies = get_taxonomies(array('object_type' => array($post_type)),'objects'); 
        $tax = array();
        if($taxonomies){
            foreach($taxonomies as $taxonomy){
                // Pass the $taxonomy name to lms_post_taxonomy to return with proper terms and links
                $tax[] = lms_post_taxonomy($post_id,$taxonomy->query_var);
            }
            return implode( " / ", $tax );
        }else{
            return false;
        }
    }
}


/**
 * Building Anchor Links for Breadcrumbs
 *
 */
if ( ! function_exists( 'lms_build_anchor_links' ) ) {
    function lms_build_anchor_links($permalink, $title){
        $anchor_body        = "<span itemscope=\"\" itemtype=\"http://data-vocabulary.org/Breadcrumb\"><a href=\"[[URL]]\" title=\"[[TITLE]]\" rel=\"[[TITLE-SMALL]]\" class=\"trail-begin\"><span itemprop=\"[[TITLE]]\">[[TITLE]]</span></a></span>";
        $anchor = str_replace('[[TITLE]]',$title,str_replace('[[URL]]',$permalink,$anchor_body));
        $title_small = strtolower(str_replace(' ','-',$title));
        $anchor = str_replace('[[TITLE-SMALL]]',$title_small, $anchor);
        return $anchor;
    }
}


/**
 * Breadcrumbs function.
 *
 */
if ( ! function_exists( 'learndash_breadcrumbs' ) ) {
    function learndash_breadcrumbs(){
        // Globals
        global $wp_query, $wp_rewrite;

        // Get post id
        $post_id = $post_id ? $post_id : lms_get_the_id();

        // Define main variables
        $trail              = array();
        $breadcrumb         = '';
        $trail[]            = lms_build_anchor_links(get_bloginfo('url'),'Home'); // Adding Home as the first crumb in the trail.
        
        // If it's on home page
        if(is_front_page()){
            $trail = array(); //Removing Single Home link from Homepage.
        }elseif ( is_singular() ) {            
            // Get singular vars (page, post, attachments)
            $post        = $wp_query->get_queried_object();
            $post_id     = absint( $wp_query->get_queried_object_id() );
            $post_type   = $post->post_type;
            $parent      = $post->post_parent;
            
            if('post' == $post_type){                
                if(lms_post_taxonomy($post_id)){
                    $trail[] = lms_post_taxonomy($post_id);
                }
                $trail[] = get_the_title($post_id);
            }elseif('page' == $post_type){                
               
               // If Woocommerce is installed and being viewed, add shop page to cart, checkout pages
                if ( class_exists('Woocommerce') ) {
                    if ( is_cart() || is_checkout() ) {
                        // Get shop page
                        if ( class_exists( 'Woocommerce' ) && function_exists( 'wc_get_page_id' ) ) {
                            $shop_id        = wc_get_page_id( 'shop' );
                            $shop_page_url    = get_permalink( $shop_id );
                            $shop_title        = get_the_title( $shop_id );
                            if ( function_exists( 'icl_object_id' ) ) {
                                $shop_title = get_the_title( icl_object_id( $shop_id, 'page' ) );
                            }
                        }

                        // Shop page
                        if ( $shop_id && $shop_title ) {
                            $trail[] = '<span '. $item_type_scope .' class="trail-type-archive">
                                    <a href="' . get_permalink( $shop_id ) . '" title="' . esc_attr( $shop_title ) . '">
                                        <span itemprop="title">' . $shop_title . '</span>
                                    </a>
                                </span>';
                        }
                    }
                    $trail[] = get_the_title($post_id);
                }else{
                    // Regular pages. See if the page has any ancestors. Add in the trail if ancestors are found
                    $ancestors = get_ancestors( $post_id, 'page' );
                    if($ancestors){
                        $ancestors = array_reverse($ancestors);
                        foreach($ancestors as $page){
                            $trail[] = lms_build_anchor_links(get_permalink($page),get_the_title($page));
                        }
                    }
                    $trail[] = get_the_title($post_id);
                }
            }elseif('sfwd-courses' == $post_type){  
                // See if Single Course is being displayed.                              
                $trail[] = lms_build_anchor_links(get_post_type_archive_link( 'sfwd-courses' ),'Courses');
                $trail[] = get_the_title($post_id);
            }elseif('sfwd-lessons' == $post_type){
                // See if Single Lesson is being displayed.
                $course_id = get_post_meta( $post_id, 'course_id', true ); // Getting Parent Course ID
                $trail[] = lms_build_anchor_links(get_post_type_archive_link( 'sfwd-courses' ),'Courses'); // Getting Main Course Page Link
                $trail[] = lms_build_anchor_links(get_permalink( $course_id ), get_the_title($course_id)); // Getting Lesson's Course Link
                $trail[] = get_the_title($post_id);
            }elseif('sfwd-topic' == $post_type){                
                // See if single Topic is being displayed
                $course_id = get_post_meta( $post_id, 'course_id', true ); // Getting Parent Course ID
                $lesson_id = get_post_meta( $post_id, 'lesson_id', true ); // Getting Parent Lesson ID
                $trail[] = lms_build_anchor_links(get_post_type_archive_link( 'sfwd-courses' ),'Courses');  // Getting Main Course Page Link
                $trail[] = lms_build_anchor_links(get_permalink( $course_id ), get_the_title($course_id)); // Getting Lesson's Course Link
                $trail[] = lms_build_anchor_links(get_permalink( $lesson_id ), get_the_title($lesson_id)); // Getting Topics's Lesson Link
                $trail[] = get_the_title($post_id);
            }elseif('sfwd-quiz' == $post_type){                
                // See if quiz is being displayed
                $course_id = get_post_meta( $post_id, 'course_id', true ); // Getting Parent Course ID
                $topic_id = get_post_meta( $post_id, 'lesson_id', true ); // Getting Parent Topic/Lesson ID
				if (get_post_type( $topic_id )=='sfwd-topic')
				{
					$lesson_id = get_post_meta ( $topic_id, 'lesson_id', true); // Getting Parent Lesson ID
				}
                $trail[] = lms_build_anchor_links(get_post_type_archive_link( 'sfwd-courses' ),'Courses');  // Getting Main Course Page Link
                $trail[] = lms_build_anchor_links(get_permalink( $course_id ), get_the_title($course_id)); // Getting Lesson's Course Link
				//If $lesson_id is false, the quiz is associated with a lesson and course but not a topic.
				if ($lesson_id)
				{
					$trail[] = lms_build_anchor_links(get_permalink( $lesson_id ), get_the_title($lesson_id)); // Getting Topics's Lesson Link
				}
				//If $topic_id is false, the quiz is associated with a course but not associated with any lessons or topics.
				if ($topic_id)
				{
					$trail[] = lms_build_anchor_links(get_permalink( $topic_id ), get_the_title($topic_id));
				}
				$trail[] = get_the_title($post_id);

            }
			else{                
                // Add shop page to single product
                if ( 'product' == $post_type ) {
                    // Get shop page
                    if ( class_exists( 'Woocommerce' ) && function_exists( 'wc_get_page_id' ) ) {
                        $shop_id        = wc_get_page_id( 'shop' );
                        $shop_page_url    = get_permalink( $shop_id );
                        $shop_title        = get_the_title( $shop_id );
                        if ( function_exists( 'icl_object_id' ) ) {
                            $shop_title = get_the_title( icl_object_id( $shop_id, 'page' ) );
                        }
                        $shop_title        = apply_filters( 'wpex_bcrums_shop_title', $shop_title );
                    }

                    // Shop page
                    if ( $shop_id && $shop_title ) {
                        $trail[] = '<span '. $item_type_scope .' class="trail-type-archive">
                                <a href="' . get_permalink( $shop_id ) . '" title="' . esc_attr( $shop_title ) . '">
                                    <span itemprop="title">' . $shop_title . '</span>
                                </a>
                            </span>';
                    }
                }
                
                // Getting terms of the post.
                if(lms_get_taxonomy($post_id,$post_type)){
                    $trail[] = lms_get_taxonomy($post_id,$post_type);
                }
                $trail[] = get_the_title($post_id);
            }
            
        }
        
        // If it's an Archive
        if(is_archive()){        
            //Ignore if Courses & Products
            if(!is_post_type_archive('sfwd-courses') && !is_post_type_archive('product')){
                if(is_category() || is_tax())   $trail[] = single_cat_title( '', false ); // If its Blog Category
                if(is_day())        $trail[] = get_the_date(); // If its Single Day Archive
                if(is_month())      $trail[] = get_the_date( _x( 'F Y', 'monthly archives date format', 'wpex' ) ). " Archives"; // If Mothly Archives
                if(is_year())       $trail[] = get_the_date( _x( 'Y', 'yearly archives date format', 'wpex' ) ). " Archives"; // If its Yearly Archives
                if(is_author())     $trail[] = get_the_author(); // If its Author's Archives            
            }elseif(is_post_type_archive('sfwd-courses')){
                $trail[] = 'Courses';
            }elseif(is_post_type_archive('product')){
                $trail[] = 'Shop';
            }
        }
        
        if(is_search()){
            $trail[] = 'Search';
            $trail[] = get_search_query();
        }        
                
        // Build breadcrumbs
        $classes = 'sfwd-breadcrumbs clr';
        if ( $breadcrumbs_position = get_theme_mod( 'breadcrumbs_position' ) ) {
            $classes .= ' position-'. $breadcrumbs_position;
        }
        
        // Open breadcrumbs
        $breadcrumb = '<nav class="'. $classes .'"><div class="breadcrumb-trail">';

        // Separator HTML
        $separator = '<span class="sep">' . "<span class=\"fa fa-angle-right\"></span> " . '</span>';

        // Join all trail items into a string
        $breadcrumb .= implode( $separator, $trail );

        // Close breadcrumbs
        $breadcrumb .= '</div></nav>';
        
        // Display breadcrumbs
        echo $breadcrumb;
    }
}

add_shortcode( 'sfwd-breadcrumbs', 'learndash_breadcrumbs' );