const getBranchesTreeUrl = 'http://localhost:3000/api/branches-tree/';

class Api {
    constructor() {
        const corsHeaders = new Headers();
        corsHeaders.append('Accept', 'application/json');
        corsHeaders.append('Content-Type', 'application/json');
        corsHeaders.append('Authorization', 'Bearer r920ut082hg');

        this.getOptions = {
            method: 'GET',
            headers: corsHeaders,
            mode: 'cors',
            cache: 'no-cache'
        }
    }
    async getBranchesTree() {
        const response = await fetch(getBranchesTreeUrl, this.getOptions);
        return await response.json();
    }
}

export default Api;