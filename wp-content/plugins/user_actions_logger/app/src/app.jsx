import React from "react";
import Table from "./components/common/table.jsx";
import "./app.css";
import "./assets/css/theme-default/bootstrap.css";
import "./assets/css/theme-default/materialadmin.css";
import "./assets/css/theme-default/font-awesome.min.css";
import "./assets/css/theme-default/material-design-iconic-font.min.css";

class App extends React.Component {
  constructor(props) {
    super(props);
      this.state = { thead: ["Id", "Email", "Session Start", "Session End"] };
  }

  render() {
    return (
        <Table thead={this.state.thead}/>
    );
  }
}

export default App;

