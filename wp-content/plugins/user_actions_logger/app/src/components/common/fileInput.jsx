import React from "react";

class FileInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isSuperAdmin:window.isSuperAdmin };
  }

  render() {
    let labelStyle = {
      backgroundImage: "url(" + this.props.url + ")",
      width          : "130px",
      height         : "130px",
      display        : "inline-block",
      backgroundSize : "cover",
      cursor         : "pointer"
    };
    if(this.props.url !== "") {
      return (
        <label for={this.props.name} style={labelStyle}>
          <input type="file" style={{ display:"none" }} className={this.props.bgInputClass} id={this.props.name} name = {this.props.name} required=""  onChange={this.props.onChangeFunction}/>
        </label>
      );
    } else {
      return (
        <input type="file" className={this.props.bgInputClass} id="backgroundImage" name="backgroundImage" required=""  onChange={this.props.onChangeFunction}/>
      );
    }
  }
}

export default FileInput;