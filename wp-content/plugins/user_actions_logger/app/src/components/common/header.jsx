import React from "react";
import {connect} from "react-redux";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { profileMenuClass: "dropdown", user: JSON.parse(localStorage.getItem("user")) };
  }
  logout = (event) => {
    event.preventDefault();
    this.props.logout();
  }
  mainMenuManage = (event) => {
    event.preventDefault();
    if (document.body.getAttribute("class") == "menubar-hoverable header-fixed  menubar-visible") {
      this.props.menu.close();
    } else {
      this.props.menu.open();
    }
  }
  profileMenuToogle = (event) => {
    event.preventDefault();
    if (this.state.profileMenuClass == "dropdown") {
      this.setState({ profileMenuClass: "dropdown open" });
    } else {
      this.setState({ profileMenuClass: "dropdown" });
    }
  }

  render() {
    return (
      <header id="header" >
        <div class="headerbar">
          <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
              <li class="header-nav-brand" >
                <div class="brand-holder">
                  <a href="../../html/dashboards/dashboard.html">
                    <span class="text-lg text-bold text-primary">DELTA7</span>
                  </a>
                </div>
              </li>
              <li>
                <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="/" onClick={this.mainMenuManage}>
                  <i class="fa fa-bars"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="headerbar-right">
            <ul class="header-nav header-nav-profile">
              <li className={this.state.profileMenuClass}>
                <a href="/" class="dropdown-toggle ink-reaction" data-toggle="dropdown" onClick={this.profileMenuToogle}>
                  <img src="../../assets/img/avatar1.jpg?1403934956" alt="" />
                  <span class="profile-info">
									<small>{this.state.user.name}</small>
								</span>
                </a>
                <ul class="dropdown-menu animation-dock">
                  <li class="dropdown-header">Config</li>
                  <li><a href="../../html/pages/profile.html">My profile</a></li>
                  <li><a href="/logout" onClick={this.logout}><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </header>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch({ type : "LOGOUT" })
  };
}
export default connect(null, mapDispatchToProps)(Header);