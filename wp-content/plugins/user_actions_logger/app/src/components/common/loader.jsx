import React from "react";

class PreLoader extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <i className="circle-preloader"></i>
    );
  }
}

export default PreLoader;