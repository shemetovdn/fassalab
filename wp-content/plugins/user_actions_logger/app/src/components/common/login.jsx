import React from "react";
import adminRepository from "./../../repositories/adminRepository.js";
import { connect } from "react-redux";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {email : "", password : "", error : ""}
    }

  changePassword = (event) => {
        this.setState({password : event.target.value});
    }
  changeEmail = (event) => {
        this.setState({email : event.target.value});
    }
  login = async(event) => {
    event.preventDefault();
    const response = await adminRepository.login(this.state.password, this.state.email);

    if (response.status) {
      localStorage.setItem("user", JSON.stringify(response.data))
      this.props.login();
    } else {
      this.setState({error: response.error});
    }

    }
    render() {
        const emailInputClass = this.state.email != "" ? "form-control dirty" : "form-control";
        const emailWrapperClass = this.state.error == "Email is not correct" ? "form-group has-error" : "form-group";
        const passwordInputClass = this.state.password != "" ? "form-control dirty" : "form-control";
        const passwordWrapperClass = this.state.error == "Password is not correct" ? "form-group has-error" : "form-group";
        return (
    <div id="base">
        <div id="content">
            <section>
                <div className="section-body contain-lg">
                    <div className="row">
                        <div className="col-lg-12">
                            <h1 className="text-primary">Login</h1>
                        </div>
                        <div className="col-lg-offset-1 col-md-8">
                            <form className="form form-validate floating-label" noValidate="novalidate">
                                <div className="card">
                                    <div className="card-body">
                                        <div className={emailWrapperClass}>
                                            <input type="email" className={"form-control"} id="email" name="email"
                                                   required="" value={this.state.email} onChange={this.changeEmail}
                                                   className={emailInputClass} />
                                            <span className="help-block">This field is unvalid.</span>
                                            <label htmlFor="name">Email</label>
                                        </div>
                                        <div className={passwordWrapperClass}>
                                            <input type="password" className={"form-control"} id="password" name="password"
                                                   required="" value={this.state.password} onChange={this.changePassword}
                                                   className={passwordInputClass} />
                                            <span className="help-block">This field is unvalid.</span>
                                            <label htmlFor="name">Password</label>
                                        </div>
                                    </div>
                                    <div className="card-actionbar">
                                        <div className="card-actionbar-row">
                                            <button type="submit" className="btn btn-flat btn-primary ink-reaction"
                                             onClick={this.login}>Login
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
);
    }
}
function mapStateToProps(state) {
  return {
    isLogin: state.isLogin
  };
}
function mapDispatchToProps(dispatch) {
  return {
    login: () => dispatch({ type : "LOGIN" }),
    logout: () => dispatch({ type : "LOGOUT" })
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);