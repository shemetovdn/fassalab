import React from "react";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
class Menubar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const menu = this.props.menu;
    return (
      <div id="menubar" class="menubar-inverse "
        onMouseEnter={menu.open}
        onMouseLeave={menu.close}>
        <div class="menubar-fixed-panel">
          <div>
            <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
              <i class="fa fa-bars"></i>
            </a>
          </div>
          <div class="expanded">
            <a href="../../html/dashboards/dashboard.html">
              <span class="text-lg text-bold text-primary ">DELTA7</span>
            </a>
          </div>
        </div>
        <div class="menubar-scroll-panel">


          <ul id="main-menu" class="gui-controls">


            <li>
              <NavLink exact to="/" >
                <div class="gui-icon"><i class="md md-home"></i></div>
                <span class="title">Dashboard</span>
              </NavLink>
            </li>
            <li class="gui-folder">
              <NavLink exact to="/admins">
                <div class="gui-icon"><i class="md md-people"></i></div>
                <span class="title">App managers</span>
              </NavLink>
            </li>
            <li className="gui-folder">
              <NavLink exact to="/users">
                <div className="gui-icon"><i className="md md-people"></i></div>
                <span className="title">Players</span>
              </NavLink>
            </li>
            <li  class="gui-folder">
              <NavLink to="/areas">
                <div class="gui-icon"><i class="md md-web"></i></div>
                <span class="title">Areas</span>
              </NavLink>
            </li>
            <li  class="gui-folder">
              <NavLink to="/pathway-list">
                <div class="gui-icon"><i class="md md-directions-subway"></i></div>
                <span class="title">Pathways</span>
              </NavLink>
            </li>
            <li class="gui-folder">
              <NavLink to={"/game-type-list"}>
                <div class="gui-icon"><i class="md md-computer"></i></div>
                <span class="title">Game Types</span>
              </NavLink>
            </li>
            <li class="gui-folder">
                <NavLink to={"/category-list"}>
                <div class="gui-icon"><span class="glyphicon glyphicon-list-alt"></span></div>
                <span class="title">Image Categories</span>
              </NavLink>
            </li>
            <li className="gui-folder">
              <NavLink to="/image-library">
                <div className="gui-icon"><i className="md md-web"></i></div>
                <span className="title">Image Library</span>
              </NavLink>
            </li>
            <li class="gui-folder">
              <a>
                <div class="gui-icon"><i class="md md-computer"></i></div>
                <span class="title">Pages</span>
              </a>

              <ul>
                <li class="gui-folder">
                  <a href="javascript:void(0);">
                    <span class="title">Contacts</span>
                  </a>

                  <ul>
                    <li><a href="../../html/pages/contacts/search.html" ><span class="title">Search</span></a></li>
                    <li><a href="../../html/pages/contacts/details.html" ><span class="title">Contact card</span></a></li>
                    <li><a href="../../html/pages/contacts/add.html" ><span class="title">Insert contact</span></a></li>
                  </ul>
                </li>
                <li class="gui-folder">
                  <a href="javascript:void(0);">
                    <span class="title">Search</span>
                  </a>
                  <ul>
                    <li><a href="../../html/pages/search/results-text.html" ><span class="title">Results - Text</span></a></li>
                    <li><a href="../../html/pages/search/results-text-image.html" ><span class="title">Results - Text and Image</span></a></li>
                  </ul>
                </li>
                <li class="gui-folder">
                  <a href="javascript:void(0);">
                    <span class="title">Blog</span>
                  </a>
                  <ul>
                    <li><a href="../../html/pages/blog/masonry.html" ><span class="title">Blog masonry</span></a></li>
                    <li><a href="../../html/pages/blog/list.html" ><span class="title">Blog list</span></a></li>
                    <li><a href="../../html/pages/blog/post.html" ><span class="title">Blog post</span></a></li>
                  </ul>
                </li>
                <li class="gui-folder">
                  <a href="javascript:void(0);">
                    <span class="title">Error pages</span>
                  </a>
                  <ul>
                    <li><a href="../../html/pages/404.html" ><span class="title">404 page</span></a></li>
                    <li><a href="../../html/pages/500.html" ><span class="title">500 page</span></a></li>
                  </ul>
                </li>
                <li><a href="../../html/pages/profile.html" ><span class="title">User profile<span class="badge style-accent">42</span></span></a></li>
                <li><a href="../../html/pages/invoice.html" ><span class="title">Invoice</span></a></li>
                <li><a href="../../html/pages/calendar.html" ><span class="title">Calendar</span></a></li>
                <li><a href="../../html/pages/pricing.html" ><span class="title">Pricing</span></a></li>
                <li><a href="../../html/pages/timeline.html" ><span class="title">Timeline</span></a></li>
                <li><a href="../../html/pages/maps.html" ><span class="title">Maps</span></a></li>
                <li><a href="../../html/pages/locked.html" ><span class="title">Lock screen</span></a></li>
                <li><a href="../../html/pages/login.html" ><span class="title">Login</span></a></li>
                <li><a href="../../html/pages/blank.html" ><span class="title">Blank page</span></a></li>
              </ul>
            </li>
            <li>
              <a href="../../html/charts/charts.html" >
                <div class="gui-icon"><i class="md md-assessment"></i></div>
                <span class="title">Charts</span>
              </a>
            </li>

            <li class="gui-folder">
              <a>
                <div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
                <span class="title">Menu levels demo</span>
              </a>
              <ul>
                <li><a href="#"><span class="title">Item 1</span></a></li>
                <li><a href="#"><span class="title">Item 1</span></a></li>
                <li class="gui-folder">
                  <a href="javascript:void(0);">
                    <span class="title">Open level 2</span>
                  </a>

                  <ul>
                    <li><a href="#"><span class="title">Item 2</span></a></li>
                    <li class="gui-folder">
                      <a href="javascript:void(0);">
                        <span class="title">Open level 3</span>
                      </a>

                      <ul>
                        <li><a href="#"><span class="title">Item 3</span></a></li>
                        <li><a href="#"><span class="title">Item 3</span></a></li>
                        <li class="gui-folder">
                          <a href="javascript:void(0);">
                            <span class="title">Open level 4</span>
                          </a>

                          <ul>
                            <li><a href="#"><span class="title">Item 4</span></a></li>
                            <li class="gui-folder">
                              <a href="javascript:void(0);">
                                <span class="title">Open level 5</span>
                              </a>
                              <ul>
                                <li><a href="#"><span class="title">Item 5</span></a></li>
                                <li><a href="#"><span class="title">Item 5</span></a></li>
                              </ul>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>

          </ul>

          <div class="menubar-foot-panel">
            <small class="no-linebreak hidden-folded">
              <span class="opacity-75">Copyright &copy; 2014</span> <strong>CodeCovers</strong>
            </small>
          </div>
        </div>
      </div>
    );
  }
}

export default Menubar;