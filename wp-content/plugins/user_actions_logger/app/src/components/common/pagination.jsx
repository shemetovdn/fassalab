import React from "react";

class Pagination extends React.Component {
    constructor(props) {
        super(props);
    }
    renderPagePagination = () => {
        let buttons = "";
        if(this.props.totalCount && this.props.limit) {
            console.log(this.props.totalCount);
            console.log(this.props.limit);
            const pageCount = Array.from(Array(Math.ceil(this.props.totalCount/this.props.limit)), (val, index) => index + 1);
            console.log(pageCount);
            return pageCount.map(this.returnButton);
        }
    }
    returnButton = (item, i) => {
        console.log("HERE");
        console.log(this.props.currentPageNumber);
        let currentClass = "paginate_button";
        if (this.props.currentPageNumber == i + 1) {
            currentClass += " current";
        }
        return <a class={currentClass} aria-controls="datatable1" data-page={i+1} onClick={this.props.changePage}>{i+1}</a>;
    }
    render() {
        return (<>
            <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                {/*<a class="paginate_button previous disabled" aria-controls="datatable1" data-dt-idx="0" tabindex="0"*/}
                   {/*id="datatable1_previous"><i class="fa fa-angle-left"></i></a>*/}
<span>
{this.renderPagePagination()}
</span>
                {/*<a class="paginate_button next" aria-controls="datatable1" data-dt-idx="7" tabindex="0"*/}
                   {/*id="datatable1_next">*/}
                    {/*<i class="fa fa-angle-right"></i>*/}
                {/*</a>*/}
            </div>
            </>
        );
    }
}

export default Pagination;