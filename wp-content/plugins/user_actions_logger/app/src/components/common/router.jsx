import React from "react";
import {
  Route,
  Switch
} from "react-router-dom";
import Home from "../home.jsx";
import Areas from "../area/areas.jsx";
import Pathways from "../pathway/pathway.jsx";
import Users from "../user/users.jsx";
import UserEdit from "../user/user-edit.jsx";
import Admin from "../admin/admin.jsx";
import AdminEdit from "../admin/admin-edit.jsx";
import AreaEdit from "../area/area-edit.jsx";
import PathwayEdit from "../pathway/pathway-edit.jsx";
import GameTypeEdit from "../game-type/game-type-edit.jsx";
import GameType from "../game-type/game-types.jsx";
import ImageCategoryEdit from "../image-categories/category-edit.jsx";
import ImageCategories from "../image-categories/category-list.jsx";
import ImageEdit from "../image-library/image-edit.jsx";
import ImageLibrary from "../image-library/image-list.jsx";

class Router extends React.Component {
  constructor(props) {
    super(props);
    const user = JSON.parse(localStorage.getItem("user"));
    this.state = { isSuperAdmin : user.role };
  }

  render() {
    let content = <h1 className={"text-primary"}>Permission denied</h1>;
    if (this.state.isSuperAdmin == "ROLE_SUPERADMIN") {
      content =
            <>
                <Route path="/admins" component={Admin}/>
                <Route path="/admin/:id" component={AdminEdit}/>
                <Route path="/admin" component={AdminEdit}/>
            </>
      ;
    }
    return (
      <Switch>
        <Route exact path="/" render={() => <Home/>}/>
        <Route path="/areas" component={Areas}/>
        <Route path="/area/:id" component={AreaEdit}/>
        <Route path="/area" component={AreaEdit}/>
        <Route path="/pathway-list" component={Pathways}/>
        <Route path="/pathway/:id" component={PathwayEdit}/>
        <Route path="/pathway" component={PathwayEdit}/>
        <Route path="/game-type-list" component={GameType}/>
        <Route path="/game-type/:id" component={GameTypeEdit}/>
        <Route path="/game-type" component={GameTypeEdit}/>
        <Route path="/category-list" component={ImageCategories}/>
        <Route path="/image-category/:id" component={ImageCategoryEdit}/>
        <Route path="/image-category/" component={ImageCategoryEdit}/>
        <Route path="/image/:id" component={ImageEdit}/>
        <Route path="/image/" component={ImageEdit}/>
        <Route path="/image-library" component={ImageLibrary}/>
        <Route path="/users" component={Users}/>
        <Route path="/user/:id" component={UserEdit}/>
        <Route path="/user" component={UserEdit}/>
        {content}
      </Switch>
    );
  }
}

export default Router;