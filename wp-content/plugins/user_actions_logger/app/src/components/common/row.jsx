import React from "react";

class Row extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (<>
            <tr>
              <td>{this.props.row.id}</td>
              <td>{this.props.row.email}</td>
              <td>{this.props.row.start_date}</td>
              <td>{this.props.row.last_activity}</td>
            </tr>
        </>
    );
  }
}

export default Row;