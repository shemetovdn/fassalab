import React from "react";
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import EntityRepository from "../../repositories/EntityRepository.js";

const SortableItem = SortableElement(({id, name}) => <tr>
  <td>{id}</td>
  <td>{name}</td>
  <td className="text-right">
  </td>
</tr>);

const SortableList = SortableContainer(({items}) => {

  return (
    <table className="table table-hover">
      <tbody>
      {items.map((item, index) => (
        <SortableItem key={`item-${index}`} index={index} id={item.id} name={item.name} />
      ))}
      </tbody>
    </table>
  );
});
class SortableTable extends React.Component {
  constructor(props)
  {
    super(props);
    this.state = {
      items: [],
      isLoading: false
    };
  }
  async componentDidMount(){
    const data =  await EntityRepository.getRelations(this.props.path, this.props.itemId);
    await this.setState({items: data});
  }
  onSortEnd = async ({oldIndex, newIndex}) => {
    this.setState(({items}) => ({
      items: arrayMove(items, oldIndex, newIndex),
    }));
    const data =  await EntityRepository.sortingRelations(this.props.path, this.props.itemId, this.state.items);
  };
  render() {
    return <SortableList items={this.state.items} onSortEnd={this.onSortEnd} />;
  }
}

export default SortableTable;