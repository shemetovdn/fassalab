import React from "react";
import axios from "axios";
import Row from "./row.jsx";
import Pagination from "./pagination.jsx";
import LoaderImage from "../../img/basicloader.gif";
import PreLoader from "./loader.jsx";
import PubSub from "pubsub-js";

class Table extends React.Component {
  constructor(props)
  {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
        email : "",
        totalCount : "",
        limit : 20,
        currentPageNumber : 1,
    };
  }
  getData = async (email = "", currentPageNumber = "", limit) => {
      try {
          const formData = new FormData();
          const config = {
              headers: {
                  "Accept": "application/json",
                  "Content-Type": "application/x-www-form-urlencoded"
              }
          };
          formData.append("action", "get_user_actions");
          formData.append("email", email);
          formData.append("limit", limit ? limit : this.state.limit);
          formData.append("currentPageNumber", currentPageNumber);
          const res = await axios.post(window.ajaxUrl,formData, config);
          this.setState({
              data: res.data.actions,
              isLoading : false,
              email : email,
              totalCount : res.data.totalCount,
              currentPageNumber : currentPageNumber ? currentPageNumber : this.state.currentPageNumber,
              limit : limit ? limit : this.state.limit,
          });
          // return res.data.data;
      } catch (err) {
          console.log(err);
          return false;
      }
  }
  async componentDidMount(){
    this.getData();
  }
  updateComponent = async () => {
    const data =  await EntityRepository.getEntities(this.props.path);
    this.setState({data: data});
  }
  componentWillUnmount(){
    PubSub.unsubscribe(this.token);
  }
  renderRows = () => {

    // const { data, isLoading } = this.state;
    if (this.state.isLoading) {
      return <PreLoader />;
    } else {
        if (this.state.data instanceof Object) {
            return this.state.data.map(function(item, i){
                return <Row key={i} row={item}/>;
            });
        }
    }
  }
    changeEmail= (event) => {
      this.setState({ email : event.target.value });
    }
    search= () => {
        this.getData(this.state.email, 1);
    }
  renderThead = () => {
    return this.props.thead.map(function(item, i){
            return <th>{item}</th>;
    });
  }

  changePage = (event) => {
      event.preventDefault();
      console.log(event.target.innerHTML);
      console.log(event.target.getAttribute("data-page"));
      // this.setState({currentPageNumber : event.target.getAttribute("data-page")});
      this.getData("", event.target.getAttribute("data-page"));
  }
    onLimitChange = (event) => {
      this.getData(this.state.email, 1, event.target.value)
    }
  render() {

    return (<>
            <section class="style-default-bright">
              <div class="section-body">
                <h1 class="text-primary">User Actions</h1>
                  <input onChange={this.changeEmail} value={this.state.email} placeholder={"search by email"}/>
                  <button onClick={this.search}>Search</button>
      <div class="dataTables_info" id="datatable1_info" role="status" aria-live="polite">Showing {/*1 to 10 of */}
          {this.state.totalCount} entries</div>
                  <div className={"form-group"}>
                      <lable>Limit
                      <select className={"form-control"} onChange={this.onLimitChange} style={{width: "100px", border: "solid 1px"}}>
                          <option value={20}>20</option>
                          <option value={30}>30</option>
                          <option value={40}>40</option>
                          <option value={50}>50</option>
                          <option value={60}>60</option>
                          <option value={70}>70</option>
                          <option value={80}>80</option>
                          <option value={90}>90</option>
                          <option value={100}>100</option>
                          <option value={250}>250</option>
                      </select>
                      </lable>
                  </div>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      {this.renderThead(this)}
                    </tr>
                  </thead>
                  <tbody>
                    {this.renderRows()}
                  </tbody>
                </table>
                  < Pagination totalCount={this.state.totalCount} limit={this.state.limit}
                               currentPageNumber={this.state.currentPageNumber}
                               changePage={this.changePage} />
              </div>
            </section>
        </>
    );
  }
}

export default Table;