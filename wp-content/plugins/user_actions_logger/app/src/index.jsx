import React from "react";
import ReactDOM from "react-dom";
import App from "./app.jsx";

const appElem = document.getElementById("app");

if (appElem) {
  ReactDOM.render(<App/>, appElem);
}
