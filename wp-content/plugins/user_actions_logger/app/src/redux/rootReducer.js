const initialState = {
  isLogin : 2
};

export default function rootReducer(state = initialState, action) {

  switch (action.type) {
    case "LOGIN":
      return {
        isLogin: 1
      };
    case "LOGOUT":
      localStorage.removeItem("user");
      return {
        isLogin: 0
      };
  }
  return state;
}