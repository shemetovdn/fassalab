import axios from "axios";

class getEntityRepository {
  constructor () {
    axios.defaults.headers.common["Content-Type"] = "application/x-www-form-urlencoded";
  }
  async getEntities(path, action = "/get-entities")
  {
    try {
      const res = await axios.post(path + action, {});
      return res.data.data;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
  async getRelations(path, id)
  {
    try {
      const res = await axios.post(path + "/get-relations", "itemId=" + id);
      return res.data.data;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
  async sortingRelations(path, id, sortData)
  {
    try {
      const res = await axios.post(path + "/sort-relations", "itemId=" + id + "&sortData=" + JSON.stringify(sortData));
      return res.data.data;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}

export default new getEntityRepository();