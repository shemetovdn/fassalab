import axios from "axios";

class adminRepository {
  constructor () {
    const user = JSON.parse(localStorage.getItem("user"));
    axios.defaults.headers.common["Content-Type"] = "application/x-www-form-urlencoded";
    if (user) {
      axios.defaults.headers.common["Authorization"] = user.authToken;
    }

    this.baseUrl = "http://delta7.loc";
    this.formData = new FormData();
    // this.config = {
    //   headers: {
    //     "Content-Type": "application/json"
    //   }
    // };
  }
  async query (url, data) {
    try {
      console.log(this.baseUrl + url)
      this.formData.set("data", JSON.stringify(data));
      const response = await axios.post(this.baseUrl + url, this.formData);
      console.log(response);
      return response.data;
    } catch (err) {
      console.log(err);
    }
  }

  async login (password, email) {
    try {
      this.formData.set("email", email);
      this.formData.set("password", password);
      const response = await axios.post(this.baseUrl + "/admin-login", this.formData);
      return response.data;
    } catch (err) {
      console.log(err);
    }
  }
  async isTokenValid (token) {
    try {
      this.formData.set("token", token);
      const response = await axios.post(this.baseUrl + "/check-token", this.formData);
      return response;
    } catch (error) {
      if (error.response) {
        console.log(error.response.data);
        if (401 === error.response.status && error.response.data.error == "Api key is not provided") {
          return false;
        }
      } else if (error.request) {
        console.log(error.request);
      } else {
        // console.log("Error", error.message);
      }
    }
  }
}

export default new adminRepository();