const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

module.exports = (env, argv) => {
  const dev = argv.mode === "development";

  const config = {
    entry: {
      index: "./src/index.jsx"
    },
    output: {
      filename: "[name].js",
      path: path.resolve(__dirname, "dist")
    },
    optimization: {
      minimize: !dev,
      splitChunks: {
        chunks: "all"
      }
    },
    devtool: false,
    module: {
      rules: [
        {
          test: /\.jsx$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options : {
              presets : [
                "@babel/preset-react",
                {'plugins' : ['@babel/plugin-proposal-class-properties']}
              ]
            }
          }
        },
        {
          test: /\.css$/,
          // exclude: /node_modules/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                hmr: dev
              }
            },
            // 'style-loader',
            "css-loader?sourceMap"
          ]
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: {
            loader: "file-loader?limit=100000",
            options: {
              outputPath: "/images"
            }
          }
        },
        {
          test: /\.(woff|woff2|eot|ttf|svg)$/,
          use: {
            loader: "file-loader?limit=100000",
            options: {
              outputPath: "fonts"
            }
          }
        }
      ]
    },
    plugins: [
      new webpack.SourceMapDevToolPlugin({
        filename: "[file].map"
      }),
      new HtmlWebpackPlugin({
        template: "./src/index.html"
      }),
      new MiniCssExtractPlugin({
        filename: "[name].css"
      }),
      new webpack.ProvidePlugin({
        jQuery: "jquery",
        $: "jquery",
        jquery: "jquery"
      })
    ]
  };

  if (!dev) {
    config.plugins.push(new CleanWebpackPlugin());
  }

  return config;
};
