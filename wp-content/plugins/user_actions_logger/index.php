<?php
/*
Plugin Name: user Logger
Plugin URI: http://weeteam.net
Description:
Version: 1.0
Author: WeeTeam
Author URI: http://weeteam.net
*/
add_action('wp_ajax_nopriv_get_user_actions', "getUserActions");
add_action('wp_ajax_get_user_actions', "getUserActions");

function getUserActions()
{
    global $wpdb;
    $where = "";
    $ofset = 0;
    $limit = $_POST["limit"];
    $searchString = trim($_POST["email"]);
    $currentPageNumber = trim($_POST["currentPageNumber"]);
    if (!empty($currentPageNumber)) {
        $ofset = ($currentPageNumber-1)*$limit;
//        var_dump($ofset);die;
    }
    if (!empty($searchString)) {
        $where = "WHERE u.user_email LIKE '%{$searchString}%'";
        if ($searchString[0] == "!") {
            $searchString = substr($searchString, 1);
            $where = "WHERE u.user_email NOT LIKE '%{$searchString}%'";
        }
    }
    $result = $wpdb->get_results("
                        SELECT ua.*, u.user_email
                        FROM
                            user_actions ua
                            LEFT JOIN learn_users u
                            ON  ua.user_id = u.ID
                             {$where}   
                             LIMIT {$ofset},{$limit}                    
                    ");
    $totalCount  = $wpdb->get_var("
                        SELECT COUNT(ua.id)
                        FROM
                            user_actions ua 
                            LEFT JOIN learn_users u
                            ON  ua.user_id = u.ID
                             {$where}                    
                    ");
    $actions = [];
    foreach ($result as $item) {
        $user = get_user_by( "ID", $item->user_id );
        $actions[] = [
            "id" => $item->id,
            "email" => $user->user_email,
            "last_activity" => $item->last_activity,
            "start_date" => $item->start_date,
        ];
    }
    echo json_encode(["actions" => $actions, "totalCount" => $totalCount]);
    wp_die();
}
    include_once plugin_dir_path( __FILE__ ) . "class-submenu.php";
    include_once plugin_dir_path( __FILE__ ) . "class-submenu-page.php";

    add_action( 'plugins_loaded', 'actions_custom_admin_settings' );
    /**
     * Starts the plugin.
     *
     * @since 1.0.0
     */
    function actions_custom_admin_settings() {

        $plugin = new Submenu( new Submenu_Page() );
        $plugin->init();

    }

