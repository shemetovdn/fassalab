<?php

    class WT_Fass_QuizH5P
    {
        private $prefix = 'wtfqh';
        private $attr = array();

        public function __construct()
        {
            $this->Init();
        }

        private function Init()
        {
            @session_start();

            $this->Attr_Set('prefix', $this->prefix);

            $this->Plugin_Paths_Init();
            $this->Plugin_Hooks_Init();

            add_action('init', array($this, 'Actions_Init'));
            add_action('wp_enqueue_scripts', array($this, 'Enqueue_Sources'));
            add_action('wp_head', array($this, 'Head_Init'));
        }

        protected function Attr_Set($key, $value)
        {
            $this->attr[$key] = $value;
        }

        public function Attr_Get($key)
        {
            if (isset($this->attr[$key]))
            {
                return $this->attr[$key];
            }
            else
            {
                return false;
            }
        }

        public function Session_Set($key, $value)
        {
            $_SESSION[$this->prefix][$key] = $value;
        }

        public function Session_Get($key)
        {
            if (isset($_SESSION[$this->prefix]) && isset($_SESSION[$this->prefix][$key]))
            {
                return $_SESSION[$this->prefix][$key];
            }
            else
            {
                return false;
            }
        }

        public function Session_Unset($key = null)
        {
            if (is_null($key))
            {
                unset($_SESSION[$this->prefix]);
            }
            else
            {
                unset($_SESSION[$this->prefix][$key]);
            }
        }

        private function Plugin_Paths_Init()
        {
            $ps = array();

            preg_match("/^(.*[\/\\\]wp-content[\/\\\]plugins[\/\\\])(.*?)[\/\\\].*$/uis", __FILE__, $ps);

            if (is_array($ps) && count($ps) == 3)
            {
                $this->Attr_Set('plugin_dir_name', $ps[2]);
                $this->Attr_Set('plugin_path', $ps[1] . $this->Attr_Get('plugin_dir_name') . '/');
                $this->Attr_Set('plugin_index_path', $this->Attr_Get('plugin_path') . 'index.php');
                $this->Attr_Set('plugin_core_path', $this->Attr_Get('plugin_path') . 'core/');
                $this->Attr_Set('plugin_actions_path', $this->Attr_Get('plugin_path') . 'core/actions/');
                $this->Attr_Set('plugin_libraries_path', $this->Attr_Get('plugin_path') . 'core/libraries/');
                $this->Attr_Set('plugin_data_path', $this->Attr_Get('plugin_path') . 'data/');
                $this->Attr_Set('plugin_templates_path', $this->Attr_Get('plugin_path') . 'data/templates/');

                $this->Attr_Set('plugin_url', plugins_url($this->Attr_Get('plugin_dir_name')) . '/');
                $this->Attr_Set('plugin_css_url', $this->Attr_Get('plugin_url') . 'data/css/');
                $this->Attr_Set('plugin_js_url', $this->Attr_Get('plugin_url') . 'data/js/');
                $this->Attr_Set('plugin_img_url', $this->Attr_Get('plugin_url') . 'data/images/');
            }
        }

        private function Plugin_Hooks_Init()
        {
            register_activation_hook($this->Attr_Get('plugin_index_path'), array($this, 'Install'));
            //register_deactivation_hook();
            register_uninstall_hook($this->Attr_Get('plugin_index_path'), array($this, 'Uninstall'));
        }

        public function Install()
        {

        }

        public function Uninstall()
        {

        }

        public function Actions_Init()
        {
            if (isset($_REQUEST['_handler']) && $_REQUEST['_handler'] == $this->prefix)
            {
                include_once($this->Attr_Get('plugin_core_path') . 'action.php');

                $action = new WT_Fass_QuizH5P_Action($this);
                $action->name = $_REQUEST['_action'];
                $action->subaction = $_REQUEST['_subaction'];
                $action->Action_Load($_REQUEST[$_REQUEST['_action']]);
            }

            $this->Action_Process_Trigger();
        }

        public function Array_Treatment_Strings(&$array, $procedure)
        {
            $type = null;

            if (is_array($array))
            {
                $type = 'array';
            }
            else
            if (is_object($array))
            {
                $type = 'object';
            }

            if (!is_null($type) && count($array))
            {
                foreach ($array as $k => $v)
                {
                    if (is_string($v) && !preg_match("/^a:[0-9]+:\{/uis", $v))
                    {
                        switch (strtolower($procedure))
                        {
                            case 'addslashes':
                                $v = addslashes($v);
                            break;
                            case 'stripslashes':
                                $v = stripslashes($v);
                            break;
                            case 'htmlspecialchars':
                                $v = htmlspecialchars($v);
                            break;
                            case 'htmlspecialchars_decode':
                                $v = htmlspecialchars_decode($v);
                            break;
                        }
                    }
                    else
                    {
                        $v = $this->Array_Treatment_Strings($v, $procedure);
                    }

                    switch ($type)
                    {
                        case 'array':
                            $array[$k] = $v;
                        break;
                        case 'object':
                            $array->{$k} = $v;
                        break;
                    }
                }
            }

            return $array;
        }

        public function Is_Quiz_Content()
        {
            if (is_page() || is_single())
            {
                global $post;

                if (preg_match("/[^a-z0-9](ld_quiz|quiz_id|LDAdvQuiz)[^a-z0-9]/uis", get_the_content())) return true;
                if (preg_match("/[^a-z0-9](ld_quiz|quiz_id|LDAdvQuiz)[^a-z0-9]/uis", $post->post_content)) return true;
                if (in_array(get_post_type(), ['sfwd-quiz'])) return true;
            }

            return false;
        }

        public function Enqueue_Sources()
        {
            if (!$this->Is_Quiz_Content()) return false;

            wp_enqueue_style($this->prefix, $this->Attr_Get('plugin_css_url') . 'front.css', array(), '1.0', 'all');
            wp_enqueue_script($this->prefix . '-front', $this->Attr_Get('plugin_js_url') . 'main.js', array(), '1.0');
        }

        public function Head_Init()
        {
            if (!$this->Is_Quiz_Content()) return false;

            ?>
            <script type="text/javascript">
                window.WTFQH.attr.url_css = '<?php echo $this->Attr_Get('plugin_css_url'); ?>';

                jQuery(document).ready(function(){
                    window.WTFQH.init_h5p_frames();
                });
            </script>
            <?php
        }

        public function H5P_Frames_Get()
        {
            global $wpdb;

            return $wpdb->get_results("
                SELECT
                    c.`id`,
                    c.`title`
                FROM
                    `{$wpdb->prefix}h5p_contents` c
                LEFT JOIN
                    `{$wpdb->prefix}h5p_libraries` l
                    ON
                    l.`id` = c.`library_id`
                WHERE
                    l.`name` != 'H5P.InteractiveVideo'    
                ORDER BY
                    c.`id`
                    DESC
            ");
        }

        public function Quiz_H5P_Fieldset_Print()
        {
            ?>
            <div class="h5p">
                <fieldset>
                    <legend><?php _e('Choose the H5P frame'); ?>:</legend>
                    <?php

                        $h5p = $this->H5P_Frames_Get();

                        if (count($h5p))
                        {
                            $h5p_id = (int)get_option($this->Attr_Get('prefix') . '-quiz-h5p-' . (int)$_REQUEST['quiz_id'] . '-' . (int)$_REQUEST['questionId']);

                            ?>
                            <select name="<?php echo $this->Attr_Get('prefix'); ?>[quiz-h5p]">
                                <?php

                                    foreach ($h5p as $v)
                                    {
                                        ?>
                                        <option value="<?php echo $v->id; ?>" <?php echo ($h5p_id == $v->id) ? 'selected="selected"' : ''; ?>><?php echo $v->title; ?></option>
                                        <?php
                                    }

                                ?>
                            </select>
                            <?php
                        }

                    ?>
                </fieldset>
            </div>
            <?php
        }

        public function Do_ShortCode_Quiz_H5P($quiz_id, $question_id)
        {
            $quiz_id = (int)$quiz_id;
            $question_id = (int)$question_id;
            $res = '';

            if ($quiz_id && $question_id)
            {
                $h5p_id = (int)get_option($this->Attr_Get('prefix') . "-quiz-h5p-{$quiz_id}-{$question_id}");

                if ($h5p_id)
                {
                    $res .= '<input type="hidden" name="' . $this->Attr_Get('prefix') . '_time" value="' . time() . '" />';

                    $res .= do_shortcode('[h5p id="' . (int)$h5p_id . '"]');
                }
            }

            return $res;
        }

        public function Action_Process_Trigger()
        {
            global $wpdb;

            if (isset($_REQUEST[$this->Attr_Get('prefix')]))
            {
                $req = &$_REQUEST[$this->Attr_Get('prefix')];

                if (count($req))
                {
                    foreach ($req as $k => $v)
                    {
                        switch (mb_strtolower($k))
                        {
                            case 'quiz-h5p':
                                $quiz_id = (int)$_REQUEST['quiz_id'];
                                $question_id = (int)$_REQUEST['questionId'];

                                if ($quiz_id && $question_id)
                                {
                                    update_option($this->Attr_Get('prefix') . "-quiz-h5p-{$quiz_id}-{$question_id}", (int)$v, false);
                                }
                            break;
                        }
                    }
                }
            }

            if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'ld_adv_quiz_pro_ajax')
            {
                $data = $_REQUEST['data'];

                if (is_array($data) && count($data) && preg_match("/h5p/uis", $data['responses']))
                {
                    $quiz_id = (int)$data['quizId'];
                    $res = json_decode(stripslashes($data['responses']), true);

                    reset($res);
                    $question_id = (int)key($res);
                    $res = current($res);

                    if (isset($res['response']) && $res['response']['key'] == 'h5p')
                    {
                        $time = (int)$res['response']['time'];

                        if ($quiz_id && $question_id && $time > 0)
                        {
                            $h5p_id = (int)get_option($this->Attr_Get('prefix') . "-quiz-h5p-{$quiz_id}-{$question_id}");

                            $result = $wpdb->get_results("
                                SELECT
                                    *
                                FROM
                                    `{$wpdb->prefix}h5p_results`
                                WHERE
                                    `content_id` = '{$h5p_id}'
                                    AND 
                                    `user_id` = '" . get_current_user_id() . "'
                            ");

                            if (count($result))
                            {
                                $result = $result[0];

                                if ((int)$result->opened >= $time && (int)$result->finished >= $time)
                                {
                                    if ((int)$result->score >= (int)$result->max_score)
                                    {
                                        die(json_encode(array(
                                            $question_id => array(
                                                'c' => true,
                                                'e' => array(
                                                    'possiblePoints' => 1
                                                )
                                            )
                                        )));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
