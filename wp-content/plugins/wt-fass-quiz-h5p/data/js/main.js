var WTFQH = {
    attr: {
        url_css: ''
    },

    init_h5p_frames: function(){
        var p = jQuery('.wpProQuiz_quiz');
        var h5p_frames = jQuery('.h5p-iframe', p);

        if (h5p_frames.length)
        {
            window.h5pii = [];

            h5p_frames.each(function(i){
                var _this = jQuery(this);

                _this.load(function(){
                    var d = _this.contents();

                    jQuery('head', d).append('<link type="text/css" rel="stylesheet" href="' + window.WTFQH.attr.url_css + 'h5p.css" media="all" />');

                    clearInterval(window.h5pii[i]);

                    window.h5pii[i] = setInterval(function(){
                        _this.css({height: jQuery('body', d).outerHeight() + 'px'});
                        if (_this.is(':visible')) clearInterval(window.h5pii[i]);
                    }, 500);
                });

                WTFQH.verify_click_simulation_put(_this.closest('.wpProQuiz_listItem'));
            });
        }
    },

    verify_click_simulation_put: function(p){
        if (p.length)
        {
            var h5p_frame = jQuery('.h5p-iframe', p);

            if (h5p_frame.length == 1)
            {
                jQuery('.wpProQuiz_button.wpProQuiz_QuestionButton', p).bind('click.vcs', function(e){
                    var d = h5p_frame.contents();
                    jQuery('.h5p-question-check-answer.h5p-joubelui-button', d).click();
                });
            }
        }
    }
}
