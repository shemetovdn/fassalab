<?php

    //
    include_once($this->core->Attr_Get('plugin_core_path') . 'deck.php');

    global $wtfa_deck;

    $wtfa_deck = new WT_Fass_Anki_Deck($this->core);
    ////

    switch ($this->subaction)
    {
        case 'decks-list-get':
            $this->result['html'] = '';
            $data['tree_branch_id'] = mb_strtolower(trim($data['tree_branch_id']));

            $where = '';
            $order = "d.`date` DESC";

            if ($data['tree_branch_id'] != '' && $data['tree_branch_id'] != 'all')
            {
                if ($where != ''){ $where .= ' AND '; }
                $where .= "d.`tree_branch_id` = '{$data['tree_branch_id']}'";

                $order = "d.`position` ASC";
            }

            if ($where != ''){ $where = "WHERE {$where}"; }
            if ($order != ''){ $order = "ORDER BY {$order}"; }

            $result = $wpdb->get_results("
                SELECT
                    d.*
                FROM
                    `{$wpdb->prefix}{$this->prefix}_decks` d
                {$where}  
                {$order}
            ");

            ob_start();

            // Global
            $deck_global = get_option("{$this->prefix}_deck_global");
            if (!is_array($deck_global)) $deck_global = array();

            $v = new stdClass();
            $v->title = $deck_global['title'];

            include($this->core->Attr_Get('plugin_templates_path') . 'items/deck-global.php');
            ////

            if (count($result))
            {
                $this->core->Array_Treatment_Strings($result, 'stripslashes');
                $this->core->Array_Treatment_Strings($result, 'htmlspecialchars');

                echo '<div class="decks-inner">';

                include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->learn_plugin_fn . '/core.php');
                include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->learn_plugin_fn . '/core/lesson.php');
                $core_fl = new WT_Fass_Learn();
                $wtfs_lesson = new WT_Fass_Learn_Lesson($core_fl);

                include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->learn_plugin_vi . '/core.php');
                $core_vi = new WT_Fass_Video();


                foreach ($result as $k => $v)
                {
                    $v->tree_branch = $wtfa_deck->Tree_Branch_Get_By('id', $v->tree_branch_id);

                    $fasslearn_id = $v->fasslearn_item;
                    $fasslearn_title = $wtfs_lesson->Get($fasslearn_id)->title;

                    $videos_id = $v->videos_item;
                    $videos_title = $core_vi->Get($videos_id)->title;

                    include($this->core->Attr_Get('plugin_templates_path') . 'items/deck.php');
                }

                echo '</div>';
            }
            else
            {
                include($this->core->Attr_Get('plugin_templates_path') . 'areas/list-empty.php');
            }

            $this->result['html'] = ob_get_contents();
            ob_end_clean();

            $this->result['status'] = true;
        break;

        case 'decks-tree-admin-get':
            $decks_tree = $wtfa_deck->Tree_Branches_Get(null);

            ob_start();

            include($this->core->Attr_Get('plugin_templates_path') . 'areas/decks-tree-admin.php');

            $this->result['html'] = ob_get_contents();
            ob_end_clean();

            $this->result['status'] = true;
        break;

        case 'decks-tree-front-get':
            $user_id = (int)get_current_user_id();
            $decks_tree = $wtfa_deck->Tree_Branches_Get($user_id, false);

            if (count($decks_tree))
            {
                ob_start();

                include($this->core->Attr_Get('plugin_templates_path') . 'areas/decks-tree-front.php');

                $this->result['html'] = ob_get_contents();
                ob_end_clean();
            }

            $this->result['status'] = true;
        break;

        case 'create':
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

            $data['title'] = trim($data['title']);
            $data['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['title']);

            if (mb_strlen($data['title']) < 3){ $this->result['error']['deck[title]'] = __('The field must contain at least 3 characters'); }

            $deck = $this->core->Session_Get('deck');

            if (!count($deck['dictionary'])){ $this->result['error']['deck[dictionary]'] = __('The deck must contain at least one word item'); }

            if (!count($this->result['error']))
            {
                $wpdb->query("
                    INSERT INTO
                      `{$wpdb->prefix}{$this->prefix}_decks`
                      (
                          `tree_branch_id`,
                          `title`,
                          `description`,
                          `status`,
                          `date`,
                          `fasslearn_item`
                      )
                      VALUES
                      (
                          '" . esc_sql($data['tree-thread']) . "',
                          '" . esc_sql($data['title']) . "',
                          '" . esc_sql($data['description']) . "',
                          '1',
                          '" . time() . "',
                          '" . esc_sql($data['fasslearn-item']) . "'
                      )
                ");

                $deck_id = (int)$wpdb->insert_id;

                if ($deck_id)
                {
                    foreach ($deck['dictionary'] as $k => $v)
                    {
                        $wpdb->query("
                            INSERT INTO
                              `{$wpdb->prefix}{$this->prefix}_decks_words`
                              (
                                  `deck_id`,
                                  `word_id`
                              )
                              VALUES
                              (
                                  '{$deck_id}',
                                  '" . (int)$v . "'
                              )
                        ");
                    }

                    $this->core->Session_Unset();

                    $this->result['message'] = __('Deck successfully created');
                    $this->result['redirect_url'] = admin_url('admin.php?page=' . $this->prefix . '_decks');
                    $this->result['status'] = true;
                }
            }
        break;

        case 'save':
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

            $data['id'] = (int)$data['id'];
            $data['title'] = trim($data['title']);
            $data['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['title']);
            // $data['fasslearn-item']

            if (mb_strlen($data['title']) < 3){ $this->result['error']['deck[title]'] = __('The field must contain at least 3 characters'); }

            if ($data['id'])
            {
                $deck = $this->core->Session_Get('deck');

                if (!count($deck['dictionary'])){ $this->result['error']['deck[dictionary]'] = __('The deck must contain at least one word item'); }
            }

            if (!count($this->result['error']))
            {
                if ($data['id'])
                {
                    $data['id'] = (int)$data['id'];

                    $wpdb->query("
                        UPDATE
                            `{$wpdb->prefix}{$this->prefix}_decks`
                        SET
                            `tree_branch_id` = '" . esc_sql($data['tree-thread']) . "',
                            `title` = '" . esc_sql($data['title']) . "',
                            `description` = '" . esc_sql($data['description']) . "',
                            `fasslearn_item` = '" . esc_sql($data['fasslearn-item']) . "',
                            `special` = '" . esc_sql($data['special']) . "',
                            `videos_item` = '" . esc_sql($data['videos-item']) . "'
                            
                        WHERE
                            `id` = '{$data['id']}'
                    ");

                    // Clear words
                    $result = $wpdb->get_results("
                        SELECT
                            dw.*
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_decks_words` dw
                        WHERE
                            dw.`deck_id` = '{$data['id']}'
                    ");

                    if (count($result))
                    {
                        foreach ($result as $v)
                        {
                            if (!in_array((int)$v->word_id, $deck['dictionary']))
                            {
                                $wpdb->query("
                                    DELETE FROM
                                        `{$wpdb->prefix}{$this->prefix}_decks_words`
                                    WHERE
                                        `deck_id` = '{$v->deck_id}'
                                        AND 
                                        `word_id` = '{$v->word_id}'
                                ");
                            }
                        }
                    }
                    ////

                    // Add new words
                    foreach ($deck['dictionary'] as $k => $v)
                    {
                        $v = (int)$v;

                        $result = $wpdb->get_results("
                            SELECT
                                dw.*
                            FROM
                                `{$wpdb->prefix}{$this->prefix}_decks_words` dw
                            WHERE
                                dw.`deck_id` = '{$data['id']}'
                                AND 
                                dw.`word_id` = '{$v}'
                        ");

                        if (!count($result))
                        {
                            $wpdb->query("
                                INSERT INTO
                                  `{$wpdb->prefix}{$this->prefix}_decks_words`
                                  (
                                      `deck_id`,
                                      `word_id`
                                  )
                                  VALUES
                                  (
                                      '{$data['id']}',
                                      '{$v}'
                                  )
                            ");
                        }
                    }
                    ////
                }
                else
                if ($data['id'] == 0)
                {
                    update_option("{$this->prefix}_deck_global", array(
                        'title'       => $data['title'],
                        'description' => $data['description']
                    ));
                }

                $this->core->Session_Unset();

                $this->result['message'] = __('Deck successfully saved');
                $this->result['redirect_url'] = admin_url('admin.php?page=' . $this->prefix . '_decks');
                $this->result['status'] = true;
            }
        break;

        case 'remove':
            $data['id'] = (int)$data['id'];

            if ($data['id'])
            {
                $wpdb->query("
                    DELETE FROM
                        `{$wpdb->prefix}{$this->prefix}_decks`
                    WHERE
                        `id` = '{$data['id']}'
                ");

                $wpdb->query("
                    DELETE FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words`
                    WHERE
                        `deck_id` = '{$data['id']}'
                ");

                $this->result['status'] = true;
            }
        break;

        case 'dictionary-manager':
            ob_start();

            include($this->core->Attr_Get('plugin_templates_path') . 'areas/deck-dictionary-manager.php');

            $this->result['html'] = ob_get_contents();
            ob_end_clean();

            $this->result['status'] = true;
        break;

        case 'dictionary-rows-select':
            $deck = $this->core->Session_Get('deck');

            if (!isset($deck['dictionary']))
            {
                $deck['dictionary'] = array();
            }

            if (count($data['items']))
            {
                foreach ($data['items'] as $v)
                {
                    $v = (int)$v;

                    if ($v > 0 && !in_array($v, $deck['dictionary']))
                    {
                        $deck['dictionary'][] = $v;
                    }
                }
            }

            $this->core->Session_Set('deck', $deck);

            $this->result['status'] = true;
        break;

        case 'dictionary-items-get':
            $deck = $this->core->Session_Get('deck');

            ob_start();

            if (count($deck['dictionary']))
            {
                $result = $wpdb->get_results("
                    SELECT
                        d.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                    WHERE
                        d.`id` IN ('" . implode("','", $deck['dictionary']) . "')
                ");

                if (count($result))
                {
                    $this->core->Array_Treatment_Strings($result, 'stripslashes');
                    $this->core->Array_Treatment_Strings($result, 'htmlspecialchars');

                    foreach ($result as $k => $v)
                    {
                        include($this->core->Attr_Get('plugin_templates_path') . 'items/deck-word-item.php');
                    }
                }
            }
            else
            {
                include($this->core->Attr_Get('plugin_templates_path') . 'areas/list-empty.php');
            }

            $this->result['html'] = ob_get_contents();
            ob_end_clean();

            $this->result['status'] = true;
        break;

        case 'dictionary-item-remove':
            $deck = $this->core->Session_Get('deck');
            $key = array_search((int)$data['id'], (array)$deck['dictionary']);

            if ($key !== false)
            {
                unset($deck['dictionary'][$key]);
                $this->core->Session_Set('deck', $deck);
            }

            $this->result['status'] = true;
        break;

        case 'word-get':
            $user_id = (int)get_current_user_id();

            $data['deck_id'] = (int)$data['deck_id'];
            $data['word_id'] = (int)$data['word_id'];
            $data['lvl'] = (int)$data['lvl'];
            if ((int)$data['splash']) $data['splash'] = true; else $data['splash'] = false;

            if ($data['deck_id'])
            {
                $deck_data = $wtfa_deck->Get($data['deck_id']);
            }
            else
            if ($data['deck_id'] == 0)
            {
                $deck_global = get_option("{$this->prefix}_deck_global");
                if (!is_array($deck_global)) $deck_global = array();

                $deck_data = new stdClass();
                $deck_data->id = 0;
                $deck_data->title = $deck_global['title'];
                $deck_data->description = $deck_global['description'];
            }

            if (is_object($deck_data) && (int)$deck_data->id >= 0 && $user_id /*&& function_exists($this->core->lms_courses_user_enrolled)*/)
            {
                if ($data['word_id'] && $data['lvl'])
                {
                    $wtfa_deck->Words_Data_User_Update(get_current_user_id(), $deck_data->id, $data['word_id'], $data['lvl']);
                }

                $deck_dictionary = $wtfa_deck->Dictionary_Get($deck_data->id);
                //$user_courses_enrolled = (array)call_user_func($this->core->lms_courses_user_enrolled, $user_id, true);

                if (count($deck_dictionary))
                {
                    $this->result['splash'] = false;

                    $res = array();
                    $dwdu = $wtfa_deck->Words_Data_User_Get($deck_data->id);

                    foreach ($deck_dictionary as $w)
                    {
                        $w->id = (int)$w->id;
                        $w->course_id = (int)$w->course_id;

                        if
                        (
                            (
                                true
                                //$w->course_id //&&
                                //in_array($w->course_id, $user_courses_enrolled)
                            )
                            &&
                            (
                                !isset($dwdu[$w->id]) ||
                                (isset($dwdu[$w->id]) && $wtfa_deck->Word_Data_Time_Expired($dwdu[$w->id]['lvl'], $dwdu[$w->id]['time']))
                            )
                        )
                        {
                            $res[] = (array)$w;
                        }
                    }

                    $this->result['deck-id'] = $deck_data->id;
                    $this->result['deck-title'] = $deck_data->title;

                    if ($data['splash'] || !count($res))
                    {
                        $data_tpl = array(
                            'words_deck_count'  => count($deck_dictionary),
                            'words_learn_count' => count($res),
                            'words_user'        => array()
                        );

                        foreach ($deck_dictionary as $w)
                        {
                            if (isset($dwdu[$w->id]))
                            {
                                $v = &$data_tpl['words_user'][$dwdu[$w->id]['lvl']];
                                $v = (int)$v + 1;
                            }
                        }

                        ob_start();

                        include($this->core->Attr_Get('plugin_templates_path') . 'areas/deck-user-statistics.php');

                        $this->result['html'] = ob_get_contents();
                        ob_end_clean();

                        $this->result['splash'] = true;
                        $this->result['status'] = true;
                    }
                    else
                    if (count($res))
                    {
                        shuffle($res);

                        $this->result['word'] = (array)$res[0];
                        $this->result['word']['text_translation'] = base64_encode($this->result['word']['text_translation']);

                        if
                        (
                            trim($this->result['word']['file']) != '' &&
                            file_exists($this->core->Attr_Get('plugin_file_uploads_path') . $this->result['word']['file'])
                        )
                        {
                            $this->result['word']['file'] = $this->core->Attr_Get('plugin_file_uploads_url') . $this->result['word']['file'];
                        }
                        else
                        {
                            $this->result['word']['file'] = false;
                        }

                        $this->result['status'] = true;
                    }
                }
            }
        break;

        case 'tree-thread-rename-form-get':
            $tree_thread = $wtfa_deck->Tree_Branch_Get_By('id', $data['tree_branch_id']);

            if (is_array($tree_thread) && count($tree_thread))
            {
                ob_start();

                include($this->core->Attr_Get('plugin_templates_path') . 'areas/tree-thread-rename-form.php');

                $this->result['html'] = ob_get_contents();
                ob_end_clean();

                $this->result['status'] = true;
            }
        break;

        case 'tree-thread-add-form-get':

            if ($data['tree_branch_id'] == 'all') { // if root
                $tree_thread = $this->core->Get_Root_Decks_Tree();// get_option("{$this->prefix}_decks_tree");
                $tree_thread['id'] = 0;
            } else {
                $tree_thread = $wtfa_deck->Tree_Branch_Get_By('id', $data['tree_branch_id']);
            }

            if (is_array($tree_thread) && count($tree_thread))
            {
                ob_start();

                include($this->core->Attr_Get('plugin_templates_path') . 'areas/tree-thread-add-form.php');

                $this->result['html'] = ob_get_contents();
                ob_end_clean();

                $this->result['status'] = true;
            }
        break;

        case 'tree-thread-remove-form-get':
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);

            $tree_thread = $wtfa_deck->Tree_Branch_Get_By('id', $data['tree_branch_id']);

            if ($data['tree_branch_id'] == 'all') { // if root
                $tree_thread = $this->core->Get_Root_Decks_Tree();//get_option("{$this->prefix}_decks_tree");
                $tree_thread['id'] = 0;
                $tree_thread['title'] = 'root'; // todo fix root name
            } else {
                $tree_thread = $wtfa_deck->Tree_Branch_Get_By('id', $data['tree_branch_id']);
            }

            if (is_array($tree_thread) && count($tree_thread))
            {
                ob_start();

                include($this->core->Attr_Get('plugin_templates_path') . 'areas/tree-thread-remove-form.php');

                $this->result['html'] = ob_get_contents();
                ob_end_clean();

                $this->result['status'] = true;
            }
        break;

        case 'sorting-branch':
            $data = $_POST["data"];
            $branches = get_option("wtfa_decks_tree");

//            var_dump($branches) ;die;
//            var_dump($data) ;die;

            foreach ($branches as $branch) {
//                var_dump($data[$branch['id']]) ;die;
                $wtfa_deck->Tree_Branches_Update($branch['id'], function(&$item)use($branch, $data){
                    $item['title'] = $branch['title'];
                    $item['price'] = $branch['price'];
                    $item['hidden'] = $branch['hidden'];
                    $item['difficulty_level'] = $branch['difficulty_level'];
                    $item['text_before_sale'] = $branch['text_before_sale'];
                    $item['number_of_scenarios'] = $branch['number_of_scenarios'];
                    $item['sort_id'] = $data[$branch['id']];
                });
            }
            $this->result['status'] = true;
            break;

        case 'tree-thread-rename-do':
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

            $data['title'] = trim($data['title']);
            $data['price'] = !empty($data['price']) ? $data['price'] : 0;
            $data['text_before_sale'] = !empty($data['text_before_sale']) ? $data['text_before_sale'] : '';
            $data['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['title']);

            if (mb_strlen($data['title']) < 3){ $this->result['error']['tree-thread[title]'] = __('The field must contain at least 3 characters'); }

            if (!count($this->result['error']))
            {
                $wtfa_deck->Tree_Branches_Update($data['tree_branch_id'], function(&$item)use($data){
                    $item['title'] = $data['title'];
                    $item['price'] = $data['price'];
                    $item['hidden'] = $data['hidden'];
                    $item['difficulty_level'] = $data['difficulty_level'];
                    $item['text_before_sale'] = $data['text_before_sale'];
                    $item['number_of_scenarios'] = $data['number_of_scenarios'];
                });
                changeAccessBranchesByHidden($data['hidden'], $data['tree_branch_id']);

                $this->result['status'] = true;
            }
        break;

        case 'tree-thread-add-do':
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

            $data['title'] = trim($data['title']);
            $data['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['title']);

            if (mb_strlen($data['title']) < 3){ $this->result['error']['tree-thread[title]'] = __('The field must contain at least 3 characters'); }


            if (!count($this->result['error']))
            {
                $data['price'] = !empty($data['price']) ? $data['price'] : 0;
                $data['price'] = !empty($data['text_before_sale']) ? $data['text_before_sale'] : '';
                $wtfa_deck->Tree_Branches_Add($data['tree_branch_id'], function(&$item)use($data){
                    $item[] = [
                        'title' => $data['title'],
                        'price' => $data['price'],
                        'hidden' => $data['hidden'],
                        'difficulty_level' => $data['difficulty_level'],
                        'text_before_sale' => $data['text_before_sale'],
                        'number_of_scenarios' => $data['number_of_scenarios'],
                        'id'    => uniqid()];
                });

                $this->result['status'] = true;
            }
            break;

        case 'tree-thread-remove-do':
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

            if (!count($this->result['error']))
            {
                $wtfa_deck->Tree_Branches_Remove($data['tree_branch_id'], function(&$item)use($data){});

                $this->result['status'] = true;
            }
            break;

        case 'sortable-sync':
            if (isset($data['sd']) && is_array($data['sd']) && count($data['sd']))
            {
                foreach ($data['sd'] as $k => $v)
                {
                    $k = (int)$k;
                    $v = (int)$v;

                    if ($k < 0) $k = 0;

                    if ($v)
                    {
                        $wpdb->query("
                            UPDATE
                                `{$wpdb->prefix}{$this->prefix}_decks`
                            SET
                                `position` = '{$k}'
                            WHERE
                                `id` = '{$v}'
                        ");
                    }
                }
            }

            $this->result['status'] = true;
        break;
    }

    function changeAccessBranchesByHidden($isHidden, $branchId)
    {
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM `learn_usermeta` WHERE `meta_key` = 'wtfa_access_branches'");

        foreach ($result as $item) {
            $accessBranches = unserialize(unserialize($item->meta_value));
            $userId = $item->user_id;
                if ($isHidden){
                    if (in_array($branchId,$accessBranches)) {
                        $accessBranches = array_diff( $accessBranches, [$branchId]);
                    }
                } else {
                    if (!in_array($branchId,$accessBranches)) {
                        $accessBranches[] = $branchId;
                    }
                }

                update_user_meta($userId, 'wtfa_access_branches', serialize($accessBranches));
            }


    }


