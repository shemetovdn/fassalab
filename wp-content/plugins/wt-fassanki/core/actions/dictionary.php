<?php

    switch ($this->subaction)
    {
        case 'table-get':
            include_once($this->core->Attr_Get('plugin_libraries_path') . 'mw-wp-pagination.php');

            $qa = array(
                'where' => '',
                'sort'  => '',
                'limit' => ''
            );

            $data['attr']['search'] = preg_replace("/[\s]{2,}/uis", ' ', trim($data['attr']['search']));

            if ($data['attr']['search'] != '')
            {
                $qa['where']  = 'WHERE ';
                $qa['where'] .= "d.`id` LIKE '%" . esc_sql($data['attr']['search']) . "%'";
                $qa['where'] .= ' OR ';
                $qa['where'] .= "d.`text_source` LIKE '%" . esc_sql($data['attr']['search']) . "%'";
                $qa['where'] .= ' OR ';
                $qa['where'] .= "d.`text_translation` LIKE '%" . esc_sql($data['attr']['search']) . "%'";
                $qa['where'] .= ' OR ';
                $qa['where'] .= "d.`description` LIKE '%" . esc_sql($data['attr']['search']) . "%'";
            }

            if ($data['attr']['sort']['by'] != '')
            {
                $qa['sort'] = 'ORDER BY d.`' . esc_sql($data['attr']['sort']['by']) . '` ';

                if ((int)$data['attr']['sort']['type'] < 1)
                {
                    $qa['sort'] .= 'ASC';
                }
                else
                {
                    $qa['sort'] .= 'DESC';
                }
            }

            $result = $wpdb->get_results("
                SELECT
                    d.`id`
                FROM
                    `{$wpdb->prefix}{$this->prefix}_dictionary` d
                {$qa['where']}    
            ");

            // Pagination
            $items_on_page = 100;
            $items_count = count($result);

            $pg = MW_WP_Pagination(array(
                'show-pages-count'    => 5,
                'items-on-page'       => $items_on_page,
                'items-count'         => $items_count,
                'current-page'        => (int)$data['attr']['page'],
                'href'                => 'javascript:window.WTFA_Admin.dictionary.page_change({page});',
                'href-first-page'     => 'javascript:window.WTFA_Admin.dictionary.page_change({page});',
                'current-html'        => "<a href=\"{href}\" class=\"active\">{page}</a>",
                'page-html'           => "<a href=\"{href}\">{page}</a>",
                'next-html'           => "<a href=\"{href}\" class=\"last\">{page}</a>",
                'previous-html'       => "<a href=\"{href}\" class=\"first\">{page}</a>",
                'next-text'           => __('next') . '&nbsp;&rsaquo;',
                'previous-text'       => '&lsaquo;&nbsp;' . __('previous'),
                'group-text'          => '...',
                'show-first-and-last' => true
            ));

            if ($pg['status'])
            {
                $qa['limit'] = 'LIMIT ' . (($pg['page'] * $items_on_page) - $items_on_page) . ',' . $items_on_page;
            }
            ////

            $result = $wpdb->get_results("
                SELECT
                    d.*,
                    pc.`post_title` AS 'course_title'
                FROM
                    `{$wpdb->prefix}{$this->prefix}_dictionary` d
                LEFT JOIN
                    `{$wpdb->prefix}posts` pc
                    ON 
                        pc.`post_type` = '{$this->core->lms_post_type_course}'
                        AND 
                        pc.`ID` = d.`course_id`    
                {$qa['where']}    
                {$qa['sort']}    
                {$qa['limit']} 
            ");

            $this->core->Array_Treatment_Strings($result, 'stripslashes');

            ob_start();

            $this->core->Template_Get('areas/dictionary-table', null, array(
                'attr'        => $data['attr'],
                'pg'          => $pg,
                'items_count' => $items_count,
                'data'        => $result
            ));

            $this->result['html'] = ob_get_contents();
            ob_end_clean();

            $this->result['status'] = true;
        break;

        case 'table-row-form':
            $data['id'] = (int)$data['id'];
            $tpl_data = array();

            if ($data['id'])
            {
                $result = $wpdb->get_results("
                    SELECT
                        d.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                    WHERE
                        d.`id` = '{$data['id']}'
                ", ARRAY_A);
            }

            if ($data['id'] && count($result))
            {
                $result[0]['id'] = (int)$result[0]['id'];
                if ((int)$result[0]['status'] > 0){ $result[0]['status'] = 1; }else{ $result[0]['status'] = 0; }

                $this->core->Array_Treatment_Strings($result[0], 'stripslashes');

                $tpl_data['caption'] = __('Editing');
                $tpl_data['row'] = $result[0];
                $tpl_data['submit']['title'] = __('Save');
            }
            else
            {
                $tpl_data['caption'] = __('Creating');
                $tpl_data['submit']['title'] = __('Create');
            }

            // Courses
            $result = $wpdb->get_results("
                SELECT
                    pc.`ID` AS 'id',
                    pc.`post_title` AS 'title'
                FROM
                    `{$wpdb->prefix}posts` pc
                WHERE
                    pc.`post_type` = '{$this->core->lms_post_type_course}'
                    AND
                    pc.`post_status` = 'publish'
                ORDER BY
                    pc.`post_title`
                    ASC
            ", ARRAY_A);

            $tpl_data['courses'] = $result;
            ////

            ob_start();

            $this->core->Template_Get('areas/dictionary-table-row-form', null, $tpl_data);

            $this->result['html'] = ob_get_contents();
            ob_end_clean();

            $this->result['status'] = true;
        break;

        case 'row-remove':
            $data['id'] = (int)$data['id'];

            if ($data['id'])
            {
                $result = $wpdb->get_results("
                    SELECT
                        d.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                    WHERE
                        d.`id` = '{$data['id']}'
                ", ARRAY_A);

                if (count($result))
                {
                    @unlink($this->core->Attr_Get('plugin_file_uploads_path') . $result[0]['file']);

                    $wpdb->query("
                        DELETE FROM
                            `{$wpdb->prefix}{$this->prefix}_dictionary`
                        WHERE
                            `id` = '{$result[0]['id']}'
                    ");
                }
            }

            $this->result['status'] = true;
        break;

        case 'row-save':
            $data['id'] = (int)$data['id'];
            $data['text_source'] = preg_replace("/[\s]{2,}/uis", ' ', trim($data['text_source']));
            $data['text_translation'] = preg_replace("/[ \t]{2,}/uis", ' ', trim($data['text_translation']));
            $data['text_transcription'] = preg_replace("/[ \t]{2,}/uis", ' ', trim($data['text_transcription']));
            $data['description'] = preg_replace("/[ \t]{2,}/uis", ' ', trim($data['description']));
            $data['course'] = (int)$data['course'];
            if ((int)$data['status'] > 0){ $data['status'] = 1; }else{ $data['status'] = 0; }

            if (mb_strlen($data['text_source']) < 1){ $this->result['error']['text_source'] = __('The field must contain at least 1 characters'); }
            if (mb_strlen($data['text_translation']) < 1){ $this->result['error']['text_translation'] = __('The field must contain at least 1 characters'); }
//            if (mb_strlen($data['text_transcription']) < 1){ $this->result['error']['text_transcription'] = __('The field must contain at least 1 characters'); }

            if (!count($this->result['error']) && isset($_FILES['dictionary']) && !$_FILES['dictionary']['error']['file'])
            {
                $file = wp_list_pluck($_FILES['dictionary'], 'file');

                if (!preg_match("/\/(mp3|mpeg|jpg|jpeg|png|gif)$/uis", $file['type'])){ $this->result['error']['file'] = __('File is wrong type'); }
            }

            if (!count($this->result['error']))
            {
                if ($data['id'])
                {
                    $wpdb->query("
                        UPDATE
                            `{$wpdb->prefix}{$this->prefix}_dictionary`
                        SET
                            `text_source` = '" . esc_sql($data['text_source']) . "',
                            `text_translation` = '" . esc_sql($data['text_translation']) . "',
                            `text_transcription` = '" . esc_sql($data['text_transcription']) . "',
                            `description` = '" . esc_sql($data['description']) . "',
                            `course_id` = '{$data['course']}',
                            `status` = '{$data['status']}',
                            `date` = '" . time() . "'
                        WHERE
                            `id` = '{$data['id']}'
                    ");
                }
                else
                {
                    $wpdb->query("
                        INSERT INTO
                            `{$wpdb->prefix}{$this->prefix}_dictionary`
                            (
                                `text_source`,
                                `text_translation`,
                                `text_transcription`,
                                `description`,
                                `course_id`,
                                `status`,
                                `date`
                            )
                            VALUES
                            (
                                '" . esc_sql($data['text_source']) . "',
                                '" . esc_sql($data['text_translation']) . "',
                                '" . esc_sql($data['text_transcription']) . "',
                                '" . esc_sql($data['description']) . "',
                                '{$data['course']}',
                                1,
                                '" . time() . "'
                            )
                    ");

                    $data['id'] = (int)$wpdb->insert_id;
                }

                if ((int)$data['file_remove'])
                {
                    $result = $wpdb->get_results("
                        SELECT
                            d.`file`
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_dictionary` d
                        WHERE
                            d.`id` = '{$data['id']}'
                    ", ARRAY_A);

                    if (count($result))
                    {
                        @unlink($this->core->Attr_Get('plugin_file_uploads_path') . $result[0]['file']);

                        $wpdb->query("
                            UPDATE
                                `{$wpdb->prefix}{$this->prefix}_dictionary`
                            SET
                                `file` = NULL 
                            WHERE
                                `id` = '{$data['id']}'
                        ");
                    }
                }

                if (isset($file) && is_array($file))
                {
                    $file_name = md5($data['id']) . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                    $file_path = $this->core->Attr_Get('plugin_file_uploads_path') . $file_name;

                    @unlink($file_path);

                    if (move_uploaded_file($file['tmp_name'], $file_path))
                    {
                        @chmod($file_path, 0777);

                        $wpdb->query("
                            UPDATE
                                `{$wpdb->prefix}{$this->prefix}_dictionary`
                            SET
                                `file` = '" . esc_sql($file_name) . "'
                            WHERE
                                `id` = '{$data['id']}'
                        ");
                    }
                }

                $this->result['status'] = true;
            }
        break;
    }
