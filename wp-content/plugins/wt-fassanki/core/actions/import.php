<?php

    // PHPExcel
    include_once($this->core->Attr_Get('plugin_libraries_path') . 'PHPExcel/Classes/PHPExcel/IOFactory.php');
    ////

    switch ($this->subaction)
    {
        case 'pre-import':
            $file = $_FILES['import'];
            $file = wp_list_pluck($file, 'file');

            if (!count($file) || $file['error']){ $this->result['error'][] = __('Upload error'); }

            if (!count($this->result['error']))
            {
                if (!preg_match("/^application\/.*?(excel|xls).*?$/uis", $file['type'])){ $this->result['error'][] = __('Wrong file type or format'); }

                if (!count($this->result['error']))
                {
                    $doc = PHPExcel_IOFactory::load($file['tmp_name']);
                    $doc->setActiveSheetIndex(0);
                    $sheet = $doc->getActiveSheet();

                    $columns_count = (int)ord($sheet->getHighestColumn()) - 64;
                    $rows_count = (int)$sheet->getHighestRow();

                    if ($columns_count != 4){ $this->result['error'][] = __('Document must contain 4 columns'); }
                    if ($rows_count < 2){ $this->result['error'][] = __('Document must contain minimum 2 rows'); }
                }
            }

            if (count($this->result['error']))
            {
                $this->result['error'] = implode('<br />', $this->result['error']);
            }
            else
            {
                $nfd = sys_get_temp_dir() . '/' . md5($file['tmp_name']);

                move_uploaded_file($file['tmp_name'], $nfd);
                chmod($nfd, 0777);

                $this->result['doc']['name'] = $file['name'];
                $this->result['doc']['size'] = number_format($file['size'] / 1024, 2, '.', '');
                $this->result['doc']['columns_count'] = $columns_count;
                $this->result['doc']['rows_count'] = $rows_count;
                $this->result['doc']['tmp_path'] = $nfd;

                $this->result['doc']['message']  = __('Uploaded file name') . ": <b>{$this->result['doc']['name']}</b><br />";
                $this->result['doc']['message'] .= __('File size') . ": <b>{$this->result['doc']['size']} KB</b><br />";
                $this->result['doc']['message'] .= __('Columns') . '/' . __('Rows') . ' ' . __('count') . ": <b>{$this->result['doc']['columns_count']}/{$this->result['doc']['rows_count']}</b><br />";
                $this->result['doc']['message'] .= '<div class="btn"><input type="button" name="import[do]" value="' . __('Continue import') . '" /></div>';

                $this->result['status'] = true;
            }
        break;

        case 'import':
            if (!file_exists($data['doc']['tmp_path'])){ __('Temporary file of document is not exist'); }

            if (!count($this->result['error']))
            {
                $res = array(
                    'rows_new'     => 0,
                    'rows_updated' => 0
                );

                $doc = PHPExcel_IOFactory::load($data['doc']['tmp_path']);
                $doc->setActiveSheetIndex(0);
                $sheet = $doc->getActiveSheet();

                $rows_count = (int)$sheet->getHighestRow();

                if ($rows_count > 1)
                {
                    for ($i1 = 2; $i1 <= $rows_count; $i1++)
                    {
                        $row = array(
                            'text_source'      => preg_replace("/[\s]{2,}/uis", ' ', trim($sheet->getCell('A' . $i1)->getValue())),
                            'text_translation' => preg_replace("/[ \t]{2,}/uis", ' ', trim($sheet->getCell('B' . $i1)->getValue())),
                            'description'      => preg_replace("/[ \t]{2,}/uis", ' ', trim($sheet->getCell('C' . $i1)->getValue())),
                            'course_id'        => (int)preg_replace("/[^0-9]+/uis", '', $sheet->getCell('D' . $i1)->getValue())
                        );

                        if ($row['text_source'] != '' && $row['text_translation'] != '')
                        {
                            $wpdb->query("
                                UPDATE
                                    `{$wpdb->prefix}{$this->prefix}_dictionary`
                                SET
                                    `text_source` = '" . esc_sql($row['text_source']) . "',
                                    `text_translation` = '" . esc_sql($row['text_translation']) . "',
                                    `description` = '" . esc_sql($row['description']) . "',
                                    `course_id` = '{$row['course_id']}',
                                    `date` = " . time() . "
                                WHERE
                                    `text_source` = '" . esc_sql($row['text_source']) . "'
                            ");

                            if (!(int)$wpdb->rows_affected)
                            {
                                $wpdb->query("
                                    INSERT INTO
                                        `{$wpdb->prefix}{$this->prefix}_dictionary`
                                        (
                                            `text_source`,
                                            `text_translation`,
                                            `description`,
                                            `course_id`,
                                            `status`,
                                            `date`
                                        )
                                        VALUES
                                        (
                                            '" . esc_sql($row['text_source']) . "',
                                            '" . esc_sql($row['text_translation']) . "',
                                            '" . esc_sql($row['description']) . "',
                                            '{$row['course_id']}',
                                            1,
                                            " . time() . "
                                        )
                                ");

                                if ((int)$wpdb->insert_id)
                                {
                                    $res['rows_new']++;
                                }
                            }
                            else
                            {
                                $res['rows_updated']++;
                            }
                        }
                    }
                }
            }

            if (count($this->result['error']))
            {
                $this->result['error'] = implode('<br />', $this->result['error']);
            }
            else
            {
                $this->result['doc']['message']  = '<b>' . __('Import data finished successfully') . '</b><br /><br />';
                $this->result['doc']['message'] .= __('New rows') . ': <b>' . $res['rows_new'] . '</b><br />';
                $this->result['doc']['message'] .= __('Updated rows') . ': <b>' . $res['rows_updated'] . '</b><br /><br />';
                $this->result['doc']['message'] .= '<a href="' . admin_url('admin.php?page=' . $this->prefix . '_dictionary') . '">' . __('Go to dictionary') . '</a>';

                $this->result['status'] = true;
            }
        break;
    }
