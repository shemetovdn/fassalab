<?php
add_action( 'init', 'andreyex_stop_heartbeat', 1 );
function andreyex_stop_heartbeat() {
    wp_deregister_script('heartbeat');
}
    class WT_Fass_Anki
    {
        private $prefix = 'wtfa';
        private $tree_branch_fid = '58aff18741639';
        public $lms_post_type_course = 'sfwd-courses';
        public $lms_course_completed_f = 'is_course_prerequities_completed';
        public $lms_courses_user_enrolled = 'learndash_user_get_enrolled_courses';
        private $attr = array();
        public $learn_plugin_fn = 'wt-fasslearn';
        public $learn_plugin_vi = 'wt-fassvideo';

        public function __construct()
        {
            $this->Init();
        }

        private function Init()
        {
            if(!isset($_SESSION)) {
                session_start();
            }
//            @session_start();

            $this->Attr_Set('prefix', $this->prefix);
            $this->Attr_Set('tree_branch_fid', $this->tree_branch_fid);

            $this->Plugin_Paths_Init();
            $this->Plugin_Hooks_Init();
            $this->Plugin_Pages_Init();

            add_action('init', array($this, 'Actions_Init'));
            add_action('init', array($this, 'Admin_Init'));
            add_action('init', array($this, 'Shortcodes_Init'));
            add_action('wp_enqueue_scripts', array($this, 'Enqueue_Sources'));
            add_action('admin_enqueue_scripts', array($this, 'Enqueue_Sources_Admin'));
            add_action('wp_head', array($this, 'Head_Init'));
            //add_action('wp', array($this, 'Vocabulary_Front_End_Init')); // todo  call_user_func_array() expects parameter 1 to be a valid callback, class 'WT_Fass_Anki' does not have a method 'Vocabulary_Front_End_Init'

            add_action( 'show_user_profile', 		array( $this, 'Show_User_Profile') );
            add_action( 'edit_user_profile', 		array( $this, 'Show_User_Profile') );
            add_action( 'edit_user_profile_update', array( $this, 'Edit_User_Profile_Update') );

            add_action('init', array($this, 'Partner_Role_Create'));

            //get_certification
            add_action('wp_ajax_get_certification', array($this, 'Generate_Certification'));
            add_action('wp_ajax_nopriv_get_certification', array($this, 'Generate_Certification'));

        }

        public function Generate_Certification()
        {
            if (!empty($_POST)) {
                include_once($this->Attr_Get('plugin_core_path') . 'deck.php');
                $wtfa_deck = new WT_Fass_Anki_Deck($this);
                $userId = (int)$_POST['userId'];
                global $wpdb;
                $result = $wpdb->get_results("
                    SELECT
                        token
                    FROM
                        `learn_wtfapi_tokens`
                    WHERE
                        `user_id` = '{$userId}'
                ");

                $result = $wtfa_deck->Tree_Branches_Get($userId, (boolean)false,false, $result[0]->token );
                $stepLerned = 0;
                $stepCount = 0;
                $wordLerned = 0;
                $wordCount = 0;
                $branchesUnlocked = 0;
                 foreach ($result as $key => $branch) {
                     foreach ($branch["decks"] as $deck) {
                         $stepCount += $deck["fasslearn"]["count"];
                         $stepLerned += $deck["fasslearn"]["learned"];
                         $wordCount += $deck["dictionary"]["count"];
                         $wordLerned += $deck["dictionary"]["learned"];
                     }
                     $branchesUnlocked++;
                 }

                 $res = (object)[
                     'steps' => [
                         'stepCount' =>$stepCount,
                         'stepLerned' =>$stepLerned,
                     ],
                     'words' => [
                         'wordCount' =>$wordCount,
                         'wordLerned' =>$wordLerned,
                     ],
                     'branchesUnlocked' => $branchesUnlocked
                 ];
//                var_dump($stepCount);
//                var_dump($result);die;

                echo json_encode($res);
                wp_die();
            }

        }

        public function Partner_Role_Create()
        {
            if (isset($_GET['dev_role'])) {
                global $wp_roles;
                echo '<pre>';
//                var_dump($wp_roles);


                if ( ! isset( $wp_roles ) )
                    $wp_roles = new WP_Roles();

                $wp_roles->add_role('partner', 'Partner');

                $role = get_role( 'partner' );
//                $role = $wp_roles->get_role( 'partner' );
                $role->add_cap( 'read' );
                $role->add_cap( 'manage_options' );
                print_r($role);

                $current_user = wp_get_current_user();
                echo '<pre>';
                $tree_thread = $this -> Get_Root_Decks_Tree();

                var_dump($tree_thread);
//                var_dump($current_user->data->user_login);
                echo '</pre>';
                die();
            }
        }

        public function Get_Root_Decks_Tree($user = null) // get partner slug from user_meta
        {
            if ($user != null) {
                $current_user = get_user_by('login', $user);
            } else {
                $current_user = wp_get_current_user();
            }

            if(in_array('partner', $current_user->roles)){ // if
                $decks_tree = get_option("{$this->prefix}_{$current_user->data->user_login}_decks_tree");
            } else {
                $decks_tree = get_option("{$this->prefix}_decks_tree");
            }
            if (!is_array($decks_tree)) $decks_tree = array();
            $decks_tree = $this->makePriceInt($decks_tree, $user);
            return $decks_tree;
        }

        private function makePriceInt($decksTree, $user) {
            $result = [];

                usort($decksTree, function($a, $b) {
                    return $a['sort_id'] - $b['sort_id'];
                });

            foreach ($decksTree as $key => $item) {
                if (isset($item['price'])) {
                    $item['price'] = (double)$item['price'];
                    $payment = $this->getPaymentInfo($user, $item['id']);
                    if (!empty($payment[0])){
                        $item['paymentStatus'] = 1;
                    } else {
                        $item['paymentStatus'] = 0;
                    }
                }
                if (isset($item['difficulty_level']))$item['difficulty_level'] = (int)$item['difficulty_level'];
                if (isset($item['number_of_scenarios']))$item['number_of_scenarios'] = (int)$item['number_of_scenarios'];

//                if ($item['hidden'] != 1) {
//                    $result[$key] = $item;
//                }
                $result[$key] = $item;

            }


            return $result;

        }

        private function getPaymentInfo($user, $branchId) {

            global $wpdb;
            $result = $wpdb->get_results("
                        SELECT 
                        *
                        FROM
                            `buy_branches`
                        WHERE
                            user_id={$user}
                        AND branch_id='{$branchId}'
                        AND status = 1
                    ");

            return $result;

        }

        public function Set_Root_Decks_Tree($decks_tree)
        {
            $current_user = wp_get_current_user();
            if(in_array('partner', $current_user->roles))  { // или партнер или у юзера есть метаполе партнер
                update_option("{$this->prefix}_{$current_user->data->user_login}_decks_tree", $decks_tree);
            } else {
                update_option("{$this->prefix}_decks_tree", $decks_tree);
            }
            if (!is_array($decks_tree)) $decks_tree = array();

            return $decks_tree;
        }

        public function Edit_User_Profile_Update($user_id){
            if ( current_user_can('edit_user',$user_id) ) {
                $access_branche = serialize($_REQUEST['access_branches']);
                update_user_meta($user_id, $this->prefix.'_access_branches', $access_branche);
            }
        }

        public function Show_User_Profile($user){
            $current_user = wp_get_current_user();

            $current_parner_slug = get_user_meta($user->ID, 'partner');
            if(in_array('partner', $current_user->roles) || $current_parner_slug) {
                $args  = array(
                    'blog_id'      => $GLOBALS['blog_id'],
                    'role'         => 'partner',
                );
                $list_of_partners = get_users($args);

                $current_parner = get_user_by('login', $current_parner_slug[0]);
                include ($this->Attr_Get('plugin_templates_path').'user_profile/partner_list.php');
            }



            $decks_tree = $this->Get_Root_Decks_Tree($current_parner_slug[0]); //get_option("{$this->prefix}_decks_tree");
            $access_branches = unserialize(get_user_meta($user->id, $this->prefix.'_access_branches', true));

            if ($access_branches == '') {
                $access_branches[] = $decks_tree[0]['id']; // set default branch
            }

            $_result = $this->Access_Branches_Rest($decks_tree, $user->id);
            $decks_tree = $_result['decks_tree'];
            $access_branches = $_result['access_branches'];

//            $_rest = array();
//            $_access_branches_rest = array();
//            foreach ($decks_tree as $branch) { // divide on access list and rest list of branches
//                $flag = true;
//                foreach ($access_branches as $a_branch) {
//                    if ($a_branch == $branch['id']){
//                        $flag = false;
//                        $_access_branches_rest[] = $branch;
//                        break;
//                    }
//                }
//                if ($flag) {
//                    $_rest[] = $branch;
//                }
//            }
//
//            $decks_tree = $_rest ;
//            $access_branches = $_access_branches_rest;

            include ($this->Attr_Get('plugin_templates_path').'user_profile/edit.php');

            include ($this->Attr_Get('plugin_templates_path').'user_profile/get_certification.php');
        }

        public function Access_Branches_Rest ($decks_tree, $user_id) {
            $access_branches = unserialize(get_user_meta($user_id, $this->prefix.'_access_branches', true));

            if ($access_branches == '') {
                $access_branches[] = $decks_tree[0]['id']; // set default branch
            }
            $_rest = array();
            $_access_branches_rest = array();
            foreach ($decks_tree as $branch) { // divide on access list and rest list of branches
                $flag = true;
                foreach ($access_branches as $a_branch) {
                    if ($a_branch == $branch['id']){
                        $flag = false;
                        $_access_branches_rest[] = $branch;
                        break;
                    }
                }
                if ($flag) {
                    $_rest[] = $branch;
                }
            }
            return array(
              'decks_tree' => $_rest,
              'access_branches' => $_access_branches_rest,
            );

        }

        public function Set_Defaut_Branch($user_id)
        {
            $decks_tree = $this->Get_Root_Decks_Tree();//get_option($this->prefix."_decks_tree");
//          $access_branches[] = $decks_tree[0]['id']; // todo set default branch
            $access_branches = [];
            foreach ($decks_tree as $item) {
                if ($item['hidden'] != 1) {
                    $access_branches[] = $item['id'];
                }

            }
            update_user_meta($user_id, $this->prefix.'_access_branches', serialize($access_branches));
            return  unserialize(get_user_meta($user_id, 'wtfa_access_branches', true));
        }

        protected function Attr_Set($key, $value)
        {
            $this->attr[$key] = $value;
        }

        public function Prefix_Get()
        {
            return $this->prefix;
        }

        public function Attr_Get($key)
        {
            if (isset($this->attr[$key]))
            {
                return $this->attr[$key];
            }
            else
            {
                return false;
            }
        }

        public function Session_Set($key, $value)
        {
            $_SESSION[$this->prefix][$key] = $value;
        }

        public function Session_Get($key)
        {
            if (isset($_SESSION[$this->prefix]) && isset($_SESSION[$this->prefix][$key]))
            {
                return $_SESSION[$this->prefix][$key];
            }
            else
            {
                return false;
            }
        }

        public function Session_Unset($key = null)
        {
            if (is_null($key))
            {
                unset($_SESSION[$this->prefix]);
            }
            else
            {
                unset($_SESSION[$this->prefix][$key]);
            }
        }

        private function Plugin_Paths_Init()
        {
            $ps = array();

            preg_match("/^(.*[\/\\\]wp-content[\/\\\]plugins[\/\\\])(.*?)[\/\\\].*$/uis", __FILE__, $ps);

            if (is_array($ps) && count($ps) == 3)
            {
                $this->Attr_Set('plugin_dir_name', $ps[2]);
                $this->Attr_Set('plugin_path', $ps[1] . $this->Attr_Get('plugin_dir_name') . '/');
                $this->Attr_Set('plugin_index_path', $this->Attr_Get('plugin_path') . 'index.php');
                $this->Attr_Set('plugin_core_path', $this->Attr_Get('plugin_path') . 'core/');
                $this->Attr_Set('plugin_actions_path', $this->Attr_Get('plugin_path') . 'core/actions/');
                $this->Attr_Set('plugin_libraries_path', $this->Attr_Get('plugin_path') . 'core/libraries/');
                $this->Attr_Set('plugin_data_path', $this->Attr_Get('plugin_path') . 'data/');
                $this->Attr_Set('plugin_templates_path', $this->Attr_Get('plugin_path') . 'data/templates/');
                $this->Attr_Set('plugin_file_uploads_path', wp_upload_dir()['basedir'] . '/' . $this->Attr_Get('prefix') . '-files/');

                $this->Attr_Set('plugin_url', plugins_url($this->Attr_Get('plugin_dir_name')) . '/');
                $this->Attr_Set('plugin_css_url', $this->Attr_Get('plugin_url') . 'data/css/');
                $this->Attr_Set('plugin_js_url', $this->Attr_Get('plugin_url') . 'data/js/');
                $this->Attr_Set('plugin_img_url', $this->Attr_Get('plugin_url') . 'data/images/');
                $this->Attr_Set('plugin_file_uploads_url', wp_upload_dir()['baseurl'] . '/' . $this->Attr_Get('prefix') . '-files/');
            }
        }

        private function Plugin_Hooks_Init()
        {
            register_activation_hook($this->Attr_Get('plugin_index_path'), array($this, 'Install'));
            //register_deactivation_hook();
            register_uninstall_hook($this->Attr_Get('plugin_index_path'), array($this, 'Uninstall'));

            add_filter('body_class', function($classes){
                global $post;

                if (is_page() || is_single())
                {
                    if
                    (
                        preg_match("/\[{$this->prefix}_deck[\s\]]?/uis", $post->post_content)
                        || preg_match("/\[{$this->prefix}[\s\]]?/uis", $post->post_content)
                    )
                    {
                        $classes[] = $this->prefix . '-deck';
                    }
                }

                return $classes;
            });
        }

        public function Install()
        {
            global $wpdb;

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_dictionary`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `text_source` LONGTEXT DEFAULT NULL,
                    `text_translation` LONGTEXT DEFAULT NULL,
                    `description` LONGTEXT DEFAULT NULL,
                    `course_id` BIGINT(20) DEFAULT NULL,
                    `status` TINYINT(1) DEFAULT NULL,
                    `file` VARCHAR(255) DEFAULT NULL,
                    `date` INT(10) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_decks`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `tree_branch_id` VARCHAR(13) DEFAULT NULL,
                    `title` VARCHAR(255) DEFAULT NULL,
                    `description` LONGTEXT DEFAULT NULL,
                    `status` TINYINT(1) DEFAULT NULL,
                    `position` INT(20) DEFAULT NULL,
                    `date` INT(10) DEFAULT NULL,
                    `fasslearn_item` INT(10) DEFAULT 0,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_decks_words`
                (
                    `deck_id` BIGINT(20) DEFAULT NULL,
                    `word_id` BIGINT(20) DEFAULT NULL
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_decks_words_data_users`
                (
                    `deck_id` BIGINT(20) DEFAULT NULL,
                    `user_id` BIGINT(20) DEFAULT NULL,
                    `data` LONGTEXT DEFAULT NULL,
                    `passed` INT(1) DEFAULT '0'
                    `word-passed` INT(1) DEFAULT '0'
                    `fasslearn_passed` INT(1) DEFAULT '0'
                    `video_passed` INT(1) DEFAULT '0'
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");
        }

        public function Uninstall()
        {
            global $wpdb;

            $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_dictionary`");
            $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_decks`");
            $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_decks_words`");
            $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_decks_words_data_users`");

            $wpdb->query("DELETE FROM `{$wpdb->prefix}options` WHERE `option_name` LIKE '{$this->prefix}%'");
            $wpdb->query("DELETE FROM `{$wpdb->prefix}usermeta` WHERE `meta_key` LIKE '{$this->prefix}%'");
        }

        public function Template_Get($name, $path_dir = null, $data = null)
        {
            if (is_null($path_dir))
            {
                $path_dir = $this->Attr_Get('plugin_templates_path');
            }

            $path = $path_dir . $name . '.php';

            if (file_exists($path))
            {
                include($path);
            }
            else
            {
                return false;
            }
        }

        public function Page_Title_Build($title)
        {
            return $title . ' :: ' . __('valizAmots');
        }

        private function Plugin_Pages_Init()
        {
            if (is_admin())
            {
                add_action('admin_menu', function(){
                    add_menu_page($this->Page_Title_Build(__('Main')), __('valizAmots'), 'manage_options', $this->prefix, function(){
                        $this->Template_Get('pages/main');
                    }, '', 202);

                    add_submenu_page('wtfa', $this->Page_Title_Build(__('Decks')), __('Decks'), 'manage_options', $this->prefix . '_decks', function(){
                        $this->Template_Get('pages/decks');
                    });

                    add_submenu_page('wtfa', $this->Page_Title_Build(__('Deck editor')), __('Deck editor'), 'manage_options', $this->prefix . '_deck_editor', function(){
                        $data = new stdClass();

                        include_once($this->Attr_Get('plugin_core_path') . 'deck.php');
                        $deck = new WT_Fass_Anki_Deck($this);

                        if (isset($_REQUEST['id']) && preg_match("/^[0-9]+$/uis", $_REQUEST['id']))
                        {
                            if ((int)$_REQUEST['id'])
                            {
                                $data = $deck->Get($_REQUEST['id']);
                                $words = $deck->Dictionary_Get($data->id);

                                $deck_data = $this->Session_Get('deck');
                                $data->id = (int)$data->id;
                                $deck_data['dictionary'] = array();

                                if ($data->id)
                                {
                                    if (count($words))
                                    {
                                        foreach ($words as $v)
                                        {
                                            $deck_data['dictionary'][] = (int)$v->id;
                                        }
                                    }
                                }
                            }
                            else
                            if ((int)$_REQUEST['id'] == 0)
                            {
                                $deck_global = get_option("{$this->prefix}_deck_global");
                                if (!is_array($deck_global)) $deck_global = array();

                                if (is_array($deck_global) && count($deck_global))
                                {
                                    $data->id = 0;
                                    $data->title = $deck_global['title'];
                                    $data->description = $deck_global['description'];
                                }
                            }

                            $this->Session_Set('deck', $deck_data);
                        }
                        else
                        {
                            $this->Session_Unset();
                        }

                        $data->tree = $deck->Tree_Branches_Get(null, false);


                        include_once($this->Attr_Get('plugin_path') . '../' . $this->learn_plugin_fn . '/core.php');
                        include_once($this->Attr_Get('plugin_path') . '../' . $this->learn_plugin_fn. '/core/lesson.php');
                        $core_fl = new WT_Fass_Learn();
                        $wtfs_lesson = new WT_Fass_Learn_Lesson($core_fl);

                        $data->lessons = $wtfs_lesson -> getAll();
                        $data->fasslearn_item = $deck->Get((int)$_REQUEST['id'])->fasslearn_item;


                        include_once($this->Attr_Get('plugin_path') . '../' . $this->learn_plugin_vi . '/core.php');
//                        include_once($this->Attr_Get('plugin_path') . '../' . $this->learn_plugin_fi . '/core/clip.php');
                        $core_fv = new WT_Fass_Video();
//                        $wtfs_lesson = new WT_Fass_Video_Clip($core_fv);
//
                        $data->videos     = $core_fv->getAll();
                        $data->video_item = $deck->Get((int)$_REQUEST['id'])->videos_item;

                        $this->Template_Get('pages/deck-editor', null, $data);
                    });

                    add_submenu_page('wtfa', $this->Page_Title_Build(__('Dictionary')), __('Dictionary'), 'manage_options', $this->prefix . '_dictionary', function(){
                        $this->Template_Get('pages/dictionary');
                    });

                    add_submenu_page('wtfa', $this->Page_Title_Build(__('Import')), __('Import'), 'manage_options', $this->prefix . '_import', function(){
                        $this->Template_Get('pages/import');
                    });
                });
            }
        }

        public function Shortcodes_Init()
        {
            add_shortcode($this->prefix, function(){
                global $wpdb;

                $post_id = (int)get_the_ID();

                $output = '';
                include_once($this->Attr_Get('plugin_core_path') . 'wtfa-view.php');
                return $output;
            });

            add_shortcode($this->prefix . '_deck', function($atts){
                $atts = shortcode_atts(array(
                    'id' => 0
                ), $atts);

                $atts['id'] = (int)$atts['id'];

                global $wpdb;

                $post_id = (int)get_the_ID();

                $output = '';
                include_once($this->Attr_Get('plugin_core_path') . 'deck-view.php');
                return $output;
            });
        }

        public function Actions_Init()
        {
            if (isset($_REQUEST['_handler']) && $_REQUEST['_handler'] == $this->prefix)
            {
                include_once($this->Attr_Get('plugin_core_path') . 'action.php');

                $action = new WT_Fass_Anki_Action($this);
                $action->name = $_REQUEST['_action'];
                $action->subaction = $_REQUEST['_subaction'];
                $action->Action_Load($_REQUEST[$_REQUEST['_action']]);
            }
        }

        public function Enqueue_Sources()
        {
            /*
            if ($this->Is_FassLearn_Single_View())
            {
                wp_enqueue_style('qtip', $this->Attr_Get('plugin_js_url') . 'qtip/jquery.qtip.min.css', array(), '1.0', 'all');
                wp_enqueue_script('qtip', $this->Attr_Get('plugin_js_url') . 'qtip/jquery.qtip.min.js', array('jquery'), '1.0');

                wp_enqueue_style($this->prefix, $this->Attr_Get('plugin_css_url') . 'main.css', array(), '1.0', 'all');
                wp_enqueue_script($this->prefix, $this->Attr_Get('plugin_js_url') . 'main.js', array('jquery'), '1.0');
            }
            */
        }

        public function Enqueue_Sources_Admin()
        {
            wp_enqueue_style($this->prefix, $this->Attr_Get('plugin_css_url') . 'admin-top.css', array(), '1.0', 'all');
        }

        public function Head_Init()
        {
            ?>
            <script type="text/javascript">
                if (typeof(WTFA) == 'object')
                {
                    window.WTFA.attr.prefix = '<?php echo $this->Attr_Get('prefix'); ?>';
                }
            </script>
            <?php
        }

        public function Admin_Init()
        {
            if (is_admin())
            {
                // Classes
                add_filter('admin_body_class', function($classes){
                    if ($_REQUEST['page'] == $this->Attr_Get('prefix') . '_dictionary' && isset($_REQUEST['ddm']))
                    {
                        $classes .= ' ' . $this->Attr_Get('prefix') . '-ddm';
                    }

                    return $classes;
                });
                ////
            }
        }

        public function Array_Treatment_Strings(&$array, $procedure)
        {
            $type = null;

            if (is_array($array))
            {
                $type = 'array';
            }
            else
            if (is_object($array))
            {
                $type = 'object';
            }

            if (!is_null($type) && count($array))
            {
                foreach ($array as $k => $v)
                {
                    if (is_string($v) && !preg_match("/^a:[0-9]+:\{/uis", $v))
                    {
                        switch (strtolower($procedure))
                        {
                            case 'addslashes':
                                $v = addslashes($v);
                            break;
                            case 'stripslashes':
                                $v = stripslashes($v);
                            break;
                            case 'htmlspecialchars':
                                $v = htmlspecialchars($v);
                            break;
                            case 'htmlspecialchars_decode':
                                $v = htmlspecialchars_decode($v);
                            break;
                        }
                    }
                    else
                    {
                        $v = $this->Array_Treatment_Strings($v, $procedure);
                    }

                    switch ($type)
                    {
                        case 'array':
                            $array[$k] = $v;
                        break;
                        case 'object':
                            $array->{$k} = $v;
                        break;
                    }
                }
            }

            return $array;
        }
    }
