<?php

    ob_start();

    $this->Template_Get('areas/deck-head');

    include_once($this->Attr_Get('plugin_core_path') . 'deck.php');
    $deck = new WT_Fass_Anki_Deck($this);

    if ($atts['id'])
    {
        $deck_data = $deck->Get($atts['id']);
    }
    else
    {
        $deck_global = get_option("{$this->prefix}_deck_global");
        if (!is_array($deck_global)) $deck_global = array();

        $deck_data = new stdClass();
        $deck_data->id = 'g';
    }

    if
    (
        is_object($deck_data) &&
        ((int)$deck_data->id || mb_strtolower($deck_data->id) == 'g') &&
        get_current_user_id()
    )
    {
        ?>
        <div class="deck-wrapper" data-id="">
            <?php $this->Template_Get('areas/deck-view'); ?>
            <div class="preloader"></div>
        </div>
        <?php
    }
    else
    {
        ?>
        <div class="deck-wrapper error">
            <div class="message">
                <p>
                <?php

                    if (!get_current_user_id())
                    {
                        ?>
                        <?php _e('Pour participer à la leçon, vous devez vous connecter à votre compte'); ?>.
                        <?php
                    }
                    else
                    {
                        ?>
                        <?php _e('Ne existe pas cette leçon') ?>.
                        <br />
                        <?php _e('Vérifiez la shortcode exposée') ?>.
                        <?php
                    }

                ?>
                </p>
            </div>
        </div>
        <?php
    }

?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFA.word_get(jQuery('body.' + window.WTFA.attr.prefix + '-deck .deck-wrapper').eq(0).data('id'), 0, 0, true);
    });
</script>
<?php

    $output = ob_get_contents();
    ob_end_clean();
