<?php

    class WT_Fass_Anki_Deck
    {
        public $core;
        public $branches;
        private $prefix;
        private $tree_branch_fid;
        private $previusDeckPassedPercent;

        public function __construct($core)
        {
            $branchTree = get_option('wtfa_decks_tree');
            foreach ($branchTree as $key => $value) {
                $this->branches[$value['id']] = $value;
            }
            $this->core = $core;
            $this->prefix = $this->core->Attr_Get('prefix');
            $this->tree_branch_fid = $this->core->Attr_Get('tree_branch_fid');
        }

        public function Get($id)
        {
            global $wpdb;

            $id = (int)$id;

            $deck = $wpdb->get_results("
                SELECT
                    d.*
                FROM
                    `{$wpdb->prefix}{$this->prefix}_decks` d
                WHERE
                    d.`id` = '{$id}'
            ");

            $deck = $deck[0];

            $this->core->Array_Treatment_Strings($deck, 'stripslashes');
            $this->core->Array_Treatment_Strings($deck, 'htmlspecialchars');

            return $deck;
        }

        public function Dictionary_Get($id)
        {
            global $wpdb;

            $id = (int)$id;

            if ($id)
            {
                $words = $wpdb->get_results("
                    SELECT
                        d.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words` dw
                    LEFT JOIN
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                        ON 
                        d.`id` = dw.`word_id`
                    WHERE
                        dw.`deck_id` = '{$id}'
                        AND
                        d.`status` = 1
                ");
            }
            else
            {
                $words = $wpdb->get_results("
                    SELECT
                        d.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                    WHERE
                        d.`status` = 1
                ");
            }
//echo  "<pre>";var_export($words);die;
            return $words;
        }

        public function Pretty_Dictionary_Get($deck_dictionary,$deck_id)
        {
            if (count($deck_dictionary) == 0) {
                return array();
            }
            $dwdu = $this->Words_Data_User_Get($deck_id);
            foreach ($deck_dictionary as $word) {
                $word->id = (int)$word->id;
                if ($word->file != '')
                {
                    $word->file = $this->core->Attr_Get('plugin_file_uploads_url') . $word->file;
                }
                else
                {
                    $word->file = null;
                }
                $word->lvl =$dwdu[$word->id]['lvl'];
            }

            return $deck_dictionary;
        }

        public function Dictionary_Get_Words($deck_id)
        {
            $words = array();

            $deck_dictionary = $this->Dictionary_Get($deck_id);
            $dwdu = $this->Words_Data_User_Get($deck_id);

            if (count($deck_dictionary))
            {
                foreach ($deck_dictionary as $w)
                {
                    $w->id = (int)$w->id;
                    $w->course_id = (int)$w->course_id;
                    if
                    (
                        (
                        true
                            //$w->course_id //&&
                            //in_array($w->course_id, $user_courses_enrolled)
                        )
                        &&
                        (
                            !isset($dwdu[$w->id]) ||
//                            (isset($dwdu[$w->id]) && $this->Word_Data_Time_Expired($dwdu[$w->id]['lvl'], $dwdu[$w->id]['time']))
                            (isset($dwdu[$w->id]) && $dwdu[$w->id]['lvl'] == 4)
                        )
                    )
                    {
                        if ($w->file != '')
                        {
                            $w->file = $this->core->Attr_Get('plugin_file_uploads_url') . $w->file;
                        }
                        else
                        {
                            $w->file = null;

                            // TMP for tests
                            //$w->file = (rand(1, 2) == 1) ? 'http://www.noiseaddicts.com/samples_1w72b820/1453.mp3' : 'https://pp.vk.me/c837239/v837239381/23959/8t3baP2tZTE.jpg';
                            ////
                        }

                        $w->lvl =$dwdu[$w->id]['lvl'];
                        $words[] = (array)$w;
                    }
                }
            }
            return $words;
        }

        public function Dictionary_Words_Count($deck_id)
        {
            global $wpdb;

            $deck_id = (int)$deck_id;

            if ($deck_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        COUNT(0) AS `count`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words` dw
                    LEFT JOIN
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                        ON 
                        d.`id` = dw.`word_id`
                    WHERE
                        dw.`deck_id` = '{$deck_id}'
                        AND
                        d.`status` = 1
                ");

                if (count($result))
                {
                    return (int)$result[0]->count;
                }
            }

            return 0;
        }

        public function Dictionary_Words($deck_id, $user_id)
        {
            global $wpdb;
            $_wtfa = new WT_Fass_Anki();
            $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);

            $deck_id = (int)$deck_id;

            if ($deck_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        id, text_source, text_translation, status 
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words` dw
                    LEFT JOIN
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                        ON 
                        d.`id` = dw.`word_id`
                    WHERE
                        dw.`deck_id` = '{$deck_id}'

                ");

                if (count($result))
                {
                    $words = [];
                    $wordsData = $wtfa_deck->getWordLwl($user_id, $deck_id);
                    foreach ($result as $word) {
                        if (count($word)) {

                            $words[] = [
                                'id'               => (int)$word->id,
                                'deckId'               => (int)$deck_id,
                                'text_source'      => $word->text_source,
                                'text_translation' => $word->text_translation,
                                'status'           => (int)$word->status,
                                'lvl'              => $wordsData[$word->id]['lvl'],
                            ];
                        }
                    }
                    return $words;
                }
            }

            return 0;
        }

        public function Word_Data_Time_Expired($lvl, $word_time)
        {
            $lvl = (int)$lvl;
            $word_time = (int)$word_time;

            switch ($lvl)
            {
                case 1:
                    $lvl_offset = 180; // minutes
                    break;
                case 2:
                    $lvl_offset = 60; // minutes
                    break;
                case 3:
                    $lvl_offset = 15; // minutes
                    break;
                case 4:
                    $lvl_offset = 0; // new - hard -> repeat
                    break;
                default:
                    $lvl_offset = 60;
            }

            $lvl_offset *= 60;

            if ((int)($word_time + $lvl_offset) <= (int)time())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function Words_Data_User_Get($deck_id)
        {
            global $wpdb;

            $deck_id = (int)$deck_id;
            $user_id = get_current_user_id();

            if ($user_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        dwdu.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                    WHERE
                        dwdu.`deck_id` = '{$deck_id}'
                        AND 
                        dwdu.`user_id` = '{$user_id}'
                ");

                if (count($result))
                {
                    return (array)@unserialize($result[0]->data);
                }
            }
        }

        public function Words_Data_User_Update($user_id, $deck_id, $word_id, $lvl)
        {
            global $wpdb;

            $user_id = (int)$user_id;
            $deck_id = (int)$deck_id;
            $word_id = (int)$word_id;
            $lvl = (int)$lvl;

            if ($user_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        dwdu.*,
                        d.`tree_branch_id`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                    LEFT JOIN
                        `{$wpdb->prefix}{$this->prefix}_decks` d
                        ON 
                        d.`id` = dwdu.`deck_id`
                    WHERE
                        dwdu.`deck_id` = '{$deck_id}'
                        AND 
                        dwdu.`user_id` = '{$user_id}'
                ");

                $exist = false;
                $data = array();
                $word_passed = 0;

                if (count($result))
                {
                    $exist = true;
                    $result = $result[0];
                    $data = (array)@unserialize($result->data);
                    $word_passed = (int)$result->word_passed;
                }

                $data[$word_id] = array(
                    'lvl'  => $lvl,
                    'time' => (int)time()
                );

                // Deck passed init
                if ($word_passed < 1 && count($data))
                {
                    $passed_r = array(
                        'count'   => $this->Dictionary_Words_Count($deck_id),
                        'word_passed'  => 0,
                        'checked' => 0
                    );

                    if ($passed_r['count'])
                    {
                        foreach ($data as $k => $v)
                        {
                            if ((is_array($v) && isset($v['lvl']) && (int)$v['lvl'] === 1 )||   // old version
                                (is_array($v) && isset($v['lvl']) && (int)$v['lvl'] === 5 ))    // new version
                            {
                                $passed_r['word_passed']++;
                            }

                            if ( (is_array($v) && isset($v['lvl']) && in_array((int)$v['lvl'], [1, 2, 3])) ||  // old version
                                 (is_array($v) && isset($v['lvl']) && in_array((int)$v['lvl'], [4, 5])) )      // new version
                            {
                                $passed_r['checked']++;
                            }
                        }

                        /*
                        $passed_r = (float)number_format((($passed_r['passed'] / $passed_r['count']) * 100), 1, '.', '');

                        if ($passed_r >= 90)
                        {
                            $passed = 1;
                        }
                        */

                        if ($passed_r['word_passed'] >= $passed_r['count'])
                        {
                            $word_passed = 1;
                        }
                    }
                }


                $data = (string)serialize((array)$data);
                if ($exist)
                {
                    $wpdb->get_results("
                        UPDATE
                            `{$wpdb->prefix}{$this->prefix}_decks_words_data_users`
                        SET
                            `data` = '{$data}',
                            `word_passed` = '{$word_passed}'
                        WHERE
                            `deck_id` = '{$deck_id}'
                            AND 
                            `user_id` = '{$user_id}'
                    ");
                }
                else
                {
                    $wpdb->get_results("
                        INSERT INTO
                            `{$wpdb->prefix}{$this->prefix}_decks_words_data_users`
                            (
                                `deck_id`,
                                `user_id`,
                                `data`,
                                `word_passed`
                            )
                            VALUES
                            (
                                '{$deck_id}',
                                '{$user_id}',
                                '{$data}',
                                '{$word_passed}'
                            )
                    ");
                }

                //$this->Tree_Branch_User_Status_Trigger($user_id, $result->tree_branch_id);
            }
        }

        /**
         * Passed of all deck.
         *
         * @param integer $user_id Number of user
         * @param integer $deck_id Number of deck
         */
        public function Passed_User_Status_Get($user_id, $deck_id)
        {
            global $wpdb;

            $user_id = (int)$user_id;
            $deck_id = (int)$deck_id;

            if ($user_id && $deck_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        dwdu.`passed`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                    WHERE
                        dwdu.`deck_id` = '{$deck_id}'
                        AND 
                        dwdu.`user_id` = '{$user_id}'
                ");

                if (count($result))
                {
                    return (int)$result[0]->passed;
                }
            }

            return 0;
        }

        public function Passed_User_Words_Status_Get($user_id, $deck_id) // passed of words in  deck
        {
            global $wpdb;

            $user_id = (int)$user_id;
            $deck_id = (int)$deck_id;

            if ($user_id && $deck_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        dwdu.`word_passed`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                    WHERE
                        dwdu.`deck_id` = '{$deck_id}'
                        AND 
                        dwdu.`user_id` = '{$user_id}'
                ");

                if (count($result))
                {
                    return (int)$result[0]->word_passed;
                }
            }
            return 0;
        }

        public function getPassedListByDeck($user_id, $deck_id)
        {
            global $wpdb;

            $user_id = (int)$user_id;
            $deck_id = (int)$deck_id;

            if ($user_id && $deck_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        dwdu.passed, 
                        dwdu.word_passed, 
                        dwdu.fasslearn_passed, 
                        dwdu.video_passed
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                    WHERE
                        dwdu.`deck_id` = '{$deck_id}'
                        AND 
                        dwdu.`user_id` = '{$user_id}'
                ");

                if (count($result))
                {
                    $result_obj = array(
                        'passed'           => (int)$result[0]->passed,
                        'word_passed'      => (int)$result[0]->word_passed,
                        'fasslearn_passed' => (int)$result[0]->fasslearn_passed,
                        'video_passed'     => (int)$result[0]->video_passed
                    );
                    return $result_obj;
                }
            }
            return 0;
        }

        public function Passing_Check($user_id, $deck_id)
        {
            global $wpdb;
            $user_id = (int)$user_id;
            $deck_id = (int)$deck_id;
            // if we have not fasslearn and video and passed dictionary -> deck is passed.
            $passed_list = $this->getPassedListByDeck ($user_id, $deck_id);
            if ($deck_id) {
                $result = $wpdb->get_results("
                    SELECT
                        d.id as deck_id,
                        d.fasslearn_item,
                        d.videos_item
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks` d
                    WHERE
                        d.`id` = '{$deck_id}'
                ");

                $result = $result[0];

                $dictionary_passed = (bool)$passed_list['word_passed'];
                $fasslearn_passed  = (bool)$passed_list['fasslearn_passed'];
                $video_passed      = (bool)($passed_list['video_passed']);

                $is_fasslearn  = (bool)$result -> fasslearn_item;
                $is_video  = (bool)$result -> videos_item;

                $passed_upd = false;


                if (!$is_video ) {
                    $passed_upd = $fasslearn_passed;
                }
                if (!$is_fasslearn ){
                    $passed_upd = $dictionary_passed;
                }
                if (!$is_fasslearn && $is_video ){ // if not exist fasslearn, but exist videos
                    $passed_upd = $dictionary_passed && $video_passed;
                }
                if ($is_video && $is_fasslearn) {
                    $passed_upd = $fasslearn_passed && $video_passed;
                }

                if ($passed_upd) {
                    $result = $wpdb->get_results("
                        UPDATE
                          `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                        SET 
                        dwdu.passed = 1
                        WHERE
                          dwdu.`deck_id` = '{$deck_id}'
                        AND 
                          dwdu.`user_id` = '{$user_id}'
                    ");
                    return true;
                } else {
                    return false;
                }
                die($passed_upd);
            }

        }

        public function getWordLwl($user_id, $deck_id)
        {
            global $wpdb;

            $user_id = (int)$user_id;
            $deck_id = (int)$deck_id;

            if ($user_id && $deck_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        *
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                    WHERE
                        dwdu.`deck_id` = '{$deck_id}'
                        AND 
                        dwdu.`user_id` = '{$user_id}'
                ");

                if (count($result))
                {
                    return unserialize($result[0]->data);
                }
            }

            return 0;
        }

        public function Percent_Passed_User_Status_Get($user_id, $deck_id)
        {
            global $wpdb;

            $user_id = (int)$user_id;
            $deck_id = (int)$deck_id;

            if ($user_id && $deck_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        *
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                    WHERE
                        dwdu.`deck_id` = '{$deck_id}'
                        AND 
                        dwdu.`user_id` = '{$user_id}'
                ");

                if (count($result))
                {
                    $decks_words_data_users = [];
                    foreach (unserialize($result[0]->data) as $k=>$v) {
                        if ($v['lvl'] == 5) {
                            $decks_words_data_users[] = $k;
                        }
                    }

                    $result_deck = $wpdb->get_results("
                        SELECT
                            dw.word_id
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_decks_words` dw
                        WHERE
                            dw.`deck_id` = '{$deck_id}'
                    ");

                    $deck_words = [];
                    foreach ($result_deck as $word) {
                        $deck_words[] = (int)$word->word_id;
                    }


                    /*
                    Get all world from this deck,
                    compare this words with word in data
                    todo - consider only lvl 5 (Learned)
                    get percent
                    */
//                    count($deck_words) / count(array_intersect ($deck_words, $decks_words_data_users));
                    $result = [
                        'deck_words'  => (isset($deck_words) ) ? count($deck_words) : 0,
                        'deck_passed' => count(array_intersect($deck_words, $decks_words_data_users)),
                    ];

                    return $result;
//                    return ($result['deck_passed'] / $result['deck_words']) * 100;

                }
            }

            return [
                'deck_words'  => 0,
                'deck_passed' => 0,
            ];
        }

        public function Open_User_Status_Get($user_id, $deck_id)
        {
            global $wpdb;

            $user_id = (int)$user_id;
            $deck_id = (int)$deck_id;

            if ($user_id && $deck_id) {
                if (!isset($this->temp_tree_branches_wso)) {
                    $this->temp_tree_branches_wso = $this->Tree_Branches_Get($user_id, false, true);
                }
                $tb = &$this->temp_tree_branches_wso;


                $flag = false;

                if (count($tb)) {
                    foreach ($tb as $branch_key_id => $branch_value) {
                        if (count($branch_value['decks'])) {
                            foreach ($branch_value['decks'] as $deck_key_id => $deck_value) {
                                if ($deck_id == $deck_value['id']) {
                                    if ($deck_value['passed']) {
                                        return 1;
                                    }
                                    if ($branch_key_id == 0 && $deck_key_id == 0) {
                                        return 1;
                                    }

                                    if ($deck_key_id > 0 && isset($branch_value['decks'][ $deck_key_id - 1 ]) && $branch_value['decks'][ $deck_key_id - 1 ]['passed']) {
                                        return 1;
                                    }

                                    if ($deck_key_id < 1 && $branch_key_id > 0) { // make open all deck before
                                        $d = &$tb[ $branch_key_id - 1 ]['decks'];
                                        if ($d[ count($d) - 1 ]['passed']) {
//                                            return 1;
                                        }
                                        unset($d);
                                    }

//                                    $flag = true;
                                } else {
//                                    if ($flag && $deck_value['passed']) {
//                                        return 1;
//                                    }
                                }
                            }
                        }
                    }
                }
            }

            return 0;
        }

        public function Get_FassLearn_by_Deck($deck_id)
        {
            global $wpdb;

            $result = $wpdb->get_results("
                        SELECT
                              fasslearn_item
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_decks` d
                        WHERE
                            d.`id` = '{$deck_id}'
                    ");


            /*add if we have access to fasslearn*/

            if (count($result)) {
                return (int)$result[0]->fasslearn_item;
            } else {
                return 0;
            }
        }

        public function getBranchIdByDeckId($deck_id)
        {
            global $wpdb;

            $result = $wpdb->get_results("
                        SELECT
                              d.tree_branch_id
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_decks` d
                        WHERE
                            d.`id` = '{$deck_id}'
                    ");


            /*add if we have access to fasslearn*/

            return $result;
        }

        public function Get_Videos_by_Deck($deck_id)
        {
            global $wpdb;

            $result = $wpdb->get_results("
                        SELECT
                              videos_item
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_decks` d
                        WHERE
                            d.`id` = '{$deck_id}'
                    ");


            /*add if we have access to fasslearn*/

            if (count($result)) {
                return (int)$result[0]->videos_item;
            } else {
                return 0;
            }
        }


        public function Tree_Branches_Get_Decks_Info(&$data, &$decks_tree_cascade, $user_id, $depth = 0/*, $open_set = false*/, $without_status_open = false, $api_token = null, $getWords = false)
        {
            global $wpdb;

            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->learn_plugin_fn . '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->learn_plugin_fn . '/core/lesson.php');
            $core_fl     = new WT_Fass_Learn();
            $wtfs_lesson = new WT_Fass_Learn_Lesson($core_fl);

            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->learn_plugin_vi . '/core.php');
            $core_vi = new WT_Fass_Video();

            $access_branches = unserialize(get_user_meta($user_id, 'wtfa_access_branches', true));

            $open_all_branch = get_option('wtfapi_open_branch'); //dev_mode /wp-admin/admin.php?page=wtfapi
            $open_all_deck = get_option('wtfapi_open_deck'); // dev_mode /wp-admin/admin.php?page=wtfapi

            if (! $access_branches) {
                $access_branches = $this->core->Set_Defaut_Branch($user_id);
            }

            if (count($data))
            {
                foreach ($data as $branch_key_id => $branch)
                {
                    $branch['depth'] = $depth;



                    $result = $wpdb->get_results("
                        SELECT
                            d.*,
                            l.title AS l_title,
                            l.description AS l_description,
                            l.rtl AS l_rtl
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_decks` d
                        LEFT JOIN `{$wpdb->prefix}{$core_fl->Attr_Get('prefix')}_lessons` l
                            ON d.fasslearn_item = l.id
                        WHERE
                            d.`tree_branch_id` = '{$branch['id']}'
                        ORDER BY
                            d.`position`
                            ASC
                    ");

                    $result_user_decks = $wpdb->get_results("
                        SELECT
                            *
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` d
                        WHERE
                            d.`user_id` = '{$user_id}'
                    ");

                    $all_decks = count($result);
                    $passed_deck = 0;
//                    $this->previusDeckPassedPercent = 0;
                    foreach ($result_user_decks as $row_ud) {
                        $row_ud->deck_id;
                        if ($row_ud->passed == 1){
                            foreach ($result as $row) {
                                if ($row->id == $row_ud->deck_id) {
                                    $passed_deck++;
                                }
                            }
                        }
                    }

                    $branch['decks-percent-passed'] = ($passed_deck / $all_decks ) * 100;
                    $branch['decks-count'] = count($result);
                    $data[$branch_key_id] = $branch;


                    /* todo uncomment to partner*/
                    if (!isset($_REQUEST['page']) ) {
//                        if (isset($_GET['dev'])) {
////                            echo "<pre>";var_dump($branch);
////                            echo "<pre>";var_dump($branch->hidden);
////                            echo "<pre>";var_dump($branch['hidden']);die;
//                            if ($branch['hidden']) {
//                                echo "<pre>";var_dump($branch);die;
//                            }
//                        }
                        if (!$open_all_branch) {  // todo broke in  https://fassalab.weeteam.net/wp-admin/admin.php?page=wtfa_deck_editor&id=86 (not show branch tree)
                            if(!in_array ($branch['id'],$access_branches )/* || $branch['hidden']*/){
                                 continue;


                            }
                        }
                    }


                    $branch['decks'] = array();

                    if ($branch['decks-count'])
                    {

                        foreach ($result as $dk => $dv)
                        {
                            $dv->passed = $this->Passed_User_Status_Get($user_id, $dv->id);

//                            $ppusg = $this->Percent_Passed_User_Status_Get($user_id, $dv->id);
//                            $dv->percent_passed =($ppusg['deck_passed'] / $ppusg['deck_words']) * 100;
//                            $dv->deck_passed = $ppusg['deck_passed'];

                            if (!$without_status_open)
                            {
                                $dv->open = $this->Open_User_Status_Get($user_id, $dv->id);
                                if (!count($branch['decks'])) {// if first deck in branch
                                    $dv->open = 1;
                                }
                            }
                            else
                            {
                                $dv->open = 0;
                            }

                            $passedList = $this->getPassedListByDeck($user_id, $dv->id);

//                            if ($this->previusDeckPassedPercent >= 90) { // todo change to "words passed" from table
//                                $dv->open = 1;
//                            }


                            //dictionary
                            $dictionary = $this->Dictionary_Status_Get($user_id, $dv->id);

                            // fasslearn_item
                            $fasslearn = null;
                            if ((int)$dv->fasslearn_item !== 0) {
                                $progress = $wtfs_lesson->Lesson_User_Progress_Get((int)$dv->fasslearn_item, $api_token);
                                $fasslearn = array(
                                    'id'          => (int)$dv->fasslearn_item,
                                    'open'        => $dictionary['passed'], // if passed dictionary -> open fasslearn
                                    'passed'      => isset($passedList['fasslearn_passed']) ? $passedList['fasslearn_passed'] : 0,
                                    'percent'     => $progress['%'],
                                    'count'       => $progress['steps-count'],
                                    'learned'     => $progress['steps-passed']
//                                    'title'       => $dv->l_title,// todo for save API Traffic
//                                    'description' => $dv->l_description,// todo for save API Traffic
//                                    'rtl'         => (int)$dv->l_rtl, // todo for save API Traffic
                                );
                            }

                            // videos_item
                            $videos = null;
                            if ((int)$dv->videos_item !== 0) {

                                $video_open = isset($passedList['fasslearn_passed']) ? $passedList['fasslearn_passed'] : //todo to test
                                              isset($dictionary['passed']) ? $dictionary['passed'] : 0;
                                $videos = array(
                                    'id'     => (int)$dv->videos_item,
                                    'title'  => $core_vi->Get((int)$dv->videos_item)->title,
                                    'open'   => $video_open,
                                    //  isset($passedList['fasslearn_passed']) ? $passedList['fasslearn_passed'] : 0,
                                    'passed' => isset($passedList['video_passed']) ? $passedList['video_passed'] : 0,
                                );
                                $clips  = $core_vi->Get_Clips_By_Videos((int)$dv->videos_item);
                                $passed_clips  = $core_vi->Get_Passed_By_User_Block($user_id, (int)$dv->videos_item);


                                foreach ($clips as $clip) {
                                    if (isset($passed_clips[$clip->id])) {
                                        $clip->passed = 1;
                                    } else {
                                        $clip->passed = 0;
                                    }
                                    $videos['clips'][] = $clip;
                                }
                                if (count($clips) == 0) {
                                    $videos['clips'] = null;
                                }
                            }


                            $deck_item = array(
                                'id'         => (int)$dv->id,
                                'title'      => $dv->title,
                                'special'      => (int)$dv->special,
                                //'description'         => $dv->description,// todo for save API Traffic
                                // todo for save API Traffic
                                //'logo'                => 'https://pp.userapi.com/c836222/v836222381/2ab28/RbZMSYAgIG8.jpg',
                                //'status'              => (int)$dv->status,// todo for save API Traffic
                                'passed'     => $dv->passed,
                                'open'       => $dv->open,
                                //   'words-count'         => $this->Dictionary_Words_Count($dv->id),
                                //   'words-passed'        => $dv->deck_passed,
                                //   'deck-percent-passed' => $dv->percent_passed,
                                //   'dictionary' => array(
                                //       'passed'   => isset($passedList['word_passed']) ? $passedList['word_passed'] : 0,
                                //       'count'    => $this->Dictionary_Words_Count($dv->id),
                                //       'learned'  => $dv->deck_passed,
                                //       'percent ' => $dv->percent_passed,
                                ////       'info'     => 'words-count + deck-percent-passed +  words-passed in root => todel',
                                //   ),
                                'dictionary' => $dictionary,
                                'fasslearn'  => $fasslearn,
                                'videos'     => $videos,

                            );

//                            $this->previusDeckPassedPercent = $dv->percent_passed; // todo to del ?

                            if ($open_all_deck) { // todo dev_mode Open all decks
                                $deck_item['open'] = 1;
                                if ($deck_item['fasslearn'] != null) {
                                    $deck_item['fasslearn']['open'] = 1;
                                }
                                if ($deck_item['videos'] != null) {
                                    $deck_item['videos']['open'] = 1;
                                }
                            }

                            $branch['decks'][] = $deck_item;

                        }
                    }
                    ////
                    $data[$branch_key_id] = $branch;

                    unset($branch['sub']);
                    $decks_tree_cascade[] = $branch;

                    if (isset($data[$branch_key_id]) && count($data[$branch_key_id]))
                    {
                        $this->Tree_Branches_Get_Decks_Info($data[$branch_key_id]['sub'], $decks_tree_cascade, $user_id, $depth + 1/*, $open_set*/, $without_status_open);
                    }

                }
            }
        }

        public function Tree_Branches_Get($user_id, $hierarchy = true, $without_status_open = false, $api_token = null)
        {
            $user_id = (int)$user_id;


            $decks_tree = $this->core->Get_Root_Decks_Tree($user_id );

            if (!is_array($decks_tree)) $decks_tree = array();

            $decks_tree_cascade = array();

            $this->Tree_Branches_Get_Decks_Info($decks_tree, $decks_tree_cascade, $user_id, 0, $without_status_open, $api_token);

            if ($hierarchy)
            {
                return $decks_tree;
            }
            else
            {
                return $decks_tree_cascade;
            }
        }


        public function getNews()
        {

            $args = array(
                'post_type' => 'news',
                'publish' => true,
            );
            $posts = get_posts( $args );
$result = [];
            foreach ($posts as $post) {
                $image = get_the_post_thumbnail_url( $post->ID, 'thumbnail' );
      $result[] = [
          'title' => $post->post_title,
                'content' => $post->post_content,
                'deck_id' => (int)get_metadata('post', (int)$post->ID, 'deck_id', 1),
          'url' =>
              get_metadata('post', (int)$post->ID, 'news_url', 1),
                'image' => $image ? $image : null
      ];
            }
            return $result;
        }

        public function Dictionary_Status_Get($user_id, $deck_id)
        {
            $ppusg = $this->Percent_Passed_User_Status_Get($user_id, $deck_id);

            $dictionary = array(
                'passed'  => $this->Passed_User_Words_Status_Get($user_id, $deck_id),
                'count'   => $this->Dictionary_Words_Count($deck_id),
                'learned' => $ppusg['deck_passed'],
                'percent' => ($ppusg['deck_passed'] / $ppusg['deck_words']) * 100,
            );

            return $dictionary;
        }

        public function getWordsList($user_id, $hierarchy = true, $without_status_open = false, $api_token = null)
        {
            $user_id = (int)$user_id;

            $decks_tree = $this->core->Get_Root_Decks_Tree();//get_option("{$this->prefix}_decks_tree");

            if (!is_array($decks_tree)) $decks_tree = array();

            $decks_tree_cascade = array();

            $_result = $this->core->Access_Branches_Rest($decks_tree, $user_id);
            $decks_tree = $_result['decks_tree'];
            $access_branches = $_result['access_branches'];

            //            $this->getDecksInfo($decks_tree, $decks_tree_cascade, $user_id, 0, $without_status_open, $api_token);
            $this->getDecksInfo($access_branches, $decks_tree_cascade, $user_id, 0, $without_status_open, $api_token);

            if ($hierarchy)
            {
                return $decks_tree;
            }
            else
            {
                return $decks_tree_cascade;
            }
        }

        public function Tree_Branches_Update($id, $callback)
        {
            if (!function_exists('Tree_Branches_Update_Walk'))
            {
                function Tree_Branches_Update_Walk(&$data, $id, $prefix, $callback)
                {
                    if (count($data))
                    {
                        foreach ($data as $k => $v)
                        {
                            if ($v['id'] == $id)
                            {
                                if (is_callable($callback))
                                {
                                    $callback($data[$k]);
                                }

                                return true;
                            }

                            if (isset($v['sub']) && count($v['sub']))
                            {
                                Tree_Branches_Update_Walk($data[$k]['sub'], $id, $prefix, $callback);
                            }
                        }
                    }
                }
            }

            $decks_tree = $this->core->Get_Root_Decks_Tree();// get_option("{$this->prefix}_decks_tree");
            if (!is_array($decks_tree)) $decks_tree = array();

            Tree_Branches_Update_Walk($decks_tree, $id, $this->prefix, $callback);

            $this->core->Set_Root_Decks_Tree($decks_tree);
            //update_option("{$this->prefix}_decks_tree", $decks_tree);
        }

        public function Tree_Branches_Add($id, $callback)
        {
            if (!function_exists('Tree_Branches_Add_Walk'))
            {
                function Tree_Branches_Add_Walk(&$data, $id, $callback)
                {

                    if (count($data))
                    {
                        foreach ($data as $k => $v)
                        {
                            if ($v['id'] == $id)
                            {
                                if (is_callable($callback))
                                {
                                    $callback($data[$k]['sub']);
                                }

                                return true;
                            }

                            if (isset($v['sub']) && count($v['sub']))
                            {
                                Tree_Branches_Add_Walk($data[$k]['sub'], $id, $callback);
                            }
                        }
                    }
                }
            }

            $decks_tree = $this->core->Get_Root_Decks_Tree();// get_option("{$this->prefix}_decks_tree");
            if (!is_array($decks_tree)) $decks_tree = array();

            if ($id == 0) { // if add in root
                $callback($decks_tree);
            } else {
                Tree_Branches_Add_Walk($decks_tree, $id, $callback);
            }
            //            die('Tree_Branches_Add');
            $this->core->Set_Root_Decks_Tree($decks_tree);
            //update_option("{$this->prefix}_decks_tree", $decks_tree);
        }

        public function Tree_Branches_Remove($id, $callback)
        {

            if (!function_exists('Tree_Branches_Remove_Walk'))
            {
                function Tree_Branches_Remove_Walk(&$data, $id, $callback, $table_prefix)
                {
                    if (count($data))
                    {
                        foreach ($data as $k => $v)
                        {
                            if ($v['id'] == $id)
                            {
                                $list_to_del = Tree_Branches_Remove_Walk_Get_List($v);
                                Tree_Branches_Remove_Walk_Remove_List($list_to_del,$table_prefix);
                                unset($data[$k]);
                                return true;
                            }

                            if (isset($v['sub']) && count($v['sub']))
                            {
                                Tree_Branches_Remove_Walk($data[$k]['sub'], $id, $callback, $table_prefix);
                            }
                        }
                    }
                }
            }
            if (!function_exists('Tree_Branches_Remove_Walk_Get_List'))
            {
                function Tree_Branches_Remove_Walk_Get_List($tail)
                {
                    $result = array();
                    if (isset($tail['id'] ) ) {
                        $result[] = $tail['id'];
                    }
                    if (isset($tail['sub'] ) ) {
                        foreach ($tail['sub'] as $item) {
                            $result = array_merge( $result , Tree_Branches_Remove_Walk_Get_List($item));
                        }
                    }
                    return $result;
                }
            }

            if (!function_exists('Tree_Branches_Remove_Walk_Remove_List'))
            {
                function Tree_Branches_Remove_Walk_Remove_List($list, $table_prefix)
                {
                    foreach ($list as &$item ) {
                        $item = "'".$item."'";
                    }

                    $list_string = implode(', ', $list);

                    global $wpdb;
                    $wpdb->query("
                        DELETE d, dw
                            FROM {$wpdb->prefix}{$table_prefix}_decks d
                            LEFT JOIN {$wpdb->prefix}{$table_prefix}_decks_words dw 
                            ON dw.deck_id = d.id
                            WHERE
                            d.tree_branch_id in ({$list_string});
                        ");
                }
            }


            $decks_tree = $this->core->Get_Root_Decks_Tree(); // get_option("{$this->prefix}_decks_tree");
            if (!is_array($decks_tree)) $decks_tree = array();
            if ($id == 0) { // if add in root
                $decks_tree = array();
            } else {
                Tree_Branches_Remove_Walk($decks_tree, $id, $callback, $this->prefix);
            }
            $this->core->Set_Root_Decks_Tree($decks_tree); //update_option("{$this->prefix}_decks_tree", $decks_tree);
        }

        public function Tree_Branch_Get_By($key, $value)
        {
            if (!function_exists('Tree_Branch_Search'))
            {
                function Tree_Branch_Search($data, $key, $value, &$branch)
                {
                    if (count($data))
                    {
                        foreach ($data as $k => $v)
                        {
                            if (isset($v[$key]) && $v[$key] == $value)
                            {
                                $branch = $v;
                                return true;
                            }

                            if (isset($v['sub']) && count($v['sub']))
                            {
                                Tree_Branch_Search($v['sub'], $key, $value, $branch);
                            }
                        }
                    }
                }
            }

            $decks_tree = $this->core->Get_Root_Decks_Tree();//get_option("{$this->prefix}_decks_tree");
            if (!is_array($decks_tree)) $decks_tree = array();

            $branch = array();

            Tree_Branch_Search($decks_tree, $key, $value, $branch);

            return $branch;
        }

        /*
        public function Tree_Branch_User_Status_Set($user_id, $branch_id, $status)
        {
            $user_id = (int)$user_id;
            $branch_id = (int)$branch_id;
            $status = (int)$status;

            if ($status < 0) $status = 0; elseif ($status > 1) $status = 1;

            if ($user_id && $branch_id)
            {
                $mk = "{$this->prefix}_decks_tree_status";

                $data = get_user_meta($user_id, $mk, true);
                if (!is_array($data)) $data = array();

                $data[$branch_id] = $status;
                update_user_meta($user_id, $mk, $data);

                return true;
            }
        }
        */

        /*
        public function Tree_Branch_User_Status_Get($user_id, $branch_id)
        {
            $user_id = (int)$user_id;

            if ($user_id && preg_match("/^[a-z0-9]{13}$/uis", $branch_id))
            {
                $data = get_user_meta($user_id, "{$this->prefix}_decks_tree_status", true);
                if (!is_array($data)) $data = array();

                // if this is the first branch - is open it
                if ($branch_id == $this->tree_branch_fid)
                {
                    return 1;
                }
                ////

                if (isset($data[$branch_id]))
                {
                    return (int)$data[$branch_id];
                }
            }

            return 0;
        }
        */

        /*
        public function Tree_Branch_User_Status_Trigger($user_id, $tree_branch_id)
        {
            global $wpdb;

            if ($user_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        d.`id`,
                        dwdu.`passed`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks` d
                    LEFT JOIN
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                        ON
                            dwdu.`deck_id` = d.`id`
                            AND
                            dwdu.`user_id` = '{$user_id}'
                    WHERE
                        d.`tree_branch_id` = '{$tree_branch_id}'
                        AND
                        d.`status` = 1
                ");

                if (count($result))
                {
                    foreach ($result as $v)
                    {
                        if ($v->passed < 1)
                        {
                            return false;
                        }
                    }

                    $this->Tree_Branch_User_Status_Set($user_id, $tree_branch_id, 1);
                }
            }
        }
        */

        function User_Statistic_Get($user_id, $deck_id = 0, $this_deck = false)
        {
            global $wpdb;

            $user_id = (int)$user_id;
            $deck_id = (int)$deck_id;
            $access_branches = unserialize(get_user_meta($user_id, 'wtfa_access_branches', true));

            $res = array();

            if ($deck_id)
            {
//                $res['deck-words-lvl-1-count'] = 0;
//                $res['deck-words-lvl-2-count'] = 0;
//                $res['deck-words-lvl-3-count'] = 0;
//
                // 2018.12.27
                $res['decks-words-lvl-4-count'] = 0;    // to repeat
                $res['decks-words-lvl-5-count'] = 0;    // learned
                $res['deck-words-lvl-4-count'] = 0;
                $res['deck-words-lvl-5-count'] = 0;
            }
            else
            {
                //$res['tree-branches-count'] = 0;
                //$res['tree-branches-opened-count'] = 0;

                $res['decks-count'] = 0;
                $res['decks-passed-count'] = 0;
                $res['decks-open-count'] = 0;
                $res['decks-words-count'] = 0;
//                $res['decks-words-lvl-1-count'] = 0;
//                $res['decks-words-lvl-2-count'] = 0;
//                $res['decks-words-lvl-3-count'] = 0;

                // 2018.12.27
                $res['decks-words-lvl-4-count'] = 0;    // to repeat
                $res['decks-words-lvl-5-count'] = 0;    // learned

                $res['decks-words-available-count'] = 0;
            }

            if ($user_id)
            {
                /*
                if ($deck_id < 1)
                {
                    //
                    //$res['tree-branches-count'] = count($this->Tree_Branches_Get($user_id, false));
                    ////

                    //
                    $data = get_user_meta($user_id, "{$this->prefix}_decks_tree_status", true);
                    if (!is_array($data)) $data = array();

                    if (count($data))
                    {
                        foreach ($data as $v)
                        {
                            if ((int)$v > 0)
                            {
                                $res['tree-branches-opened-count']++;
                            }
                        }
                    }
                    ////
                }
                */

                $where = '';

                if ($deck_id > 0)
                {
                    $where .= ' AND ';
                    $where .= "d.`id` = '{$deck_id}'";
                }

                //
                $result = $wpdb->get_results("
                    SELECT
                        d.`id`,
                        d.`special`,
                        d.`tree_branch_id`,
                        COUNT(dw.`deck_id`) AS `words_count`,
                        dwdu.`data`,
                        dwdu.`passed`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks` d
                    LEFT JOIN
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu
                        ON 
                            dwdu.`deck_id` = d.`id`
                            AND 
                            dwdu.`user_id` = '{$user_id}'
                    LEFT JOIN
                        `{$wpdb->prefix}{$this->prefix}_decks_words` dw
                        ON
                        dw.`deck_id` = d.`id`       
                    WHERE 
                        d.`status` = 1
                        {$where}
                    GROUP BY
                        d.`id`
                ");

                if (count($result))
                {
                    foreach ($result as $v)
                    {
                        $isHiddenCurrentBranch = isset($this->branches[$v->tree_branch_id]) && $this->branches[$v->tree_branch_id]['hidden'] ? true : false;
                        $isHiddenCurrentBranch = false;
//                        if (isset($_GET['dev'])){
//                            var_export($this->branches[$v->tree_branch_id]);die;
//                        }
                        if (
                            !$v->special
                            && in_array($v->tree_branch_id, $access_branches)
                            && !$isHiddenCurrentBranch
                        ) {
                            if ($deck_id < 1)
                            {
                                $res['decks-count']++;

                                if ((int)$v->passed > 0)
                                {
                                    $res['decks-passed-count']++;
                                }

                                if ($this->Open_User_Status_Get($user_id, $v->id))
                                {
                                    $res['decks-open-count']++;
                                    $res['decks-words-available-count'] += (int)$v->words_count;
                                }

                                $res['decks-words-count'] += (int)$v->words_count;
                            }

                            $data = unserialize($v->data);
                            if (!is_array($data)) $data = array();

                            if (count($data))
                            {
                                foreach ($data as $k=>$wv)
                                {
                                    if (isset($wv['lvl']))
                                    {
                                        if ($wv['lvl'] == 4 || $wv['lvl'] == 5){
                                            if ($deck_id)
                                            {
                                                $res["deck-words-lvl-{$wv['lvl']}-count"]++;
                                            }
//                                        else
//                                        {
                                            $res["decks-words-lvl-{$wv['lvl']}-count"]++;
//                                        }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                ////
            }

            return $res;
        }

        function User_Statistic_Get_API($user_id){
            
            return array(
                'test' => 'dev'
            );
        }

        public function getDecksInfo(&$data, &$decks_tree_cascade, $user_id, $depth = 0/*, $open_set = false*/, $without_status_open = false, $api_token = null, $getWords = false)
        {
            global $wpdb;

            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->learn_plugin_fn . '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->learn_plugin_fn . '/core/lesson.php');
            $core_fl     = new WT_Fass_Learn();
            $wtfs_lesson = new WT_Fass_Learn_Lesson($core_fl);

            if (count($data))
            {
                foreach ($data as $k => $v)
                {
//                    $v['depth'] = $depth;

                    //
                    $result = $wpdb->get_results("
                        SELECT
                            d.*,
                            l.title AS l_title,
                            l.description AS l_description,
                            l.rtl AS l_rtl
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_decks` d
                        LEFT JOIN `{$wpdb->prefix}{$core_fl->Attr_Get('prefix')}_lessons` l
                            ON d.fasslearn_item = l.id
                        WHERE
                            d.`tree_branch_id` = '{$v['id']}'
                        ORDER BY
                            d.`position`
                            ASC
                    ");

                    //$v['open'] = $this->Tree_Branch_User_Status_Get($user_id, $v['id']);

                    /*
                    if
                    (
                        !$open_set
                        && $this->Tree_Branch_User_Status_Get($user_id, $v['id'])
                    )
                    {
                        if ($v['id'] == $this->tree_branch_fid)
                        {
                            $v['open'] = 1;
                        }
                        else
                        {
                            $open_set = true;
                        }
                    }
                    else
                    if ($open_set)
                    {
                        $v['open'] = 1;
                        $open_set = false;
                    }
                    else
                    {
                        $v['open'] = 0;
                    }
                    */

                  /*  $result_user_decks = $wpdb->get_results("
                        SELECT
                            *
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` d
                        WHERE
                            d.`user_id` = '{$user_id}'
                    ");

                    $all_decks = count($result);
                    $passed_deck = 0;

                    foreach ($result_user_decks as $row_ud) {
                        $row_ud->deck_id;
                        if ($row_ud->passed == 1){
                            foreach ($result as $row) {
                                if ($row->id == $row_ud->deck_id) {
                                    $passed_deck++;
                                }
                            }
                        }
                    }*/

//                    $v['decks-percent-passed'] = ($passed_deck / $all_decks ) * 100;
//                    $v['decks-count'] = count($result);
                    $v['decks'] = array();

                    if (count($result))
                    {
                        foreach ($result as $dk => $dv)
                        {
                            $dv->passed = $this->Passed_User_Status_Get($user_id, $dv->id);

                            $ppusg = $this->Percent_Passed_User_Status_Get($user_id, $dv->id);
                            $dv->percent_passed =($ppusg['deck_passed'] / $ppusg['deck_words']) * 100;
                            $dv->deck_passed = $ppusg['deck_passed'];

                            if (!$without_status_open)
                            {
                                $dv->open = $this->Open_User_Status_Get($user_id, $dv->id);
                            }
                            else
                            {
                                $dv->open = 0;
                            }

//                            $dv->l_status = 0;
//                            if ($this->Passed_User_Status_Get($user_id, $data['deck-id'])){
//                                if ($dv->percent_passed > 90){
//                                    $dv->l_status = 1;
//                                }
//                            }
//                            $l_percent_passed = $wtfs_lesson-> Lesson_User_Progress_Get($dv->fasslearn_item, $api_token);
                            if (!count($v['decks'])) {
                                $dv->open = 1;
                            }
                            $deck_item = array(
                                'id'                  => (int)$dv->id,
                                'title'               => $dv->title,
//                                'description'         => $dv->description,
//                                'logo'                => 'https://pp.userapi.com/c836222/v836222381/2ab28/RbZMSYAgIG8.jpg',
//                                'status'              => (int)$dv->status,
                                'passed'              => $dv->passed,
                                'open'                => $dv->open,
//                                'words-count'         => $this->Dictionary_Words_Count($dv->id),
                                'words'               => $this->Dictionary_Words($dv->id, $user_id),
//                                'words-passed'        => $dv->deck_passed,
//                                'deck-percent-passed' => $dv->percent_passed,
//                                'fasslearn'           => array(
//                                    'l_id'     => (int)$dv->fasslearn_item,
//                                    //'l_open'   => $dv->l_open, // todo ?
//                                    'l_status' => $dv->l_status,
//                                )
                            );

//                            if ((int)$dv->fasslearn_item !== 0) {
//                                $deck_item['fasslearn']['l_percent-passed'] = $l_percent_passed; // to do ????$l_percent_passed;
//                                $deck_item['fasslearn']['l_title']          = $dv->l_title;
//                                $deck_item['fasslearn']['l_description']    = $dv->l_description;
//                                $deck_item['fasslearn']['l_rtl']            = (int)$dv->l_rtl;
//                            }

                            $v['decks'][] = $deck_item;

                        }
                    }
                    ////

                    $data[$k] = $v;

                    unset($v['sub']);
                    $decks_tree_cascade[] = $v;

                    if (isset($data[$k]) && count($data[$k]))
                    {
                        $this->getDecksInfo($data[$k]['sub'], $decks_tree_cascade, $user_id, $depth + 1/*, $open_set*/, $without_status_open);
                    }
                }
            }
        }

        public function resetDeckWords ($user_id, $deck_id, $force = false )
        {
            global $wpdb;

            if ($force) {
                return 'todo force reset';
            } else{
                $result = $wpdb->get_results("
                    SELECT
                      *
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_decks_words_data_users` dwdu       
                    WHERE 
                        dwdu.`deck_id` = $deck_id
                        AND
                        dwdu.`user_id` = $user_id
                ");
                if (count($result)) {

                    foreach ($result as $v){
                        $data = $v->data;
                        $data = (array)@unserialize($data);

                        foreach ($data as &$row) {
                            $row['lvl'] = 4;
                        }
                        $data = serialize($data);
                        $result = $wpdb->get_results("
                        UPDATE 
                          `{$wpdb->prefix}{$this->prefix}_decks_words_data_users`
                          SET 
                          data = '$data'
                        WHERE  
                          `deck_id` = $deck_id
                          AND
                          `user_id` = $user_id
                        ");
//                        echo '<pre>';
//                        var_dump($wpdb -> last_result);
//                        var_dump($wpdb -> last_query);
//                        var_dump($wpdb -> last_error);
//                        echo '</pre>';
//                        die('sdfg');
                        return $wpdb -> last_result;
                    }
                } else {
                    return false;
                }

            }
        }
    }
