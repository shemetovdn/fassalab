<?php

    ob_start();

    $this->Template_Get('areas/deck-head');

    include_once($this->Attr_Get('plugin_core_path') . 'deck.php');
    $deck = new WT_Fass_Anki_Deck($this);

    if (get_current_user_id())
    {
        $tree = $deck->Tree_Branches_Get(null, false);

        ?>
        <div class="deck-wrapper" data-id="">
            <div class="decks-tree"></div>
            <div class="deck-view">
                <?php $this->Template_Get('areas/deck-view'); ?>
            </div>
            <div class="preloader"></div>
        </div>
        <?php
    }
    else
    {
        ?>
        <div class="deck-wrapper error">
            <div class="message">
                <p>
                    <?php

                        if (!get_current_user_id())
                        {
                            ?>
                            <?php _e('Vous devez vous connecter à votre compte'); ?>.
                            <?php
                        }

                    ?>
                </p>
            </div>
        </div>
        <?php
    }

    ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            window.WTFA.decks_tree_get();
        });
    </script>
<?php

    $output = ob_get_contents();
    ob_end_clean();
