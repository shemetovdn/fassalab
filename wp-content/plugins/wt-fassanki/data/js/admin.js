jQuery(document).ready(function(){

});

var WTFA_Admin = {
    attr: {
        p: null,
        text: {},
        t: null,
        tsd: false
    },

    preloader: function(show){
        var id = window.WTFA_Admin.attr.prefix + '-preloader';
        var speed = 200;
        var preloader = jQuery('#' + id);

        if (show)
        {
            if (preloader.length)
            {
                preloader.data({c: parseInt(preloader.data('c')) + 1});
                preloader.stop().fadeIn(speed).unbind();
            }
            else
            {
                preloader = jQuery('<div>').attr({id: id}).hide();

                jQuery('body').append(preloader);

                preloader = jQuery('#' + id);

                preloader.data({c: 1});
                preloader.fadeIn(speed).unbind();
            }

            setTimeout(function(){
                preloader.click(function(){
                    jQuery(this).data({c: 1});
                    window.WTFA_Admin.preloader(false);
                });
            }, 5000);
        }
        else
        {
            preloader.data({c: parseInt(preloader.data('c')) - 1});

            if (parseInt(preloader.data('c')) < 1)
            {
                preloader.fadeOut(speed, function(){
                    jQuery(this).remove();
                });
            }
        }
    },

    init: {
        pre_import: function(){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-import');
            var form = jQuery('form[name="import"]', p);
            var res_area = jQuery('.response', form);

            jQuery('input[name="import[file]"]').unbind().bind('change', function(){
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {
                        _handler: window.WTFA_Admin.attr.prefix,
                        _action: 'import',
                        _subaction: 'pre-import'
                    },
                    beforeSubmit: function(){
                        res_area.hide().removeClass('error success');
                        jQuery('*', form).attr({disabled: true});
                        window.WTFA_Admin.preloader(true);
                    },
                    success: function(responseText, statusText, xhr){
                        if (responseText.status)
                        {
                            res_area.html(responseText.doc.message).addClass('success');
                            window.WTFA_Admin.init.import({
                                doc: {
                                    tmp_path: responseText.doc.tmp_path
                                }
                            });
                        }
                        else
                        {
                            res_area.html(responseText.error).addClass('error');
                        }

                        res_area.slideDown();

                        window.WTFA_Admin.preloader(false);
                        jQuery('*', form).attr({disabled: false});
                    }
                });
            });
        },

        import: function(data){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-import');
            var form = jQuery('form[name="import"]', p);
            var res_area = jQuery('.response', form);

            jQuery('input[name="import[do]"]', form).unbind().bind('click', function(){
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {
                        _handler: window.WTFA_Admin.attr.prefix,
                        _action: 'import',
                        _subaction: 'import',

                        import: data
                    },
                    beforeSubmit: function(){
                        res_area.hide().removeClass('error success');
                        jQuery('*', form).attr({disabled: true});
                        window.WTFA_Admin.preloader(true);
                    },
                    success: function(responseText, statusText, xhr){
                        if (responseText.status)
                        {
                            res_area.html(responseText.doc.message).addClass('success');
                        }
                        else
                        {
                            res_area.html(responseText.error).addClass('error');
                        }

                        res_area.slideDown();

                        window.WTFA_Admin.preloader(false);
                        jQuery('*', form).attr({disabled: false});
                    }
                });
            });
        },

        table_dictionary: function(){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-dictionary');
            var form = jQuery('form[name="dictionary"]', p);
            var table = jQuery('.table', form);

            jQuery('thead th', table).removeClass('sorted');
            jQuery('thead th[data-key="' + window.WTFA_Admin.dictionary.table_attr.sort.by + '"]', table).addClass('sorted');

            var sorted_index = jQuery('thead th.sorted', table).index();

            if (sorted_index >= 0)
            {
                jQuery('tr td:nth-child(' + (sorted_index + 1) + ')', table).addClass('sorted');
            }

            jQuery('thead th[data-key!=""]', table).unbind().bind('click', function(){
                var d = jQuery(this).data();

                if (typeof(d.st) == 'undefined' || parseInt(d.st) > 0)
                {
                    var st = 0;
                }
                else
                {
                    var st = 1;
                }

                window.WTFA_Admin.dictionary.table_attr.sort.by = d.key;
                window.WTFA_Admin.dictionary.table_attr.sort.type = st;

                window.WTFA_Admin.dictionary.table_get();
            });

            jQuery('input[name="search"]', form).focus();

            window.WTFA_Admin.init.table_rows_dictionary();

            jQuery('input[name="dictionary[row][create]"]', form).unbind().bind('click', function(){
                window.WTFA_Admin.dictionary.table_row_form();
            });

            jQuery('input[name="dictionary[row][choose]"]', form).unbind().bind('click', function(){
                var ids = new Array();

                // jQuery('input[name="de[s][]"]:checked', table).each(function(){
                jQuery('input[name="de[s][]"]:checked', form).each(function(){
                    ids.push(parseInt(jQuery(this).val()));
                });

                window.parent.jQuery.fancybox.close();
                window.parent.WTFA_Deck_Editor.deck_dictionary_rows_choose(ids);
            });

            jQuery('input[type="checkbox"][name="des-all"]', table).unbind().bind('click', function(){
                jQuery('input[type="checkbox"][name="de[s][]"]', table).attr({checked: jQuery(this).prop('checked')});
            });
        },

        search_dictionary: function(){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-dictionary');
            var form = jQuery('form[name="dictionary"]', p);
            var s = jQuery('input[name="search"]', form);

            s.unbind().bind('keyup', function(){
                clearTimeout(window.WTFA_Admin.attr.t);
                var _this = jQuery(this);

                window.WTFA_Admin.attr.t = setTimeout(function(){
                    window.WTFA_Admin.dictionary.table_attr.search = _this.val();
                    window.WTFA_Admin.dictionary.table_attr.page = 1;

                    window.WTFA_Admin.dictionary.table_get();
                }, 500);
            });
        },

        table_rows_dictionary: function(){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-dictionary');
            var form = jQuery('form[name="dictionary"]', p);
            var table = jQuery('.table', form);

            jQuery('td', table).unbind();

            jQuery('td', table).bind('hover', function(){
                jQuery('tr', table).removeClass('hover');
                jQuery(this).closest('tr').addClass('hover');
            });

            jQuery('td', table).bind('click', function(){
                var ftd = jQuery('td:first', jQuery(this).closest('tr'));

                if (ftd.hasClass('s'))
                {
                    var cb = jQuery('input[type="checkbox"]', ftd);

                    if (cb.is(':checked'))
                    {
                        cb.attr({checked: false});
                    }
                    else
                    {
                        console.log('dev mode');
                        cb.closest('tr').appendTo(".result-list table");
                        cb.attr({checked: true});
                    }
                }
                else
                {
                    window.WTFA_Admin.dictionary.table_row_form(jQuery(this).closest('tr').data('id'));
                }
            });

            jQuery('a', table).unbind().bind('click', function(e){
                e.stopPropagation();
            });

            jQuery('input[name="de[s][]"]', table).unbind().bind('click', function(e){
                e.stopPropagation();
            });
        },

        table_row_form_dictionary: function(){
            var fb = jQuery('.' + window.WTFA_Admin.attr.prefix + '-table-row-form');
            var form = jQuery('form[name="dictionary"]', fb);

            var form_submit = function(action){
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {
                        _handler: window.WTFA_Admin.attr.prefix,
                        _action: 'dictionary',
                        _subaction: 'row-' + action
                    },
                    beforeSubmit: function(){
                        jQuery('*', form).attr({disabled: true});
                        window.WTFA_Admin.preloader(true);
                    },
                    success: function(responseText, statusText, xhr){
                        if (!responseText.status)
                        {
                            jQuery('.error', form).remove();

                            jQuery.each(responseText.error, function(k, v){
                                var input = jQuery('[name="dictionary[' + k + ']"]');

                                if (input.length)
                                {
                                    var error = jQuery('<div class="error">' + v + '</div>');
                                    error.insertAfter(input);
                                }
                            });

                            jQuery('.error', form).stop().slideDown();
                        }
                        else
                        {
                            jQuery.fancybox.close();
                            window.WTFA_Admin.dictionary.table_get();
                        }

                        window.WTFA_Admin.preloader(false);
                        jQuery('*', form).attr({disabled: false});
                    }
                });
            }

            jQuery('input[name="dictionary[cancel]"]', form).unbind().bind('click', function(){
                jQuery.fancybox.close();
            });

            jQuery('input[name="dictionary[remove]"]', form).unbind().bind('click', function(){
                if (!confirm(window.WTFA_Admin.attr.text.dictionary_row_remove)) return;
                form_submit('remove');
            });

            jQuery('input[name="dictionary[submit]"]', form).unbind().bind('click', function(){
                form_submit('save');
            });
        },

        decks: function(){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-decks .decks-list');

            window.WTFA_Admin.init.deck_remove();

            if (p.data('tree-thread') != 'all')
            {
                jQuery('.decks-inner', p).sortable({
                    revert: true,
                    delay: 300,
                    stop: function(event, ui){
                        window.WTFA_Admin.decks.sortable_sync();
                    }
                });

                jQuery('.decks-inner, .decks-inner > .deck', p).disableSelection();
            }
        },

        deck_remove: function(){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-decks');

            jQuery('.decks-list .deck .actions .remove', p).unbind().click(function(){
                if (confirm(window.WTFA_Admin.attr.text.deck_remove))
                {
                    window.WTFA_Admin.decks.deck_remove(jQuery(this).closest('.deck').data('id'));
                }

                return false;
            });
        },

        decks_tree: function(callback){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-decks');

            jQuery('.decks-tree', p).each(function(){
                var dt = this;

                //jQuery('li:has("ul")', dt).find('a:first').prepend('<em class="marker"></em>');
                //jQuery('li:not(:has("ul"))', dt).find('a').addClass('wul');

                jQuery('li span', dt).each(function(){
                    var li = jQuery(this.parentNode);

                    if (!li.next().length)
                    {
                        li.find('ul:first > li').addClass('last');
                    }
                });

                jQuery('li span', dt).unbind().bind('click', function(){
                    jQuery('a.current', dt).removeClass('current');

                    var a = jQuery('a:first', this.parentNode);
                    //var li = jQuery(this.parentNode);

                    window.WTFA_Admin.decks.list_get(a.data('key'));

                    a.toggleClass('current');

                    /*
                    if (!li.next().length)
                    {
                        li.find('ul:first > li').addClass('last');
                    }

                    var ul = jQuery('ul:first', this.parentNode);

                    if (ul.length)
                    {
                        ul.slideToggle(300);

                        var em = jQuery('em:first', this.parentNode);
                        em.toggleClass('open');
                    }
                    */
                });

                jQuery('a.all', p).unbind().bind('click', function(){
                    window.WTFA_Admin.decks.list_get(jQuery(this).data('key'));

                    jQuery('li span a', p).removeClass('current');
                    jQuery(this).addClass('current');
                });

                // Actions
                var ap = jQuery('.actions', dt);

                jQuery('.thread-rename', ap).unbind().bind('click', function(){
                    var tree_branch_id = jQuery('a.current', dt).data('key');

                    jQuery.ajax({
                        url: window.ajaxUrl,
                        type: 'post',
                        dataType: 'json',
                        global: true,
                        async: true,
                        cache: false,
                        data: {
                            _handler: window.WTFA_Admin.attr.prefix,
                            _action: 'deck',
                            _subaction: 'tree-thread-rename-form-get',
                            deck: {
                                tree_branch_id: tree_branch_id
                            }
                        },
                        beforeSend: function(){
                            window.WTFA_Admin.preloader(true);
                        },
                        timeout: function(){},
                        error: function(){},
                        success: function(response){
                            if (response.status)
                            {
                                var wc = window.WTFA_Admin.attr.prefix + '-tree-thread-rename-form';

                                jQuery.fancybox.open({
                                    wrapCSS: wc,
                                    content: response.html,
                                    afterShow: function(){
                                        var f = jQuery('.' + wc);

                                        jQuery('input[name="tree-thread[submit]"]', f).unbind().bind('click', function(){
                                            var tree_branch_id = jQuery('input[name="tree-thread[id]"]', f).val();
                                            var title = jQuery('input[name="tree-thread[title]"]', f).val();
                                            var price = jQuery('input[name="tree-thread[price]"]', f).val();
                                            var number_of_scenarios = jQuery('input[name="tree-thread[number_of_scenarios]"]', f).val();
                                            var text_before_sale = jQuery('textarea[name="tree-thread[text_before_sale]"]', f).val();
                                            var difficulty_level = jQuery('select[name="tree-thread[difficulty_level]"]', f).val();
                                            var hidden = jQuery('input[name="tree-thread[hidden]"]:checked', f).val();

                                            jQuery.ajax({
                                                url: window.ajaxUrl,
                                                type: 'post',
                                                dataType: 'json',
                                                global: true,
                                                async: true,
                                                cache: false,
                                                data: {
                                                    _handler: window.WTFA_Admin.attr.prefix,
                                                    _action: 'deck',
                                                    _subaction: 'tree-thread-rename-do',
                                                    deck: {
                                                        tree_branch_id: tree_branch_id,
                                                        title: title,
                                                        price: price,
                                                        difficulty_level: difficulty_level,
                                                        text_before_sale: text_before_sale,
                                                        number_of_scenarios: number_of_scenarios,
                                                        hidden: hidden,
                                                    }
                                                },
                                                beforeSend: function(){
                                                    jQuery('*', f).attr({disabled: true});
                                                    window.WTFA_Admin.preloader(true);
                                                },
                                                timeout: function(){},
                                                error: function(){},
                                                success: function(response){
                                                    if (response.status)
                                                    {
                                                        jQuery.fancybox.close();

                                                        window.WTFA_Admin.decks.tree.draw(function(){
                                                            jQuery('a[data-key="' + tree_branch_id + '"]', dt).closest('span').click();
                                                        });
                                                    }
                                                    else
                                                    {
                                                        jQuery('*', f).attr({disabled: false});

                                                        jQuery('.error', f).remove();

                                                        jQuery.each(response.error, function(k, v){
                                                            var input = jQuery('[name="' + k + '"]');

                                                            if (input.length)
                                                            {
                                                                var error = jQuery('<div class="error">' + v + '</div>');
                                                                error.insertAfter(input);
                                                            }
                                                        });

                                                        jQuery('.error', f).stop().slideDown();
                                                    }
                                                },
                                                complete: function(){
                                                    window.WTFA_Admin.preloader(false);
                                                }
                                            });
                                        });

                                        jQuery('input[name="tree-thread[cancel]"]', f).unbind().bind('click', function(){
                                            jQuery.fancybox.close();
                                        });
                                    }
                                });
                            }
                        },
                        complete: function(){
                            window.WTFA_Admin.preloader(false);
                        }
                    });
                });
                ////

                jQuery('.thread-add', ap).unbind().bind('click', function(){
                    var tree_branch_id = jQuery('a.current', dt).data('key');

                    jQuery.ajax({
                        url: window.ajaxUrl,
                        type: 'post',
                        dataType: 'json',
                        global: true,
                        async: true,
                        cache: false,
                        data: {
                            _handler: window.WTFA_Admin.attr.prefix,
                            _action: 'deck',
                            _subaction: 'tree-thread-add-form-get',
                            deck: {
                                tree_branch_id: tree_branch_id
                            }
                        },
                        beforeSend: function(){
                            window.WTFA_Admin.preloader(true);
                        },
                        timeout: function(){},
                        error: function(){},
                        success: function(response){
                            if (response.status)
                            {
                                var wc = window.WTFA_Admin.attr.prefix + '-tree-thread-add-form';

                                jQuery.fancybox.open({
                                    wrapCSS: wc,
                                    content: response.html,
                                    afterShow: function(){
                                        var f = jQuery('.' + wc);

                                        jQuery('input[name="tree-thread[submit]"]', f).unbind().bind('click', function(){
                                            var tree_branch_id = jQuery('input[name="tree-thread[id]"]', f).val();
                                            var title = jQuery('input[name="tree-thread[title]"]', f).val();
                                            var price = jQuery('input[name="tree-thread[price]"]', f).val();
                                            var number_of_scenarios = jQuery('input[name="tree-thread[number_of_scenarios]"]', f).val();
                                            var text_before_sale = jQuery('textarea[name="tree-thread[text_before_sale]"]', f).val();
                                            var difficulty_level = jQuery('select[name="tree-thread[difficulty_level]"]', f).val();
                                            var hidden = jQuery('input[name="tree-thread[hidden]"]', f).val();

                                            jQuery.ajax({
                                                url: window.ajaxUrl,
                                                type: 'post',
                                                dataType: 'json',
                                                global: true,
                                                async: true,
                                                cache: false,
                                                data: {
                                                    _handler: window.WTFA_Admin.attr.prefix, // todo (check)
                                                    _action: 'deck',
                                                    _subaction: 'tree-thread-add-do',
                                                    deck: {
                                                        tree_branch_id: tree_branch_id,
                                                        title: title,
                                                        price: price,
                                                        difficulty_level: difficulty_level,
                                                        text_before_sale: text_before_sale,
                                                        number_of_scenarios: number_of_scenarios,
                                                        hidden: hidden,
                                                    }
                                                },
                                                beforeSend: function(){
                                                    jQuery('*', f).attr({disabled: true});
                                                    window.WTFA_Admin.preloader(true);
                                                },
                                                timeout: function(){},
                                                error: function(){},
                                                success: function(response){
                                                    if (response.status)
                                                    {
                                                        jQuery.fancybox.close();

                                                        window.WTFA_Admin.decks.tree.draw(function(){
                                                            jQuery('a[data-key="' + tree_branch_id + '"]', dt).closest('span').click();
                                                        });
                                                    }
                                                    else
                                                    {
                                                        jQuery('*', f).attr({disabled: false});

                                                        jQuery('.error', f).remove();

                                                        jQuery.each(response.error, function(k, v){
                                                            var input = jQuery('[name="' + k + '"]');

                                                            if (input.length)
                                                            {
                                                                var error = jQuery('<div class="error">' + v + '</div>');
                                                                error.insertAfter(input);
                                                            }
                                                        });

                                                        jQuery('.error', f).stop().slideDown();
                                                    }
                                                },
                                                complete: function(){
                                                    window.WTFA_Admin.preloader(false);
                                                }
                                            });
                                        });

                                        jQuery('input[name="tree-thread[cancel]"]', f).unbind().bind('click', function(){
                                            jQuery.fancybox.close();
                                        });
                                    }
                                });
                            }
                        },
                        complete: function(){
                            window.WTFA_Admin.preloader(false);
                        }
                    });
                });

                jQuery('.thread-remove', ap).unbind().bind('click', function(){
                    var tree_branch_id = jQuery('a.current', dt).data('key');

                    jQuery.ajax({
                        url: window.ajaxUrl,
                        type: 'post',
                        dataType: 'json',
                        global: true,
                        async: true,
                        cache: false,
                        data: {
                            _handler: window.WTFA_Admin.attr.prefix,
                            _action: 'deck',
                            _subaction: 'tree-thread-remove-form-get',
                            deck: {
                                tree_branch_id: tree_branch_id
                            }
                        },
                        beforeSend: function(){
                            window.WTFA_Admin.preloader(true);
                        },
                        timeout: function(){},
                        error: function(){},
                        success: function(response){
                            if (response.status)
                            {
                                var wc = window.WTFA_Admin.attr.prefix + '-tree-thread-remove-form';

                                jQuery.fancybox.open({
                                    wrapCSS: wc,
                                    content: response.html,
                                    afterShow: function(){
                                        var f = jQuery('.' + wc);

                                        jQuery('input[name="tree-thread[submit]"]', f).unbind().bind('click', function(){
                                            var tree_branch_id = jQuery('input[name="tree-thread[id]"]', f).val();

                                            jQuery.ajax({
                                                url: window.ajaxUrl,
                                                type: 'post',
                                                dataType: 'json',
                                                global: true,
                                                async: true,
                                                cache: false,
                                                data: {
                                                    _handler: window.WTFA_Admin.attr.prefix, // todo (check)
                                                    _action: 'deck',
                                                    _subaction: 'tree-thread-remove-do',
                                                    deck: {
                                                        tree_branch_id: tree_branch_id,
                                                    }
                                                },
                                                beforeSend: function(){
                                                    jQuery('*', f).attr({disabled: true});
                                                    window.WTFA_Admin.preloader(true);
                                                },
                                                timeout: function(){},
                                                error: function(){},
                                                success: function(response){
                                                    if (response.status)
                                                    {
                                                        jQuery.fancybox.close();

                                                        window.WTFA_Admin.decks.tree.draw(function(){
                                                            jQuery('a[data-key="' + tree_branch_id + '"]', dt).closest('.decks-tree').find('.all').click();
                                                            window.WTFA_Admin.decks.list_get();
                                                        });
                                                    }
                                                    else
                                                    {
                                                        jQuery('*', f).attr({disabled: false});

                                                        jQuery('.error', f).remove();

                                                        jQuery.each(response.error, function(k, v){
                                                            var input = jQuery('[name="' + k + '"]');

                                                            if (input.length)
                                                            {
                                                                var error = jQuery('<div class="error">' + v + '</div>');
                                                                error.insertAfter(input);
                                                            }
                                                        });

                                                        jQuery('.error', f).stop().slideDown();
                                                    }
                                                },
                                                complete: function(){
                                                    window.WTFA_Admin.preloader(false);
                                                }
                                            });
                                        });

                                        jQuery('input[name="tree-thread[cancel]"]', f).unbind().bind('click', function(){
                                            jQuery.fancybox.close();
                                        });
                                    }
                                });
                            }
                        },
                        complete: function(){
                            window.WTFA_Admin.preloader(false);
                        }
                    });
                });

                if (typeof(callback) == 'function')
                {
                    callback();
                }
            });
        }
    },

    decks: {
        list_get: function(tree_branch_id){
            jQuery.ajax({
                url: window.ajaxUrl,
                type: 'post',
                dataType: 'json',
                global: true,
                async: true,
                cache: false,
                data: {
                    _handler: window.WTFA_Admin.attr.prefix,
                    _action: 'deck',
                    _subaction: 'decks-list-get',
                    deck: {
                        tree_branch_id: tree_branch_id
                    }
                },
                beforeSend: function(){
                    window.WTFA_Admin.preloader(true);
                },
                timeout: function(){},
                error: function(){},
                success: function(response){
                    if (response.status)
                    {
                        jQuery('.decks-list').data('tree-thread', tree_branch_id).html(response.html);
                        window.WTFA_Admin.init.decks();
                    }
                },
                complete: function(){
                    window.WTFA_Admin.preloader(false);
                }
            });
        },

        deck_remove: function(id){
            jQuery.ajax({
                url: '',
                type: 'post',
                dataType: 'json',
                global: true,
                async: true,
                cache: false,
                data: {
                    _handler: window.WTFA_Admin.attr.prefix,
                    _action: 'deck',
                    _subaction: 'remove',
                    deck: {
                        id: id
                    }
                },
                beforeSend: function(){
                    window.WTFA_Admin.preloader(true);
                },
                timeout: function(){},
                error: function(){},
                success: function(response){
                    if (response.status)
                    {
                        window.WTFA_Admin.decks.list_get();
                    }
                },
                complete: function(){
                    window.WTFA_Admin.preloader(false);
                }
            });
        },

        sortable_sync: function(){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-decks .decks-list');
            var sd = {};

            jQuery('.deck', p).each(function(i){
                sd[i] = jQuery(this).data('id');
            });

            jQuery.ajax({
                url: '',
                type: 'post',
                dataType: 'json',
                global: true,
                async: true,
                cache: false,
                data: {
                    _handler: window.WTFA_Admin.attr.prefix,
                    _action: 'deck',
                    _subaction: 'sortable-sync',
                    deck: {
                        sd: sd
                    }
                },
                beforeSend: function(){
                    window.WTFA_Admin.preloader(true);
                },
                timeout: function(){},
                error: function(){},
                success: function(response){},
                complete: function(){
                    window.WTFA_Admin.preloader(false);
                }
            });
        },

        tree: {
            draw: function(callback){
                jQuery.ajax({
                    url: window.ajaxUrl,
                    type: 'post',
                    dataType: 'json',
                    global: true,
                    async: true,
                    cache: false,
                    data: {
                        _handler: window.WTFA_Admin.attr.prefix,
                        _action: 'deck',
                        _subaction: 'decks-tree-admin-get'
                    },
                    beforeSend: function(){
                        window.WTFA_Admin.preloader(true);
                    },
                    timeout: function(){},
                    error: function(){},
                    success: function(response){
                        if (response.status)
                        {
                            jQuery('.decks-tree').html(response.html);
                            window.WTFA_Admin.init.decks_tree(callback);
                        }
                    },
                    complete: function(){
                        window.WTFA_Admin.preloader(false);
                    }
                });
            }
        }
    },

    dictionary: {
        table_attr: {
            sort: {
                by: 'date',
                type: 1
            },
            page: 1,
            search: ''
        },

        table_get: function(){
            var p = jQuery('#' + window.WTFA_Admin.attr.prefix + '-dictionary');
            var form = jQuery('form[name="dictionary"]', p);
            jQuery.ajax({
                url: localStorage.getItem('ajaxUrl'),
                global: false,
                type: 'post',
                dataType: 'json',
                async: true,
                cache: false,
                data: {
                    _handler: window.WTFA_Admin.attr.prefix,
                    _action: 'dictionary',
                    _subaction: 'table-get',

                    dictionary: {
                        attr: window.WTFA_Admin.dictionary.table_attr
                    }
                },
                beforeSend: function(){
                    jQuery('*', form).attr({disabled: true});
                    window.WTFA_Admin.preloader(true);
                },
                success: function(response){
                    jQuery('*', form).attr({disabled: false});

                    if (response.status)
                    {
                        jQuery('.table', form).html(response.html);

                        window.WTFA_Admin.init.table_dictionary();
                    }
                },
                complete: function(){
                    window.WTFA_Admin.preloader(false);
                }
            });
        },

        page_change: function(page){
            window.WTFA_Admin.dictionary.table_attr.page = parseInt(page);
            window.WTFA_Admin.dictionary.table_get();

            return false;
        },

        table_row_form: function(id){
            jQuery.ajax({
                url: '',
                global: false,
                type: 'post',
                dataType: 'json',
                async: true,
                cache: false,
                data: {
                    _handler: window.WTFA_Admin.attr.prefix,
                    _action: 'dictionary',
                    _subaction: 'table-row-form',

                    dictionary: {
                        id: id
                    }
                },
                beforeSend: function(){
                    window.WTFA_Admin.preloader(true);
                },
                success: function(response){
                    if (response.status)
                    {
                        jQuery.fancybox.open({
                            wrapCSS: window.WTFA_Admin.attr.prefix + '-table-row-form',
                            content: response.html,
                            afterShow: function(){
                                window.WTFA_Admin.init.table_row_form_dictionary();
                            }
                        });
                    }
                },
                complete: function(){
                    window.WTFA_Admin.preloader(false);
                }
            });
        }
    }
}
