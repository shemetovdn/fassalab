jQuery(document).ready(function(){

});

var WTFA_Deck_Editor = {
    attr: {
        p: null,
        form: null,
        text: {},
        items_mp_key_max: 0
    },

    init: function(){
        var p = jQuery('#deck-editor');
        var form = jQuery('form[name="deck"]', p);

        window.WTFA_Deck_Editor.attr.p = p;
        window.WTFA_Deck_Editor.attr.form = form;

        jQuery('input[name="deck[submit]"]', form).unbind().click(function(){
            window.WTFA_Deck_Editor.form.submit('create', function(responseText, statusText, xhr){
                var data = jQuery.parseJSON(responseText);

                if (!data.status)
                {
                    window.WTFA_Deck_Editor.form.error(data.error);
                }
                {
                    jQuery.fancybox({
                        content: data.message,
                        afterClose: function(){
                            window.WTFA_Admin.preloader(true);
                            window.location.href = data.redirect_url;
                        }
                    });
                }
            });
        });

        jQuery('input[name="deck[save]"]', form).unbind().click(function(){
            window.WTFA_Deck_Editor.form.submit('save', function(responseText, statusText, xhr){
                var data = jQuery.parseJSON(responseText);

                if (!data.status)
                {
                    window.WTFA_Deck_Editor.form.error(data.error);
                }
                {
                    jQuery.fancybox({
                        content: data.message,
                        afterClose: function(){
                            window.WTFA_Admin.preloader(true);
                            window.location.href = data.redirect_url;
                        }
                    });
                }
            });
        });

        jQuery('input[name="deck[item-new]"]', form).unbind().click(function(){
            window.WTFA_Deck_Editor.deck_dictionary_manager();
        });

        window.WTFA_Deck_Editor.deck_dictionary_items_get();
    },

    form: {
        error: function (error){
            var p = jQuery('#deck-editor');
            var form = jQuery('form[name="deck"]', p);

            jQuery('.error', form).remove();

            jQuery.each(error, function(k, v){
                var input = jQuery('[name="' + k + '"]');

                if (input.length)
                {
                    var error = jQuery('<div class="error">' + v + '</div>');
                    error.insertAfter(input);
                }
            });

            jQuery('.error', form).stop().slideDown();
        },

        submit: function(subaction, callback_success){
            var p = jQuery('#deck-editor');
            var form = jQuery('form[name="deck"]', p);

            form.ajaxSubmit({
                data: {
                    _action: 'deck',
                    _subaction: subaction
                },
                beforeSubmit: function(){
                    jQuery('*', form).attr({disabled: true});
                    window.WTFA_Admin.preloader(true);
                },
                success: function(responseText, statusText, xhr){
                    if (typeof(callback_success) == 'function'){ callback_success(responseText, statusText, xhr); }

                    window.WTFA_Admin.preloader(false);
                    jQuery('*', form).attr({disabled: false});
                }
            });
        }
    },

    deck_dictionary_manager: function(){
        jQuery.ajax({
            url: window.ajaxUrl,
            global: false,
            type: 'post',
            dataType: 'json',
            async: true,
            cache: false,
            data: {
                _handler: window.WTFA_Admin.attr.prefix,
                _action: 'deck',
                _subaction: 'dictionary-manager',

                deck: {}
            },
            beforeSend: function(){
                window.WTFA_Admin.preloader(true);
            },
            success: function(response){
                if (response.status)
                {
                    jQuery.fancybox.open({
                        wrapCSS: window.WTFA_Admin.attr.prefix + '-dictionary-manager',
                        content: response.html,
                        afterShow: function(){}
                    });
                }
            },
            complete: function(){
                window.WTFA_Admin.preloader(false);
            }
        });
    },

    deck_dictionary_rows_choose: function(ids){
        jQuery.ajax({
            url: '',
            global: false,
            type: 'post',
            dataType: 'json',
            async: true,
            cache: false,
            data: {
                _handler: window.WTFA_Admin.attr.prefix,
                _action: 'deck',
                _subaction: 'dictionary-rows-select',

                deck: {
                    items: ids
                }
            },
            beforeSend: function(){
                window.WTFA_Admin.preloader(true);
            },
            success: function(response){
                if (response.status)
                {
                    window.WTFA_Deck_Editor.deck_dictionary_items_get();
                }
            },
            complete: function(){
                window.WTFA_Admin.preloader(false);
            }
        });
    },

    deck_dictionary_items_get: function(){
        var p = jQuery('#deck-editor');
        var form = jQuery('form[name="deck"]', p);

        jQuery.ajax({
            url: window.ajaxUrl,
            global: false,
            type: 'post',
            dataType: 'json',
            async: true,
            cache: false,
            data: {
                _handler: window.WTFA_Admin.attr.prefix,
                _action: 'deck',
                _subaction: 'dictionary-items-get'
            },
            beforeSend: function(){
                window.WTFA_Admin.preloader(true);
            },
            success: function(response){
                if (response.status)
                {
                    jQuery('.items-list', form).html(response.html);
                    window.WTFA_Deck_Editor.deck_dictionary_items_init();
                }
            },
            complete: function(){
                window.WTFA_Admin.preloader(false);
            }
        });
    },

    deck_dictionary_items_init: function(){
        var p = jQuery('#deck-editor');
        var form = jQuery('form[name="deck"]', p);
        var list = jQuery('.items-list', form);

        jQuery('.word-item .remove', list).unbind().click(function(){
            window.WTFA_Deck_Editor.deck_dictionary_item_remove(jQuery(this).closest('.word-item').data('id'));
            return false;
        });
    },

    deck_dictionary_item_remove: function(id){
        jQuery.ajax({
            url: window.ajaxUrl,
            global: false,
            type: 'post',
            dataType: 'json',
            async: true,
            cache: false,
            data: {
                _handler: window.WTFA_Admin.attr.prefix,
                _action: 'deck',
                _subaction: 'dictionary-item-remove',

                deck: {
                    id: id
                }
            },
            beforeSend: function(){
                window.WTFA_Admin.preloader(true);
            },
            success: function(response){
                if (response.status)
                {
                    window.WTFA_Deck_Editor.deck_dictionary_items_get();
                }
            },
            complete: function(){
                window.WTFA_Admin.preloader(false);
            }
        });
    }
}
