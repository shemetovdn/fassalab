jQuery(document).ready(function(){
    window.WTFA.init();
});

var WTFA = {
    attr: {
        p: null,
        w: null
    },

    init: function(){
        var p = jQuery('body.' + window.WTFA.attr.prefix + '-deck .deck-wrapper');
        var w = jQuery('.word-wrapper', p);

        window.WTFA.attr.p = p;
        window.WTFA.attr.w = w;
    },

    word_answer_init: function(){
        var p = window.WTFA.attr.p;
        var w = window.WTFA.attr.w;
        var l = jQuery('.footer .level', p);

        jQuery('.answer', w).hide();
        l.hide();
        jQuery('.footer .answer-show', p).show();

        var btn_stop_init = function(){
            jQuery('.button.stop', p).unbind().bind('click', function(){
                window.WTFA.word_get(p.eq(0).data('id'), 0, 0, true);
                return false;
            });
        }

        jQuery('.button.answer', p).unbind().bind('click', function(){
            jQuery('.answer', w).html(window.WTFA_Base64.decode(jQuery('.answer', w).data('h'))).show();

            jQuery('.footer .answer-show', p).hide();
            l.show();

            jQuery('.button', l).unbind().bind('click', function(){
                window.WTFA.word_get(p.data('id'), w.data('id'), jQuery(this).data('lvl'));
            });

            btn_stop_init();

            return false;
        });

        btn_stop_init();
    },

    preloader: function(show){
        var p = window.WTFA.attr.p;
        var speed = 200;
        var preloader = jQuery('.preloader', p);

        if (show)
        {
            if (preloader.length)
            {
                preloader.stop().fadeIn(speed).unbind();
            }
            else
            {
                preloader.fadeIn(speed).unbind();
            }

            setTimeout(function(){
                preloader.click(function(){
                    window.WTFA.preloader(false);
                });
            }, 5000);
        }
        else
        {
            preloader.fadeOut(speed);
        }
    },

    word_get: function(deck_id, word_id, lvl, splash, callback){
        var p = window.WTFA.attr.p;

        deck_id =  parseInt(deck_id);
        lvl = parseInt(lvl);
        if (splash !== true) splash = 0; else splash = 1;

        jQuery.ajax({
            url: '',
            global: false,
            type: 'post',
            dataType: 'json',
            async: true,
            cache: false,
            data: {
                _handler: window.WTFA.attr.prefix,
                _action: 'deck',
                _subaction: 'word-get',

                deck: {
                    deck_id: deck_id,
                    word_id: word_id,
                    lvl: lvl,
                    splash: splash
                }
            },
            beforeSend: function(){
                window.WTFA.preloader(true);
            },
            success: function(response){
                if (response.status)
                {
                    var p = window.WTFA.attr.p;
                    var w = window.WTFA.attr.w;

                    p.data({id: response['deck-id']});
                    jQuery('.header h2', p).html(response['deck-title']);

                    if (!response.splash)
                    {
                        w.data({id: response.word.id});

                        jQuery('.word', w).html(response.word.text_source);

                        // File
                        if (typeof(response.word.file) == 'string')
                        {
                            if (response.word.file.match(/\.(mp3|mpeg)$/gi))
                            {
                                var file = jQuery('<audio>');

                                file.attr({
                                    src: response.word.file,
                                    controls: true
                                });
                            }
                            else
                            {
                                var file = jQuery('<div class="image">');

                                file.css({
                                    backgroundImage: "url('" + response.word.file + "')"
                                }).bind('click', function(){
                                    window.open(response.word.file);
                                });
                            }

                            jQuery('.word', w).append(file);
                        }
                        ////

                        jQuery('.answer', w).data({h: response.word.text_translation});
                        jQuery('.answer, .footer', p).show();

                        window.WTFA.word_answer_init();
                    }
                    else
                    {
                        jQuery('.word', w).html(response.html);
                        jQuery('.answer, .footer', p).hide();

                        //
                        var dw = jQuery('.deck-view', p);

                        if (dw.length)
                        {
                            dw.show();

                            jQuery('.button.tree', dw).removeClass('hide').unbind().bind('click', function(){
                                window.WTFA.decks_tree_get();
                                return false;
                            });
                        }
                        ////
                    }

                    if (typeof(callback) == 'function') callback();
                }
            },
            complete: function(){
                window.WTFA.preloader(false);
            }
        });
    },

    decks_tree_get: function(){
        var p = window.WTFA.attr.p;

        jQuery.ajax({
            url: '',
            global: false,
            type: 'post',
            dataType: 'json',
            async: true,
            cache: false,
            data: {
                _handler: window.WTFA.attr.prefix,
                _action: 'deck',
                _subaction: 'decks-tree-front-get'
            },
            beforeSend: function(){
                window.WTFA.preloader(true);
            },
            success: function(response){
                if (response.status)
                {
                    jQuery('.decks-tree', p).html(response.html).show();
                    jQuery('.deck-view', p).hide();

                    window.WTFA.decks_tree_init();
                }
            },
            complete: function(){
                window.WTFA.preloader(false);
            }
        });
    },

    decks_tree_init: function(){
        var p = window.WTFA.attr.p;
        var dt = jQuery('.decks-tree', p);

        jQuery('.lessons > a', dt).unbind().bind('click', function(){
            if (!jQuery(this).closest('.lessons').hasClass('close'))
            {
                window.WTFA.word_get(jQuery(this).data('id'), 0, 0, true, function(){
                    jQuery('.decks-tree', p).hide().html('');
                });
            }

            return false;
        });
    }
}
