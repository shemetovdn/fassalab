<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>decks-tree-front.css">
<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>main.css">

<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>base64.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>main.js"></script>

<script type="text/javascript">
    window.WTFA.attr.prefix = '<?php echo $this->Attr_Get('prefix'); ?>';
    //window.WTFL.attr.url_site = '<?php echo site_url(); ?>';
    //window.WTFL.attr.url_plugin_images = '<?php echo $this->Attr_Get('plugin_img_url'); ?>';
    //window.WTFL.attr.url_h5p_css = '<?php echo $this->Attr_Get('plugin_css_url'); ?>h5p.css';

    //window.WTFL.attr.text.next = '<?php _e('Suivant'); ?>';
    //window.WTFL.attr.text.check = '<?php _e('Valider'); ?>';
    //window.WTFL.attr.text.lesson_exit = '<?php _e('Voulez-vous vraiment quitter cette leçon?'); ?>';
    //window.WTFL.attr.text.lesson_finish = ['<?php _e('Félicitations'); ?>', '<?php _e('Ce cours est terminé'); ?>'];
</script>
