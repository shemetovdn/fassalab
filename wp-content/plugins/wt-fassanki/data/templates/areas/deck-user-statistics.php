<div class="deck-user-statistics splash">
    <ul>
        <li>
            <span class="k">
                <?php _e('Mes mots'); ?>
            </span>:
            <span class="v">
                <?php echo $data_tpl['words_learn_count']; ?>
            </span>
        </li>
        <li>
            <span class="k">
                <?php _e('Mémorisation'); ?>
                <?php /*(<span class="easy"><?php _e('facile'); ?></span> / <span class="good"><?php _e('moyen'); ?></span> / <span class="hard"><?php _e('difficile'); ?></span>)*/ ?>
                (<span class="easy"><?php _e('Learned'); ?></span>  / <span class="hard"><?php _e('Repeat'); ?></span>)
            </span>:
            <span class="v">
                <?php /*
                <span class="easy"><?php echo (int)$data_tpl['words_user'][1]; ?></span>
                /
                <span class="good"><?php echo (int)$data_tpl['words_user'][2]; ?></span>
                /
                <span class="hard"><?php echo (int)$data_tpl['words_user'][3]; ?></span>
            */ ?>
                <span class="easy"><?php echo (int)$data_tpl['words_user'][5]; ?></span>
                /
                <span class="hard"><?php echo (int)$data_tpl['words_user'][4]; ?></span><?php // to repeat ?>

            </span>
        </li>
        <?php /*
        <li>
            <span class="k">
                <?php _e('Mots maîtrisés'); ?>
            </span>:
            <span class="v green">
                <?php echo $data_tpl['words_learn_count']; ?>
            </span>
        </li>
        */ ?>
    </ul>
    <?php

        if ($data_tpl['words_learn_count'])
        {
            ?>
            <div class="actions">
                <a href="#" class="button tree hide"><?php _e('Back to tree'); ?></a>
                <a href="#" class="button start"><?php _e("Je révise"); ?></a>
            </div>
            <?php
        }
        else
        {
            ?>
            <div class="completed">
                <p>
                    <?php _e('Dans le deck actuel vous avez appris tous les mots'); ?>.
                    <br />
                    <?php _e('Veuillez venir ici plus tard pour les réexaminer'); ?>.
                </p>
            </div>
            <br />
            <a href="#" class="button tree hide"><?php _e('Back to tree'); ?></a>
            <?php
        }

    ?>
</div>

<script type="text/javascript">
    jQuery(jQuery('body.' + window.WTFA.attr.prefix + '-deck .splash .button.start')).unbind().bind('click', function(){
        window.WTFA.word_get(jQuery('body.' + window.WTFA.attr.prefix + '-deck .deck-wrapper').eq(0).data('id'), 0, 0, false);
        return false;
    });
</script>
