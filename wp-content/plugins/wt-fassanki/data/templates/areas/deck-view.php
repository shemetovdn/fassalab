<div class="header">
    <h2></h2>
</div>
<div class="body">
    <div class="word-wrapper" data-id="">
        <div class="word"></div>
        <div class="answer"></div>
    </div>
</div>
<div class="footer">
    <div class="answer-show">
        <a href="#" class="button stop"><?php _e('Terminer la session'); ?></a>
        <a href="#" class="button answer"><?php _e('Traduction'); ?></a>
    </div>
    <div class="level">
        <a href="#" class="button stop"><?php _e('Complete'); ?></a>
        <?php /*<a href="#" class="button again" data-lvl="0"><?php _e('À revoir'); ?></a>*/ ?>
        <?php // 2018.12.27 ?>
        <?php /*<a href="#" class="button hard" data-lvl="3"><?php _e('Difficile'); ?></a>
        <a href="#" class="button good" data-lvl="2"><?php _e('Moyen'); ?></a>
        <a href="#" class="button easy" data-lvl="1"><?php _e('Facile'); ?></a>*/ ?>
        <a href="#" class="button hard" data-lvl="4"><?php _e('Repeat'); ?></a>
        <a href="#" class="button easy" data-lvl="5"><?php _e('Learned'); ?></a>
    </div>
</div>
