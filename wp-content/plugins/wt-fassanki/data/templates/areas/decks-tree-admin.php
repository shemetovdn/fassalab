<?php

    function Tree_Draw($data)
    {
        if (count($data))
        {
            ?>
            <ul id="decks-sortable">
                <?php

                    foreach ($data as $k => $v)
                    {
                        if (isset($v['sub']) && count($v['sub'])) $sub = true; else $sub = false;

                        ?>
                        <li>
                            <span>
                                <a href="#" class="<?php if (!$sub) echo 'wul'; ?>" data-key="<?php echo $v['id']; ?>" data-sort="<?php echo $v['sort_id']; ?>">
                                    <?php

                                        if ($sub)
                                        {
                                            ?>
                                            <em class="marker open"></em>
                                            <?php
                                        }

                                        echo $v['title'] . ' (' . $v['decks-count'] . ')';

                                    ?>
                                </a>
                            </span>
                            <?php if ($sub) Tree_Draw($v['sub']); ?>
                        </li>
                        <?php
                    }

                ?>
            </ul>
            <?php
        }
    }

?>
<h4><a href="#" class="all current" data-key="all"><?php _e('All decks'); ?></a></h4>
<?php Tree_Draw($decks_tree); ?>
<div class="actions">
    <a href="#" class="thread-add"><?php _e('Add new thread'); ?></a>
    <a href="#" class="thread-rename"><?php _e('Rename thread'); ?></a>
    <a href="#" class="thread-remove"><?php _e('Remove thread'); ?></a>
</div>
    <script>
        jQuery(document).ready(function () {
            jQuery( "#decks-sortable" ).sortable({
                // connectWith: "ul",
                update: function( event, ui ) {
                    var branches =jQuery( "#decks-sortable > li > span > a" );
                    var branchesSort = {};

                    for(var i = 0; i < branches.length; i++){
                        var  clipId = jQuery(branches[i]).attr("data-key");
                        branchesSort[clipId] = i;
                    }
                    console.log(branchesSort);
                    console.log(JSON.stringify(branchesSort));

                    jQuery.ajax({
                        url : window.ajaxUrl,
                        type : "POST",
                        data: {
                            _handler: window.WTFA_Admin.attr.prefix,
                            _action: 'deck',
                            _subaction: 'sorting-branch',
                            data: branchesSort
                        },
                        success: function (data) {
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>
