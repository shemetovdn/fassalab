<?php

    if (count($decks_tree))
    {
        /*
        function WTFA_Tree_Decks_Walk(&$decks_tree, $ftk, $fdk, $callback)
        {
            if (is_callable($callback) && count($decks_tree))
            {
                foreach ($decks_tree as $tk => $tv)
                {
                    if (count($tv['decks']))
                    {
                        foreach ($tv['decks'] as $dk => $dv)
                        {
                            if ($tk >= $ftk && $dk > $fdk)
                            {
                                $callback($decks_tree[$tk]['decks'][$dk]);
                            }
                        }
                    }
                }
            }
        }
        */

        $colors = array(
            'red',
            'green',
            'blue',
            'lilac'
        );

        $dv_open_next_flag = false;

        foreach ($decks_tree as $tk => $tv)
        {
            ?>
            <div class="lessonsline-item <?php if (isset($colors[$tk])) echo $colors[$tk]; else echo $colors[0]; ?>" data-id="<?php echo $tv['id']; ?>">
                <h2><?php echo $tv['title']; ?></h2>
                <?php

                    if (count($tv['decks']))
                    {
                        foreach ($tv['decks'] as $dk => $dv)
                        {
                            $class = array('lessons');

                            /*
                            if ($dv['passed'])
                            {
                                $class[] = 'pass';
                                $dv_open_next_flag = true;
                            }
                            else
                            {
                                if
                                (
                                    $dv_open_next_flag
                                    || ($tv['open'] && $dk == 0)
                                )
                                {
                                    $dv_open_next_flag = false;
                                }
                                else
                                {
                                    WTFA_Tree_Decks_Walk($decks_tree, $tk, $dk, function($data)use(&$dv_open_next_flag){
                                        if ($data['passed'])
                                        {
                                            $dv_open_next_flag = true;
                                            return true;
                                        }
                                    });

                                    if (!$dv_open_next_flag)
                                    {
                                        $class[] = 'close';
                                    }
                                    else
                                    {
                                        $class[] = 'open';
                                    }
                                }
                            }
                            */

                            if ($dv['passed'] || $dv['open'])
                            {
                                if ($dv['passed']) $class[] = 'pass';
                                if ($dv['open']) $class[] = 'open';
                            }
                            else
                            {
                                $class[] = 'close';
                            }

                            ?>
                            <div class="<?php echo implode(' ', $class); ?>">
                                <a href="#" data-id="<?php echo $dv['id']; ?>"></a>
                                <h3 class="name-lessons"><?php echo $dv['title']; ?></h3>
                            </div>
                            <?php
                        }
                    }

                ?>
            </div>
            <?php
        }
    }

?>
