<div class="info top">
    <?php

        if ($data['pg']['status'])
        {
            ?>
            <div class="pg">
                <?php echo $data['pg']['html']; ?>
            </div>
            <?php
        }

    ?>
    <div class="count">
        <span><?php _e('Rows found'); ?>: <b><?php echo $data['items_count']; ?></b></span>
    </div>
    <div class="btns">
        <?php

            if (isset($_REQUEST['ddm']))
            {
                ?>
                <input type="button" name="dictionary[row][choose]" value="<?php _e('Choose selected rows for deck'); ?>" />&nbsp;
                <?php
            }
            else
            {
                ?>
                <input type="button" name="dictionary[row][create]" value="<?php _e('Create row'); ?>" />
                <?php
            }

        ?>
    </div>
</div>

<?php

    if (count($data['data']))
    {
        ?>
        <table>
            <thead>
                <?php if (isset($_REQUEST['ddm'])){ ?><th data-key=""><input type="checkbox" name="des-all" value="" /></th><?php } ?>
                <?php $k = 'id'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('ID'); ?></th>
                <?php $k = 'text_source'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Source text'); ?></th>
                <?php $k = 'text_translation'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Translation text'); ?></th>
                </th>
                <?php $k = 'text_transcription'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Transcription text'); ?>
                </th>
                <?php $k = 'description'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Description'); ?></th>
                <?php /* $k = 'course'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Course'); ?></th> */ ?>
                <th data-key=""><?php _e('File'); ?></th>
                <?php $k = 'status'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Status'); ?></th>
                <?php $k = 'date'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Updating date'); ?></th>
            </thead>
            <tbody>
            <?php

                foreach ($data['data'] as $v)
                {
                    $v->text_source = htmlspecialchars($v->text_source);
                    $v->text_translation = htmlspecialchars($v->text_translation);
                    $v->text_translation = preg_replace("/[\r\n]+/uis", '<br>', $v->text_translation);
                    $v->description = preg_replace("/[\r\n]+/uis", '<br>', $v->description);

                    ?>
                    <tr data-id="<?php echo $v->id; ?>">
                        <?php if (isset($_REQUEST['ddm'])){ ?><td class="s"><input type="checkbox" name="de[s][]" value="<?php echo $v->id; ?>" /></td><?php } ?>
                        <td class="id"><?php echo $v->id; ?></td>
                        <td><?php echo $v->text_source; ?></td>
                        <td><?php echo $v->text_translation; ?></td>
                        <td><?php echo $v->text_transcription; ?></td>
                        <td><?php echo $v->description; ?></td>
                        <?php /*
                        <td>
                            <?php

                                if ((int)$v->course_id && trim($v->course_title) != '')
                                {
                                    ?>
                                    <a href="<?php echo admin_url('post.php?post=' . $v->course_id . '&action=edit'); ?>" target="_blank"><?php echo $v->course_title; ?></a>
                                    <?php
                                }

                            ?>
                        </td>
                        */ ?>
                        <td class="file">
                            <?php

                                if ($v->file != '')
                                {
                                    ?>
                                    <a href="<?php echo $this->Attr_Get('plugin_file_uploads_url') . $v->file; ?>" class="file-open <?php if (preg_match("/\.(mp3|mpeg)$/uis", $v->file)){ ?>audio<?php }else{ ?>image<?php } ?>" target="_blank" title="<?php _e('Open file'); ?>"></a>
                                    <?php
                                }

                            ?>
                        </td>
                        <td class="status"><?php ((int)$v->status ? _e('Active') : _e('Disabled')); ?></td>
                        <td class="date"><?php echo date('d.m.Y / H:i', $v->date); ?></td>
                    </tr>
                    <?php
                }

            ?>
            </tbody>
        </table>
        <?php
    }
    else
    {
        include($this->Attr_Get('plugin_templates_path') . 'areas/list-empty.php');
    }

?>

<div class="info bottom">
    <?php

        if ($data['pg']['status'])
        {
            ?>
            <div class="pg">
                <?php echo $data['pg']['html']; ?>
            </div>
            <?php
        }

    ?>
    <div class="count">
        <span><?php _e('Rows found'); ?>: <b><?php echo $data['items_count']; ?></b></span>
    </div>
    <div class="btns">
        <?php

            if (isset($_REQUEST['ddm']))
            {
                ?>
                <input type="button" name="dictionary[row][choose]" value="<?php _e('Choose selected rows for deck'); ?>" />&nbsp;
                <?php
            }
            else
            {
                ?>
                <input type="button" name="dictionary[row][create]" value="<?php _e('Create row'); ?>" />
                <?php
            }

        ?>
    </div>
</div>
