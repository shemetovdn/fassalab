<div class="<?php echo $this->prefix; ?> list-empty">
    <p>
        <?php

            _e('List is empty');
            echo '...';

            if (is_object($this) && isset($this->subaction))
            {
                echo '<br />';

                switch ($this->subaction)
                {
                    case 'decks-list-get':
                        ?>
                        <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_deck_editor'); ?>"><?php _e('Create new deck'); ?></a>
                        <?php
                    break;
                }
            }

        ?>
    </p>
</div>
