<!--<link type="text/css" rel="stylesheet" media="all" href="<?php /*echo $this->Attr_Get('plugin_css_url');*/ ?>main.css" />-->
<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_js_url'); ?>fancybox/jquery.fancybox.css">
<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>admin.css" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>deck-editor.css">
<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>decks-tree-admin.css">

<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>admin.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>deck-editor.js"></script>

<script type="text/javascript">
    window.WTFA_Admin.attr.prefix = '<?php echo $this->Attr_Get('prefix'); ?>';

    window.WTFA_Admin.attr.text.deck_remove = '<?php _e('Voulez-vous supprimer cette deck?'); ?>';
    window.WTFA_Admin.attr.text.dictionary_row_remove = '<?php _e('Voulez-vous définitivement supprimer cette écriture du vocabulaire?'); ?>';
</script>

<div class="wrap">
    <h1><?php echo get_admin_page_title(); ?></h1>
</div>
