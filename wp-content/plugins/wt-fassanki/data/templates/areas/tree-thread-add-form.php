<?php
$difficultyLevels = ['---', 'a1', 'a2', 'b1', 'b2', 'c1', 'c2'];
?>
<h4><?php _e('Creating thread of decks tree'); ?></h4>
<form action="" method="post" name="tree-thread">
    <input type="hidden" name="tree-thread[id]" value="<?php echo $tree_thread['id']; ?>" />

    <fieldset>
        <legend><?php _e('Tree thread title'); ?>:</legend>
        <input type="text" name="tree-thread[title]"  />
        <legend><?php _e('Tree thread price'); ?>:</legend>
        <input type="text" name="tree-thread[price]"  />
        <legend><?php _e('Tree thread Number of scenarios'); ?>:</legend>
        <input type="number" name="tree-thread[number_of_scenarios]"/>
        <legend ><?php _e('Tree thread text popup'); ?>:</legend>
        <textarea type="text" name="tree-thread[text_before_sale]"></textarea>
        <legend><?php _e('Tree thread difficulty levels'); ?>:</legend>
        <select name="tree-thread[difficulty_level]">
            <?php foreach ($difficultyLevels as $key => $level){?>
                <option value="<?php echo $key;?>" <?php echo $key == $tree_thread['difficulty_level'] ? "selected" : ""?>><?php echo $level;?></option>
            <?php }?>
        </select>
        <legend><?php _e('Hidden'); ?>:</legend>
        <label>Yes<input type="radio" name="tree-thread[hidden]"  value="1"/></label>
        <label>No<input type="radio" name="tree-thread[hidden]"  value="0" checked/></label>
    </fieldset>

    <div class="btns">
        <input type="button" name="tree-thread[submit]" value="<?php _e('Save'); ?>" />
        <input type="button" name="tree-thread[cancel]" value="<?php _e('Cancel'); ?>" />
    </div>
</form>
