<h4><?php _e('Remove thread of decks tree'); ?></h4>
<form action="" method="post" name="tree-thread">
    <input type="hidden" name="tree-thread[id]" value="<?php echo $tree_thread['id']; ?>" />

    <fieldset>
        <label>
            <?php _e('You want to remove tail with name '); ?>
            "<?php echo $tree_thread['title']; ?>" <br>
            <?php _e('You delete this and all child threads of decks tree'); ?> <br>
            <?php _e('Are you sure?'); ?>
        </label>
    </fieldset>

    <div class="btns">
        <input type="button" name="tree-thread[submit]" value="<?php _e('Save'); ?>" />
        <input type="button" name="tree-thread[cancel]" value="<?php _e('Cancel'); ?>" />
    </div>
</form>
