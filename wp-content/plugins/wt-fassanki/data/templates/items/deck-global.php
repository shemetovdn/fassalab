<div class="deck deck-0" data-id="0">
    <div class="rw">
        <div class="r1"><b><?php echo $v->title; ?></b></div>
        <div class="r2"><?php _e('Deck shortcode'); ?>: <input type="text" value="[<?php echo $this->prefix; ?>_deck]" readonly="readonly" /></div>
    </div>
    <div class="actions">
        <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_deck_editor&id=0'); ?>" class="edit" title="<?php _e('Edit'); ?>"><?php _e('Edit'); ?></a>
    </div>
</div>
