<div class="deck deck-<?php echo $v->id; ?>" data-id="<?php echo $v->id; ?>">
    <div class="rw">
        <div class="r1"><b><?php echo $v->title; ?></b></div>
        <div class="r2"><?php _e('Deck shortcode'); ?>: <input type="text" value="[<?php echo $this->prefix; ?>_deck id=<?php echo $v->id; ?>]" readonly="readonly" /></div>
        <div class="r3">
            <?php _e('Tree branch'); ?>:
            <?php if (is_array($v->tree_branch) && count($v->tree_branch)) echo $v->tree_branch['title']; ?>
        </div>

        <div class="r4">
            <?php _e('FassLearn step'); ?>:
            <?php if ($fasslearn_title != null) { ?>
                <?php echo $fasslearn_title; ?>
                (<input type="text" value=" [wtfl_lesson id=<?php echo $fasslearn_id; ?>]" readonly="readonly" />)
            <?php } else { ?>
                <strong><?php _e('None'); ?> </strong>
            <?php }?>
        </div>
        <div class="r5">
            <?php if ($videos_id != 0) { ?>
                <i class="dashicons dashicons-video-alt3"></i>
                <?php echo $videos_title; ?>
            <?php } else {?>
                <i class="dashicons dashicons-video-alt3" style="opacity: 0.5"></i>
            <?php } ?>
        </div>
    </div>
    <div class="actions">
        <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_deck_editor&id=' . $v->id); ?>" class="edit" title="<?php _e('Edit'); ?>"><?php _e('Edit'); ?></a>
        <a href="#" class="remove" title="<?php _e('Remove'); ?>"><?php _e('Remove'); ?></a>
    </div>
</div>
