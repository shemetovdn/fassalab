<?php $this->Template_Get('areas/page-admin-head'); ?>

<div id="deck-editor">
    <form action="" method="post" name="deck" enctype="multipart/form-data">
        <input type="hidden" name="_handler" value="<?php echo $this->prefix; ?>" />
        <input type="hidden" name="deck[id]" value="<?php echo $data->id; ?>" />
        <fieldset class="deck">
            <fieldset>
                <legend><?php _e('Title of deck'); ?>:</legend>
                <div><input type="text" name="deck[title]" value="<?php echo $data->title; ?>" /></div>
            </fieldset>

            <fieldset>
                <legend><?php _e('Description of deck'); ?>:</legend>
                <div><textarea name="deck[description]"><?php echo $data->description; ?></textarea></div>
            </fieldset>
            <?php

                if (!isset($data->id) || $data->id)
                {
                    if (isset($data->tree) && is_array($data->tree) && count($data->tree))
                    {
                        ?>
                        <fieldset>
                            <legend><?php _e('Tree thread'); ?>:</legend>
                            <div>
                                <select name="deck[tree-thread]">
                                    <option value="">- <?php _e('non sélectionnée'); ?> -</option>
                                    <?php

                                        foreach ($data->tree as $k => $v)
                                        {
                                            ?>
                                            <option value="<?php echo $v['id']; ?>" <?php if ($v['id'] == $data->tree_branch_id){ ?>selected="selected"<?php } ?>>
                                                <?php

                                                    if ($v['depth'])
                                                    {
                                                        for ($i = 0; $i < $v['depth']; $i++)
                                                        {
                                                            echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                                                        }
                                                    }

                                                ?>
                                                <?php echo $v['title']; ?>
                                            </option>
                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </fieldset>
                        <?php
                    }

                    ?>
                    <fieldset class="items">
                        <legend><?php _e('Items'); ?>:</legend>
                        <p class="submit top">
                            <input type="button" name="deck[item-new]" class="button" value="<?php _e('Add new item'); ?>">
                        </p>
                        <input type="hidden" name="deck[dictionary]" value="" />
                        <div class="items-list"></div>
                        <p class="submit bottom">
                            <input type="button" name="deck[item-new]" class="button" value="<?php _e('Add new item'); ?>">
                        </p>
                    </fieldset>


                    <fieldset class="fasslearn-item">
                        <legend><?php _e('Fasslearn block'); ?>:</legend>
                        <select name="deck[fasslearn-item]" >
                            <option hidden value="0"><?php _e('Select Fasslearn items'); ?></option>
                            <?php
                            if (isset($data->lessons) && is_array($data->lessons) && count($data->lessons))
                            {
                                foreach ($data->lessons as $lesson)
                                {
                                    ?>
                                    <option value="<?php echo $lesson->id; ?>" <?php echo ($lesson->id == $data->fasslearn_item) ? 'selected': '' ?>>
                                        <?php echo $lesson->title.' &emsp;(id='.$lesson->id.')'; ?>
                                    </option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </fieldset>


                    <fieldset class="videos-item">
                        <legend><?php _e('Videos items'); ?>:</legend>
                        <select name="deck[videos-item]" >
                            <option hidden value="0"><?php _e('Select videos item'); ?></option>
                            <?php
                            if (isset($data->videos) && is_array($data->videos) && count($data->videos))
                            {
                                foreach ($data->videos as $video)
                                {
                                    ?>
                                    <option value="<?php echo $video->id; ?>" <?php echo ($video->id == $data->videos_item) ? 'selected': '' ?>>
                                        <?php echo $video->title.' &emsp;(id='.$video->id.')'; ?>
                                    </option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </fieldset>

                    <fieldset class="items">
                        <legend><?php _e('In News'); ?>:</legend>
                        <label>Yes<input type="radio" name="deck[special]" value="1"
                            <?php echo !empty($data->special) ? "checked" : "";?>/></label>
                        <label>No<input type="radio" name="deck[special]" value="0"
                                <?php echo empty($data->special) ? "checked" : "";?>/></label>
                    </fieldset>

                    <?php
                }

            ?>

        <p class="submit">
            <?php

                if (isset($data->id))
                {
                    ?>
                    <input type="submit" name="deck[save]" class="button button-primary" value="<?php _e('Sauvegarder les changements'); ?>" />
                    <?php
                }
                else
                {
                    ?>
                    <input type="submit" name="deck[submit]" class="button button-primary" value="<?php _e('Create new deck'); ?>" />
                    <?php
                }

            ?>
        </p>
    </form>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFA_Deck_Editor.init();
    });
</script>
