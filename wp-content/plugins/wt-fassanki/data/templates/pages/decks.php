<?php $this->Template_Get('areas/page-admin-head'); ?>

<div id="<?php echo $this->prefix; ?>-decks">
    <div class="decks-tree"></div>
    <div class="decks-list"></div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFA_Admin.decks.tree.draw();
        window.WTFA_Admin.decks.list_get('all');
    });
</script>
