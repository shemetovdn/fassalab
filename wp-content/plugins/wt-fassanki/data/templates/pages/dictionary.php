<?php

    $this->Template_Get('areas/page-admin-head');

?>
<div id="<?php echo $this->prefix; ?>-dictionary">
    <form action="" method="post" name="dictionary">
        <div class="search">
            <input type="text" name="search" value="" />
        </div>
        <div class="result-list">
            <table></table>
        </div>
        <div class="table"></div>
    </form>
</div>
<?php

if (isset($_REQUEST['ddm']))
{
?>
    <script>
        localStorage.setItem('ajaxUrl', "<?php echo admin_url( 'admin-ajax.php' ) . "?ddm=1";?>");
    </script>
<?php
} else { ?>
    <script>
        localStorage.removeItem('ajaxUrl');
    </script>
    <?php
}
?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFA_Admin.init.search_dictionary();
        window.WTFA_Admin.dictionary.table_get();
    });
</script>
