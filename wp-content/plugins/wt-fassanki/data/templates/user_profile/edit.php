<div class="select-branch-holder">
    <h3>Select branch</h3>

    <div class="panel">
        <h4>Open</h4>
        <ul class="left">
            <?php foreach ($access_branches as $branch) { ?>
                <li><input type="hidden"
                           value="<?php echo $branch['id']; ?>"
                           name="access_branches[]"
                           >
                    <?php echo $branch['title']; ?>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="panel">
        <h4>Close</h4>
        <ul class="right" disabled>
            <?php foreach ($decks_tree as $branch) { ?>
                <li><input type="hidden"
                           value="<?php echo $branch['id']; ?>"
                           name="access_branches[]"
                           disabled>
                    <?php echo $branch['title']; ?>
                </li>
            <?php } ?>
        </ul>
    </div>

    <style>
        .select-branch-holder .panel{
            display: inline-block;
            width: 40%;
            vertical-align: top;
        }
        .select-branch-holder .panel h4{
            text-align: center;
        }
        .select-branch-holder ul {
            background: #fff;
            min-height: 45px;
            padding: 2px;
        }
        .select-branch-holder ul li{
            background: #eee;
            border-radius: 4px;
            padding: 5px 10px;
            margin-bottom: 2px;
        }
        .select-branch-holder ul li:last-child{
            margin-bottom: 0;
        }
    </style>
    <script>
        jQuery(document).ready(function () {
            // setInterval(function (e) { // todo to fix
            //     jQuery('.select-branch-holder ul.left input').attr('disabled', 'disabled');
            //     jQuery('.select-branch-holder ul.right input').attr('disabled', null)
            // }, 500);
            jQuery( ".select-branch-holder ul.right, .select-branch-holder ul.left" ).sortable({
                connectWith: "ul",
                update: function( event, ui ) {
                    jQuery('.select-branch-holder ul.right input').attr('disabled', 'disabled');
                    jQuery('.select-branch-holder ul.left input').attr('disabled', null)
                }
            });

        })
    </script>

</div>
