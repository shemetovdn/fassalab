
<div class="hide" style="">
<h3>Get Serification</h3>

<a href="#" target="_blank" class="downloadSerification" data-id="<?php echo $user->ID; ?>">show statistic</a>
    <p><a id="download-statistics" target="_blank" href="<?php echo admin_url('admin.php?page=pdf_exp&user_id=' . $user->ID); ?>">download statistic</a></p>
    <style>
        .modal{
            display: none;
            position: fixed;
            z-index: 1;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        .modal > ul{
            padding: 5px 20px;
            background: #fff;
            border: solid 1px;
            border-radius: 5px;
        }
        .modal > ul > li:last-child{
            text-align: right;
        }
    </style>
    <div class="modal">
        <ul>
            <li>a fait <span class="stepLerned">x</span> exercices</li>
            <li>a débloqué <span class="wordLerned">x</span> mots de vocabulaire</li>
            <li>a débloqué la carte de compétence <span class="branchesUnlocked">x</span></li>
            <li><p class="button close">Close</p></li>
        </ul>
    </div>
<script>
    jQuery(document).ready(function (e) {
        downloadSerification();

        jQuery.ajax({
            type: 'POST',
            url: '<?php echo admin_url( 'admin-ajax.php' );?>',
            data: {
                action: 'get_certification',
                userId: <?php echo $user->ID; ?>
            },
            success: function(data){
             $('#download-statistics').attr('href', $('#download-statistics').attr('href') + '&statistics=' + data)
            },
            error: function(){

            }
        });
    });
    function downloadSerification() {
        jQuery('.downloadSerification').on('click', function (e) {
            e.preventDefault();
            user_id = jQuery(this).attr('data-id');
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo admin_url( 'admin-ajax.php' );?>',
                data: {
                    action: 'get_certification',
                    userId: user_id
                },
                success: function(data){

                    data = JSON.parse(data);
                    $('.stepLerned').text(data.steps.stepLerned);
                    $('.wordLerned').text(data.words.wordLerned);
                    $('.branchesUnlocked').text(data.branchesUnlocked);
                    $('.modal').show('show');

                },
                error: function(){

                }
            });
        })

        $('.close').click(function () {
            $('.modal').hide('show');
        })
    }
</script>
</div>