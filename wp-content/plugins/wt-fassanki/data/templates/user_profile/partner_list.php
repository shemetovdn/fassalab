

<div class="select-partner-holder">
    <h3><?php _e('Select partner of this user') ?></h3>
    <select name="partner">
        <option value="0" ><?php _e('No Partner'); ?></option>
        <?php foreach ($list_of_partners as $partner){ ?>
            <option value="<?php echo $partner->ID; ?>" <?php echo ($partner->ID == $current_parner->ID) ? 'selected' : ''; ?>>
                <?php echo $partner->user_login.' ( '.$partner->user_url.' )'; ?>
            </option>
        <?php } ?>
    </select>

</div>

<style>
    .select-partner-holder select{
        min-width: 40%;
    }
</style>