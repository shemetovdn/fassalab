<?php

    //
    include_once($this->core->Attr_Get('plugin_core_path') . 'user.php');

    global $wtfapi_user;
    $wtfapi_user = new WT_Fass_API_User($this->core);
    ////

    if (($user_id = $wtfapi_user->Token_Login($data['token'])) !== false)
    {
            if (class_exists(UserLog)) {
                $userLogger = new UserLog();
                $userLogger->logAction($user_id,$this->subaction);
            }
        $wtfapi_user->Token_DT_Update($data['token']);

        //
        include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
        include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');

        $_wtfa = new WT_Fass_Anki();
        $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);
        ////

        switch ($this->subaction)
        {

            /* { "_action": "deck", "_subaction": "get-tree-branches", "deck": { "token": "{{ token  }}" } } */
            case 'get-tree-branches':
                $this->result['tree-branches'] = $wtfa_deck->Tree_Branches_Get($user_id, (boolean)$data['hierarchy'],false, $data['token'] );
                $this->result['news'] = $wtfa_deck->getNews();
                $this->result['status'] = true;
            break;

            /* {"_action": "deck","_subaction": "get-words-list","deck": {"token": "{{ token  }}"}} */
            case 'get-words-list':
                $this->result['tree-branches'] = $wtfa_deck->getWordsList($user_id, (boolean)$data['hierarchy'],$data['token'] );
                $this->result['status'] = true;
                break;

            /* {"_action": "deck","_subaction": "get-words-list","deck": {"token": "{{ token  }}"}} */
            case 'reset-deck-words':
                $this->result['status-query'] = $wtfa_deck->resetDeckWords($user_id, $data['deck-id'] );
                $this->result['status'] = true;
                break;
        }
    } else {
        if ($this->subaction !=  "login") {
            $this->result['error']['code'] = 'token';
            $this->result['error']['token'] = __('wrong ID');
        }

    }
