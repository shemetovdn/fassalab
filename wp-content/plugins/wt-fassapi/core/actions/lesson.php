<?php

//
include_once($this->core->Attr_Get('plugin_core_path') . 'user.php');

global $wtfapi_user;
$wtfapi_user = new WT_Fass_API_User($this->core);
////

if (($user_id = $wtfapi_user->Token_Login($data['token'])) !== false)
{
    if (class_exists(UserLog)) {
        $userLogger = new UserLog();
        $userLogger->logAction($user_id,$this->subaction);
    }
    $wtfapi_user->Token_DT_Update($data['token']);

    //
    include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->fasslearn_plugin_fn. '/core.php');
    include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->fasslearn_plugin_fn . '/core/lesson.php');

    $_wtfl = new WT_Fass_Learn();
    $wtfl_lesson = new WT_Fass_Learn_Lesson($_wtfl);
    ////

    switch ($this->subaction)
    {
        /* {"_action": "lesson","_subaction": "get-lesson","lesson": {"token": "{{ token  }}","deck-id": "42","lesson-id": "110"}} */
        case 'get-lesson':

            $data['deck-id'] = (int)$data['deck-id'];
            $data['lesson-id'] = (int)$data['lesson-id'];

            if ($data['deck-id'] < 0) {
                $this->result['error']['deck-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'deck-id';
            }

            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');

            $_wtfa = new WT_Fass_Anki();
            $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);
            if ($data['lesson-id'] != $wtfa_deck->Get_FassLearn_by_Deck($data['deck-id'])) {
                $this->result['error']['lesson-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'lesson-id';
            }

            if (!count($this->result['error']))
            {
                /*
                 * if open this deck
                 * if end of this deck words
                 */
                if( true /*$wtfa_deck->Passed_User_Words_Status_Get($user_id, $data['deck-id'])*/){
                    // check if status more 90 percents
                    if ($wtfa_deck->Percent_Passed_User_Status_Get($user_id, $data['deck-id']) > 90) {
                        //                            $this->result['lesson'] = $wtfl_lesson->Get($data['lesson-id']);
                        $this->result['lesson'] = $wtfl_lesson->Get_Api($data['lesson-id']);
                        $this->result['status'] = true;
                    } else {
                        $this->result['error']['lesson-id'] = __('Access denied. Go through all the words first. Now:');
                        $this->result['error']['Percent_Passed_User_Status_Get'] =
                            $wtfa_deck->Percent_Passed_User_Status_Get($user_id, $data['deck-id']);
                        $this->result['error']['code'] = 'lesson-id';
                    }
                } else {
                    $this->result['error']['deck-id'] = __('Access denied.');
                    $this->result['error']['code'] = 'deck-id';
                }
            }
            break;

        /* {"_action": "lesson","_subaction": "get-step","lesson": {"token": "{{ token  }}","deck-id": "42","lesson-id": "110"}} */
        case 'get-step':
            $data['deck-id'] = (int)$data['deck-id'];
            $data['lesson-id'] = (int)$data['lesson-id'];
            $data['step-id'] = (int)$data['step-id'];

            if ($data['deck-id'] < 0) {
                $this->result['error']['deck-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'deck-id';
            }

            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');

            $_wtfa = new WT_Fass_Anki();
            $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);
            if ($data['lesson-id'] != $wtfa_deck->Get_FassLearn_by_Deck($data['deck-id'])) {
                $this->result['error']['lesson-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'lesson-id';
            }

            if (false){ // todo debug mode
//            if(!$wtfa_deck->Passed_User_Words_Status_Get($user_id, $data['deck-id'])){ // to do change to if passed words.
                $this->result['error']['deck-id'] = __('Not passed.');
                $this->result['error']['code'] = 'deck-id';
            }

            if (!count($this->result['error'])){


                $_passed_list = $wtfa_deck->getPassedListByDeck($user_id, $data['deck-id']);

                $passed_fl = 0;
                if ($_passed_list['fasslearn_passed'] == 1) {$passed_fl = 1;};

                $this->result['step'] = $wtfl_lesson->Get_Next_Step($data['lesson-id'], $data['token'], $data['step-id'], $passed_fl);

                $this->result['progress'] = $wtfl_lesson->Lesson_User_Progress_Get($data['lesson-id'], $data['token']);

                $this->result['status'] = true;
            }

        break;

        /* { "_action": "lesson", "_subaction": "step-send", "lesson": { "token": "{{ token  }}", "deck-id": "42", "lesson-id": "110", "step-id": "1270", "answer" : 1 } } */
        case 'step-send':
            $data['deck-id'] = (int)$data['deck-id'];
            $data['lesson-id'] = (int)$data['lesson-id'];
            $data['step-id'] = (int)$data['step-id'];
            $data['answer'] = boolval($data['answer']);

            if ($data['deck-id'] < 0) {
                $this->result['error']['deck-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'deck-id';
            }
            if ($data['lesson-id'] < 0) {
                $this->result['error']['lesson-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'lesson-id';
            }
            if ($data['step-id'] < 0) {
                $this->result['error']['step-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'step-id';
            }
            if (!is_bool($data['answer'])) {
                $this->result['error']['answer'] = __('Invalid type');
                $this->result['error']['code'] = 'answer';
            }

            // if exist in this deck -> lesson this step ?

            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');
            $_wtfa = new WT_Fass_Anki();
            $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);

            $step = $wtfl_lesson->Step_Get($data['step-id']);
            if ($step->lesson_id != $data['lesson-id']) {
                $this->result['error']['step-id'] = __('Invalid ID (not this lesson)');
                $this->result['error']['code'] = 'step-id';
            }

            if ($data['lesson-id'] != $wtfa_deck->Get_FassLearn_by_Deck($data['deck-id'])) {
                $this->result['error']['lesson-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'lesson-id';
            }

            if (!count($this->result['error']))
            {
                // if set new answer in DB
                // change work to

                $passed_before = $wtfa_deck->getPassedListByDeck($user_id, $data['deck-id']);
                $wtfl_lesson->Passing($step->lesson_id, $step->id, $data['answer'], $data['token']);

                $this->result['progress'] = $wtfl_lesson->Lesson_User_Progress_Get($step->lesson_id, $data['token']);

                $this->result['next-step'] = $wtfl_lesson->Get_Next_Step($data['lesson-id'], $data['token'], null, $passed_before['fasslearn_passed'], $step->id  );

                if ($this->result['next-step'] == null) { // if empty next step passed fasslearn of this deck
                    $wtfl_lesson->Passing_Fasslearn($user_id, $data['deck-id']);

                }
                $wtfa_deck -> Passing_Check($user_id, $data['deck-id'] );

                $passed_after = $wtfa_deck->getPassedListByDeck($user_id, $data['deck-id']);

                if (($passed_before['fasslearn_passed'] == 0) && ($passed_after['fasslearn_passed'] == 1)) { // first fin
                    $this->result["fin"] = true;
                }

                $this->result['status'] = true;
            }

            break;
    }

} else {
    $this->result['error']['token'] = __('wrong ID');
    $this->result['error']['code'] = 'token';

}
