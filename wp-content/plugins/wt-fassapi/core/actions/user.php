<?php

    //
    include_once($this->core->Attr_Get('plugin_core_path') . 'user.php');

    global $wtfapi_user;
    $wtfapi_user = new WT_Fass_API_User($this->core);
    ////

    //
    $wtfapi_user->Clear_Expired_Tokens();
    ////
if (($user_id = $wtfapi_user->Token_Login($data['token'])) !== false)
{

    if (class_exists(UserLog) && $this->subaction != "set-session-start-time") {
        $userLogger = new UserLog();
        $userLogger->logAction($user_id,$this->subaction);
    }
    $wtfapi_user->Token_DT_Update($data['token']);
    $user = $wtfapi_user->Get_Info($user_id);

    switch ($this->subaction)
    {
        //{"_action": "user","_subaction": "get-info","user": {"token": "{{ token  }}"}}
        case 'get-info':
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');

            $_wtfa = new WT_Fass_Anki();
            $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);

            $this->result['user'] = array(
                'id'                => (int)$user['ID'],
                'login'             => $user['user_login'],
                'display_name'      => $user['display_name'],
                'email'             => $user['user_email'],
                'registered'        => $user['user_registered'],
                'global-statistic'  => $wtfa_deck->User_Statistic_Get($user_id),
                'global-statistic-new'  => $wtfa_deck->User_Statistic_Get_API($user_id)
            );

            $this->result['status'] = true;
            break;
        case 'set-session-start-time':
            if (class_exists(UserLog)) {
                $userLogger = new UserLog();
                $userLogger->logStartSesion($user['ID'],$this->subaction);
            }

            $this->result['status'] = true;
            break;

        //{"_action": "user","_subaction": "get-deck","user": {"token": "{{ token  }}"}}
        case 'get-deck-word':
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');

            $_wtfa = new WT_Fass_Anki();
            $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);
            $data['deck-id'] = (int)$data['deck-id'];
            if ($data['deck-id'] < 0) $data['deck-id'] = 0;
            $words = array();

            $wtfa_deck -> Passing_Check($user_id, $data['deck-id'] );

            if ($data['deck-id'] == 0)
            {
                $deck_global = (array)get_option($_wtfa->Attr_Get('prefix') . '_deck_global');

                $deck_data = new stdClass();
                $deck_data->id = 0;
                $deck_data->title = $deck_global['title'];
                $deck_data->description = $deck_global['description'];
                $deck_data->passed = 0;
                $deck_data->open = 1;
            }
            else
            {
                $deck_data = $wtfa_deck->Get($data['deck-id']);
                $deck_data->passed = $wtfa_deck->Passed_User_Status_Get($user_id, $data['deck-id']);
                $deck_data->open = $wtfa_deck->Open_User_Status_Get($user_id, $data['deck-id']);
            }

            if (!is_object($deck_data) || !isset($deck_data->id)) {
                $this->result['error']['deck-id'] = __('Deck is not found');
                $this->result['error']['code'] = 'deck-id';
            }

            if (!count($this->result['error']))
            {

                /*
                 * todo :
                 * if word-id == -1
                 *      if deck is passed -> send 1st word
                 *      if deck not passed -> send word with lvl null
                 *      if deck not passed AND not null-> send word with lvl 4
                 * */


                $words = $wtfa_deck->Dictionary_Get_Words($data['deck-id']);
                $all_word = $wtfa_deck ->Pretty_Dictionary_Get ($wtfa_deck->Dictionary_Get($data['deck-id']), $data['deck-id']);

//                        $deck_dictionary = $wtfa_deck->Dictionary_Get($data['deck-id']);

                $this->result['word'] =  array();

                if ($data['word-id'] != -1) { // get random word from deck

                    foreach ($all_word  as $word) {
//                                if ($word['id'] == $data['word-id']) {
                        if ($word->id == $data['word-id']) {
                            $this->result['word'] = $word;
                        }
                    }

                } else {
                    if (count($words))
                    {
                        shuffle($words);
                        (array)$words[0]['status'] = (int)(array)$words[0]['status'];
                        $this->result['word'] = (array)$words[0];
//                            } else {
//                                $this->result['word'] = array();
                    } else {
                        $this->result['word'] = (array)$all_word[0];
                    }
                }

                $ppusg = $wtfa_deck->Percent_Passed_User_Status_Get($user_id, $data['deck-id']);
                $deck_data->percent =($ppusg['deck_passed'] / $ppusg['deck_words']) * 100;

                $this->result['deck'] = array(
                    'id'             => (int)$deck_data->id,
                    'passed'         => $deck_data->passed,
                    'open'           => $deck_data->open,
//                            'percent'        => $deck_data->percent ,
//                            'words-count'    => $wtfa_deck->Dictionary_Words_Count($deck_data->id),
//                            'user-statistic' => $wtfa_deck->User_Statistic_Get($user_id, $deck_data->id)
                );
                $dictionary = $wtfa_deck -> Dictionary_Status_Get($user_id, $deck_data->id);
                $this->result['deck']['dictionary'] = $dictionary;

                // Messages
                if (!count($this->result['word'])) {
                    $this->result['word'] = null;
                }
                if (!count($this->result['word']) && $data['word-id'] == -1)
                {
                    $this->result['message'] = array();
                    $this->result['message']['caption'] = __('Fin');
                    $this->result['message']['body'] = array();

                    if ($deck_data->passed)
                    {
                        $this->result['message']['body'][] = __("L'entraînement de cette leçon est terminé") . '.';
                        $this->result['message']['body'][] = __("Vous avez découvert tous les mots, bravo") . ' !';
                        $this->result['message']['body'][] = __("Vous pourrez revenir ici pour réviser") . '.';
                    }
                    else
                    {
                        $this->result['message']['body'][] = __("Le pourcentage des mots indiqués comme \"facile\" doit être supérieur à 90% pour passer à la leçon suivante") . '.';
                        $this->result['message']['body'][] = __("Entraînez-vous davantage pour apprendre les mots de cette leçon et débloquer la prochaine étape") . '.';
                    }

                    $this->result['message']['body'] = implode("\n", $this->result['message']['body']);
                }

                $this->result['status'] = true;
            }
            //}
            break;

        // { "_action": "user", "_subaction": "word-send", "user": { "token": "{{ token  }}", "deck-id": "42", "word-id": "1329", "lvl" : "5" } }
        case 'word-send': // todo check if deck is open
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');

            $_wtfa = new WT_Fass_Anki();
            $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);
            $data['deck-id'] = (int)$data['deck-id'];
            $data['word-id'] = (int)$data['word-id'];
            $data['lvl'] = (int)$data['lvl'];

            if ($data['deck-id'] < 0) {
                $this->result['error']['deck-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'deck-id';
            }
            if (!$data['word-id']) {
                $this->result['error']['word-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'word-id';
            }
//                if (!in_array($data['lvl'], [1, 2, 3])) $this->result['error']['level'] = __('Invalid level');
            if (!in_array($data['lvl'], [4, 5])) {
                $this->result['error']['lvl'] = __('Invalid level');
                $this->result['error']['code'] = 'lvl';
            }

            if ($data['deck-id'] == 0) /*todo - unduplicate!!!*/
            {
                $deck_global = (array)get_option($_wtfa->Attr_Get('prefix') . '_deck_global');

                $deck_data = new stdClass();
                $deck_data->id = 0;
                $deck_data->title = $deck_global['title'];
                $deck_data->description = $deck_global['description'];
                $deck_data->word_block_passed = 0;
                $deck_data->open = 1;
            }
            else
            {
                $deck_data                    = $wtfa_deck->Get($data['deck-id']);
                $deck_data->word_block_passed = $wtfa_deck->Passed_User_Words_Status_Get($user_id, $data['deck-id']);
                $deck_data->open              = $wtfa_deck->Open_User_Status_Get($user_id, $data['deck-id']);
            }

            /*$deck_data
                ["id"]
                ["tree_branch_id"]
                ["title"]
                ["description"]
                ["status"]
                ["position"]
                ["date"]
                ["fasslearn_item"]
                ["passed"]
                ["open"]
             * */

            if (!is_object($deck_data) || !isset($deck_data->id)) {
                $this->result['error']['deck-id'] = __('Deck is not found');
                $this->result['error']['code'] = 'deck-id';
            }

            if (!count($this->result['error']))
            {

                $wtfa_deck->Words_Data_User_Update($user_id, $data['deck-id'], $data['word-id'], $data['lvl']);
                $wtfa_deck -> Passing_Check($user_id, $data['deck-id'] );
                $words = $wtfa_deck->Dictionary_Get_Words($data['deck-id']);

                if (count($words)) {
                    shuffle($words);
                    (array)$words[0]['status'] = (int)(array)$words[0]['status'];
                    $this->result['word'] = (array)$words[0];
                } else {
                    if ($wtfa_deck->Passed_User_Words_Status_Get($user_id, $data['deck-id']) == 1) {
                        $all_word = $wtfa_deck->Pretty_Dictionary_Get($wtfa_deck->Dictionary_Get($data['deck-id']), $data['deck-id']);
                        foreach ($all_word as $key => $word) {
                            if ($word->id == $data['word-id']) {
                                if (isset($all_word[ $key + 1 ])) {
                                    $this->result['word'] = $all_word[ $key + 1 ];

                                } else {
                                    $this->result['word'] = $all_word[0];
                                }
                                break 1;
                            }
                        }
                    } else {
                        $this->result['word'] = null;
                    }
                }

                $ppusg = $wtfa_deck->Percent_Passed_User_Status_Get($user_id, $data['deck-id']);
                $deck_data->percent =($ppusg['deck_passed'] / $ppusg['deck_words']) * 100;

                $deck_data->word_block_passed_upd = $wtfa_deck->Passed_User_Words_Status_Get($user_id, $data['deck-id']);


                if (/*($deck_data->passed_upd != $deck_data->passed) &&*/
                    ($deck_data->word_block_passed == 0) &&
                    ($deck_data->word_block_passed_upd == 1)/*&& is_null ($this->result['word']) */
                ) {
                    $this->result["fin"] = true;
                }

                $dictionary = $wtfa_deck -> Dictionary_Status_Get($user_id, $deck_data->id);

                $deck_data->passed = $wtfa_deck->Passed_User_Status_Get($user_id, $data['deck-id']);

                $this->result['deck'] = array(
                    'id'             => (int)$deck_data->id,
                    'passed'         => $deck_data->passed,
                    'open'           => $deck_data->open,
//                        'percent'        => $deck_data->percent , // todo del
//                        'words-count'    => $wtfa_deck->Dictionary_Words_Count($deck_data->id), // todo del
//                        'user-statistic' => $wtfa_deck->User_Statistic_Get($user_id, $deck_data->id), // todo del
                );
//                    $this->result['deck']['words'] = array(
//                        'block-passed'   => $deck_data->word_block_passed_upd,
//                        'words-count'    => $wtfa_deck->Dictionary_Words_Count($deck_data->id),
//                        'percent'        => $deck_data->percent,
//                    );

                $this->result['deck']['dictionary'] = $dictionary;
//                    $this->result['info!!!'] = 'todel : deck.words-count + deck.user-statistic + deck.words';

                $this->result['status'] = true;
            }
            break;

        /* { "_action": "user", "_subaction": "set-points", "user": { "token": "{{ token  }}", "points": "12", "level" : "test", "loginBonusLastDate": "000030303" } }*/
        case 'set-points':
            include_once($this->core->Attr_Get('plugin_core_path') . 'point.php');
            $wtfapi_point = new WT_Fass_API_Point($this->core);

            if ($data['points'] < 0) {
                $this->result['error']['points'] = __('Invalid points');
                $this->result['error']['code'] = 'points';
            }

            if (!$wtfapi_point -> is_timestamp($data['loginBonusLastDate'])) {
                $this->result['error']['loginBonusLastDate'] = __('Invalid loginBonusLastDate');
                $this->result['error']['code'] = 'loginBonusLastDate';
//                    $this->result['error']['debug'] = $data;
            }

            if (!count($this->result['error'])){
                if ($wtfapi_point->SetPoints($user_id, $data['points'], $data['level'], $data['loginBonusLastDate'])) {
                    $this->result['status'] = true;
                } else {
                    $this->result['error']['SetPoints'] = __('Invalid add to DB');
                    $this->result['error']['code'] = 'SetPoints';
                }
            }

            break;

        /* { "_action": "user", "_subaction": "get-points", "user": { "token": "{{ token  }}" } } */
        case 'get-points':
            include_once($this->core->Attr_Get('plugin_core_path') . 'point.php');
            $wtfapi_point = new WT_Fass_API_Point($this->core);

            $this->result['points-block'] = $wtfapi_point ->GetPoints($user_id);
            $this->result['status'] = true;

            break;


        case 'dev' :
            /* // del users
            require_once(ABSPATH.'wp-admin/includes/user.php');
            $mail = $data['mail'];

            $args = array(
                'exclude' => array(777),
                'search'  => '*' . $mail,
                'number' => 30,
                'paged'=>1,
                'role'=>'subscriber',
                'orderby'=>'registered',
            );
            $blogusers = get_users($args );//'search=*'.$mail.'&number=40&paged=1&role=subscriber&orderby=registered'
            echo 'search = *'.$mail.PHP_EOL;
            echo count(get_users('search=*'.$mail));
            foreach ($blogusers as $user) {
                echo '<li>' .$user->user_registered. ' delete: ';
//                    $del = wp_delete_user($user->id, 777 );
//                    echo $del . ' -->';
                echo $user->id. ' ' . $user->user_email. '</li>';
                    }
die();
*/
            $this->result['dev'] = 'dev';
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->fasslearn_plugin_fn. '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->fasslearn_plugin_fn . '/core/lesson.php');
            $_wtfl = new WT_Fass_Learn();
            $wtfl_lesson = new WT_Fass_Learn_Lesson($_wtfl);

            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core ->anki_plugin_fn . '/core.php');
            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');
            $_wtfa = new WT_Fass_Anki();
            $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);
            $this->result['lesson'] = $wtfl_lesson->Get_Api(201);




            $this->result['status'] = true;
            break;

    }
} else {
    if ($this->subaction !=  "login" &&
        $this->subaction !=  "registration" &&
        $this->subaction !=  "password-reset" ) {
        $this->result['error']['token'] = __('wrong ID');
        $this->result['error']['code'] = 'token';
    }

}
    switch ($this->subaction)
    {
        case 'set-cerdentials':
            $user = get_userdata($user_id);

            if (!is_email($data['email'])) {
                $this->result['error']['email'] = 'bad_email'; //__('E-mail is not valid');
                $this->result['error']['code'] = 'email';
            }
            else
                if ($user->user_email != $data['email'] && email_exists($data['email'])) {
                    $this->result['error']['email'] = 'used';//__('This E-mail is already in use');
                    $this->result['error']['code'] = 'email';
                }

            if (!empty($data['password'])) {
                if (mb_strlen($data['password']) < 6) {
                    $this->result['error']['password'] = 'wrong_password'; //__('Le mot de passe doit contenir 6 caractères minimum');
                    $this->result['error']['code'] = 'password';
                }
                else
                    if ($data['password'] != $data['repeat_password']) {
                        $this->result['error']['repeat_password'] = 'wrong_password_repeat';//__('Wrong password repeat');
                        $this->result['error']['code'] = 'repeat_password';
                    }
                }
            if (!wp_check_password( $data['current_password'], $user->data->user_pass)) {
                $this->result['error']['current_password'] = 'wrong_current_password';//__('Wrong password repeat');
                $this->result['error']['code'] = 'current_password';
            }
            if (!count($this->result['error']))
            {
                $userData = (object)['ID' => $user_id];
                if (!empty($data['email']) && $user->user_email != $data['email']) {
                    $userData->user_email = $data['email'];
                }
                if (!empty($data['password'])) {
                    $userData->user_pass = $data['password'];
                }
                $user_id = wp_update_user($userData);
                if ( is_wp_error( $user_id ) ) {
                    $this->result['error']['code'] = 'update_fail';
                }
                else {
                    $this->result['status'] = true;
                }
            }

        break;
        case 'registration':
            if (!validate_username($data['login'])) {
                $this->result['error']['login'] = 'bad_login'; //__('Login is not valid');
                $this->result['error']['code'] = 'login';
            }
            else
            if (username_exists($data['login'])) {
                $this->result['error']['login'] = 'used';// __('This login is already in use');
                $this->result['error']['code'] = 'login';
            }

            if (!is_email($data['email'])) {
                $this->result['error']['email'] = 'bad_email'; //__('E-mail is not valid');
                $this->result['error']['code'] = 'email';
            }
            else
            if (email_exists($data['email'])) {
                $this->result['error']['email'] = 'used';//__('This E-mail is already in use');
                $this->result['error']['code'] = 'email';
            }

            if (mb_strlen($data['password']) < 6) {
                $this->result['error']['password'] = 'wrong_password'; //__('Le mot de passe doit contenir 6 caractères minimum');
                $this->result['error']['code'] = 'password';
            }
            else
            if ($data['password'] != $data['rpassword']) {
                $this->result['error']['rpassword'] = 'wrong_password_repeat';//__('Wrong password repeat');
                $this->result['error']['code'] = 'rpassword';
            }
            if(isset($data['partner'])) {
                $partner = get_user_by( 'login' ,$data['partner'] );

                if(!$partner) {
                    $this->result['error']['partner'] = 'wrong_partner';
                    $this->result['error']['code'] = 'partner';
                } else  if(!in_array('partner', $partner->roles )) {
                    $this->result['error']['partner'] = 'wrong_partner_role';
                    $this->result['error']['code'] = 'partner';
                }
//                echo '<pre>';
//                var_dump($this->result);
//                var_dump($partner->user_login);
//                var_dump($partner);
//                echo '</pre>';
//                die();
            }

            if (!count($this->result['error']))
            {
                $user = wp_create_user($data['login'], $data['password'], $data['email']);

                if (is_wp_error($user))
                {
                    $this->result['error']['user'] = 'not_created'; //__('User has not been created');
                    $this->result['error']['code'] = 'user';
                }
                else
                {
                    $this->result['id'] = (int)$user;

                    update_user_meta($this->result['id'], 'upme_user_profile_status', 'ACTIVE');

                    if ($partner ) {
                        update_user_meta($this->result['id'], 'partner', $partner->user_login);
                        $this->result['partner'] = $partner->nickname;
                    }


                    $this->result['authorization'] = $this->result['status'] = $this->core->API_Call('user', 'login', array(
                        'login'    => $data['login'],
                        'password' => $data['password']
                    ), true);

                    $this->result['status'] = true;
                }
            }
        break;

        case 'password-reset':
            if (!is_email($data['email'])) {
                $this->result['error']['email'] = 'not_found';//__('E-mail is not valid');
                $this->result['error']['code'] = 'email';
            }
            else
            if (!email_exists($data['email'])) {
                $this->result['error']['email'] = 'not_found';//__('E-mail is not valid');
                $this->result['error']['code'] = 'email';

            }

            if (!count($this->result['error']))
            {
                $user = get_user_by('email', $data['email']);

//                if (is_wp_error($user))
//                {
//                    $this->result['error']['password'] = __('Password has not been reset');
//                    $this->result['error']['code'] = 'password';
//                }
//                else
//                {
                    $password = substr(uniqid(), -6);

                    wp_set_password($password, (int)$user->ID);

                    add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));

                    $mail = array(
                        'to'      => $data['email'],
                        'subject' => sprintf(
                            __('Request a password reset for your account has been successfully done [%s]'),
                            wp_specialchars_decode(get_option('blogname'), ENT_QUOTES)
                        ),
                        'content' => '',
                        'headers' => array(
                            'Content-Type: text/html; charset=UTF-8',
                            'From: ' . get_option('blogname') . ' <info@fassaha.com>',
                            'Reply-To: ' . get_option('blogname') . ' <info@fassaha.com>'
                        )
                    );

                    //$mail['content'] .= sprintf(__('Bonjour, %s!'), $user->data->user_login) . '<br />';
                    $mail['content'] .= __('Bonjour') . ',<br />';
                    $mail['content'] .= __('Votre demande de modification de mot de passe a bien été prise en compte') . '.<br /><br />';
                    $mail['content'] .= sprintf(__('Identifiant: %s'), $user->data->user_login) . '<br />';
                    $mail['content'] .= sprintf(__('Mot de passe: %s'), $password) . '<br /><br />';
                    $mail['content'] .= sprintf('<a href="%s" target="_blank">%s</a>', site_url(), wp_specialchars_decode(get_option('blogname'), ENT_QUOTES));

                    if (wp_mail($mail['to'], $mail['subject'], $mail['content'], $mail['headers']))
                    {
                        $wtfapi_user->Clear_User_Tokens((int)$user->ID);
                    }

                    $this->result['status'] = true;
//                }
            }
        break;

            //{"_action": "user","_subaction": "login","user": {"login": "login","password": "pass"}}
        case 'login':

            $user = wp_signon(array(
                'user_login'    => $data['login'],
                'user_password' => $data['password']
            ), false);

            if (is_wp_error($user))
            {
                $this->result['error']['login'] = 'not_found';//__('Cet identifiant est déjà utilisé');
                $this->result['error']['code'] = 'login';
            }
            else
            {
                $wtfapi_user->Clear_User_Tokens($user->ID);
                $token = $wtfapi_user->Generate_New_Token($user->ID);

                if ($token !== false)
                {
                    include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
                    include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');

                    include_once($this->core->Attr_Get('plugin_core_path') . 'point.php');
                    $wtfapi_point = new WT_Fass_API_Point($this->core);

                    $_wtfa = new WT_Fass_Anki();
                    $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);
                    $user = $wtfapi_user->Get_Info($user->data->ID);

                    $this->result['token'] = $token;

                    $this->result['info'] = array(
                        'id'                => (int)$user['ID'],
                        'login'             => $user['user_login'],
                        'display_name'      => $user['display_name'],
                        'email'             => $user['user_email'],
                        'registered'        => $user['user_registered'],
                        'global-statistic'  => $wtfa_deck->User_Statistic_Get($user['ID']) //
                    );

                    $this->result['points-block'] = $wtfapi_point->GetPoints($user['ID']);
                    $this->result['status'] = true;
                }
            }
        break;
    }
