<?php

//
include_once($this->core->Attr_Get('plugin_core_path') . 'user.php');

global $wtfapi_user;
$wtfapi_user = new WT_Fass_API_User($this->core);
////

if (($user_id = $wtfapi_user->Token_Login($data['token'])) !== false)
{
    if (class_exists(UserLog)) {
        $userLogger = new UserLog();
        $userLogger->logAction($user_id,$this->subaction);
    }
    $wtfapi_user->Token_DT_Update($data['token']);

    //
    include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
    include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');

    $_wtfa = new WT_Fass_Anki();
    $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);
    ////

    switch ($this->subaction)
    {
        /* { "_action": "video", "_subaction": "clip-send", "video": { "token": "{{ token  }}", "deck-id": "43", "videos-id": "16", "clip-id": "1", "passed" : 1 } }*/
        case 'clip-send':
//            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core.php');
//            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->anki_plugin_fn . '/core/deck.php');
//            $_wtfa = new WT_Fass_Anki();
//            $wtfa_deck = new WT_Fass_Anki_Deck($_wtfa);

            $deck_id   = (int)$data['deck-id'];
            $videos_id = (int)$data['videos-id'];
            $clip_id   = (int)$data['clip-id'];
            $passed    = (int)$data['passed'];

            if ($data['videos-id'] != $wtfa_deck->Get_Videos_by_Deck($deck_id)) {
                $this->result['error']['videos-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'videos-id';
            }

            include_once($this->core->Attr_Get('plugin_path') . '../' . $this->core->fassvideo_plugin_vi . '/core.php');
            $core_vi = new WT_Fass_Video();

            $clips = $core_vi->Get_Clips_By_Videos($videos_id);
            $check_clip = false;
            foreach ($clips as $clip) {
                if ($clip->id == $clip_id) {
                    $check_clip = true;
                }
            }
            if (!$check_clip) {
                $this->result['error']['clip-id'] = __('Invalid ID');
                $this->result['error']['code'] = 'clip-id';
            }

            if (!count($this->result['error']))
            {
                $this->result['success'] = $core_vi->Passing($user_id, $videos_id, $clip_id, $passed);
                $this->result['passed_videos'] = $core_vi->Passing_Videos_Block($user_id, $videos_id, $deck_id);
                $this->result['passed_deck'] = $wtfa_deck -> Passing_Check($user_id, $data['deck-id'] );

                $this->result['status'] = true;
            }

        break;
    }
} else {
    if ($this->subaction !=  "login") {
        $this->result['error']['code'] = 'token';
        $this->result['error']['token'] = __('wrong ID');
    }

}
