<?php

    class WT_Fass_API
    {
        private $prefix = 'wtfapi';
        public $anki_plugin_fn = 'wt-fassanki';
        public $fasslearn_plugin_fn = 'wt-fasslearn';
        public $fassvideo_plugin_vi = 'wt-fassvideo';
        private $attr = array();

        public function __construct()
        {
            $this->Init();
        }

        private function Init()
        {
            if(!isset($_SESSION)) {
                session_start();
            }

            $this->Attr_Set('prefix', $this->prefix);

            $this->Plugin_Paths_Init();

            $this->Plugin_Hooks_Init();

            $this->Plugin_Pages_Init();

            add_action('init', array($this, 'Actions_Init'));
        }

        protected function Attr_Set($key, $value)
        {
            $this->attr[$key] = $value;
        }

        public function Attr_Get($key)
        {
            if (isset($this->attr[$key]))
            {
                return $this->attr[$key];
            }
            else
            {
                return false;
            }
        }

        public function Session_Set($key, $value)
        {
            $_SESSION[$this->prefix][$key] = $value;
        }

        public function Session_Get($key)
        {
            if (isset($_SESSION[$this->prefix]) && isset($_SESSION[$this->prefix][$key]))
            {
                return $_SESSION[$this->prefix][$key];
            }
            else
            {
                return false;
            }
        }

        public function Session_Unset($key = null)
        {
            if (is_null($key))
            {
                unset($_SESSION[$this->prefix]);
            }
            else
            {
                unset($_SESSION[$this->prefix][$key]);
            }
        }

        private function Plugin_Paths_Init()
        {
            $ps = array();

            preg_match("/^(.*[\/\\\]wp-content[\/\\\]plugins[\/\\\])(.*?)[\/\\\].*$/uis", __FILE__, $ps);

            if (is_array($ps) && count($ps) == 3)
            {
                $this->Attr_Set('plugin_dir_name', $ps[2]);
                $this->Attr_Set('plugin_path', $ps[1] . $this->Attr_Get('plugin_dir_name') . '/');
                $this->Attr_Set('plugin_index_path', $this->Attr_Get('plugin_path') . 'index.php');
                $this->Attr_Set('plugin_core_path', $this->Attr_Get('plugin_path') . 'core/');
                $this->Attr_Set('plugin_actions_path', $this->Attr_Get('plugin_path') . 'core/actions/');
                $this->Attr_Set('plugin_libraries_path', $this->Attr_Get('plugin_path') . 'core/libraries/');
                $this->Attr_Set('plugin_data_path', $this->Attr_Get('plugin_path') . 'data/');
                $this->Attr_Set('plugin_templates_path', $this->Attr_Get('plugin_path') . 'data/templates/');

                $this->Attr_Set('plugin_url', plugins_url($this->Attr_Get('plugin_dir_name')) . '/');
                $this->Attr_Set('plugin_css_url', $this->Attr_Get('plugin_url') . 'data/css/');
                $this->Attr_Set('plugin_js_url', $this->Attr_Get('plugin_url') . 'data/js/');
                $this->Attr_Set('plugin_img_url', $this->Attr_Get('plugin_url') . 'data/images/');
            }
        }

        private function Plugin_Hooks_Init()
        {
            register_activation_hook($this->Attr_Get('plugin_index_path'), array($this, 'Install'));
            //register_deactivation_hook();
            register_uninstall_hook($this->Attr_Get('plugin_index_path'), array($this, 'Uninstall'));
        }

        public function Install()
        {
            global $wpdb;

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_tokens`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `token` VARCHAR(32) DEFAULT NULL,
                    `user_id` BIGINT(20) DEFAULT NULL,
                    `dt` INT(10) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_user_points`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `user_id` BIGINT(20),
                    `points`  VARCHAR(32) DEFAULT 0,
                    `level`   VARCHAR(32) DEFAULT NULL,
                    `loginBonusLastDate` bigint unsigned DEFAULT 0,
                    `attributes` LONGTEXT DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");
        }

        public function Uninstall()
        {

        }

        public function Actions_Init()
        {
            if (isset($_REQUEST['_handler']) && $_REQUEST['_handler'] == $this->prefix)
            {
                include_once($this->Attr_Get('plugin_core_path') . 'action.php');

                $action = new WT_Fass_API_Action($this);
                $data = @json_decode(file_get_contents('php://input'), true);

                $action->name = $data['_action'];
                $action->subaction = $data['_subaction'];
                $action->Action_Load($data[$data['_action']]);
            }
        }

        public function Array_Treatment_Strings(&$array, $procedure)
        {
            $type = null;

            if (is_array($array))
            {
                $type = 'array';
            }
            else
            if (is_object($array))
            {
                $type = 'object';
            }

            if (!is_null($type) && count($array))
            {
                foreach ($array as $k => $v)
                {
                    if (is_string($v) && !preg_match("/^a:[0-9]+:\{/uis", $v))
                    {
                        switch (strtolower($procedure))
                        {
                            case 'addslashes':
                                $v = addslashes($v);
                            break;
                            case 'stripslashes':
                                $v = stripslashes($v);
                            break;
                            case 'htmlspecialchars':
                                $v = htmlspecialchars($v);
                            break;
                            case 'htmlspecialchars_decode':
                                $v = htmlspecialchars_decode($v);
                            break;
                        }
                    }
                    else
                    {
                        $v = $this->Array_Treatment_Strings($v, $procedure);
                    }

                    switch ($type)
                    {
                        case 'array':
                            $array[$k] = $v;
                        break;
                        case 'object':
                            $array->{$k} = $v;
                        break;
                    }
                }
            }

            return $array;
        }

        public function API_Call($action, $subaction, $data, $json_decode = false)
        {
            if (!is_string($action) || $action == '') return false;
            if (!is_string($subaction) || $subaction == '') return false;
            if (!is_array($data) || !count($data)) return false;

            $post = array(
                '_action'    => $action,
                '_subaction' => $subaction,
                $action      => $data
            );

            $c = curl_init();

            curl_setopt($c, CURLOPT_URL, site_url() . '?_handler=' . $this->prefix);
            curl_setopt($c, CURLOPT_POST, true);
            curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($c, CURLOPT_TIMEOUT, 10);
            curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($post));

            $res = curl_exec($c);
            curl_close($c);

            if ($json_decode)
            {
                return (array)@json_decode($res, true);
            }
            else
            {
                return $res;
            }
        }

        public function Template_Get($name, $path_dir = null, $data = null)
        {
            if (is_null($path_dir))
            {
                $path_dir = $this->Attr_Get('plugin_templates_path');
            }

            $path = $path_dir . $name . '.php';

            if (file_exists($path))
            {
                include($path);
            }
            else
            {
                return false;
            }
        }

        public function Page_Title_Build($title)
        {
            return $title . ' :: ' . __('Fass API');
        }

        public function Plugin_Pages_Init()
        {
            if (is_admin())
            {
                add_action('admin_menu', function()
                {
                    add_menu_page($this->Page_Title_Build(__('Main')), __('Fass API'), 'manage_options', $this->prefix, function()
                    {
                        if (isset($_REQUEST['open_branch'])) {
                            $this->Set_Dev_Settings();
                        }
                        $this->Template_Get('pages/main', null ,$this->Get_Dev_Settings());
                    }, '', 202);
                });
            }
        }

        public function Set_Dev_Settings ()
        {
            $open_deck = (int)$_REQUEST['open_deck'];
            update_option($this->prefix.'_open_deck', $open_deck);
            $open_branch = (int)$_REQUEST['open_branch'];
            update_option($this->prefix.'_open_branch', $open_branch);
        }

        public function Get_Dev_Settings ()
        {
            $open_deck = get_option($this->prefix.'_open_deck');
            $open_branch = get_option($this->prefix.'_open_branch');
            $result =array(
                'open_deck' => $open_deck,
                'open_branch' =>  $open_branch
            );

            return $result;
        }
    }
