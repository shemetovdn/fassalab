<?php

class WT_Fass_API_Point
{
    public $core;
    private $prefix;
    private $wpdb;
    private $points;

    public function __construct($core)
    {
        global $wpdb;
        $this->core   = $core;
        $this->prefix = $this->core->Attr_Get('prefix');
        $this->wpdb = $wpdb;
    }

    public function SetPoints($user_id, $points, $level, $loginBonusLastDate, $attributes)
    {

        $points = (int)$points;
        $level  = (int)$level;
        $loginBonusLastDate = (int)$loginBonusLastDate;
        if ($attributes === null) {
            $attributes = null;
        } else {
           // $attributes = mysqli_escape_string($attributes); // todo - to escape
        }

        $exist = $this->existPoints($user_id);

        if ($exist) {
            $result = $this->wpdb->get_results("
                UPDATE
                    `{$this->wpdb->prefix}{$this->prefix}_user_points`
                SET
                    `points` = '{$points}',
                    `level` = '{$level}',
                    `loginBonusLastDate` = '{$loginBonusLastDate}',
                    `attributes` = '{$attributes}'
                WHERE
                    `user_id` = '{$user_id}'
            ");
        } else {
            $result = $this->wpdb->get_results("
                INSERT INTO
                    `{$this->wpdb->prefix}{$this->prefix}_user_points`
                    (
                        user_id,
                        points,
                        level,
                        loginBonusLastDate,
                        attributes
                    )
                    VALUES
                    (
                        '{$user_id}',
                        '{$points}',
                        '{$level}',
                        '{$loginBonusLastDate}',
                        '{$attributes}'
                    )
            ");
        }

        if ($result !== null) {
            return true;
        } else {
            return false;
        }
    }
    public function GetPoints($user_id)
    {
        $exist = $this->existPoints($user_id);

        $result = null;

        if ($exist) {
            $result = array(
                'points'             => (int)$this->points->points,
                'level'              => (int)$this->points->level,
                'loginBonusLastDate' => (int)$this->points->loginBonusLastDate
            );
            if ($this->points->attributes !== null && $this->points->attributes !== '') {
                $result['attributes'] = $this->points->attributes;
            }
        }

        return $result;
    }

    public function is_timestamp($timestamp)
    {
        return $timestamp; // TODO add real check of $timestamp
        if (strtotime(date('d-m-Y H:i:s', $timestamp)) === (int)$timestamp) {
            return $timestamp;
        } else {
            return false;
        }
    }

    private function existPoints($user_id)
    {
        $result = $this->wpdb->get_row("
            SELECT
               *
            FROM
                `{$this->wpdb->prefix}{$this->prefix}_user_points` up
            WHERE
                up.`user_id` = '{$user_id}'
        ");

        $this-> points = $result;
        return ($result == null) ? false : true;

    }
}
