<?php

    class WT_Fass_API_User
    {
        public $core;
        private $prefix;

        public function __construct($core)
        {
            $this->core = $core;
            $this->prefix = $this->core->Attr_Get('prefix');
        }

        public function Generate_New_Token($user_id)
        {
            global $wpdb;

            $user_id = (int)$user_id;

            if ($user_id)
            {
                $token = md5(rand(1000, 9999) . uniqid() . rand(1000, 9999));

                $wpdb->query("
                    INSERT INTO
                        `{$wpdb->prefix}{$this->prefix}_tokens`
                        (
                            `token`,
                            `user_id`,
                            `dt`
                        )
                        VALUES
                        (
                            '{$token}',
                            '{$user_id}',
                            '" . time() . "'
                        )
                ");

                return $token;
            }

            return false;
        }

        public function Clear_Expired_Tokens()
        {
            global $wpdb;

            $wpdb->query("
                DELETE FROM
                    `{$wpdb->prefix}{$this->prefix}_tokens`
                WHERE
                    `dt` < " . (time() - 3 * 24 * 60 * 60) . "
            ");
        }

        public function Clear_User_Tokens($user_id)
        {
            global $wpdb;

            $user_id = (int)$user_id;

            if ($user_id)
            {
                $wpdb->query("
                    DELETE FROM
                        `{$wpdb->prefix}{$this->prefix}_tokens`
                    WHERE
                        `user_id` = '{$user_id}'
                ");
            }
        }

        public function Token_Login($token)
        {
            global $wpdb;

            if (preg_match("/^[a-z0-9]{32}$/uis", $token))
            {
                $result = $wpdb->get_results("
                    SELECT
                        t.`user_id`,
                        u.`user_login`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_tokens` t
                    LEFT JOIN
                        `{$wpdb->prefix}users` u
                        ON 
                        u.`ID` = t.`user_id`
                    WHERE
                        t.`token` = '{$token}'
                    LIMIT
                        1
                ");

                if (count($result))
                {
                    $user = $result[0];
                    $user_id = (int)$user->user_id;

                    if ($user_id)
                    {
                        wp_set_current_user($user_id, $user->user_login);
                        wp_set_auth_cookie($user_id);
                        do_action('wp_login', $user->user_login);

                        return $user_id;
                    }
                }
            }

            return false;
        }

        public function Token_DT_Update($token)
        {
            global $wpdb;

            if (preg_match("/^[a-z0-9]{32}$/uis", $token))
            {
                $wpdb->query("
                    UPDATE
                        `{$wpdb->prefix}{$this->prefix}_tokens`
                    SET
                        `dt` = '" . time() . "'
                    WHERE
                        `token` = '{$token}'
                ");
            }
        }

        public function Get_Info($user_id)
        {
            global $wpdb;

            $user_id = (int)$user_id;

            if ($user_id)
            {
                $result = $wpdb->get_results("
                    SELECT
                        u.*
                    FROM
                        `{$wpdb->prefix}users` u
                    WHERE
                        u.`ID` = '{$user_id}'
                    LIMIT
                        1
                ", ARRAY_A);

                return $result[0];
            }
        }
    }
