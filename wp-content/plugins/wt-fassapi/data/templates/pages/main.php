<div class="container">
    <br>
    <form method="post">
        <dl>
            <dt>all branch open</dt>
            <dd>
                <label>
                    yes
                    <input type="radio" name="open_branch" value="1" <?php echo ($data['open_branch']) ? 'checked' : ''; ?>>
                </label>
                <label>
                    no
                    <input type="radio" name="open_branch" value="0" <?php echo ($data['open_branch']) ? '' : 'checked'; ?>>
                </label>
            </dd>
            <dt>all deck open</dt>
            <dd>
                <label>
                    yes
                    <input type="radio" name="open_deck" value="1" <?php echo ($data['open_deck']) ? 'checked' : ''; ?>>
                </label>
                <label>
                    no
                    <input type="radio" name="open_deck" value="0" <?php echo ($data['open_deck']) ? '' : 'checked'; ?>>
                </label>
            </dd>
        </dl>
        <br>
        <input type="submit" value="save">
    </form>
</div>