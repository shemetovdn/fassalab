<?php

    //
    include_once($this->core->Attr_Get('plugin_core_path') . 'lesson.php');

    global $wtfs_lesson;

    $wtfs_lesson = new WT_Fass_Learn_Lesson($this->core);
    ////

    switch ($this->subaction)
    {
        case 'step-create':
        case 'step-save':
            $this->core->Array_Treatment_Strings($data, 'stripslashes');
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars');

            $data['step']['removed'] = false;
            $data['step']['title'] = trim($data['step']['title']);
            $data['step']['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['step']['title']);

            if (mb_strlen($data['step']['title']) < 3){ $this->result['error']['lesson[step][title]'] = __('The field must contain at least 3 characters'); }

            if (!count($this->result['error']))
            {
                $lesson = $this->core->Session_Get('lesson');

                if (!isset($lesson['steps']))
                {
                    $lesson['steps'] = array();
                }

                switch ($this->subaction)
                {
                    case 'step-create':
                        $step_key = count($lesson['steps']);
                        $data['step']['data'][$data['step']['type']]['files'] = $lesson['steps'][$step_key]['data'][$data['step']['type']]['files'];

                        if ((int)$data['step']['type'] == 1) // for audio files
                        {
                            $data['step']['data'][$data['step']['type']]['audio'] = $lesson['steps'][$step_key]['data'][$data['step']['type']]['audio'];
                        }

                        array_push($lesson['steps'], $data['step']);
                    break;
                    case 'step-save':
                        $step_key = (int)$data['step']['key'];
                        $data['step']['data'][$data['step']['type']]['files'] = $lesson['steps'][$step_key]['data'][$data['step']['type']]['files'];

                        if ((int)$data['step']['type'] == 1) // for audio files
                        {
                            $data['step']['data'][$data['step']['type']]['audio'] = $lesson['steps'][$step_key]['data'][$data['step']['type']]['audio'];
                        }

                        $lesson['steps'][$step_key] = $data['step'];
                    break;
                }

                // Files
                $files = $wtfs_lesson->File_Upload_Temp('lesson-step-data-file', $data['step']['data']['file']['type']);

                if (count($files))
                {
                    foreach ($files as $k => $v)
                    {
                        $lesson['steps'][$step_key]['data'][$data['step']['type']]['files'][$k] = $v;
                    }
                }
                ////

                if ((int)$data['step']['type'] == 1) // for audio files
                {
                    // Audio for variants
                    $files = $wtfs_lesson->File_Upload_Temp('lesson-step-data-audio', $data['step']['data']['audio']['type']);

                    if (count($files))
                    {
                        foreach ($files as $k => $v)
                        {
                            $lesson['steps'][$step_key]['data'][$data['step']['type']]['audio'][$k] = $v;
                        }
                    }
                    ////
                }

                $this->core->Session_Set('lesson', $lesson);

                $this->result['status'] = true;
            }
        break;

        case 'create':
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

            $data['title'] = trim($data['title']);
            $data['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['title']);
            $data['prize'] = (int)$data['prize'];

            if (mb_strlen($data['title']) < 3){ $this->result['error']['lesson[title]'] = __('The field must contain at least 3 characters'); }
            if (!empty($_POST["lesson"]['step'])) {
                $this->core->sessionSetByPost('lesson');
            }
            $lesson = $this->core->Session_Get('lesson');

            if (!count($lesson['steps'])){ $this->result['error']['steps'] = __('The lesson must contain at least one step'); }

            if (!count($this->result['error']))
            {
                $wpdb->query("
                    INSERT INTO
                      `{$wpdb->prefix}{$this->prefix}_lessons`
                      (
                          `title`,
                          `description`,
                          `status`,
                          `date`
                      )
                      VALUES
                      (
                          '" . esc_sql($data['title']) . "',
                          '" . esc_sql($data['description']) . "',
                          '1',
                          '" . time() . "'
                      )
                ");

                $lesson_id = (int)$wpdb->insert_id;

                if ($lesson_id)
                {
                    foreach ($lesson['steps'] as $k => $v)
                    {
                        if ($v['removed'] === true)
                        {
                            continue;
                        }

                        $step_id = $wtfs_lesson->Step_Save($lesson_id, null, $v['type'], $v['title']);

                        if ($step_id)
                        {
                            if (isset($v['data']) && isset($v['data'][$v['type']]))
                            {
                                $wtfs_lesson->File_Upload_Temp_To_Storage($v['data'][$v['type']]['files']);

                                if ((int)$v['type'] == 1) // for audio files
                                {
                                    $wtfs_lesson->File_Upload_Temp_To_Storage($v['data'][$v['type']]['audio']);
                                }

                                $wtfs_lesson->Step_Data_Save($step_id, $v['data'][$v['type']]);
                            }
                        }
                    }

                    if ($data['prize'])
                    {
                        $wpdb->query("
                            UPDATE
                                `{$wpdb->prefix}{$this->prefix}_prizes`
                            SET
                                `fl_id` = '{$lesson_id}'
                            WHERE
                                `id` = '{$data['prize']}'
                        ");
                    }

                    $this->core->Session_Unset();

                    $this->result['message'] = __('Lesson successfully created');
                    $this->result['redirect_url'] = admin_url('admin.php?page=' . $this->prefix . '_lessons');
                    $this->result['status'] = true;
                }
            }
        break;

        case 'save':
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

            $data['id'] = (int)$data['id'];
            $data['title'] = trim($data['title']);
            $data['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['title']);
            $data['prize'] = (int)$data['prize'];

            if (!empty($_POST["lesson"]['step'])) {
                $this->core->sessionSetByPost('lesson');
            }
            $lesson = $this->core->Session_Get('lesson');

            if (!count($lesson['steps'])){ $this->result['error']['steps'] = __('The lesson must contain at least one step'); }

            if (mb_strlen($data['title']) < 3){ $this->result['error']['lesson[title]'] = __('The field must contain at least 3 characters'); }

            if (!count($this->result['error']) && $data['id'])
            {
                $wpdb->query("
                    UPDATE
                        `{$wpdb->prefix}{$this->prefix}_lessons`
                    SET
                        `title` = '" . esc_sql($data['title']) . "',
                        `description` = '" . esc_sql($data['description']) . "'
                    WHERE
                        `id` = '{$data['id']}'
                ");

                foreach ($lesson['steps'] as $k => $v)
                {
                    $v['id'] = (int)$v['id'];

                    if ($v['id'] && $v['removed'] === true)
                    {
                        // Remove step data
                        $wpdb->query("
                            DELETE FROM
                                `{$wpdb->prefix}{$this->prefix}_lessons_steps_data`
                            WHERE
                              `step_id` = '{$v['id']}'
                        ");
                        ////

                        // Remove step
                        $wpdb->query("
                            DELETE FROM
                                `{$wpdb->prefix}{$this->prefix}_lessons_steps`
                            WHERE
                                `id` = '{$v['id']}'
                        ");
                        ////

                        continue;
                    }

                    $v['id'] = $wtfs_lesson->Step_Save($data['id'], $v['id'], $v['type'], $v['title']);

                    if ($v['id'] && isset($v['data']) && isset($v['data'][$v['type']]))
                    {
                        $wtfs_lesson->File_Upload_Temp_To_Storage($v['data'][$v['type']]['files']);

                        if ((int)$v['type'] == 1)
                        {
                            $wtfs_lesson->File_Upload_Temp_To_Storage($v['data'][$v['type']]['audio']);
                        }

                        $wtfs_lesson->Step_Data_Save($v['id'], $v['data'][$v['type']]);
                    }

                    if (isset($v['data'][0][$v['type']]) && !empty($_POST)) {
                        $wtfs_lesson->Step_Data_Save($v['id'], $v['data'][0][$v['type']]);
                    }
                }

                $wtfs_lesson->Files_Storage_Diff_Clear();
                $wtfs_lesson->Files_Storage_Diff_Clear('audio');

                if ($data['prize'])
                {
                    $wpdb->query("
                        UPDATE
                            `{$wpdb->prefix}{$this->prefix}_prizes`
                        SET
                            `fl_id` = '{$data['id']}'
                        WHERE
                            `id` = '{$data['prize']}'
                    ");
                }

                $this->core->Session_Unset();

                $this->result['message'] = __('Lesson successfully saved');
                //$this->result['redirect_url'] = admin_url('admin.php?page=' . $this->prefix . '_lessons');
                $this->result['redirect_url'] = admin_url('admin.php?page=' . $this->prefix . '_lesson_editor&id=' . $data['id']);
                $this->result['status'] = true;
            }
        break;

        case 'steps-session-list-get':
            $lesson = $this->core->Session_Get('lesson');

            if (count($lesson['steps']))
            {
                ob_start();

                foreach ($lesson['steps'] as $k => $v)
                {
                    if ($v['removed'] === true)
                    {
                        continue;
                    }

                    include($this->core->Attr_Get('plugin_templates_path') . 'items/lesson-editor-step.php');
                }

                $this->result['html'] = ob_get_contents();
                ob_end_clean();
            }

            $this->result['status'] = true;
        break;

        case 'lessons-list-get':
            $this->result['html'] = '';

            $result = $wpdb->get_results("
                SELECT
                    l.*,
                    p.`id` AS 'prize_id',
                    p.`title` AS 'prize_title'
                FROM
                    `{$wpdb->prefix}{$this->prefix}_lessons` l
                LEFT JOIN
                    `{$wpdb->prefix}{$this->prefix}_prizes` p 
                    ON 
                    p.`fl_id` = l.`id`
                ORDER BY
                    l.`date`
                    DESC
            ");

            if (count($result))
            {
                $this->core->Array_Treatment_Strings($result, 'stripslashes');
                $this->core->Array_Treatment_Strings($result, 'htmlspecialchars');

                ob_start();

                foreach ($result as $k => $v)
                {
                    include($this->core->Attr_Get('plugin_templates_path') . 'items/lesson.php');
                }

                $this->result['html'] = ob_get_contents();
                ob_end_clean();
            }

            $this->result['status'] = true;
        break;

        case 'send':
            $data['step']['id'] = (int)$data['step']['id'];

            $user_id = (int)wp_get_current_user()->ID;

            if ($data['step']['id'] && $user_id)
            {
                $step = $wtfs_lesson->Step_Get($data['step']['id']);

                if (is_object($wtfs_lesson))
                {
                    $answer = false;

                    if ($data['step']['answer'] != '-skip-' && trim((string)$data['step']['answer']) != '')
                    {
                        switch ((int)$step->type)
                        {
                            case 1:
                                $answer = in_array((int)$data['step']['answer'], (array)$step->data['answer-true']);
                            break;

                            case 2:
                            case 7:
                                $data['step']['answer'] = mb_strtolower(trim($data['step']['answer']));
                                $data['step']['answer'] = preg_replace("/[\s]{1,}/uis", ' ', $data['step']['answer']);
                                $data['step']['answer'] = stripslashes($data['step']['answer']);

                                $percent = 0;
                                $p = 0;

                                if (count($step->data['ttvt']))
                                {
                                    foreach ($step->data['ttvt'] as $v)
                                    {
                                        $v = mb_strtolower(trim($v));
                                        $v = preg_replace("/[\s]{1,}/uis", ' ', $v);

                                        similar_text($data['step']['answer'], $v, $p);

                                        if ($p > $percent)
                                        {
                                            $percent = $p;
                                        }
                                    }

                                    if ($percent == 100)
                                    {
                                        $answer = true;
                                    }
                                }
                            break;

                            case 3:
                                $prefix = $step->data['ttvtp'][(int)$data['step']['answer_prefix']];

                                $data['step']['answer'] = mb_strtolower(trim($prefix . ' ' . $data['step']['answer']));
                                $data['step']['answer'] = preg_replace("/[\s]{1,}/uis", ' ', $data['step']['answer']);
                                $data['step']['answer'] = stripslashes($data['step']['answer']);

                                $percent = 0;
                                $p = 0;

                                if (count($step->data['ttvt']))
                                {
                                    foreach ($step->data['ttvt'] as $v)
                                    {
                                        $v = mb_strtolower(trim($v));
                                        $v = preg_replace("/[\s]{1,}/uis", ' ', $v);

                                        similar_text($data['step']['answer'], $v, $p);

                                        if ($p > $percent)
                                        {
                                            $percent = $p;
                                        }
                                    }

                                    if ($percent == 100)
                                    {
                                        $answer = true;
                                    }
                                }
                            break;

                            case 4:
                                $answer = in_array((int)$data['step']['answer'], (array)$step->data['answer-true']);
                            break;

                            case 5:
                            case 8:
                                $data['step']['answer'] = mb_strtolower(trim($data['step']['answer']));
                                $data['step']['answer'] = preg_replace("/[\s]{1,}/uis", ' ', $data['step']['answer']);
                                $data['step']['answer'] = stripslashes($data['step']['answer']);

                                $percent = 0;
                                $p = 0;

                                if (count($step->data['ttvt']))
                                {
                                    foreach ($step->data['ttvt'] as $v)
                                    {
                                        $v = mb_strtolower(trim($v));
                                        $v = preg_replace("/[\s]{1,}/uis", ' ', $v);

                                        similar_text($data['step']['answer'], $v, $p);

                                        if ($p > $percent)
                                        {
                                            $percent = $p;
                                        }
                                    }

                                    if ($percent == 100)
                                    {
                                        $answer = true;
                                    }
                                }
                            break;

                            case 6:
                                $answer = true;
                            break;
                        }
                    }

                    $passing = $wtfs_lesson->Passing($step->lesson_id, $step->id, $answer);
                    $this->result['progress'] = $wtfs_lesson->Lesson_User_Progress_Get($step->lesson_id);

                    // Mark is done of LMS
                    $lms_id = $this->core->Session_Get('lesson-passing')['lms-id'];

                    if ($this->result['progress']['%'] >= 100/* && !$this->core->Mod('LearnDash_LMS')->Is_Lesson_Passed($lms_id)*/)
                    {
                        include_once($this->core->Attr_Get('plugin_core_path') . 'prize.php');
                        $wtfs_prize = new WT_Fass_Learn_Prize($this->core);

                        $this->core->Mod('LearnDash_LMS')->Lesson_Status_Passed_Set($lms_id, true);

                        $this->result['lms-next'] = true;

                        $lms_next = $this->core->Mod('LearnDash_LMS')->Course_Lesson_Next_Get($lms_id);
                        $cln_next = false;

                        // Init finish popup
                        $this->result['lms-message'] = __('Bravo! On continue') . '.';

                        if ($lms_next !== false)
                        {
                            $message = get_option($this->prefix . '-message-spl');
                        }
                        else
                        {
                            // Next lesson of course
                            $course_id = $this->core->Mod('LearnDash_LMS')->Lesson_Course_Get($lms_id);
                            $lsns_ids = $this->core->Mod('LearnDash_LMS')->Course_Lessons_Get($course_id);
                            $lsn_id = $this->core->Mod('LearnDash_LMS')->Topic_Lesson_Parent_Get($lms_id);
                            $k = array_search($lsn_id, $lsns_ids);

                            if ($k !== false && isset($lsns_ids[$k + 1]))
                            {
                                $tps_ids = $this->core->Mod('LearnDash_LMS')->Course_Topics_Get($course_id, (int)$lsns_ids[$k + 1]);

                                if (count($tps_ids))
                                {
                                    $cln_next = (int)$tps_ids[0];
                                }
                            }
                            ////

                            $message = get_option($this->prefix . '-message-wpc');
                        }

                        if (trim(strip_tags($message)) != '') $this->result['lms-message'] = $message;

                        $lesson = $wtfs_lesson->Get($step->lesson_id);
                        $prize = $wtfs_prize->Get($lesson->prize_id);

                        if (isset($prize->id))
                        {
                            if (trim(strip_tags($prize->message)) != '')
                            {
                                $this->result['lms-message'] = $prize->message;
                            }

                            if (trim(strip_tags($prize->description)) != '')
                            {
                                if ($this->result['lms-message'] != ''){ $this->result['lms-message'] .= '<br />'; }
                                $this->result['lms-message'] .= $prize->description;
                            }
                        }

                        $this->result['lms-message'] = '<p>' . $this->result['lms-message'] . '</p>';

                        $prize->video_data = (array)unserialize($prize->video_data);

                        if (count($prize->video_data))
                        {
                            foreach ($prize->video_data as $v)
                            {
                                if ($v['popup'] === true)
                                {
                                    $h5p_frame = do_shortcode('[h5p id="' . $v['h5p'] . '"]');

                                    preg_match("/.*(<iframe .*<\/iframe>)/uis", $h5p_frame, $m);

                                    if (isset($m[1]))
                                    {
                                        $this->result['lms-message'] .= '<div class="h5p-video">';
                                        $this->result['lms-message'] .= $m[1];
                                        $this->result['lms-message'] .= '</div>';

                                        $this->result['lms-message'] .= '<div class="link-user-profile">';
                                        $this->result['lms-message'] .= '<a href="' . get_permalink(112) . '" target="_blank">' . __("Mon profil") . '</a>';
                                        $this->result['lms-message'] .= '</div>';
                                    }

                                    break;
                                }
                            }
                        }
                        ////

                        if ($lms_next !== false)
                        {
                            $this->result['lms-message'] .= '<p class="actions"><a href="' . get_permalink($lms_next) . '" class="btn lms-lesson-next green">' . __('Leçon suivante') . ' <i class="fa fa-arrow-right"></i></a></p>';
                        }
                        else
                        {
                            if ($cln_next !== false)
                            {
                                $link = get_permalink($cln_next);
                                $btn_caption = __('Leçon suivante');
                            }
                            else
                            {
                                $link = get_permalink($this->core->Mod('LearnDash_LMS')->Lesson_Course_Get($lms_id));
                                $btn_caption = __('Back to course');
                            }

                            $this->result['lms-message'] .= '<p class="actions"><a href="' . $link . '" class="btn lms-lesson-next green">' . $btn_caption . ' <i class="fa fa-arrow-right"></i></a></p>';
                        }

                        $wtfs_prize->User_Prizes_Update($user_id, $lesson->id, 'add');
                    }
                    else
                    {
                        $this->result['lms-next'] = false;
                    }
                    ////

                    if ($answer)
                    {
                        $this->result['answer'] = $step->data['result-message']['true'];
                    }
                    else
                    {
                        if ($data['step']['answer'] != '-skip-')
                        {
                            $this->result['error']['answer'] = $step->data['result-message']['false'];
                            $this->result['error']['answer-correct'] = $step->data['result-message']['correct'];
                        }
                        else
                        {
                            $this->result['error']['skip'] = true;
                        }
                    }

                    $this->result['status'] = true;
                }
            }
        break;

        case 'remove':
            $data['id'] = (int)$data['id'];

            if ($data['id'])
            {
                // Remove steps data
                $result = $wpdb->get_results("
                    SELECT
                        ls.`id`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons_steps` ls
                    WHERE
                        `lesson_id` = '{$data['id']}'
                ");

                if (count($result))
                {
                    foreach ($result as $v)
                    {
                        $wpdb->query("
                            DELETE FROM
                                `{$wpdb->prefix}{$this->prefix}_lessons_steps_data`
                            WHERE
                                `step_id` = '{$v->id}'
                        ");
                    }
                }
                ////

                // Remove steps
                $wpdb->query("
                    DELETE FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons_steps`
                    WHERE
                        `lesson_id` = '{$data['id']}'
                ");
                ////

                // Remove lesson
                $wpdb->query("
                    DELETE FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons`
                    WHERE
                        `id` = '{$data['id']}'
                ");
                ////

                // Clear prize
                $wpdb->query("
                    UPDATE
                        `{$wpdb->prefix}{$this->prefix}_prizes`
                    SET
                        `fl_id` = NULL
                    WHERE
                        `fl_id` = '{$data['id']}'
                ");
                ////

                $wtfs_lesson->Files_Storage_Diff_Clear();

                $this->result['status'] = true;
            }
        break;

        case 'step-edit-get':
            $lesson = $this->core->Session_Get('lesson');

            if (!isset($lesson['steps'][$data['step']['key']]))
            {
                $step = $wtfs_lesson->Step_Get($data['step']['id']);
            }
            else
            {
                $step = (object)$lesson['steps'][$data['step']['key']];
            }

            if (is_object($step))
            {
                $step->key = $data['step']['key'];

                ob_start();

                $this->core->Template_Get('areas/lesson-step-editor-form', null, $step);

                $this->result['html'] = ob_get_contents();
                ob_end_clean();

                $this->result['status'] = true;
            }
        break;

        case 'step-data-get':
            $data['step']['type'] = (int)$data['step']['type'];

            if ($data['step']['type'])
            {
                $lesson = $this->core->Session_Get('lesson');
                $template_data = array();

                if (preg_match("/^[0-9]+$/uis", $data['step']['key']))
                {
                    $data['step']['key'] = (int)$data['step']['key'];

                    $template_data['data'] = $lesson['steps'][$data['step']['key']]['data'][$data['step']['type']];
                }

                $template_data['type'] = $data['step']['type'];

                ob_start();

                $this->core->Template_Get('lessons/step-data-ims-' . $data['step']['type'], null, $template_data);

                $this->result['html'] = ob_get_contents();
                ob_end_clean();

                $this->result['status'] = true;
            }
        break;

        case 'step-remove':
            //$data['step']['id'] = (int)$data['step']['id'];
            $data['step']['key'] = (int)$data['step']['key'];

            $lesson = $this->core->Session_Get('lesson');

            if (isset($lesson['steps'][$data['step']['key']]))
            {
                $lesson['steps'][$data['step']['key']]['removed'] = true;

                $this->core->Session_Set('lesson', $lesson);

                $this->result['status'] = true;
            }
        break;

        case 'step-data-item-file-remove':
            $sk = (int)$data['step']['key'];
            $sdt = (int)$data['step']['data']['type'];
            $sdik = $data['step']['data']['item']['key'];

            if (is_numeric($sdik))
            {
                $sdik = (int)$sdik;
            }

            $lesson = $this->core->Session_Get('lesson');

            $files = &$lesson['steps'][$sk]['data'][$sdt]['files'];

            if (is_array($files) && isset($files[$sdik]))
            {
                $files[$sdik]['removed'] = true;

                $this->core->Session_Set('lesson', $lesson);
            }

            $this->result['status'] = true;
        break;

        case 'lesson-clone':
            $data['id'] = (int)$data['id'];
            $data['title'] = trim($data['title']);
            $data['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['title']);

            if (!$data['id']){ $this->result['error']['id'] = __('Invalid lesson ID'); }
            if (mb_strlen($data['title']) > 0 && mb_strlen($data['title']) < 3){ $this->result['error']['title'] = __('The field must contain at least 3 characters'); }

            if (!count($this->result['error']))
            {
                $result = $wpdb->get_results("
                    SELECT
                        l.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons` l
                    WHERE
                        `id` = '{$data['id']}'
                ");

                if (!count($result))
                {
                    $this->result['error']['lesson'] = __('Lesson with this ID does not exist');
                }
                else
                {
                    $l = $result[0];

                    if (mb_strlen($data['title']) == 0){ $data['title'] = $l->title; }

                    $wpdb->query("
                        INSERT INTO
                          `{$wpdb->prefix}{$this->prefix}_lessons`
                          (
                              `title`,
                              `description`,
                              `status`,
                              `date`
                          )
                          VALUES
                          (
                              '" . esc_sql($data['title']) . "',
                              '" . esc_sql($l->description) . "',
                              '" . esc_sql($l->status) . "',
                              '" . time() . "'
                          )
                    ");

                    $lesson_id = (int)$wpdb->insert_id;

                    if (!$lesson_id)
                    {
                        $this->result['error']['lesson'] = __('Error creating lesson');
                    }
                    else
                    {
                        $result = $wpdb->get_results("
                            SELECT
                                ls.*
                            FROM
                                `{$wpdb->prefix}{$this->prefix}_lessons_steps` ls
                            WHERE
                                ls.`lesson_id` = '{$l->id}'
                        ");

                        if (count($result))
                        {
                            foreach ($result as $ls)
                            {
                                $wpdb->query("
                                    INSERT INTO
                                      `{$wpdb->prefix}{$this->prefix}_lessons_steps`
                                      (
                                          `lesson_id`,
                                          `type`,
                                          `title`
                                      )
                                      VALUES
                                      (
                                          '{$lesson_id}',
                                          '" . esc_sql($ls->type) . "',
                                          '" . esc_sql($ls->title) . "'
                                      )
                                ");

                                $lesson_step_id = (int)$wpdb->insert_id;

                                if ($lesson_step_id)
                                {
                                    $result_sd = $wpdb->get_results("
                                        SELECT
                                            lsd.*
                                        FROM
                                            `{$wpdb->prefix}{$this->prefix}_lessons_steps_data` lsd
                                        WHERE
                                            lsd.`step_id` = '{$ls->id}'
                                    ");

                                    if (count($result_sd))
                                    {
                                        foreach ($result_sd as $lsd)
                                        {
                                            $wpdb->query("
                                                INSERT INTO
                                                  `{$wpdb->prefix}{$this->prefix}_lessons_steps_data`
                                                  (
                                                      `step_id`,
                                                      `data`
                                                  )
                                                  VALUES
                                                  (
                                                      '{$lesson_step_id}',
                                                      '" . esc_sql($lsd->data) . "'
                                                  )
                                            ");
                                        }
                                    }
                                }
                            }
                        }

                        $this->result['redirect_url'] = admin_url('admin.php?page=' . $this->prefix . '_lesson_editor&id=' . $lesson_id);
                        $this->result['status'] = true;
                    }
                }
            }
        break;

        case 'steps-clone-form-get':
            $data['id'] = (int)$data['id'];

            if ($data['id'] && is_array($data['steps']) && count($data['steps']))
            {
                ob_start();

                $this->core->Template_Get('areas/lesson-steps-clone-form', null, array(
                    'lessons'     => $wtfs_lesson->getAll(array($data['id']), "`date` DESC"),
                    'lesson-from' => $wtfs_lesson->Get($data['id']),
                    'steps-clone' => $data['steps']
                ));

                $this->result['html'] = ob_get_contents();
                ob_end_clean();

                $this->result['status'] = true;
            }
        break;

        case 'steps-clone':
            $data['id_from'] = (int)$data['id_from'];
            $data['id_to'] = (int)$data['id_to'];

            if (!$data['id_from'] || !$data['id_to']){ $this->result['error']['id'] = __('Invalid lesson ID'); }
            if (!is_array($data['steps']) || !count($data['steps'])){ $this->result['error']['steps'] = __('List of steps is empty'); }

            if (!count($this->result['error']))
            {
                $result_f = $wpdb->get_results("
                    SELECT
                        l.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons` l
                    WHERE
                        `id` = '{$data['id_from']}'
                ");

                $result_t = $wpdb->get_results("
                    SELECT
                        l.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons` l
                    WHERE
                        `id` = '{$data['id_to']}'
                ");

                if (!count($result_f) || !count($result_t))
                {
                    $this->result['error']['lesson'] = __('Lesson with this ID does not exist');
                }
                else
                {
                    $l_f = $result_f[0];
                    $l_t = $result_t[0];

                    $result_ls = $wpdb->get_results("
                        SELECT
                            ls.*
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_lessons_steps` ls
                        WHERE
                            `lesson_id` = '{$l_f->id}'
                            AND 
                            `id` IN ('" . implode("', '", $data['steps']) . "')
                    ");

                    if (count($result_ls))
                    {
                        foreach ($result_ls as $ls)
                        {
                            $wpdb->query("
                                INSERT INTO
                                  `{$wpdb->prefix}{$this->prefix}_lessons_steps`
                                  (
                                      `lesson_id`,
                                      `type`,
                                      `title`
                                  )
                                  VALUES
                                  (
                                      '{$l_t->id}',
                                      '" . esc_sql($ls->type) . "',
                                      '" . esc_sql($ls->title) . "'
                                  )
                            ");

                            $lesson_step_id = (int)$wpdb->insert_id;

                            $result_lsd = $wpdb->get_results("
                                SELECT
                                    lsd.*
                                FROM
                                    `{$wpdb->prefix}{$this->prefix}_lessons_steps_data` lsd
                                WHERE
                                    `step_id` = '{$ls->id}'
                            ");

                            if (count($result_lsd))
                            {
                                foreach ($result_lsd as $lsd)
                                {
                                    $wpdb->query("
                                        INSERT INTO
                                          `{$wpdb->prefix}{$this->prefix}_lessons_steps_data`
                                          (
                                              `step_id`,
                                              `data`
                                          )
                                          VALUES
                                          (
                                              '{$lesson_step_id}',
                                              '" . esc_sql($lsd->data) . "'
                                          )
                                    ");
                                }
                            }
                        }
                    }

                    $this->result['message']  = __('Les étapes sont bien sauvegardés dans la leçon');
                    $this->result['message'] .= ' "' . $l_t->title . '". ';
                    $this->result['message'] .= __('Voudriez-vous rédiger cette leçon?');

                    $this->result['redirect_url'] = admin_url('admin.php?page=' . $this->prefix . '_lesson_editor&id=' . $l_t->id);
                    $this->result['status'] = true;
                }
            }
        break;

        case 'exit':
            $this->core->Session_Unset();
            $this->result['redirect_url'] = site_url();
            $this->result['status'] = true;
        break;
    }
