<?php

    //
    include_once($this->core->Attr_Get('plugin_core_path') . 'prize.php');

    global $wtfs_prize;

    $wtfs_prize = new WT_Fass_Learn_Prize($this->core);
    ////

    switch ($this->subaction)
    {
        case 'prizes-list-get':
            $this->result['html'] = '';

            $result = $wpdb->get_results("
                SELECT
                    p.*,
                    l.`id` AS 'fs_id',
                    l.`title` AS 'fs_title'
                FROM
                    `{$wpdb->prefix}{$this->prefix}_prizes` p
                LEFT JOIN
                    `{$wpdb->prefix}{$this->prefix}_lessons` l 
                    ON 
                    l.`id` = p.`fl_id`
                ORDER BY
                    p.`date`
                    DESC
            ");

            if (count($result))
            {
                $this->core->Array_Treatment_Strings($result, 'stripslashes');
                $this->core->Array_Treatment_Strings($result, 'htmlspecialchars');

                ob_start();

                foreach ($result as $k => $v)
                {
                    include($this->core->Attr_Get('plugin_templates_path') . 'items/prize.php');
                }

                $this->result['html'] = ob_get_contents();
                ob_end_clean();
            }

            $this->result['status'] = true;
        break;

        case 'remove':
            $data['id'] = (int)$data['id'];

            if ($data['id'])
            {
                $wpdb->query("
                    DELETE FROM
                        `{$wpdb->prefix}{$this->prefix}_prizes`
                    WHERE
                        `id` = '{$data['id']}'
                ");

                $this->result['status'] = true;
            }
        break;

        case 'create':
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

            $data['title'] = trim($data['title']);
            $data['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['title']);
            $data['description'] = trim($data['description']);
            $data['description'] = preg_replace("/[\s]{2,}/uis", ' ', $data['description']);
            $data['message'] = trim($data['message']);
            $data['message'] = preg_replace("/[\s]{2,}/uis", ' ', $data['message']);
            $data['lesson'] = (int)$data['lesson'];

            if (isset($data['video']))
            {
                if (isset($data['video']['popup']) && $data['video'][$data['video']['popup']])
                {
                    $data['video'][$data['video']['popup']]['popup'] = true;
                    unset($data['video']['popup']);
                }

                $data['video'] = serialize((array)$data['video']);
            }
            else
            {
                $data['video'] = '';
            }

            if (mb_strlen($data['title']) < 3){ $this->result['error']['prize[title]'] = __('The field must contain at least 3 characters'); }

            if (!count($this->result['error']))
            {
                $wpdb->query("
                    INSERT INTO
                      `{$wpdb->prefix}{$this->prefix}_prizes`
                      (
                          `fl_id`,
                          `title`,
                          `description`,
                          `message`,
                          `video_data`,
                          `date`
                      )
                      VALUES
                      (
                          '{$data['lesson']}',
                          '" . esc_sql($data['title']) . "',
                          '" . esc_sql($data['description']) . "',
                          '" . esc_sql($data['message']) . "',
                          '{$data['video']}',
                          '" . time() . "'
                      )
                ");

                $this->result['message'] = __('Prize successfully created');
                $this->result['redirect_url'] = admin_url('admin.php?page=' . $this->prefix . '_prizes');
                $this->result['status'] = true;
            }
        break;

        case 'save':
            $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

            $data['id'] = (int)$data['id'];
            $data['title'] = trim($data['title']);
            $data['title'] = preg_replace("/[\s]{2,}/uis", ' ', $data['title']);
            $data['description'] = trim($data['description']);
            $data['description'] = preg_replace("/[\s]{2,}/uis", ' ', $data['description']);
            $data['message'] = trim($data['message']);
            $data['message'] = preg_replace("/[\s]{2,}/uis", ' ', $data['message']);
            $data['lesson'] = (int)$data['lesson'];

            if (isset($data['video']))
            {
                if (isset($data['video']['popup']) && $data['video'][$data['video']['popup']])
                {
                    $data['video'][$data['video']['popup']]['popup'] = true;
                    unset($data['video']['popup']);
                }

                $data['video'] = serialize((array)$data['video']);
            }
            else
            {
                $data['video'] = '';
            }

            if (mb_strlen($data['title']) < 3){ $this->result['error']['prize[title]'] = __('The field must contain at least 3 characters'); }

            if (!count($this->result['error']) && $data['id'])
            {
                $wpdb->query("
                    UPDATE
                        `{$wpdb->prefix}{$this->prefix}_prizes`
                    SET
                        `fl_id` = '{$data['lesson']}',
                        `title` = '" . esc_sql($data['title']) . "',
                        `description` = '" . esc_sql($data['description']) . "',
                        `message` = '" . esc_sql($data['message']) . "',
                        `video_data` = '{$data['video']}'
                    WHERE
                        `id` = '{$data['id']}'
                ");

                $this->result['message'] = __('Prize successfully saved');
                $this->result['redirect_url'] = admin_url('admin.php?page=' . $this->prefix . '_prizes');
                $this->result['status'] = true;
            }
        break;

        case 'h5p-video-render-get':
            $user = wp_get_current_user();

            $data['user_id'] = (int)$user->ID;
            $data['id']  = (int)$data['id'];

            if ($data['user_id'] && $data['id'])
            {
                $prizes = $wtfs_prize->User_Prizes_Get($data['user_id']);

                if (count($prizes))
                {
                    foreach ($prizes as $v1)
                    {
                        $vd = (array)@unserialize($v1->video_data);

                        if (count($vd))
                        {
                            foreach ($vd as $v2)
                            {
                                if ((int)$v2['h5p'] == $data['id'])
                                {
                                    $this->result['html'] = do_shortcode('[h5p id="' . $data['id'] . '"]');
                                    $this->result['status'] = true;

                                    break;
                                }
                            }
                        }
                    }
                }
            }
        break;
    }
