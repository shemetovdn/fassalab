<?php

    switch ($this->subaction)
    {
        case 'messages-save':
            if (count($_POST))
            {
                foreach ($_POST as $k => $v)
                {
                    if (preg_match("/^{$this->prefix}\-message\-/uis", $k))
                    {
                        update_option($k, $v, false);
                    }
                }
            }

            wp_redirect($_SERVER['HTTP_REFERER']);
        break;
    }
