<?php
/*
 * 1 это на базе типа 1 (новый тип #10)
 * второй на базе 7 (новый тип #9 - Drag&drop)
 * */

    class WT_Fass_Learn
    {
        private $prefix = 'wtfl';
        private $attr = array();

        public function __construct()
        {
            $this->Init();
        }

        private function Init()
        {
            if(!isset($_SESSION)) {
                session_start();
            }
            $this->Attr_Set('prefix', $this->prefix);

            $this->Plugin_Paths_Init();

            //
            global $_wtfl_dr;
            include_once($this->Attr_Get('plugin_data_path') . 'data-relations.php');
            ////

            $this->Mods_Load();
            $this->Plugin_Hooks_Init();
            $this->Plugin_Pages_Init();

            add_action('init', array($this, 'Shortcodes_Init'));
            add_action('init', array($this, 'Actions_Init'));
            add_action('post_updated', array($this, 'Posts_Delete_Filter'), 10, 3);
        }

        protected function Attr_Set($key, $value)
        {
            $this->attr[$key] = $value;
        }

        public function Attr_Get($key)
        {
            if (isset($this->attr[$key]))
            {
                return $this->attr[$key];
            }
            else
            {
                return false;
            }
        }

        public function Prefix_Get()
        {
            return $this->prefix;
        }

        public function Session_Set($key, $value)
        {
            $_SESSION[$this->prefix][$key] = $value;
        }

        public function sessionSetByPost($key)
        {
            if (!empty($_POST["lesson"]['step'])) {
                    $lesson['steps'][] = array(
                        'id'      => $_POST["lesson"]['step']['id'],
                        'type'    => $_POST["lesson"]['step']['type'],
                        'title'   => $_POST["lesson"]['step']['title'],
                        'data'    => array(
                             $_POST["lesson"]['step']['data']
                        ),
                        'removed' => false
                    );
                $_SESSION[$this->prefix][$key] = $lesson;
            }
        }

        public function Session_Get($key)
        {
            if (isset($_SESSION[$this->prefix]) && isset($_SESSION[$this->prefix][$key]))
            {
                return $_SESSION[$this->prefix][$key];
            }
            else
            {
                return false;
            }
        }

        public function Session_Unset($key = null)
        {
            if (is_null($key))
            {
                unset($_SESSION[$this->prefix]);
            }
            else
            {
                unset($_SESSION[$this->prefix][$key]);
            }
        }

        private function Plugin_Paths_Init()
        {
            $ps = array();

            preg_match("/^(.*[\/\\\]wp-content[\/\\\]plugins[\/\\\])(.*?)[\/\\\].*$/uis", __FILE__, $ps);

            if (is_array($ps) && count($ps) == 3)
            {
                $this->Attr_Set('plugin_dir_name', $ps[2]);
                $this->Attr_Set('plugin_path', $ps[1] . $this->Attr_Get('plugin_dir_name') . '/');
                $this->Attr_Set('plugin_index_path', $this->Attr_Get('plugin_path') . 'index.php');
                $this->Attr_Set('plugin_core_path', $this->Attr_Get('plugin_path') . 'core/');
                $this->Attr_Set('plugin_actions_path', $this->Attr_Get('plugin_path') . 'core/actions/');
                $this->Attr_Set('plugin_mods_path', $this->Attr_Get('plugin_path') . 'core/mods/');
                $this->Attr_Set('plugin_data_path', $this->Attr_Get('plugin_path') . 'data/');
                $this->Attr_Set('plugin_templates_path', $this->Attr_Get('plugin_path') . 'data/templates/');
                $this->Attr_Set('plugin_file_uploads_temp_path', wp_upload_dir()['basedir'] . '/' . $this->Attr_Get('prefix') . '-files-temp/');
                $this->Attr_Set('plugin_file_uploads_path', wp_upload_dir()['basedir'] . '/' . $this->Attr_Get('prefix') . '-files/');

                $this->Attr_Set('plugin_url', plugins_url($this->Attr_Get('plugin_dir_name')) . '/');
                $this->Attr_Set('plugin_core_url', $this->Attr_Get('plugin_url') . 'core/');
                $this->Attr_Set('plugin_css_url', $this->Attr_Get('plugin_url') . 'data/css/');
                $this->Attr_Set('plugin_js_url', $this->Attr_Get('plugin_url') . 'data/js/');
                $this->Attr_Set('plugin_img_url', $this->Attr_Get('plugin_url') . 'data/images/');
                $this->Attr_Set('plugin_file_uploads_temp_url', wp_upload_dir()['baseurl'] . '/' . $this->Attr_Get('prefix') . '-files-temp/');
                $this->Attr_Set('plugin_file_uploads_url', wp_upload_dir()['baseurl'] . '/' . $this->Attr_Get('prefix') . '-files/');
            }
        }

        private function Plugin_Hooks_Init()
        {
            register_activation_hook($this->Attr_Get('plugin_index_path'), array($this, 'Install'));
            //register_deactivation_hook();
            register_uninstall_hook($this->Attr_Get('plugin_index_path'), array($this, 'Uninstall'));

            add_filter('body_class', function($classes){
                global $post;

                if (is_single())
                {
                    if (preg_match("/\[{$this->prefix}_lesson[\s\]]?/uis", $post->post_content))
                    {
                        $classes[] = $this->prefix . '-lesson';
                    }

                    if ($post->post_name == $this->prefix . '-lesson-preview')
                    {
                        $classes[] = $this->prefix . '-lesson-preview';
                    }
                }

                return $classes;
            });
        }

        public function Install()
        {
            global $wpdb;

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_lessons`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `title` VARCHAR(255) DEFAULT NULL,
                    `description` LONGTEXT DEFAULT NULL,
                    `status` TINYINT(1) DEFAULT NULL,
                    `date` INT(10) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_lessons_steps`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `lesson_id` BIGINT(20) DEFAULT NULL,
                    `type` TINYINT(1) DEFAULT NULL,
                    `title` VARCHAR(255) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_lessons_steps_data`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `step_id` BIGINT(20) DEFAULT NULL,
                    `data` LONGTEXT DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_lessons_passing`
                (
                    `user_id` BIGINT(20) DEFAULT NULL,
                    `session_id` VARCHAR(255) DEFAULT NULL,
                    `lesson_id` BIGINT(20) DEFAULT NULL,
                    `step_id` BIGINT(20) DEFAULT NULL,
                    `passed` TINYINT(1) DEFAULT NULL,
                    `date` INT(10) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_prizes`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `fl_id` BIGINT(20) DEFAULT NULL,
                    `ls_id` BIGINT(20) DEFAULT NULL,
                    `title` VARCHAR(255) DEFAULT NULL,
                    `description` LONGTEXT DEFAULT NULL,
                    `message` LONGTEXT DEFAULT NULL,
                    `video_data` LONGTEXT DEFAULT NULL,
                    `date` INT(10) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");
        }

        public function Uninstall()
        {
            global $wpdb;

            $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_lessons`");
            $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_lessons_steps`");
            $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_lessons_steps_data`");
            $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_lessons_passing`");
            $wpdb->query("DELETE FROM `{$wpdb->prefix}options` WHERE `option_name` LIKE '{$this->prefix}%'");
            $wpdb->query("DELETE FROM `{$wpdb->prefix}usermeta` WHERE `option_name` LIKE '{$this->prefix}%'");
        }

        public function Posts_Delete_Filter($id, $post_after, $post_before)
        {
            global $wpdb;

            $pt = 'sfwd-lessons';
            $st = array('private');

            if
            (
                is_object($post_before) &&
                isset($post_before->ID) &&
                $post_before->post_type == $pt &&
                preg_match("/^{$this->prefix}\-/uis", $post_before->post_name) &&
                in_array($post_before->post_status, $st) &&
                !in_array($post_after->post_status, $st)
            )
            {
                $wpdb->query("
                    UPDATE
                        `{$wpdb->prefix}posts`
                    SET
                        `post_name` = '{$post_before->post_name}',
                        `post_status` = '{$st[0]}'
                    WHERE
                        `ID` = '{$id}'
                ");
            }
        }

        public function Template_Get($name, $path_dir = null, $data = null)
        {
            if (is_null($path_dir))
            {
                $path_dir = $this->Attr_Get('plugin_templates_path');
            }

            $path = $path_dir . $name . '.php';

            if (file_exists($path))
            {
                include($path);
            }
            else
            {
                return false;
            }
        }

        private function Mods_Load()
        {
            $mods = glob($this->Attr_Get('plugin_mods_path') . '*.php');

            if (count($mods))
            {
                foreach ($mods as $v)
                {
                    include_once($v);
                }
            }
        }

        public function Mod($name)
        {
            $class = 'WT_Fass_Learn_Mod__' . $name;

            if (class_exists($class))
            {
                return new $class;
            }
        }

        public function Page_Title_Build($title)
        {
            return $title . ' :: ' . __('Fass Learn');
        }

        private function Plugin_Pages_Init()
        {
            if (is_admin())
            {
                add_action('admin_menu', function(){
                    add_menu_page($this->Page_Title_Build(__('Main')), __('Fass Learn'), 'manage_options', $this->prefix, function(){
                        if (isset($_REQUEST['preview']) && preg_match("/^[0-9]+$/uis", $_REQUEST['preview']))
                        {
                            $this->Template_Get('pages/lesson-preview');
                        }
                        else
                        {
                            $this->Template_Get('pages/main');
                        }
                    }, 'dashicons-book', 200);

                    add_submenu_page('wtfl', $this->Page_Title_Build(__('Lessons')), __('Lessons'), 'manage_options', $this->prefix . '_lessons', function(){
                        $this->Template_Get('pages/lessons');
                    });

                    add_submenu_page('wtfl', $this->Page_Title_Build(__('Lesson editor')), __('Lesson editor'), 'manage_options', $this->prefix . '_lesson_editor', function(){
                        $data = array();

                        if (isset($_REQUEST['id']) && preg_match("/^[0-9]+$/uis", $_REQUEST['id']))
                        {
                            include_once($this->Attr_Get('plugin_core_path') . 'lesson.php');

                            $lesson = new WT_Fass_Learn_Lesson($this);
                            $data = $lesson->Get($_REQUEST['id']);

                            // Build session of lesson and steps
                            $lesson = $this->Session_Get('lesson');
                            $lesson['steps'] = array();

                            if (isset($data->steps) && count($data->steps))
                            {
                                foreach ($data->steps as $k => $v)
                                {
                                    $lesson['steps'][] = array(
                                        'id'      => $v->id,
                                        'type'    => $v->type,
                                        'title'   => $v->title,
                                        'data'    => array(
                                            (int)$v->type => $v->data
                                        ),
                                        'removed' => false
                                    );
                                }
                            }

                            $this->Session_Set('lesson', $lesson);
                            ////
                        }
                        else
                        {
                            $this->Session_Unset();
                        }

                        $this->Template_Get('pages/lesson-editor', null, $data);
                    });

                    add_submenu_page('wtfl', $this->Page_Title_Build(__('Prizes')), __('Prizes'), 'manage_options', $this->prefix . '_prizes', function(){
                        $this->Template_Get('pages/prizes');
                    });

                    add_submenu_page('wtfl', $this->Page_Title_Build(__('Prize editor')), __('Prize editor'), 'manage_options', $this->prefix . '_prize_editor', function(){
                        $data = array();

                        if (isset($_REQUEST['id']) && preg_match("/^[0-9]+$/uis", $_REQUEST['id']))
                        {
                            include_once($this->Attr_Get('plugin_core_path') . 'prize.php');

                            $prize = new WT_Fass_Learn_Prize($this);
                            $data = $prize->Get($_REQUEST['id']);
                        }

                        $this->Template_Get('pages/prize-editor', null, $data);
                    });

                    add_submenu_page('wtfl', $this->Page_Title_Build(__('Settings')), __('Settings'), 'manage_options', $this->prefix . '_settings', function(){
                        $this->Template_Get('pages/settings');
                    });
                });
            }
        }

        public function Shortcodes_Init()
        {
            add_shortcode($this->prefix . '_lesson', function($atts){
                $atts = shortcode_atts(array(
                    'id' => 0
                ), $atts);

                $atts['id'] = (int)$atts['id'];

                global $wpdb, $_wtfl_dr;

                $lms_id = (int)get_the_ID();

                include_once($this->Attr_Get('plugin_core_path') . 'lesson-view.php');
            });

            add_shortcode($this->prefix . '_prizes', function($atts){
                $atts = shortcode_atts(array(
                    'user_id' => 0
                ), $atts);

                $atts['user_id'] = (int)$atts['user_id'];

                if (!$atts['user_id'] && isset($_REQUEST['user_id']))
                {
                    $atts['user_id'] = (int)$_REQUEST['user_id'];
                }

                if (!$atts['user_id'])
                {
                    $user = wp_get_current_user();
                    $atts['user_id'] = (int)$user->ID;
                }

                if (!$atts['user_id'])
                {
                    return true;
                }

                global $wpdb, $_wtfl_dr;

                $output = '';
                include_once($this->Attr_Get('plugin_core_path') . 'prizes-view.php');
                return $output;
            });
        }

        public function Actions_Init()
        {
            if (isset($_REQUEST['_handler']) && $_REQUEST['_handler'] == $this->prefix)
            {
                include_once($this->Attr_Get('plugin_core_path') . 'action.php');

                $action = new WT_Fass_Learn_Action($this);
                $action->name = $_REQUEST['_action'];
                $action->subaction = $_REQUEST['_subaction'];
                $action->Action_Load($_REQUEST[$_REQUEST['_action']]);
            }
        }

        public function Array_Treatment_Strings(&$array, $procedure)
        {
            $type = null;

            if (is_array($array))
            {
                $type = 'array';
            }
            else
            if (is_object($array))
            {
                $type = 'object';
            }

            if (!is_null($type) && count($array))
            {
                foreach ($array as $k => $v)
                {
                    if (is_string($v) && !preg_match("/^a:[0-9]+:\{/uis", $v))
                    {
                        switch (strtolower($procedure))
                        {
                            case 'addslashes':
                                $v = addslashes($v);
                            break;
                            case 'stripslashes':
                                $v = stripslashes($v);
                            break;
                            case 'htmlspecialchars':
                                $v = htmlspecialchars($v);
                            break;
                            case 'htmlspecialchars_decode':
                                $v = htmlspecialchars_decode($v);
                            break;
                        }
                    }
                    else
                    {
                        $v = $this->Array_Treatment_Strings($v, $procedure);
                    }

                    switch ($type)
                    {
                        case 'array':
                            $array[$k] = $v;
                        break;
                        case 'object':
                            $array->{$k} = $v;
                        break;
                    }
                }
            }

            return $array;
        }
    }
