<?php

    include_once('../../../../wp-load.php');

    global $wpdb, $_wtfl;

    $id = (int)$_REQUEST['id'];
    $step_id = (int)$_REQUEST['step-id'];

    if ($id)
    {
        $post_preview = get_page_by_path($_wtfl->Prefix_Get() . '-lesson-preview', OBJECT, 'sfwd-lessons');

        if (is_object($post_preview) && isset($post_preview->ID))
        {
            $url = get_permalink($post_preview);

            $wpdb->query("
                UPDATE
                    `{$wpdb->prefix}posts`
                SET
                    `post_content` = '[" . $_wtfl->Prefix_Get() . "_lesson id={$id}]'
                WHERE
                    `ID` = '{$post_preview->ID}'
            ");

            if ($step_id) $url .= '?step-id=' . $step_id;

            wp_redirect($url);
            exit;
        }
    }
