<?php

    $this->Session_Set('lesson-passing', array(
        'session-id' => uniqid(),
        'lms-id'     => $lms_id
    ));

    $this->Template_Get('areas/lesson-head');

    if ($atts['id'] > 0)
    {
        include_once($this->Attr_Get('plugin_core_path') . 'lesson.php');

        global $lesson;

        $lesson = new WT_Fass_Learn_Lesson($this);
        $lesson_data = $lesson->Get($atts['id']);

        if (is_object($lesson_data))
        {
            // Fix h5p video prizes init
            if (isset($lesson_data->prize_id) && (int)$lesson_data->prize_id)
            {
                include_once($this->Attr_Get('plugin_core_path') . 'prize.php');
                $wtfs_prize = new WT_Fass_Learn_Prize($this);

                $prize = $wtfs_prize->Get($lesson_data->prize_id);
                $prize->video_data = (array)unserialize($prize->video_data);

                if (count($prize->video_data))
                {
                    foreach ($prize->video_data as $v)
                    {
                        if ($v['popup'] === true)
                        {
                            do_shortcode('[h5p id="' . $v['h5p'] . '"]');
                            break;
                        }
                    }
                }
            }
            ////

            ?>
            <div class="lesson-steps-wrapper">
                <header id="header">
                    <?php

                    $lesson_data->description = preg_replace("/[\r\n]+/uis", '<br />', trim($lesson_data->description));

                    /*
                    if (trim(strip_tags($lesson_data->description)) != '')
                    {
                        ?>
                        <a class="faq" href="#"><?php _e('Conseils & remarques'); ?></a>
                        <div id="lesson-description"><?php echo $lesson_data->description; ?></div>
                        <?php
                    }
                    */

                    ?>
                    <!--Progressbar starts-->
                    <div class="ui small progress" data-percent="0" id="current_progress">
                        <div class="bar"></div>
                    </div>
                    <!--Progressbar ends-->
                    <a class="exit" href="#"><?php _e('Sortir'); ?></a>
                </header>
                <div class="lesson-steps-wrapper-inner">
                    <?php

                        if (is_object($lesson_data))
                        {
                            if (count($lesson_data->steps))
                            {
                                // Move slide type 6 to end of array
                                $slide_video = null;

                                foreach ($lesson_data->steps as $k => $v)
                                {
                                    if ((int)$v->type == 6)
                                    {
                                        $slide_video = $v;
                                        unset($lesson_data->steps[$k]);
                                    }
                                }
                                ////

                                shuffle($lesson_data->steps);

                                // Move slide type 6 to end of array
                                if (is_object($slide_video))
                                {
                                    array_push($lesson_data->steps, $slide_video);
                                }
                                ////

                                foreach ($lesson_data->steps as $k => $v)
                                {
                                    if (isset($_REQUEST['step-id']) && (int)$_REQUEST['step-id'])
                                    {
                                        if ((int)$v->id == (int)$_REQUEST['step-id'])
                                        {
                                            $k = 0;
                                        }
                                        else
                                        {
                                            continue;
                                        }
                                    }

                                    $class = array();

                                    $class[] = 'lesson-step-type-' . $v->type;
                                    $class[] = 'not-passed';

                                    if ($k == 0){ $class[] = 'active'; }
                                    //if ((int)$v->data['rtl'] > 0){ $class[] = 'rtl'; }

                                    $v->data['vk'] = (int)$v->data['vk'];

                                    $data = array(
                                        'lms-id'     => $lms_id,
                                        'lms-passed' => $this->Mod('LearnDash_LMS')->Is_Lesson_Passed($lms_id),
                                        'key'        => $k,
                                        'class'      => implode(' ', $class),
                                        'id'         => (int)$v->id,
                                        'type'       => $v->type,
                                        'title'      => $v->title,
                                        'data'       => $v->data
                                    );

                                    $this->Template_Get('lessons/ims-' . $v->type, null, $data);
                                }
                            }
                        }

                    ?>
                    <div class="lesson-layer-finish">
                        <?php

                            $content = get_option($this->prefix . '-message-wpl');

                            if (trim(strip_tags($content)) != '')
                            {
                                echo $content;
                            }
                            else
                            {
                                ?>
                                <p><?php _e('Bravo! On continue.'); ?></p>
                                <?php
                            }

                            $lms_next = $this->Mod('LearnDash_LMS')->Course_Lesson_Next_Get($lms_id);

                            ?>
                            <p class="actions">
                                <?php

                                    if ($lms_next !== false)
                                    {
                                        ?>
                                        <a href="<?php echo get_permalink($lms_next); ?>" class="btn lms-lesson-next green"><?php _e('Leçon suivante'); ?> <i class="fa fa-arrow-right"></i></a>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <a href="<?php echo get_site_url(); ?>" class="btn lms-lesson-next green"><?php _e('Sortir'); ?> <i class="fa fa-arrow-right"></i></a>
                                        <?php
                                    }

                                ?>
                            </p>
                            <?php

                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        else
        {
            ?>
            <div class="lesson-steps-wrapper error">
                <div class="message">
                    <p><?php _e('Ne existe pas cette leçon') ?>.</p>
                    <p><?php _e('Vérifiez la shortcode exposée') ?>.</p>
                </div>
            </div>
            <?php
        }
    }
