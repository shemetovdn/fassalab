<?php

    class WT_Fass_Learn_Lesson
    {
        public $core;
        private $prefix;

        public function __construct($core)
        {
            $this->core = $core;
            $this->prefix = $this->core->Attr_Get('prefix');
        }

        public function Get($id)
        {
            global $wpdb;

            $lesson = $wpdb->get_results("
                SELECT
                    l.*,
                    p.`id` AS 'prize_id',
                    p.`title` AS 'prize_title'
                FROM
                    `{$wpdb->prefix}{$this->prefix}_lessons` l
                LEFT JOIN
                    `{$wpdb->prefix}{$this->prefix}_prizes` p
                    ON
                    p.`fl_id` = l.`id`
                WHERE
                    l.`id` = '{$id}'
            ");

            if (count($lesson))
            {
                $lesson = $lesson[0];

                $lesson_steps = $wpdb->get_results( "
                    SELECT
                        ls.*,
                        lsd.`data`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons_steps` ls
                    LEFT JOIN
                        `{$wpdb->prefix}{$this->prefix}_lessons_steps_data` lsd
                        ON
                        lsd.`step_id` = ls.`id`
                    WHERE
                        ls.`lesson_id` = '{$lesson->id}'
                    ORDER BY
                        ls.`id`
                        ASC
                ");

                $lesson->steps = $lesson_steps;

                if (count($lesson->steps))
                {
                    foreach ($lesson->steps as $k => $v)
                    {
                        $lesson->steps[$k]->data = (array)unserialize($v->data);
                    }
                }
            }

            $this->core->Array_Treatment_Strings($lesson, 'stripslashes');
            $this->core->Array_Treatment_Strings($lesson, 'htmlspecialchars');

            return $lesson;
        }

        public function Get_Api($id)
        {
            $get_id = $this-> Get($id);
            $get_id->id =(int)$get_id->id;
            $get_id->rtl =(int)$get_id->rtl;
            $get_id->status =(int)$get_id->status;
            foreach ($get_id->steps as &$step) {
                $this->Pretty_Step($step);
            }
            return $get_id;
        }

        public function Pretty_Step($step)
        {
            $step->type      = (int)$step->type;
            $step->lesson_id = (int)$step->lesson_id;
            $step->id        = (int)$step->id;
            if (isset($step->data) && isset($step->data['answer-true'])) {
                foreach ($step->data['answer-true'] as $answer_k=>$answer_v) {
                    $step->data['answer-true'] = (int)$answer_v;
                }
                if(isset($step->data['tia'])){
                    $pretty_tia = array();
                    foreach ($step->data['tia'] as $k=>$tia) {
                        $pretty_tia[$k.'_'] = $tia ;
                    }
                    $step->data ['tia'] = $pretty_tia;
                }

                if(isset($step->data['files'])){
                    $pretty_files = array();
                    foreach ($step->data['files'] as $k=>$files) {
                        $pretty_files[$k.'_'] = $files ;
                    }
                    $step->data ['files'] = $pretty_files;
                }
                if(isset($step->data['audio'])){
                    $pretty_audio = array();
                    foreach ($step->data['audio'] as $k=>$audio) {
                        $pretty_audio[$k.'_'] = $audio ;
                    }
                    $step->data ['audio'] = $pretty_audio;
                }

            }
            return $step;
        }

        public function Step_Get($id)
        {
            global $wpdb;

            $lesson_step = $wpdb->get_results("
                SELECT
                    ls.*,
                    lsd.`data`
                FROM
                    `{$wpdb->prefix}{$this->prefix}_lessons_steps` ls
                LEFT JOIN
                    `{$wpdb->prefix}{$this->prefix}_lessons_steps_data` lsd
                    ON
                    lsd.`step_id` = ls.`id`
                WHERE
                    ls.`id` = '{$id}'
            ");

            if (count($lesson_step))
            {
                $lesson_step = $lesson_step[0];
                $lesson_step->data = (array)unserialize($lesson_step->data);

                $this->core->Array_Treatment_Strings($lesson_step, 'stripslashes');
                $this->core->Array_Treatment_Strings($lesson_step, 'htmlspecialchars');

                return $lesson_step;
            }
        }

        public function getAll($exclude_ids = array(), $order = '')
        {
            global $wpdb;

            $where = '';

            if (is_array($exclude_ids) && count($exclude_ids))
            {
                if ($where != ''){ $where .= ' AND '; }
                $where .= "`id` NOT IN ('" . implode("', '", $exclude_ids) . "')";
            }

            if ($where != ''){ $where = "WHERE {$where}"; }
            if ($order != ''){ $order = "ORDER BY {$order}"; }

            return $wpdb->get_results("
                SELECT
                    *
                FROM
                    `{$wpdb->prefix}{$this->prefix}_lessons`
                {$where}    
                {$order}
            ");
        }

        public function Step_Save($lesson_id, $step_id, $type, $title)
        {
            global $wpdb;

            $lesson_id = (int)$lesson_id;
            $step_id = (int)$step_id;
            $type = (int)$type;

            $title = htmlspecialchars_decode($title);
            $title = preg_replace("/[\s]+/uis", ' ', trim($title));

            if ($lesson_id > 0)
            {
                if ($step_id > 0)
                {
                    $wpdb->query("
                        UPDATE
                            `{$wpdb->prefix}{$this->prefix}_lessons_steps`
                        SET
                            `type` = '{$type}',
                            `title` = '" . esc_sql($title) . "'
                        WHERE
                            `id` = '{$step_id}'
                            AND
                            `lesson_id` = '{$lesson_id}'
                    ");
                }
                else
                {
                    $wpdb->query("
                        INSERT INTO
                          `{$wpdb->prefix}{$this->prefix}_lessons_steps`
                          (
                              `lesson_id`,
                              `type`,
                              `title`
                          )
                          VALUES
                          (
                              '{$lesson_id}',
                              '{$type}',
                              '" . esc_sql($title) . "'
                          )
                    ");

                    $step_id = (int)$wpdb->insert_id;
                }
            }

            return $step_id;
        }

        public function Step_Data_Save($step_id, $data)
        {
            global $wpdb;

            $step_id = (int)$step_id;

            if ($step_id)
            {
                // Clear removed files
                if (isset($data['files']))
                {
                    foreach ($data['files'] as $k => $v)
                    {
                        if ($v['removed'] === true)
                        {
                            unset($data['files'][$k]);
                        }
                    }
                }
                ////

                $this->core->Array_Treatment_Strings($data, 'htmlspecialchars_decode');

                $data = esc_sql(serialize((array)$data));

                $step_data = $wpdb->get_results("
                    SELECT
                        lsd.`id`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons_steps_data` lsd
                    WHERE
                        lsd.`step_id` = '{$step_id}'
                ");

                if (count($step_data))
                {
                    $wpdb->query("
                        UPDATE
                            `{$wpdb->prefix}{$this->prefix}_lessons_steps_data`
                        SET
                            `data` = '{$data}'
                        WHERE
                            `step_id` = '{$step_id}'
                    ");
                }
                else
                {
                    $wpdb->query("
                        INSERT INTO
                          `{$wpdb->prefix}{$this->prefix}_lessons_steps_data`
                          (
                              `step_id`,
                              `data`
                          )
                          VALUES
                          (
                              '{$step_id}',
                              '{$data}'
                          )
                    ");
                }

                return true;
            }

            return false;
        }

        public function Files_Temp_Clear($time_offset = 43200) // 12 hours
        {
            $files = glob($this->core->Attr_Get('plugin_file_uploads_temp_path') . '*.*');

            if (count($files))
            {
                foreach ($files as $v)
                {
                    if ((filectime($v) + $time_offset) <= time())
                    {
                        @unlink($v);
                    }
                }
            }
        }

        public function File_Upload_Temp($key, $file_types)
        {
            $files = array();

            $this->Files_Temp_Clear();

            if (isset($_FILES[$key]))
            {
                $fks = array_keys($_FILES[$key]);

                if (count($_FILES[$key]['error']))
                {
                    foreach ($_FILES[$key]['error'] as $k1 => $v1)
                    {
                        if (isset($file_types[$k1]))
                        {
                            $type_filter = $file_types[$k1];
                        }
                        else
                        {
                            $type_filter = '*.';
                        }

                        if (!$v1 && preg_match("/\/{$type_filter}$/uis", $_FILES[$key]['type'][$k1]))
                        {
                            foreach ($fks as $k2 => $v2)
                            {
                                $files[$k1][$v2] = $_FILES[$key][$v2][$k1];
                            }
                        }
                    }
                }

                if (count($files))
                {
                    if (!file_exists($this->core->Attr_Get('plugin_file_uploads_temp_path')))
                    {
                        @mkdir($this->core->Attr_Get('plugin_file_uploads_temp_path'), 0777);
                    }

                    foreach ($files as $k => $v)
                    {
                        $file_name = md5($v['tmp_name']) . '.' . explode('/', $v['type'])[1];

                        if (move_uploaded_file($v['tmp_name'], $this->core->Attr_Get('plugin_file_uploads_temp_path') . $file_name))
                        {
                            chmod($this->core->Attr_Get('plugin_file_uploads_temp_path') . $file_name, 0777);

                            $files[$k]['fu_path'] = $this->core->Attr_Get('plugin_file_uploads_temp_path') . $file_name;
                            $files[$k]['fu_url'] = $this->core->Attr_Get('plugin_file_uploads_temp_url') . $file_name;
                            $files[$k]['fu_temp'] = true;
                            $files[$k]['removed'] = false;
                        }
                        else
                        {
                            unset($files[$k]);
                        }
                    }
                }
            }

            return $files;
        }

        public function Files_Storage_Diff_Clear($file_key = 'files')
        {
            if (!function_exists('Name_From_Path_Get'))
            {
                function Name_From_Path_Get($value)
                {
                    return preg_replace("/^(.*[\/\\\])(.*?\.[a-z0-9]{1,4})$/uis", "$2", $value);
                }
            }

            global $wpdb;

            $files = glob($this->core->Attr_Get('plugin_file_uploads_path') . '*.*');
            $files_d = array();

            if (count($files))
            {
                foreach ($files as $k => $v)
                {
                    $files[$k] = Name_From_Path_Get($v);
                }
            }

            $step_data = $wpdb->get_results("
                SELECT
                    lsd.`data`
                FROM
                    `{$wpdb->prefix}{$this->prefix}_lessons_steps_data` lsd
            ");

            if (count($step_data))
            {
                foreach ($step_data as $v)
                {
                    $v->data = (array)unserialize($v->data);

                    if (isset($v->data[$file_key]))
                    {
                        foreach ($v->data[$file_key] as $v)
                        {
                            if ($v['fu_temp'] === false)
                            {
                                if (($key = array_search(Name_From_Path_Get($v['fu_path']), $files)) === false)
                                {
                                    $files_d[] = Name_From_Path_Get($v['fu_path']);
                                }
                            }
                        }
                    }
                }

                if (count($files_d))
                {
                    foreach ($files_d as $v)
                    {
                        @unlink($this->core->Attr_Get('plugin_file_uploads_path') . $v);
                    }
                }
            }
        }

        public function File_Upload_Temp_To_Storage(&$files)
        {
            if (count($files))
            {
                if (!file_exists($this->core->Attr_Get('plugin_file_uploads_path')))
                {
                    @mkdir($this->core->Attr_Get('plugin_file_uploads_path'), 0777);
                }

                foreach ($files as $k => $v)
                {
                    if ($v['removed'] === false && $v['fu_temp'] === true)
                    {
                        $file_name = md5($v['tmp_name']) . '.' . explode('/', $v['type'])[1];

                        if (rename($v['fu_path'], $this->core->Attr_Get('plugin_file_uploads_path') . $file_name))
                        {
                            $files[$k]['fu_path'] = $this->core->Attr_Get('plugin_file_uploads_path') . $file_name;
                            $files[$k]['fu_url'] = $this->core->Attr_Get('plugin_file_uploads_url') . $file_name;
                            $files[$k]['fu_temp'] = false;
                            $files[$k]['removed'] = false;
                        }

                        unset($files[$k]['tmp_name']);
                    }
                }
            }
        }

        public function File_Url_Fix($file)
        {
            if (is_array($file))
            {
                $file_name = preg_replace("/^(.*[\/\\\])(.*?\.[a-z0-9]{1,4})$/uis", "$2", $file['fu_url']);

                if ($file['fu_temp'])
                {
                    return $this->core->Attr_Get('plugin_file_uploads_temp_url') . $file_name;
                }
                else
                {
                    return $this->core->Attr_Get('plugin_file_uploads_url') . $file_name;
                }
            }
        }

        public function Passing($lesson_id, $step_id, $passed, $api_token = null) // passed step
        {
            global $wpdb;

            $user_id = (int)wp_get_current_user()->ID;
            $lesson_id = (int)$lesson_id;
            $step_id = (int)$step_id;

            if ($api_token != null) {
                $lp['session-id'] = esc_sql($api_token);
            } else {
                $lp = $this->core->Session_Get('lesson-passing');
            }


            if (!$passed || (int)$passed < 1) {
                $passed = 0;
            } else {
                $passed = 1;
            }

            if ($user_id &&
                $lesson_id &&
                $step_id &&
                ( preg_match("/^[a-z0-9]{13}$/uis", $lp['session-id']) || preg_match("/^[a-z0-9]{32}$/uis", $lp['session-id'])) )
            {
                $result = $wpdb->get_results("
                    SELECT
                        lp.`session_id`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons_passing` lp
                    WHERE
                        lp.`user_id` = '{$user_id}'
                        AND
                        lp.`session_id` = '{$lp['session-id']}'
                        AND
                        lp.`lesson_id` = '{$lesson_id}'
                        AND
                        lp.`step_id` = '{$step_id}'
                ");

                if (!count($result))
                {
                    $wpdb->query("
                        INSERT INTO
                            `{$wpdb->prefix}{$this->prefix}_lessons_passing`
                            (
                                `user_id`,
                                `session_id`,
                                `lesson_id`,
                                `step_id`,
                                `passed`,
                                `date`
                            )
                            VALUES
                            (
                                '{$user_id}',
                                '{$lp['session-id']}',
                                '{$lesson_id}',
                                '{$step_id}',
                                '{$passed}',
                                '" . time() . "'
                            )
                    ");

                    return true;
                }
                else
                {
                    $wpdb->query("
                        UPDATE
                            `{$wpdb->prefix}{$this->prefix}_lessons_passing`
                        SET
                            `passed` = '{$passed}',
                            `date` = '" . time() . "'
                        WHERE
                            `user_id` = '{$user_id}'
                            AND
                            `session_id` = '{$lp['session-id']}'
                            AND
                            `lesson_id` = '{$lesson_id}'
                            AND
                            `step_id` = '{$step_id}'
                    ");
                }
            }

            return false;
        }

        public function Get_Passing_list($lesson_id , $api_token)
        {
            global $wpdb;

            $user_id = (int)wp_get_current_user()->ID;
            $lesson_id = (int)$lesson_id;

            $result = $wpdb->get_results("
                    SELECT
                        lp.`session_id`, 
                        lp.`step_id`,
                        lp.`passed`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons_passing` lp
                    WHERE
                        lp.`user_id` = '{$user_id}'
                        AND
                        lp.`lesson_id` = '{$lesson_id}'
                ");

            $list = [];
            if (count($result)) {
                foreach ($result as $row) {
                    $value = (int)$row->passed;
                    if (isset($list[$row->step_id])) {
                        $value = ((int)$list[ $row->step_id ] > $value) ? (int)$list[$row->step_id] : $value;
                    }
                    $list[$row->step_id] = $value;
                }
            }

            return $list;
        }

        public function Passing_Fasslearn($user_id, $deck_id)
        {
            global $wpdb;

            $result = $wpdb->get_results( "SELECT 
                        * 
                        FROM
                         `{$wpdb->prefix}wtfa_decks_words_data_users`
                         WHERE
                          `user_id` = '{$user_id}'
                         AND
                          `deck_id` = '{$deck_id}'
                      ");

            if (count($result)) {
                $wpdb->query("
                     UPDATE
                        `{$wpdb->prefix}wtfa_decks_words_data_users`
                    SET
                        `fasslearn_passed` = '1'
                    WHERE
                        `user_id` = '{$user_id}'
                        AND
                        `deck_id` = '{$deck_id}'
                ");
            } else {
                $empty_array = serialize(array());
                $wpdb->query("
                    INSERT INTO
                      `{$wpdb->prefix}wtfa_decks_words_data_users`
                      (`user_id` , `deck_id`, `fasslearn_passed` ,`data`)
                    VALUE 
                      ('{$user_id}', '{$deck_id}', 1, '{$empty_array}')
            ");

            }


//            echo '<pre>';
//            var_dump($user_id, $deck_id);
//            var_dump($wpdb->last_result);
//            var_dump($wpdb->last_error);
//            var_dump($wpdb->last_query);
//            echo '</pre>';
//            die();
        }

        /*
         * $passed_fl = 1 if passed fasslearn in this deck
         * */
        public function Get_Next_Step($lesson_id , $api_token, $step_id, $passed_fl = 0, $get_next_by_num = false)
        {

            if (isset($step_id) && $step_id != 0) {
                $step = $this ->Step_Get($step_id);
                return $this->Pretty_Step($step);
            }
            $lesson_steps_list = $this->Get_Api($lesson_id);
            if ($passed_fl && $get_next_by_num) {
                foreach ($lesson_steps_list->steps as $key => $step) {

                    if ($step->id == $get_next_by_num) {
                        if ($key != count($lesson_steps_list->steps)-1) {
                            return $this->Pretty_Step($lesson_steps_list->steps[ $key + 1 ]);
                        } else {
                            return $this->Pretty_Step($lesson_steps_list->steps[0]);
                        }
                    }
                }
            } else if ($passed_fl) { // if passed - return 1st element
                return $this->Pretty_Step($lesson_steps_list->steps[0]);
            }


            $list_passed = $this->Get_Passing_list($lesson_id, $api_token);
            $list_unpassed = [];

            foreach ($lesson_steps_list->steps as $step){
                $flag = true;
                foreach ($list_passed as $k => $v) {
                    $temp_k[]=[
                        $k =>$v
                    ];

                    if ($k == $step->id && $v == 1) {
                        $flag = false;
                    }
                }
                if ($flag){
                    $list_unpassed[] = $step->id;
                }
            }

            if (count($list_unpassed)) {
                shuffle($list_unpassed);
                $step = $this ->Step_Get($list_unpassed[0]);

                return $this->Pretty_Step($step);
//                return $this->Pretty_Step('1148');
            }

          return null;
        }

        public function Lesson_User_Progress_Get($lesson_id,  $api_token = null)
        {
            global $wpdb;

            $res = array(
                'steps-count'  => 0,
                'steps-passed' => 0
            );

            $user_id = (int)wp_get_current_user()->ID;
            $lesson_id = (int)$lesson_id;

            if($api_token === true){
                $data__ = @json_decode(file_get_contents('php://input'), true);
                $token = ($data__[$data__['_action']]['token']);
                $lp['session-id'] = esc_sql($token );
            }else if ($api_token != null) {
                $lp['session-id'] = esc_sql($api_token);
            } else {
                $lp = $this->core->Session_Get('lesson-passing');
            }

            if ($user_id &&
                $lesson_id &&
                ( preg_match("/^[a-z0-9]{13}$/uis", $lp['session-id']) || preg_match("/^[a-z0-9]{32}$/uis", $lp['session-id'])) )
            {
                $result = $wpdb->get_results("
                    SELECT
                        COUNT(ls.`id`)
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons_steps` ls
                    WHERE
                        ls.`lesson_id` = '{$lesson_id}'
                ");

                foreach ($result[0] as $k =>$v) {
                    $res['steps-count'] = (int)$v;
                }

                //$result = $wpdb->get_results("SELECTCOUNT(lp.`session_id`)FROM`{$wpdb->prefix}{$this->prefix}_lessons_passing` lpWHERElp.`user_id` = '{$user_id}'ANDlp.`session_id` = '{$lp['session-id']}'ANDlp.`lesson_id` = '{$lesson_id}'ANDlp.`passed` = 1");
                $result = $wpdb->get_results("
                    SELECT
                        *
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_lessons_passing` lp
                    WHERE
                        lp.`user_id` = '{$user_id}'
                        AND
                        lp.`lesson_id` = '{$lesson_id}'
                        AND
                        lp.`passed` = 1
                ");



                $list = [] ;
                foreach ($result as $k =>$v) {
                    if(isset ($list[$v-> step_id])) {
                        $list[ $v->step_id ] = ($list[ $v->step_id ] == 1) ? $list[ $v->step_id ] : $v->passed;
                    } else {
                        $list[ $v->step_id ] = $v->passed;
                    }
                }

//                foreach ($result[0] as $k =>$v) {
                    $res['steps-passed'] = count($list);
//                }
//                $res['steps-passed'] = count($result);

//                $res['%'] = (int)number_format($res['steps-passed'] / $res['steps-count'] * 100, 0, null, '');
                $res['%'] = $res['steps-passed'] / $res['steps-count'] * 100;
            }

            return $res;
        }
    }
