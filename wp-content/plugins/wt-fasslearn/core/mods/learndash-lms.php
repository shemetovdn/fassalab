<?php

    $this->Mod('LearnDash_LMS')->Init();

    class WT_Fass_Learn_Mod__LearnDash_LMS
    {
        static $user_meta_key_course_progress = '_sfwd-course_progress';
        static $intercepted = 'Intercepted by Fasslearn';
        static $lessons_post_type = 'sfwd-lessons';
        static $topic_post_type = 'sfwd-topic';

        static function Init()
        {
            self::Hooks_Init();
        }

        static function Hooks_Init()
        {
            if (isset($_REQUEST['sfwd_mark_complete']))
            {
                //die(self::$intercepted);
            }

//            add_action('wp', array('WT_Fass_Learn_Mod__LearnDash_LMS', 'Lesson_View'));
        }

        static function Lesson_Course_Get($lesson_id)
        {
            return (int)get_post_meta((int)$lesson_id, 'course_id', true);
        }

        static function Topic_Lesson_Parent_Get($topic_id)
        {
            return (int)get_post_meta((int)$topic_id, 'lesson_id', true);
        }

        static function Lesson_Data_Get($lesson_id)
        {
            $res = array();
            $res['course-progress'] = array();

            $lesson_id = (int)$lesson_id;
            $user_id = (int)wp_get_current_user()->ID;
            $course_id = self::Lesson_Course_Get($lesson_id);

            //
            $data = (array)get_user_meta($user_id, self::$user_meta_key_course_progress, true);

            if (isset($data[$course_id]) && isset($data[$course_id]['lessons'][$lesson_id]))
            {
                $res['course-progress'] = $data[$course_id];
            }
            ////

            return $res;
        }

        static function Course_Lessons_Get($course_id)
        {
            global $wpdb;

            $course_id = (int)$course_id;

            $result = $wpdb->get_results("
                SELECT
                    p.`ID`
                FROM
                    `{$wpdb->prefix}postmeta` pm
                LEFT JOIN
                    `{$wpdb->prefix}posts` p
                    ON
                    p.`ID` = pm.`post_id`
                WHERE
                    pm.`meta_key` = 'course_id'
                    AND
                    pm.`meta_value` = '{$course_id}'
                    AND
                    p.`post_type` = '" . self::$lessons_post_type . "'
                    AND
                    p.`post_status` = 'publish'
                ORDER BY
                    p.`menu_order`
                    ASC
            ", ARRAY_A);

            return wp_list_pluck($result, 'ID');
        }

        static function Course_Topics_Get($course_id, $lesson_parent_id)
        {
            global $wpdb;

            $course_id = (int)$course_id;
            $lesson_parent_id = (int)$lesson_parent_id;

            $result = $wpdb->get_results("
                SELECT
                    p.`ID`
                FROM
                    `{$wpdb->prefix}postmeta` pm
                LEFT JOIN
                    `{$wpdb->prefix}posts` p
                    ON
                    p.`ID` = pm.`post_id`
                LEFT JOIN
                    `{$wpdb->prefix}postmeta` pm2
                    ON
                    pm2.`post_id` = pm.`post_id`    
                WHERE
                    pm.`meta_key` = 'course_id'
                    AND
                    pm.`meta_value` = '{$course_id}'
                    AND
                    pm2.`meta_key` = 'lesson_id'
                    AND
                    pm2.`meta_value` = '{$lesson_parent_id}'
                    AND
                    p.`post_type` = '" . self::$topic_post_type . "'
                    AND
                    p.`post_status` = 'publish'
                ORDER BY
                    p.`menu_order`
                    ASC    
            ", ARRAY_A);

            return wp_list_pluck($result, 'ID');
        }

        static function Course_Lesson_Next_Get($lesson_id)
        {
            $lesson_id = (int)$lesson_id;
            $course_id = self::Lesson_Course_Get($lesson_id);

            switch (get_post_type($lesson_id))
            {
                case self::$lessons_post_type:
                    $lessons = self::Course_Lessons_Get($course_id);
                break;
                case self::$topic_post_type:
                    $lesson_parent_id = self::Topic_Lesson_Parent_Get($lesson_id);
                    $lessons = self::Course_Topics_Get($course_id, $lesson_parent_id);
                break;
            }

            $k = (int)array_search($lesson_id, $lessons);

            if (isset($lessons[$k + 1]))
            {
                return (int)$lessons[$k + 1];
            }

            return false;
        }

        static function Is_Lesson_Passed($lesson_id)
        {
            $lesson_id = (int)$lesson_id;

            $data = self::Lesson_Data_Get($lesson_id)['course-progress'];

            switch (get_post_type($lesson_id))
            {
                case self::$lessons_post_type:
                    $k = 'lessons';
                    $data_l = &$data[$k][$lesson_id];
                break;
                case self::$topic_post_type:
                    $k = 'topics';
                    $lesson_parent_id = self::Topic_Lesson_Parent_Get($lesson_id);
                    $data_l = &$data[$k][$lesson_parent_id][$lesson_id];
                break;
            }

            return (bool)$data_l;
        }

        static function Lesson_Status_Passed_Set($lesson_id, $status)
        {
            global $wpdb;

            $lesson_id = (int)$lesson_id;
            $course_id = self::Lesson_Course_Get($lesson_id);
            $status = (boolean)$status;
            $user_id = (int)wp_get_current_user()->ID;
            $k = '';

            $data = (array)get_user_meta($user_id, self::$user_meta_key_course_progress, true);
            $course = &$data[$course_id];

            switch (get_post_type($lesson_id))
            {
                case self::$lessons_post_type:
                    $k = 'lessons';
                    $data_l = &$course[$k];
                break;
                case self::$topic_post_type:
                    $k = 'topics';
                    $lesson_parent_id = self::Topic_Lesson_Parent_Get($lesson_id);
                    $data_l = &$course[$k][$lesson_parent_id];
                break;
            }

            if ($k != '')
            {
                if ($status)
                {
                    $data_l[$lesson_id] = 1;
                }
                else
                {
                    unset($data_l[$lesson_id]);
                }

                $course['completed'] = count($data_l);

                // Valid set successfull progress of global lesson of topics
                if (isset($lesson_parent_id) && (int)$lesson_parent_id > 0)
                {
                    $result = $wpdb->get_results("
                        SELECT
                            pm.`post_id`
                        FROM
                            `{$wpdb->prefix}postmeta` pm
                        LEFT JOIN
                            `{$wpdb->prefix}posts` p
                            ON
                            p.`ID` = pm.`post_id`   
                        WHERE
                            pm.`meta_key` = 'lesson_id'
                            AND
                            pm.`meta_value` = '{$lesson_parent_id}'
                            AND
                            p.`post_status` = 'publish'   
                    ", ARRAY_A);

                    if (count($result))
                    {
                        foreach ($result as $rk => $rv)
                        {
                            if ((bool)$data_l[$rv['post_id']])
                            {
                                unset($result[$rk]);
                            }
                        }

                        if (!count($result))
                        {
                            $course['lessons'][$lesson_parent_id] = 1;
                        }
                        else
                        {
                            $course['lessons'][$lesson_parent_id] = 0;
                        }
                    }
                }
                ////

                update_user_meta($user_id, self::$user_meta_key_course_progress, (array)$data);
            }
        }

        static function Lesson_View()
        {

            //if (learndash_lesson_progression_enabled() === false) return;
//echo '<pre>';
//var_dump(!isset($_REQUEST['without_redirect']));
//var_dump(is_single());
//echo '</pre>';
            if (!isset($_REQUEST['without_redirect']) && is_single())
            {
                global $post;

                if ($post->post_type == self::$lessons_post_type)
                {
                    if (!self::Is_Lesson_Passed($post->ID))
                    {
                        $course_id = self::Lesson_Course_Get($post->ID);
                        $lessons = self::Course_Lessons_Get($course_id);

                        $k = (int)array_search($post->ID, $lessons);

                        if ($k > 0 && !self::Is_Lesson_Passed($lessons[$k - 1]))
                        {
                            for ($i = $k - 1; $i >= 0; $i--)
                            {
                                if ($i == 0 || self::Is_Lesson_Passed($lessons[$i - 1]))
                                {
                                    wp_redirect(get_permalink($lessons[$i]));
                                    die();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
