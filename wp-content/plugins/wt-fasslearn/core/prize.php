<?php

    class WT_Fass_Learn_Prize
    {
        public $core;
        private $prefix;

        public function __construct($core)
        {
            $this->core = $core;
            $this->prefix = $this->core->Attr_Get('prefix');
        }

        public function Get($id)
        {
            global $wpdb;

            $prize = $wpdb->get_results("
                SELECT
                    p.*
                FROM
                    `{$wpdb->prefix}{$this->prefix}_prizes` p
                WHERE
                    p.`id` = '{$id}'
            ");

            if (count($prize))
            {
                $prize = $prize[0];
            }

            $this->core->Array_Treatment_Strings($prize, 'stripslashes');
            $this->core->Array_Treatment_Strings($prize, 'htmlspecialchars');

            return $prize;
        }

        public function User_Prizes_Update($user_id, $fl_ids, $action)
        {
            $user_id = (int)$user_id;

            if (is_array($fl_ids) && count($fl_ids))
            {
                array_walk($fl_ids, function(&$v, $k){
                    $v = (int)$v;
                });
            }
            else
            {
                $fl_ids = array((int)$fl_ids);
            }

            if ($user_id && count($fl_ids) && $fl_ids[0] && $action != '')
            {
                $mk = "{$this->prefix}_prizes";
                $data = (array)get_user_meta($user_id, $mk, true);
                if (count($data) == 1 && $data[0] == '') $data = array();

                switch (mb_strtolower($action))
                {
                    case 'remove':
                        foreach ($fl_ids as $k => $v)
                        {
                            $s = array_search($v, $data);

                            if ($s !== false)
                            {
                                unset($data[$s]);
                            }
                        }
                    break;
                    case 'add':
                        foreach ($fl_ids as $k => $v)
                        {
                            if (!in_array($v, $data))
                            {
                                $data[] = $v;
                            }
                        }
                    break;
                }

                update_user_meta($user_id, $mk, (array)array_values($data));

                return true;
            }
        }

        public function User_Prizes_Get($user_id)
        {
            global $wpdb;

            $user_id = (int)$user_id;

            if ($user_id)
            {
                $data = (array)get_user_meta($user_id, "{$this->prefix}_prizes", true);

                if (count($data))
                {
                    $prizes = $wpdb->get_results("
                        SELECT
                            p.*
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_prizes` p
                        WHERE
                            p.`fl_id` IN ('" . implode("','", $data) . "')
                    ");

                    if (count($prizes))
                    {
                        return $prizes;
                    }
                }
            }
        }
    }
