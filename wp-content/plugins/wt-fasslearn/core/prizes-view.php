<?php

    if ($atts['user_id'] && isset($_wtfl_dr['prize']['category']) && count($_wtfl_dr['prize']['category']))
    {
        include_once($this->Attr_Get('plugin_core_path') . 'prize.php');
        $wtfs_prize = new WT_Fass_Learn_Prize($this);

        $prizes = $wtfs_prize->User_Prizes_Get($atts['user_id']);
        $user_prizes = array();

        if (count($prizes))
        {
            foreach ($prizes as $k1 => $v1)
            {
                $video_data = (array)@unserialize($v1->video_data);

                if (is_array($video_data) && count($video_data))
                {
                    foreach ($video_data as $k2 => $v2)
                    {
                        $v2['title'] = preg_replace("/[\s]{2,}/uis", ' ', trim($v2['title']));

                        $data = array(
                            'title'       => $v1->title,
                            'description' => $v1->description,
                            'h5p'         => (int)$v2['h5p'],
                            'category'    => (int)$v2['category']
                        );

                        if ($v2['title'] != '')
                        {
                            $data['caption'] = $v2['title'];
                        }

                        $user_prizes[(int)$v2['category']][] = $data;

                        if (!$h5p_init_id)
                        {
                            $h5p_init_id = (int)$v2['h5p'];
                        }
                    }
                }
            }
        }

        if (count($user_prizes))
        {
            ob_start();

            ?>
            <link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_js_url'); ?>fancybox/jquery.fancybox.css">
            <link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>prizes-view.css">

            <script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>fancybox/jquery.fancybox.pack.js"></script>
            <script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>prizes-view.js"></script>
            <script type="text/javascript">
                window.WTFL_Prizes_View.attr.prefix = '<?php echo $this->prefix; ?>';
                window.WTFL_Prizes_View.attr.url_h5p_css = '<?php echo $this->Attr_Get('plugin_css_url'); ?>prizes-view-h5p.css';

                jQuery(document).ready(function(){
                    window.WTFL_Prizes_View.init();
                });
            </script>

            <div class="<?php echo $this->prefix; ?>_prizes shortcode">
                <?php

                    foreach ($_wtfl_dr['prize']['category'] as $k1 => $v1)
                    {
                        ?>
                        <div class="tab">
                            <div class="caption"><?php echo $v1; ?></div>
                            <div class="content">
                                <?php

                                    if (isset($user_prizes[$k1]) && count($user_prizes[$k1]))
                                    {
                                        foreach ($user_prizes[$k1] as $v2)
                                        {
                                            ?>
                                            <div class="prize h5p-video" data-id="<?php echo $v2['h5p']; ?>">
                                                <a href="#" class="c">&nbsp;</a>
                                                <div class="preview">
                                                    <div class="iframe">
                                                        <?php echo do_shortcode('[h5p id="' . $v2['h5p'] . '"]'); ?>
                                                    </div>
                                                </div>
                                                <div class="info">
                                                    <?php

                                                        $vd['title'] = preg_replace("/[\s]{2,}/uis", ' ', trim($vd['title']));

                                                        if (isset($v2['caption']))
                                                        {
                                                            ?>
                                                            <div class="title"><?php echo $v2['caption']; ?></div>
                                                            <?php
                                                        }

                                                    ?>
                                                    <div class="caption"><?php echo $v2['title']; ?></div>
                                                    <div class="description"><?php echo $v2['description']; ?></div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        ?>
                                        <span><?php _e('Liste est vide'); ?></span>
                                        <?php
                                    }

                                ?>
                            </div>
                        </div>
                        <?php
                    }

                ?>
            </div>
            <?php

            $output = ob_get_contents();
            ob_end_clean();
        }
    }
