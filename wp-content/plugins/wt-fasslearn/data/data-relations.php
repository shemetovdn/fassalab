<?php

    $_wtfl_dr = array(
        'lesson' => array(
            'step' => array(
                'type' => array(
                    1 => __('Sélection de la bonne réponse'),
                    2 => __('Traductions du texte'),
                    3 => __('Traduction de mots/phrases'),
                    4 => __('Sélection du mot manquant'),
                    5 => __('Écoutez et écrire'),
                    6 => __('La vidéo interactive'),
                    7 => __('Traductions du texte (mots entiers)'),
                    8 => __('Écoutez et écrire (mots entiers)'),
                    9 => __('Drag&drop (DEV!)'),
                    10 => __('Sélection de la bonne réponse v2 (DEV!)'),
                )
            )
        ),

        'prize' => array(
            'category' => array(
                1 => __('Divers'),
                2 => __('Dialogues'),
                3 => __('Leçons'),
                4 => __('Test 1'),
                5 => __('Test 2')
            )
        ),

        'keyboard' => array(
            'arabic' => array(
                0  => 'ض',
                1  => 'ص',
                2  => 'ث',
                3  => 'ق',
                4  => 'ف',
                5  => 'غ',
                6  => 'ع',
                7  => 'ه',
                8  => 'خ',
                9  => 'ح',
                10 => 'ج',
                11 => 'د',
                12 => 'ش',
                13 => 'س',
                14 => 'ي',
                15 => 'ب',
                16 => 'ل',
                17 => 'ا',
                18 => 'ت',
                19 => 'ن',
                20 => 'م',
                21 => 'ك',
                22 => 'ط',
                23 => 'ذ',
                24 => 'إ',
                25 => 'أ',
                26 => 'ئ',
                27 => 'ء',
                28 => 'ؤ',
                29 => 'ر',
                30 => 'لا',
                31 => 'ى',
                32 => 'ة',
                33 => 'و',
                34 => 'ز',
                35 => 'ظ',
                36 => 'آ',
                //37 => 'َ',
                //38 => 'ِ',
                //39 => 'ُ',
                //40 => 'ً',
                //41 => 'ٍ',
                //42 => 'ٌ',
                //43 => 'ّ'
            )
        )
    );
