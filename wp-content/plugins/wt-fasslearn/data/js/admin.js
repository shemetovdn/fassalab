jQuery(document).ready(function(){

});

var WTFL_Admin = {
    attr: {
        p: null,
        text: {},
        url: '',
        url_plugin_core: ''
    },

    preloader: function(show){
        var id = window.WTFL_Admin.attr.prefix + '-preloader';
        var speed = 200;
        var preloader = jQuery('#' + id);

        if (show)
        {
            if (preloader.length)
            {
                preloader.stop().fadeIn(speed).unbind();
            }
            else
            {
                preloader = jQuery('<div>').attr({id: id}).hide();

                jQuery('body').append(preloader);

                preloader = jQuery('#' + id);
                preloader.fadeIn(speed).unbind();
            }

            setTimeout(function(){
                preloader.click(function(){
                    window.WTFL_Admin.preloader(false);
                });
            }, 5000);
        }
        else
        {
            preloader.fadeOut(speed, function(){
                jQuery(this).remove();
            });
        }
    },

    init: function(){
        var p = jQuery('#' + window.WTFL_Admin.attr.prefix + '-lessons');
        var pp = jQuery('#' + window.WTFL_Admin.attr.prefix + '-prizes');

        jQuery('.lessons-list > .lesson .actions .preview', p).unbind().click(function(){
            window.WTFL_Admin.lesson_preview(jQuery(this).closest('.lesson').data('id'));

            return false;
        });

        jQuery('.lessons-list > .lesson .actions .remove', p).unbind().click(function(){
            if (confirm(window.WTFL_Admin.attr.text.lesson_remove))
            {
                window.WTFL_Admin.lesson_remove(jQuery(this).closest('.lesson').data('id'));
            }

            return false;
        });

        jQuery('.prizes-list > .prize .actions .remove', pp).unbind().click(function(){
            if (confirm(window.WTFL_Admin.attr.text.prize_remove))
            {
                window.WTFL_Admin.prize_remove(jQuery(this).closest('.prize').data('id'));
            }

            return false;
        });
    },

    lessons_list_get: function(){
        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL_Admin.attr.prefix,
                _action: 'lesson',
                _subaction: 'lessons-list-get'
            },
            beforeSend: function(){
                window.WTFL_Admin.preloader(true);
            },
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    jQuery('.lessons-list').html(response.html);
                    window.WTFL_Admin.init();
                }
            },
            complete: function(){
                window.WTFL_Admin.preloader(false);
            }
        });
    },

    lesson_preview: function (id){
        var width = 940;
        var height = 575;

        var html  = '<iframe id="' + window.WTFL_Admin.attr.prefix + '-lesson-preview-iframe" src="' + window.WTFL_Admin.attr.url_plugin_core + 'lesson-preview.php?id=' + id + '" width="' + width + '" height="' + height + '" style="display: none;" onload="window.WTFL_Admin.lesson_preview_iframe_preload(false);"></iframe>';
            html += '<p id="' + window.WTFL_Admin.attr.prefix + '-lesson-preview-iframe-msg">Please, wait few seconds...</p>';

        jQuery.fancybox.open({
            content: html,
            width: width,
            height: height,
            autoSize: false
        });
    },

    lesson_remove: function(id){
        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL_Admin.attr.prefix,
                _action: 'lesson',
                _subaction: 'remove',
                lesson: {
                    id: id
                }
            },
            beforeSend: function(){
                window.WTFL_Admin.preloader(true);
            },
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    window.WTFL_Admin.lessons_list_get();
                }
            },
            complete: function(){
                window.WTFL_Admin.preloader(false);
            }
        });
    },

    lesson_preview_iframe_preload: function(show){
        if (show)
        {

        }
        else
        {
            jQuery('#' + window.WTFL_Admin.attr.prefix + '-lesson-preview-iframe-msg').hide();
            jQuery('#' + window.WTFL_Admin.attr.prefix + '-lesson-preview-iframe').fadeIn(1000);
        }
    },

    prizes_list_get: function(){
        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL_Admin.attr.prefix,
                _action: 'prize',
                _subaction: 'prizes-list-get'
            },
            beforeSend: function(){
                window.WTFL_Admin.preloader(true);
            },
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    jQuery('.prizes-list').html(response.html);
                    window.WTFL_Admin.init();
                }
            },
            complete: function(){
                window.WTFL_Admin.preloader(false);
            }
        });
    },

    prize_remove: function(id){
        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL_Admin.attr.prefix,
                _action: 'prize',
                _subaction: 'remove',
                prize: {
                    id: id
                }
            },
            beforeSend: function(){
                window.WTFL_Admin.preloader(true);
            },
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    window.WTFL_Admin.prizes_list_get();
                }
            },
            complete: function(){
                window.WTFL_Admin.preloader(false);
            }
        });
    }
}
