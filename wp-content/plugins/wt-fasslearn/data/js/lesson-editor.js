jQuery(document).ready(function(){

});

var WTFL_Lesson_Editor = {
    attr: {
        p: null,
        form: null,
        text: {},
        items_mp_key_max: 0
    },

    init: function(){
        var p = jQuery('#lesson-editor');
        var form = jQuery('form[name="lesson"]', p);

        window.WTFL_Lesson_Editor.attr.p = p;
        window.WTFL_Lesson_Editor.attr.form = form;

        jQuery('input[name="lesson[step-new]"]', form).unbind().click(function(){
            var step_new_btn = jQuery(this);
            var se = jQuery('.step-editor.temp', p).clone();

            jQuery('.steps .step-editor', form).remove();
            se.removeClass('temp');
            se.insertAfter(jQuery('.steps-list', form));

            se = jQuery('.steps .step-editor', form);

            jQuery('input[name="lesson[step][submit]"]', se).unbind().click(function(){
                window.WTFL_Lesson_Editor.form.submit('step-create', function(responseText, statusText, xhr){
                    var data = jQuery.parseJSON(responseText);

                    if (!data.status)
                    {
                        window.WTFL_Lesson_Editor.form.error(data.error);
                    }
                    else
                    {
                        se.slideUp(function(){
                            jQuery(this).remove();
                        });

                        step_new_btn.show();

                        /*
                        jQuery.fancybox({
                            content: data.message
                        });
                        */

                        window.WTFL_Lesson_Editor.steps_session_list_get();
                    }
                });
            });

            jQuery('input[name="lesson[step][cancel]"]', se).unbind().click(function(){
                se.slideUp(function(){
                    jQuery(this).remove();
                });

                step_new_btn.show();
            });

            window.WTFL_Lesson_Editor.init_step_data();

            se.slideDown();
            step_new_btn.hide();
        });

        jQuery('input[name="lesson[submit]"]', form).unbind().click(function(){
            window.WTFL_Lesson_Editor.form.submit('create', function(responseText, statusText, xhr){
                var data = jQuery.parseJSON(responseText);

                if (!data.status)
                {
                    window.WTFL_Lesson_Editor.form.error(data.error);
                }
                {
                    jQuery.fancybox({
                        content: data.message,
                        afterClose: function(){
                            window.WTFL_Admin.preloader(true);
                            window.location.href = data.redirect_url;
                        }
                    });
                }
            });

            return false;
        });

        jQuery('input[name="lesson[save]"]', form).unbind().click(function(){
            window.WTFL_Lesson_Editor.form.submit('save', function(responseText, statusText, xhr){
                var data = jQuery.parseJSON(responseText);

                if (!data.status)
                {
                    window.WTFL_Lesson_Editor.form.error(data.error);
                }
                {
                    jQuery.fancybox({
                        content: data.message,
                        afterClose: function(){
                            window.WTFL_Admin.preloader(true);
                            window.location.href = data.redirect_url;
                        }
                    });
                }
            });

            return false;
        });

        window.WTFL_Lesson_Editor.steps_session_list_get();
        window.WTFL_Lesson_Editor.init_rtl_inputs();
        window.WTFL_Lesson_Editor.lesson_steps_copy_to.init();
    },

    init_steps: function(){
        var p = jQuery('#lesson-editor');
        var form = jQuery('form[name="lesson"]', p);
        var steps = jQuery('.steps-list > .step', p);

        steps.unbind().click(function(){
            var _this = jQuery(this);
            var id = jQuery(this).data('id');
            var key = jQuery(this).data('key');

            var editor = jQuery('.steps-list .step-editor.edit[data-id="' + id + '"]', p);

            if (editor.length)
            {
                editor.slideUp(function(){
                    jQuery(this).remove();
                });
            }
            else
            {
                jQuery.ajax({
                    url: '',
                    type: 'post',
                    dataType: 'json',
                    global: true,
                    async: true,
                    cache: false,
                    data: {
                        _handler: window.WTFL_Lesson_Editor.attr.prefix,
                        _action: 'lesson',
                        _subaction: 'step-edit-get',
                        lesson: {
                            step: {
                                id: id,
                                key: key
                            }
                        }
                    },
                    beforeSend: function(){
                        window.WTFL_Admin.preloader(true);
                    },
                    timeout: function(){},
                    error: function(){},
                    success: function(response){
                        if (response.status)
                        {
                            jQuery('.steps .step-editor', form).remove();
                            jQuery('input[name="lesson[step-new]"]', form).show();

                            var se = jQuery(response.html);

                            se.removeClass('temp').addClass('edit');
                            se.insertAfter(_this);

                            se = jQuery('.steps .step-editor', form);

                            jQuery('input[name="lesson[step][save]"]', se).unbind().click(function(){
                                window.WTFL_Lesson_Editor.form.submit('step-save', function(responseText, statusText, xhr){
                                    var data = jQuery.parseJSON(responseText);

                                    if (!data.status)
                                    {
                                        window.WTFL_Lesson_Editor.form.error(data.error);
                                    }
                                    else
                                    {
                                        _this.click();

                                        /*
                                        jQuery.fancybox({
                                            content: data.message
                                        });
                                        */

                                        window.WTFL_Lesson_Editor.steps_session_list_get();
                                    }
                                });
                            });

                            jQuery('input[name="lesson[step][cancel]"]', se).unbind().click(function(){
                                _this.click();
                            });

                            window.WTFL_Lesson_Editor.init_step_data();

                            se.slideDown();
                        }
                    },
                    complete: function(){
                        window.WTFL_Admin.preloader(false);
                    }
                });
            }
        });

        jQuery('.edit', steps).unbind().bind('click', function(){
            jQuery(this).closest('.step').click();
            return false;
        })

        jQuery('.actions .remove', steps).unbind().click(function(){
            if (confirm(window.WTFL_Lesson_Editor.attr.text.lesson_step_remove))
            {
                window.WTFL_Lesson_Editor.step_remove(jQuery(this).closest('.step').data('id'), jQuery(this).closest('.step').data('key'));
            }

            return false;
        });

        jQuery('.actions .preview', steps).unbind().click(function(){
            var lesson_id = jQuery('input[name="lesson[id]"]', jQuery(this).closest('form')).val();
            var step_id = jQuery(this).closest('.step').data('id');

            var width = 970;
            var height = 590;

            var html  = '<iframe id="' + window.WTFL_Admin.attr.prefix + '-lesson-preview-iframe" src="' + window.WTFL_Admin.attr.url_plugin_core + 'lesson-preview.php?id=' + lesson_id + '&step-id=' + step_id + '" width="' + width + '" height="' + height + '" style="display: none;" onload="window.WTFL_Admin.lesson_preview_iframe_preload(false);"></iframe>';
                html += '<p id="' + window.WTFL_Admin.attr.prefix + '-lesson-preview-iframe-msg">Please, wait few seconds...</p>';

            jQuery.fancybox.open({
                content: html,
                width: width,
                height: height,
                autoSize: false
            });

            return false;
        });
    },

    init_step_data: function(){
        var p = jQuery('#lesson-editor');
        var form = jQuery('form[name="lesson"]', p);

        jQuery('.steps .step-editor', p).each(function(){
            var _this = jQuery(this);

            jQuery('input[name="lesson[step][type]"]', _this).unbind().change(function(){
                var step_editor = jQuery(this).closest('.step-editor');
                var key = jQuery('input[name="lesson[step][key]"]', step_editor).val();
                var type = jQuery(this).val();

                jQuery.ajax({
                    url: '',
                    type: 'post',
                    dataType: 'json',
                    global: true,
                    async: true,
                    cache: false,
                    data: {
                        _handler: window.WTFL_Lesson_Editor.attr.prefix,
                        _action: 'lesson',
                        _subaction: 'step-data-get',
                        lesson: {
                            step: {
                                key: key,
                                type: type
                            }
                        }
                    },
                    beforeSend: function(){
                        window.WTFL_Admin.preloader(true);
                    },
                    timeout: function(){},
                    error: function(){},
                    success: function(response){
                        if (response.status)
                        {
                            var step_data = jQuery('.step-data', step_editor);
                                step_data.html(response.html);

                            window.WTFL_Lesson_Editor.init_step_data_type(step_data, type);
                        }
                    },
                    complete: function(){
                        window.WTFL_Admin.preloader(false);
                    }
                });
            });

            jQuery('input[name="lesson[step][type]"]:checked', _this).change();
        });
    },

    init_step_data_type: function(p, type){
        var type = parseInt(type);
        var step_data_ims = jQuery('.step-data-ims.ims-' + type, p);

        if (step_data_ims.length)
        {
            /*
            jQuery('.step-rtl', step_data_ims).unbind().change(function(){
                var se = jQuery(this).closest('.step-editor');

                if (jQuery(this).is(':checked'))
                {
                    se.closest('.step-editor').addClass('rtl');
                }
                else
                {
                    se.removeClass('rtl');
                }
            }).change();
            */

            switch (type)
            {
                case 1:
                case 10:
                    var tia = jQuery('.tia', step_data_ims);

                    window.WTFL_Lesson_Editor.attr.items_mp_key_max = jQuery('.wi ', tia).length - 1;

                    var item_add_event = function(){
                        window.WTFL_Lesson_Editor.attr.items_mp_key_max++;

                        var item_key = window.WTFL_Lesson_Editor.attr.items_mp_key_max;
                        var wi = jQuery(this).closest('.wi');
                        var wic = wi.clone();

                        jQuery('.item-add', wi).attr('class', 'item-remove').unbind().bind('click', item_remove_event);

                        jQuery('input[type="text"]', wic).val('');
                        jQuery('.file-up', wic).remove();
                        jQuery('.item-remove', wic).unbind().attr('class', 'item-add');

                        jQuery('input[type="checkbox"]', wic).attr({checked: false, value: item_key});

                        //
                        var input_text = jQuery('input[type="text"]', wic);

                        if (input_text.length)
                        {
                            input_text.each(function(){
                                jQuery(this).attr({name: jQuery(this).attr('name').replace(/\[[0-9]+\]$/gi, '[' + item_key + ']')});
                            });
                        }
                        ////

                        //
                        var file = jQuery('input[type="file"]', wic);

                        if (file.length)
                        {
                            file.each(function(){
                                jQuery(this).attr({name: jQuery(this).attr('name').replace(/\[[0-9]+\]$/gi, '[' + item_key + ']')});
                            });
                        }
                        ////

                        //
                        var file_type = jQuery('input[name^="lesson[step][data][file][type]"]', wic);

                        if (file_type.length)
                        {
                            file_type.each(function(){
                                jQuery(this).attr({name: jQuery(this).attr('name').replace(/\[[0-9]+\]$/gi, '[' + item_key + ']')});
                            });
                        }
                        ////

                        jQuery('.item-add', wic).unbind().bind('click', item_add_event);
                        jQuery('.file .remove', wic).unbind().bind('click', item_file_remove_event);

                        wic.insertAfter(wi);

                        //window.WTFL_Lesson_Editor.init_step_data_type(p, type);

                        return false;
                    };

                    var item_remove_event = function(){
                        jQuery(this).closest('.wi').remove();

                        return false;
                    };

                    var item_file_remove_event = function(){
                        window.WTFL_Lesson_Editor.step_data_file_remove(jQuery(this));

                        return false;
                    };

                    jQuery('.item-add', tia).unbind().bind('click', item_add_event);
                    jQuery('.file .remove', tia).unbind().bind('click', item_file_remove_event);
                    jQuery('.item-remove', tia).unbind().bind('click', item_remove_event);
                break;

                case 2:
                case 7:
                case 9:
                    var ttvt = jQuery('.ttvt', step_data_ims);

                    jQuery('.item-add', ttvt).unbind().click(function(){
                        var wi = jQuery(this).closest('.wi');
                        var wic = wi.clone();

                        jQuery('textarea', wi).val('');
                        jQuery('.file-up', wi).remove();
                        jQuery('.item-add', wic).attr('class', 'item-remove');

                        wic.insertBefore(wi);

                        //var file = jQuery('input[type="file"]', wi);
                        //file.attr({name: file.attr('name').replace(/\[[0-9]+\]$/gi, '[' + (jQuery('.wi', ttvt).length - 1) + ']')});

                        //var file_type = jQuery('input[name^="lesson[step][data][file][type]"]', wi);
                        //file_type.attr({name: file_type.attr('name').replace(/\[[0-9]+\]$/gi, '[' + (jQuery('.wi', ttvt).length - 1) + ']')});

                        window.WTFL_Lesson_Editor.init_step_data_type(p, type);

                        return false;
                    });

                    jQuery('.file .remove', ttvt).unbind().click(function(){
                        window.WTFL_Lesson_Editor.step_data_file_remove(jQuery(this));

                        return false;
                    });

                    jQuery('.item-remove', ttvt).unbind().click(function(){
                        jQuery(this).closest('.wi').remove();

                        return false;
                    });
                break;

                case 3:
                    var ttvt = jQuery('.ttvtp, .ttvt', step_data_ims);

                    jQuery('.item-add', ttvt).unbind().click(function(){
                        var wi = jQuery(this).closest('.wi');
                        var wic = wi.clone();

                        jQuery('input[type="text"], textarea', wi).val('');
                        jQuery('.file-up', wi).remove();
                        jQuery('.item-add', wic).attr('class', 'item-remove');

                        wic.insertBefore(wi);

                        window.WTFL_Lesson_Editor.init_step_data_type(p, type);

                        return false;
                    });

                    jQuery('.file .remove', ttvt).unbind().click(function(){
                        window.WTFL_Lesson_Editor.step_data_file_remove(jQuery(this));

                        return false;
                    });

                    jQuery('.item-remove', ttvt).unbind().click(function(){
                        jQuery(this).closest('.wi').remove();

                        return false;
                    });
                break;

                case 4:
                    var words = jQuery('.words', step_data_ims);

                    jQuery('.item-add', words).unbind().click(function(){
                        var wi = jQuery(this).closest('.wi');
                        var wic = wi.clone();

                        jQuery('input[type="text"]', wi).val('');
                        jQuery('.item-add', wic).attr('class', 'item-remove');

                        wic.insertBefore(wi);
                        jQuery('input[type="checkbox"]', wi).attr({checked: false, value: jQuery('.wi', words).length - 1});

                        window.WTFL_Lesson_Editor.init_step_data_type(p, type);

                        return false;
                    });

                    jQuery('.item-remove', words).unbind().click(function(){
                        jQuery(this).closest('.wi').remove();

                        return false;
                    });
                break;

                case 5:
                case 8:
                    var ttvt = jQuery('.ttvt', step_data_ims);

                    jQuery('.item-add', ttvt).unbind().click(function(){
                        var wi = jQuery(this).closest('.wi');
                        var wic = wi.clone();

                        jQuery('textarea', wi).val('');
                        jQuery('.file-up', wi).remove();
                        jQuery('.item-add', wic).attr('class', 'item-remove');

                        wic.insertBefore(wi);

                        window.WTFL_Lesson_Editor.init_step_data_type(p, type);

                        return false;
                    });

                    jQuery('.file .remove', ttvt).unbind().click(function(){
                        window.WTFL_Lesson_Editor.step_data_file_remove(jQuery(this));

                        return false;
                    });

                    jQuery('.item-remove', ttvt).unbind().click(function(){
                        jQuery(this).closest('.wi').remove();

                        return false;
                    });
                break;
            }

            window.WTFL_Lesson_Editor.init_rtl_inputs();
        }
    },

    init_rtl_inputs: function(){
        var p = jQuery('#lesson-editor');
        var form = jQuery('form[name="lesson"]', p);

        var f = function(_this){
            var dir = 'ltr';

            if (window.WTFL_Tool.is_text_rtl(_this.val()))
            {
                dir = 'rtl';
            }

            _this.attr({dir: dir}).removeClass('rtl ltr').addClass(dir);
        }

        jQuery('input[type="text"], textarea', form).each(function(){
            f(jQuery(this));
        }).unbind('keyup').bind('keyup', function(){
            f(jQuery(this));
        });
    },

    lesson_steps_copy_to: {
        init: function(){
            var p = jQuery('#lesson-editor');
            var lcs = jQuery('input[name="lesson[clone-steps]"]', p);
            var ch = jQuery('.steps-list .step > .ch', p);

            ch.hide();
            jQuery('input[type="checkbox"]', ch).attr({checked: false});
            jQuery('input[name="lesson[clone-steps-cancel]"]', p).remove();
            lcs.val(lcs.data('value'));
            lcs.data({s: 1});

            lcs.unbind().bind('click', function(){
                var ch = jQuery('.steps-list .step > .ch', p);
                var checkbox = jQuery('input[type="checkbox"]', ch);

                if (lcs.data('s') == 1)
                {
                    checkbox.unbind().bind('click', function(e){
                        e.stopPropagation();

                        window.WTFL_Lesson_Editor.lesson_steps_copy_to.items_count_refresh();
                    });

                    window.WTFL_Lesson_Editor.lesson_steps_copy_to.items_count_refresh();
                    jQuery('<input type="button" name="lesson[clone-steps-cancel]" class="button" value="' + window.WTFL_Admin.attr.text.cancel + '" />').insertAfter(lcs);

                    jQuery('input[name="lesson[clone-steps-cancel]"]', p).unbind().bind('click', function(){
                        window.WTFL_Lesson_Editor.lesson_steps_copy_to.init();
                    });

                    lcs.data({s: 2});
                    ch.show();
                }
                else
                if (lcs.data('s') == 2)
                {
                    var steps = [];

                    jQuery('.steps-list .step > .ch input[type="checkbox"]:checked', p).each(function(){
                        steps.push(jQuery(this).val());
                    });

                    window.WTFL_Lesson_Editor.lesson_steps_copy_to.get_form(jQuery('input[name="lesson[id]"]', p).val(), steps);
                }

                return false;
            });
        },

        items_count_refresh: function(){
            var p = jQuery('#lesson-editor');
            var steps_copy_count = jQuery('.steps-list .step > .ch input[type="checkbox"]', p).filter(':checked').length;

            jQuery('input[name="lesson[clone-steps]"]', p).val(window.WTFL_Lesson_Editor.attr.text.lesson_steps_copy_to + ' (' + steps_copy_count + ')');
        },

        get_form: function(lesson_id, steps){
            jQuery.ajax({
                url: '',
                type: 'post',
                dataType: 'json',
                global: true,
                async: true,
                cache: false,
                data: {
                    _handler: window.WTFL_Lesson_Editor.attr.prefix,
                    _action: 'lesson',
                    _subaction: 'steps-clone-form-get',
                    lesson: {
                        id: lesson_id,
                        steps: steps
                    }
                },
                beforeSend: function(){
                    window.WTFL_Admin.preloader(true);
                },
                timeout: function(){},
                error: function(){},
                success: function(response){
                    if (response.status)
                    {
                        jQuery.fancybox.open({
                            wrapCSS: 'lcs',
                            content: response.html,
                            afterShow: function(){
                                var f = jQuery('.fancybox-wrap.lcs');

                                jQuery('input[name="clone"]', f).unbind().bind('click', function(){
                                    window.WTFL_Lesson_Editor.lesson_steps_copy_to.items_copy(lesson_id, steps, jQuery('select[name="lesson-to"]', f).val());
                                });

                                jQuery('input[name="cancel"]', f).unbind().bind('click', function(){
                                    jQuery.fancybox.close();
                                });
                            }
                        });
                    }
                },
                complete: function(){
                    window.WTFL_Admin.preloader(false);
                }
            });
        },

        items_copy: function(lesson_from, steps, lesson_to){
            jQuery.ajax({
                url: '',
                type: 'post',
                dataType: 'json',
                global: true,
                async: true,
                cache: false,
                data: {
                    _handler: window.WTFL_Lesson_Editor.attr.prefix,
                    _action: 'lesson',
                    _subaction: 'steps-clone',
                    lesson: {
                        id_from: lesson_from,
                        id_to: lesson_to,
                        steps: steps
                    }
                },
                beforeSend: function(){
                    jQuery.fancybox.close();
                    window.WTFL_Admin.preloader(true);
                },
                timeout: function(){},
                error: function(){},
                success: function(response){
                    if (response.status)
                    {
                        window.WTFL_Lesson_Editor.lesson_steps_copy_to.init();

                        if (confirm(response.message))
                        {
                            window.WTFL_Admin.preloader(true);
                            window.location.href = response.redirect_url;
                        }
                    }
                },
                complete: function(){
                    window.WTFL_Admin.preloader(false);
                }
            });
        }
    },

    form: {
        error: function (error){
            var p = jQuery('#lesson-editor');
            var form = jQuery('form[name="lesson"]', p);

            jQuery('.error', form).remove();

            jQuery.each(error, function(k, v){
                var input = jQuery('[name="' + k + '"]');

                if (input.length)
                {
                    var error = jQuery('<div class="error">' + v + '</div>');
                    error.insertAfter(input);
                }
            });

            jQuery('.error', form).stop().slideDown();

        },

        submit: function(subaction, callback_success){
            var p = jQuery('#lesson-editor');
            var form = jQuery('form[name="lesson"]', p);

            form.ajaxSubmit({
                data: {
                    _action: 'lesson',
                    _subaction: subaction
                },
                beforeSubmit: function(){
                    jQuery('*', form).attr({disabled: true});
                    window.WTFL_Admin.preloader(true);
                },
                success: function(responseText, statusText, xhr){
                    if (typeof(callback_success) == 'function'){ callback_success(responseText, statusText, xhr); }

                    window.WTFL_Admin.preloader(false);
                    jQuery('*', form).attr({disabled: false});
                }
            });
        }
    },

    steps_session_list_get: function(){
        var p = jQuery('#lesson-editor');

        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL_Lesson_Editor.attr.prefix,
                _action: 'lesson',
                _subaction: 'steps-session-list-get'
            },
            beforeSend: function(){
                window.WTFL_Admin.preloader(true);
            },
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    jQuery('.steps-list', p).html(response.html);
                    window.WTFL_Lesson_Editor.init_steps();
                }
            },
            complete: function(){
                window.WTFL_Admin.preloader(false);
            }
        });
    },

    step_remove: function(id, key){
        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL_Lesson_Editor.attr.prefix,
                _action: 'lesson',
                _subaction: 'step-remove',
                lesson: {
                    step: {
                        id: id,
                        key: key
                    }
                }
            },
            beforeSend: function(){
                window.WTFL_Admin.preloader(true);
            },
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    window.WTFL_Lesson_Editor.steps_session_list_get();
                }
            },
            complete: function(){
                window.WTFL_Admin.preloader(false);
            }
        });
    },

    step_data_file_remove: function(_this){
        if (!confirm(window.WTFL_Lesson_Editor.attr.text.lesson_step_data_item_file_remove))
        {
            return false;
        }

        var step_editor = _this.closest('.step-editor.edit');
        var ttvt = _this.closest('.ifi');
        var wi = jQuery('.wi', ttvt);
        var wic = _this.closest('.wi');
        var fu = _this.closest('.file-up');

        var step_key = jQuery('input[name="lesson[step][key]"]', step_editor).val();
        var step_data_type = jQuery('.step-data-ims', step_editor).data('ims');

        if (_this.data('file-key') !== undefined)
        {
            var step_data_item_key = _this.data('file-key');
        }
        else
        {
            var step_data_item_key = wi.index(wic);
        }

        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL_Lesson_Editor.attr.prefix,
                _action: 'lesson',
                _subaction: 'step-data-item-file-remove',
                lesson: {
                    step: {
                        key: step_key,
                        data: {
                            type: step_data_type,
                            item: {
                                key: step_data_item_key
                            }
                        }
                    }
                }
            },
            beforeSend: function(){
                window.WTFL_Admin.preloader(true);
            },
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    if (typeof(step_data_item_key) == 'number')
                    {
                        //jQuery('.file-up', wi.eq(step_data_item_key)).remove();
                        fu.remove();
                    }
                    else
                    {
                        //jQuery('.file-up', wi).remove();
                        fu.remove();
                    }
                }
            },
            complete: function(){
                window.WTFL_Admin.preloader(false);
            }
        });
    },

    lesson_clone: function(id){
        var title = prompt('Entrez un nom pour une nouvelle leçon', jQuery('input[name="lesson[title]"]', window.WTFL_Lesson_Editor.attr.form).val());

        if (title === null) return false;

        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL_Lesson_Editor.attr.prefix,
                _action: 'lesson',
                _subaction: 'lesson-clone',
                lesson: {
                    id: id,
                    title: title
                }
            },
            beforeSend: function(){
                window.WTFL_Admin.preloader(true);
            },
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    window.location.href = response.redirect_url;
                }
                else
                {
                    window.WTFL_Admin.preloader(false);
                }
            }
        });
    }
}
