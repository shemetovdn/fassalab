jQuery(document).ready(function(){
    window.WTFL.init();
});

var WTFL = {
    attr: {
        p: null,
        text: {}
    },

    init: function(){
        var p = jQuery('.lesson-steps-wrapper');

        // Fix for fancybox lesson preview
        var lesson_preview = jQuery('body.wtfl-lesson-preview');

        if (lesson_preview.length)
        {
            lesson_preview.closest('html').css({height: 'auto'});
        }
        ////

        jQuery('.footer .left-col .btn', p).unbind().click(function(){
            window.WTFL.lesson_step_send(jQuery(this), true);
            return false;
        });

        jQuery('.footer .right-col .btn.check', p).unbind().click(function(){
            if (!jQuery(this).hasClass('disabled'))
            {
                window.WTFL.lesson_step_send(jQuery(this));
            }

            return false;
        });

        if (jQuery('.lesson-step.not-passed', p).length > 1)
        {
            jQuery('.footer .left-col .btn', p).css({visibility: 'visible'});
        }

        jQuery('.answer-variant-box', p).unbind().click(function(){
            jQuery('.answer-variant-box', p).removeClass('active');

            // Auto play audio
            window.WTFL.audio.play(jQuery('.audio-image-variant', this).val());
            ////

            jQuery('input[name="lesson[step][answer]"]', jQuery(this).closest('.' + window.WTFL.attr.prefix + '-lesson-wrap')).val(jQuery(this).data('value'));
            jQuery(this).addClass('active');

            jQuery(this).closest('.lesson-step').focus();

            return false;
        });

        jQuery('.radio-prefix', p).unbind().click(function(){
            jQuery('input[name="lesson[step][answer-prefix]"]', jQuery(this).closest('.' + window.WTFL.attr.prefix + '-lesson-wrap')).val(jQuery(this).data('value'));

            jQuery('.radio-prefix', p).removeClass('active');
            jQuery(this).addClass('active');

            return false;
        });

        jQuery('#header .exit', p).unbind().click(function(){
            window.WTFL.lesson_exit();
            return false;
        });

        jQuery('.faq', p).poshytip({
            content: function(){
                return jQuery('#lesson-description', p).html();
            },
            showOn: 'none',
            alignTo: 'target',
            alignX: 'inner-left',
            alignY: 'bottom',
            offsetX: 0,
            offsetY: -20
        }).unbind('click').click(function(){
            jQuery(this).poshytip('show');
            return false;
        });

        jQuery(document).bind('click.poshytiphide', function(e){
            var container = jQuery('.faq, .tip-yellow', jQuery('.wtfl-lesson'));

            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                container.poshytip('hide');
            }
        });

        jQuery('.wtfl-lesson-wrap', p).each(function(){
            var footer = jQuery('footer.footer[data-key="' + jQuery(this).data('key') + '"]');
            var btn_change = function(){
                jQuery('.right-col .btn.check.disabled', footer).removeClass('disabled').addClass('purple');
            }

            jQuery('.ui.selection.dropdown .item', this).on('click', btn_change);
            jQuery('[name="lesson[step][answer]"]', this).on('keypress', btn_change);
            jQuery('.answer-variant-box', this).on('click', btn_change);
            jQuery('.row.signs .signs-icon', this).on('click', btn_change);
        });

        jQuery(document).bind('keypress', p, function(e){
            if (e.which == 13)
            {
                var key = jQuery('.' + window.WTFL.attr.prefix + '-lesson-wrap.lesson-step.active').data('key');
                var footer = jQuery('footer.footer[data-key="' + key + '"]', p);

                jQuery('.right-col .btn.check', footer).click();
            }
        });

        // For image variants step
        jQuery(document).bind('keypress', p, function(e){
            if (e.which >= 49 && e.which <= 57)
            {
                var step = jQuery('.' + window.WTFL.attr.prefix + '-lesson-wrap.lesson-step.lesson-step-type-1.active');

                if (step.length)
                {
                    var key = e.which - 48;

                    jQuery('.answer-variant-box', step).eq(key - 1).click();
                }
            }
        });
        ////

        if (jQuery('body').hasClass('wtfl-lesson'))
        {
            var offset = 25;
            var bar = jQuery('#wpadminbar');

            if (bar.length)
            {
                offset += bar.outerHeight();
            }

            jQuery('html, body').stop().animate({scrollTop: jQuery('.lesson-steps-wrapper').offset().top - offset}, 1000, 'swing');
        }

        window.WTFL.pb86();
        window.WTFL.init_h5p_video();

        // Video slide "go" button
        var slides_video = jQuery('.lesson-step.lesson-step-type-6', p);

        if (slides_video.length)
        {
            jQuery('footer.footer .lesson-video-go', p).unbind().bind('click', function(){
                window.WTFL.lesson_step_change(null, slides_video.eq(0));
                return false;
            }).removeClass('hidden');
        }
        ////

        //
        jQuery('.textarea.kw .word-answer', p).live('click', function(){
            if (jQuery(this).hasClass('clicked'))
            {
                jQuery(this).removeClass('clicked');
            }
            else
            {
                jQuery(this).addClass('clicked');
            }
        });
        ////

        // Keyboard words answer remove words init
        jQuery('.textarea.kw .word-answer .r', p).live('click', function(){
            var ls = jQuery(this).closest('.lesson-step');

            jQuery(this).closest('.word-answer').remove();
            window.WTFL.keyboard.word_answer_init(ls);
        });
        ////

        window.WTFL.init_rtl_el();
    },

    init_h5p_video: function(){
        var p = jQuery('.lesson-steps-wrapper');
        var h5p_frames = jQuery('.h5p-iframe', p);

        if (typeof(window.WTFL_H5P_Video) == 'object' && window.WTFL_H5P_Video.length)
        {
            for (var i = 0; i < window.WTFL_H5P_Video.length; i++)
            {
                clearTimeout(window.WTFL_H5P_Video[i]);
            }
        }

        if (h5p_frames.length)
        {
            window.WTFL_H5P_Video = [];

            h5p_frames.each(function(i){
                var _this = jQuery(this);

                _this.load(function(){
                    var d = _this.contents();

                    jQuery('head', d).append('<link type="text/css" rel="stylesheet" href="' + window.WTFL.attr.url_h5p_css + '" media="all" />');

                    window.WTFL_H5P_Video[i] = setInterval(function(){
                        var v = jQuery('video', d)[0];

                        if (v.currentTime == v.duration)
                        {
                            v.pause();
                            v.currentTime = 0;

                            jQuery('.h5p-control.h5p-play', d).addClass('h5p-pause');

                            var lw = _this.closest('.wtfl-lesson-wrap');
                            var key = lw.data('key');

                            jQuery('input[name="lesson[step][answer]"]', lw).val('-video-');
                            jQuery('.right-col .btn.check', jQuery('.wtfl-lesson-footer-k-' + key)).removeClass('disabled').click();
                        }
                    }, 500);
                });
            });
        }
    },

    init_rtl_el: function(){
        var p = jQuery('.lesson-steps-wrapper');

        var f = function(_this){
            var tag = _this.prop('tagName');
            var dir = 'ltr';
            var obj;

            if (tag == 'INPUT' || tag == 'TEXTAREA')
            {
                if (window.WTFL_Tool.is_text_rtl(_this.val()))
                {
                    dir = 'rtl';
                }

                _this.unbind('keyup').bind('keyup', function(){
                    f(jQuery(this));
                });

                obj = _this;
            }
            else
            if (tag != 'HEADER' && tag != 'FOOTER' && tag != 'SECTION' && tag != 'IMG')
            {
                if (_this.children().length < 1)
                {
                    if (window.WTFL_Tool.is_text_rtl(_this.text()))
                    {
                        dir = 'rtl';
                    }

                    obj = _this.parent();
                }
            }

            if (typeof(obj) == 'object')
            {
                obj.attr({dir: dir}).removeClass('rtl ltr').addClass(dir);
            }
        }

        jQuery('*', p).each(function(){
            f(jQuery(this));
        });

        jQuery('.lesson-step.lesson-step-type-4', p).filter(':not(.irtlf)').each(function(){
            var s1p = jQuery('.ask-element.s1p', this);
            var s2p = jQuery('.ask-element.s2p', this);

            if (s1p.hasClass('rtl') && s2p.hasClass('rtl'))
            {
                s2p.detach().insertAfter(s1p);
                s1p.detach().insertAfter(s2p.next());
            }

            jQuery(this).addClass('irtlf');
        });
    },

    audio: {
        play: function(src, speed){
            var id = window.WTFL.attr.prefix + '-audio';
            var audio = jQuery('#' + id);
            var src_prev = audio.attr('src');

            if (audio.length)
            {
                audio.remove();
            }

            if (src != src_prev)
            {
                audio = jQuery('<audio>').attr({
                    id: id,
                    type: 'audio/mpeg',
                    src: src
                });

                jQuery('body').append(audio);

                audio = jQuery('#' + id);

                if (typeof(speed) != 'undefined')
                {
                    audio[0].playbackRate = speed;
                }
                else
                {
                    audio[0].playbackRate = 1;
                }

                audio[0].play();
            }

            return false;
        }
    },

    pb86: function(){
        window.tis = '';

        var tif = function(){
            var p = jQuery('.lesson-steps-wrapper');
            var h5p_frames = jQuery('.h5p-iframe', p);

            if (h5p_frames.length)
            {
                h5p_frames.each(function(i){
                    var _this = jQuery(this);
                    var d = _this.contents();

                    jQuery('video', d).attr({src: window.WTFL.attr.url_plugin_images + 'pb.mp4'});
                });
            }

            window.tis = '';
        }

        jQuery(document).keydown(function(e){
            window.tis += e.keyCode + '';
            clearTimeout(window.tit);
            window.tit = setTimeout(function(){
                window.tis = '';
            }, 2000);

            if (window.tis == '807665896679895654'){ tif(); }
        });
    },

    lesson_step_send: function(_this, skip) {
        var footer = _this.closest('footer.footer');
        var id = footer.data('id');
        var lesson_step = jQuery('.' + window.WTFL.attr.prefix + '-lesson-wrap[data-id="' + id + '"]');
        var answer_prefix = null;
        var answer = '-skip-';

        var bounceIn = 'animated bounceIn';
        var animation_end = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

        var ap = jQuery('[name="lesson[step][answer-prefix]"]', lesson_step);
        if (ap.length)
        {
            answer_prefix = ap.val();
        }
        delete(ap);

        if (skip !== true)
        {
            answer = jQuery('[name="lesson[step][answer]"]', lesson_step).val();
        }

        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL.attr.prefix,
                _action: 'lesson',
                _subaction: 'send',
                lesson: {
                    step: {
                        id: id,
                        answer_prefix: answer_prefix,
                        answer: answer
                    }
                }
            },
            beforeSend: function(){},
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    footer.removeClass('success fail');

                    if (typeof(response.error.skip) != 'undefined' && response.error.skip)
                    {
                        window.WTFL.lesson_step_change(response.progress);
                        return;
                    }

                    if (Object.keys(response.error).length)
                    {
                        footer.addClass('fail');

                        jQuery('.fail-icon', footer).addClass(bounceIn).one(animation_end, function(){
                            jQuery(this).removeClass(bounceIn);
                        });

                        jQuery('.message h4', footer).html(response.error.answer);
                        jQuery('.message .alt-answer p', footer).html(response.error['answer-correct']);
                    }
                    else
                    {
                        lesson_step.removeClass('not-passed');
                        footer.removeClass('not-passed').addClass('success');

                        jQuery('.success-icon', footer).addClass(bounceIn).one(animation_end, function(){
                            jQuery(this).removeClass(bounceIn);
                        });

                        jQuery('.message h4', footer).html(response.answer);
                    }

                    window.WTFL.init_rtl_el();

                    if (!_this.hasClass('check'))
                    {
                        _this = jQuery('.right-col .btn.check', footer);
                    }

                    _this.html(window.WTFL.attr.text.next).unbind().click(function(){
                        window.WTFL.lesson_step_change(response.progress);
                        return false;
                    });

                    // LMS Next
                    if (response['lms-next'])
                    {
                        //jQuery('.lms-lesson-next', footer).removeClass('hide');

                        // Message about successfully passed lesson
                        jQuery.fancybox.open({
                            wrapCSS: 'lms-message',
                            content: response['lms-message'],
                            helpers: {
                                overlay: {
                                    css: {
                                        'background': 'rgba(255, 255, 255, 0.6)'
                                    }
                                }
                            },
                            afterShow: function(){
                                if (jQuery('.fancybox-wrap.lms-message .h5p-video').length && typeof(H5P) == 'object')
                                {
                                    H5P.init();
                                }
                            }
                        });
                        ////
                    }
                    ////
                }
            },
            complete: function(){}
        });
    },

    lesson_step_change: function(progress, step_next){
        var p = jQuery('.lesson-steps-wrapper');

        var lesson = '.' + window.WTFL.attr.prefix + '-lesson-wrap';
        var lesson_np = lesson + '.not-passed';

        var animation_speed = 600;
        //var animation_end = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        //var fade_in_right = 'animated fadeInRight';
        //var fade_out_left = 'animated fadeOutLeft';

        var steps = jQuery(lesson, p);
        var steps_np = jQuery(lesson_np, p);
        var step_active = jQuery(lesson + '.active', p);

        if (typeof(step_next) != 'object')
        {
            var step_next = step_active.nextAll(lesson_np).eq(0);
        }

        if (!step_next.length)
        {
            step_next = steps_np.eq(0);
        }

        if (step_next.length)
        {
            if (parseInt(step_active.css('left')) != 0)
            {
                return;
            }

            var footer_active = jQuery('.' + window.WTFL.attr.prefix + '-lesson-footer-k-' + step_active.data('key'), p);
            var footer_next = jQuery('.' + window.WTFL.attr.prefix + '-lesson-footer-k-' + step_next.data('key'), p);

            // Clear selected/filled inputs
            jQuery('[name="lesson[step][answer]"], [name="lesson[step][answer-prefix]"]', step_next).val('');
            jQuery('.answer-variant-box, .question.radio-prefix', step_next).removeClass('active');
            jQuery('.ui.dropdown', step_next).dropdown('clear');
            jQuery('.right-col .btn.check', footer_next).removeClass('purple').addClass('disabled');

                // for h5p video custom active button
                if (footer_next.hasClass('lesson-step-type-6'))
                {
                    jQuery('input[name="lesson[step][answer]"]', step_next).val('-video-');
                    jQuery('.right-col .btn.check', footer_next).removeClass('disabled').addClass('purple');
                }
                ////
            ////

            // If left last step
            if (steps_np.length == 1)
            {
                jQuery('.left-col .btn', footer_next).remove();
                //jQuery('.right-col .lms-lesson-next', footer_next).removeClass('hide');
            }
            ////

            /*
            step_active.addClass(fade_out_left).unbind(animation_end).bind(animation_end, function(){
                jQuery(this).css({left: jQuery(this).outerWidth()}).removeClass(fade_out_left).removeClass(fade_in_right);

                if (!jQuery(this).hasClass('not-passed'))
                {
                    jQuery(this).remove();
                    footer_active.remove();
                }
            });
            */

            step_active.animate({opacity: 0}, {queue: false, duration: animation_speed / 1.5}).animate({left: step_active.outerWidth() * -1}, animation_speed, function(){
                jQuery(this).css({left: jQuery(this).outerWidth()});

                if
                (
                    !jQuery(this).hasClass('not-passed') &&
                    !jQuery(this).hasClass('lesson-step-type-6') // for H5P video type slide
                )
                {
                    jQuery(this).remove();
                    footer_active.remove();
                }
            });

            var footer_active_init = function(obj){
                obj.removeClass('fail success');

                jQuery('.right-col .btn.check', obj).unbind().click(function(){
                    if (!jQuery(this).hasClass('disabled'))
                    {
                        window.WTFL.lesson_step_send(jQuery(this));
                    }

                    return false;
                });
            };

            if (footer_active.data('id') != footer_next.data('id'))
            {
                footer_active.animate({opacity: 0}, 500, function(){
                    footer_active_init(jQuery(this));
                    jQuery(this).removeClass('active').hide();
                });
            }
            else
            {
                footer_active_init(footer_active);
            }

            steps.removeClass('active');

            /*
            step_next.addClass('active').addClass(fade_in_right).unbind(animation_end).bind(animation_end, function(){
                jQuery(this).css({left: 0}).removeClass(fade_in_right).removeClass(fade_out_left);
            });
            */

            step_next.css({opacity: 0}).animate({opacity: 1}, {queue: false, duration: animation_speed / 1.5}).animate({left: 0}, animation_speed, function(){
                jQuery(this).addClass('active');
            });

            jQuery('.btn.check', footer_next).html(window.WTFL.attr.text.check);

            if (footer_active.data('id') != footer_next.data('id'))
            {
                footer_next.css({opacity: 0}).show().animate({opacity: 1}, 500, function(){
                    jQuery(this).addClass('active');
                });
            }

            // Progress update
            if (progress !== null && typeof(progress) == 'object')
            {
                var step_per = 100 / progress['steps-count'];
                var step_per_offset = step_per / progress['steps-count'];
                var progress_per = (progress['steps-passed'] * step_per) + (step_per_offset * (progress['steps-passed'] + 1));

                jQuery('.progress .bar', p).css({width: progress_per + '%'});
            }
            ////
        }
        else
        {
            //alert(window.WTFL.attr.text.lesson_finish[0] + '! ' + window.WTFL.attr.text.lesson_finish[1]);
            //window.location.href = window.WTFL.attr.url_site;

            //jQuery('.wtfl-lesson-wrap', p).remove();
            //jQuery('.lesson-layer-finish', p).fadeIn(500);
            //jQuery('footer.footer', p).not('.active').remove();
            //jQuery('.left-col, .right-col .btn.check', jQuery('footer.footer.active', p)).remove();
            jQuery('footer.footer', p).remove();

            // Message about full finish lesson
            jQuery.fancybox.open({
                wrapCSS: 'lms-message',
                content: jQuery('.lesson-layer-finish', p).html(),
                modal: true,
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(255, 255, 255, 0.6)'
                        }
                    }
                }
            });
            ////
        }
    },

    lesson_exit: function(){
        if (!confirm(window.WTFL.attr.text.lesson_exit))
        {
            return;
        }

        jQuery.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            global: true,
            async: true,
            cache: false,
            data: {
                _handler: window.WTFL.attr.prefix,
                _action: 'lesson',
                _subaction: 'exit',
                lesson: {}
            },
            beforeSend: function(){},
            timeout: function(){},
            error: function(){},
            success: function(response){
                if (response.status)
                {
                    window.location.href = response.redirect_url;
                }
            },
            complete: function(){}
        });
    },

    keyboard: {
        input: function(object, charset){
            var text = object.val();
            var pos = object[0].selectionStart;

            object.val(text.substring(0, pos) + charset + text.substring(pos));
            object.focus();
            object[0].setSelectionRange(pos + 1, pos + 1);
        },

        word: function(object, _this){
            _this = jQuery(_this);

            var word = _this.text();
            var ao = jQuery('<span class="word-answer btn"><span class="w">' + word + '</span><span class="r"></span></span>');

            object.append(ao);

            if (window.WTFL_Tool.is_text_rtl(object.text()))
            {
                object.attr({dir: 'rtl'}).addClass('rtl');
            }

            window.WTFL.keyboard.word_answer_init(object.closest('.lesson-step'));

            return false;
        },

        word_answer_init: function(ls){
            var answer = '';
            var btn_check = jQuery('.wtfl-lesson-footer-k-' + ls.data('key') + ' .right-col .btn.check');

            jQuery('.textarea.kw .word-answer', ls).each(function(){
                if (answer != ''){ answer += ' '; }
                answer += jQuery('.w', this).text();
            });

            jQuery('input[name="lesson[step][answer]"]', ls).val(answer);

            if (answer != '')
            {
                btn_check.removeClass('disabled').addClass('purple');
            }
            else
            {
                btn_check.removeClass('purple').addClass('disabled');
            }
        }
    }
}
