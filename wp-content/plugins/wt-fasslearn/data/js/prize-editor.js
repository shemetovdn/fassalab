jQuery(document).ready(function(){

});

var WTFL_Prize_Editor = {
    attr: {
        p: null,
        form: null,
        text: {},
        items_mp_key_max: 0
    },

    init: function(){
        var p = jQuery('#prize-editor');
        var form = jQuery('form[name="prize"]', p);

        window.WTFL_Prize_Editor.attr.p = p;
        window.WTFL_Prize_Editor.attr.form = form;

        WTFL_Prize_Editor.init_video_multi_add();

        jQuery('input[name="prize[submit]"]', form).unbind().click(function(){
            window.WTFL_Prize_Editor.form.submit('create', function(responseText, statusText, xhr){
                var data = jQuery.parseJSON(responseText);

                if (!data.status)
                {
                    window.WTFL_Prize_Editor.form.error(data.error);
                }
                {
                    jQuery.fancybox({
                        content: data.message,
                        afterClose: function(){
                            window.WTFL_Admin.preloader(true);
                            window.location.href = data.redirect_url;
                        }
                    });
                }
            });

            return false;
        });

        jQuery('input[name="prize[save]"]', form).unbind().click(function(){
            window.WTFL_Prize_Editor.form.submit('save', function(responseText, statusText, xhr){
                var data = jQuery.parseJSON(responseText);

                if (!data.status)
                {
                    window.WTFL_Prize_Editor.form.error(data.error);
                }
                {
                    jQuery.fancybox({
                        content: data.message,
                        afterClose: function(){
                            window.WTFL_Admin.preloader(true);
                            window.location.href = data.redirect_url;
                        }
                    });
                }
            });

            return false;
        });
    },

    init_video_multi_add: function(){
        var v = jQuery('.h5p-video', window.WTFL_Prize_Editor.attr.form);
        var wi = jQuery('.wi', v);

        jQuery('.wi', v).each(function(i){
            jQuery('input[name*="[category]"], select[name*="[h5p]"], input[name*="[title]"]', this).each(function(){
                jQuery(this).attr({name: jQuery(this).attr('name').replace(/\[[0-9]+\]/gi, '[' + i + ']')});
            });

            jQuery('input[name*="[popup]"]', this).val(i);
        });

        jQuery('input[name*="[category]"][checked="checked"]', v).attr({checked: true});
        jQuery('.item-action', v).unbind().removeClass('item-add').addClass('item-remove').eq(-1).removeClass('item-remove').addClass('item-add');

        wi.each(function(i){
            var _wi = this;

            jQuery('.item-add', _wi).unbind().bind('click', function(){
                var c = jQuery(this).closest('.wi').clone();

                jQuery('select[name*="[h5p]"] option', c).attr({selected: false}).eq(0).attr({selected: true});
                jQuery('input[name*="[category]"]', c).attr({checked: false}).eq(0).attr({checked: true});
                jQuery('input[name*="[popup]"]', c).attr({checked: false});

                v.append(c);

                WTFL_Prize_Editor.init_video_multi_add();
                return false;
            });

            jQuery('.item-remove', _wi).unbind().bind('click', function(){
                jQuery(this).closest('.wi').remove();

                WTFL_Prize_Editor.init_video_multi_add();
                return false;
            });

            jQuery('input[name*="[category]"]', _wi).unbind().bind('click', function(){
                jQuery('input[name*="[category]"]', _wi).attr({checked: false});
                jQuery(this).attr({checked: true});
            });

            jQuery('input[name*="[popup]"]', _wi).unbind().bind('click', function(){
                jQuery('input[name*="[popup]"]', _wi).attr({checked: false});
                jQuery(this).attr({checked: true});
            });
        });
    },

    form: {
        error: function (error){
            var p = jQuery('#prize-editor');
            var form = jQuery('form[name="prize"]', p);

            jQuery('.error', form).remove();

            jQuery.each(error, function(k, v){
                var input = jQuery('[name="' + k + '"]');

                if (input.length)
                {
                    var error = jQuery('<div class="error">' + v + '</div>');
                    error.insertAfter(input);
                }
            });

            jQuery('.error', form).stop().slideDown();

        },

        submit: function(subaction, callback_success){
            var p = jQuery('#prize-editor');
            var form = jQuery('form[name="prize"]', p);

            form.ajaxSubmit({
                data: {
                    _action: 'prize',
                    _subaction: subaction
                },
                beforeSubmit: function(){
                    jQuery('*', form).attr({disabled: true});
                    window.WTFL_Admin.preloader(true);
                },
                success: function(responseText, statusText, xhr){
                    if (typeof(callback_success) == 'function'){ callback_success(responseText, statusText, xhr); }

                    window.WTFL_Admin.preloader(false);
                    jQuery('*', form).attr({disabled: false});
                }
            });
        }
    }
}
