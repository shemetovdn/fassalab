var WTFL_Prizes_View = {
    attr: {

    },
    init: function(){
        var p = jQuery('.' + window.WTFL_Prizes_View.attr.prefix + '_prizes.shortcode');

        if (p.length)
        {
            p.each(function(){
                var _p = this;

                jQuery('.tab > .caption', _p).unbind().bind('click', function(){
                    var t = jQuery(this).closest('.tab');
                    var c = jQuery('> .content', t);

                    if (c.is(':hidden'))
                    {
                        jQuery('.tab > .content', _p).stop().slideUp();
                        c.stop().slideDown();
                    }
                });

                jQuery('.prize.h5p-video .preview iframe', p).each(function(){
                    var _this = jQuery(this);

                    _this.load(function(){
                        var d = _this.contents();

                        jQuery('head', d).append('<link type="text/css" rel="stylesheet" href="' + window.WTFL_Prizes_View.attr.url_h5p_css + '" media="all" />');

                        _this.closest('.iframe').css({visibility: 'visible'});
                    });
                });

                jQuery('.prize.h5p-video > a.c', p).unbind().bind('click', function(){
                    var id = jQuery(this).parent().data('id');

                    jQuery.ajax({
                        url: '',
                        type: 'post',
                        dataType: 'json',
                        global: true,
                        async: true,
                        cache: false,
                        data: {
                            _handler: window.WTFL_Prizes_View.attr.prefix,
                            _action: 'prize',
                            _subaction: 'h5p-video-render-get',
                            prize: {
                                id: id
                            }
                        },
                        beforeSend: function(){
                            //window.WTFL_Admin.preloader(true);
                        },
                        timeout: function(){},
                        error: function(){},
                        success: function(response){
                            if (response.status)
                            {
                                jQuery.fancybox.open({
                                    wrapCSS: 'hp5-video',
                                    content: response.html,
                                    afterShow: function(){
                                        window.H5P.init();

                                        var iframe = jQuery('.fancybox-inner iframe.h5p-iframe');

                                        iframe.load(function(){
                                            var d = jQuery(this).contents();

                                            jQuery('.h5p-actions', d).remove();
                                            jQuery(this).attr('style', 'height: ' + jQuery('.h5p-content', d).outerHeight() + 'px !important;');
                                            jQuery.fancybox.update();
                                        });
                                    }
                                });
                            }
                        },
                        complete: function(){
                            //window.WTFL_Admin.preloader(false);
                        }
                    });

                    return false;
                });

                jQuery('.tab > .caption', _p).eq(0).click();
            });
        }
    }
}
