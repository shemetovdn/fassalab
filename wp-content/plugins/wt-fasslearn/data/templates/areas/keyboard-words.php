<?php

    $words = array();

    /*
    if (isset($data['data']['ttvt']) && count($data['data']['ttvt']))
    {
        foreach ($data['data']['ttvt'] as $v1)
        {
            $v1 = preg_replace("/[\s]+/uis", ' ', trim($v1));
            $v1 = preg_split("/[\s]+/uis", $v1);

            if (count($v1))
            {
                foreach ($v1 as $v2)
                {
                    $words[mb_strtolower($v2)] = 1;
                }
            }
        }

        $words = array_keys($words);
    }
    */

    if (isset($data['data']['swtc']))
    {
        $v1 = preg_replace("/[\s]+/uis", ' ', trim($data['data']['swtc']));
        $v1 = preg_split("/[\s]+/uis", $v1);

        if (count($v1))
        {
            foreach ($v1 as $v2)
            {
                $words[mb_strtolower($v2)] = 1;
            }
        }

        $words = array_keys($words);
    }

    if (count($words))
    {
        shuffle($words);

        ?>
        <div class="row keyboard-words">
            <?php

                foreach ($words as $v1)
                {
                    ?>
                    <a href="#" class="btn" onclick="return window.WTFL.keyboard.word(jQuery('.kw-<?php echo $data['id']; ?>-input'), this);"><?php echo $v1; ?></a>
                    <?php
                }

            ?>
        </div>
        <?php
    }

?>
