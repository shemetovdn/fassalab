<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>lessons/normalize.min.css">
<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>lessons/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>lessons/transition.min.css">
<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>lessons/progress.css">
<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>lessons/dropdown.min.css">
<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>lessons/all.css">
<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>lessons/media.css">
<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_js_url'); ?>fancybox/jquery.fancybox.css">

<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>animate.css">
<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>main.css">

<!--[if lt IE 9]>
<script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<link rel="stylesheet" href="<?php echo $this->Attr_Get('plugin_js_url'); ?>/poshytip/tip-yellow/tip-yellow.css">
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>poshytip/jquery.poshytip.min.js"></script>

<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>progress.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>custom.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>tools.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>main.js"></script>

<script type="text/javascript">
    window.WTFL.attr.prefix = '<?php echo $this->Attr_Get('prefix'); ?>';
    window.WTFL.attr.url_site = '<?php echo site_url(); ?>';
    window.WTFL.attr.url_plugin_images = '<?php echo $this->Attr_Get('plugin_img_url'); ?>';
    window.WTFL.attr.url_h5p_css = '<?php echo $this->Attr_Get('plugin_css_url'); ?>h5p.css';

    window.WTFL.attr.text.next = '<?php _e('Suivant'); ?>';
    window.WTFL.attr.text.check = '<?php _e('Valider'); ?>';
    window.WTFL.attr.text.lesson_exit = '<?php _e('Voulez-vous vraiment quitter cette leçon?'); ?>';
    window.WTFL.attr.text.lesson_finish = ['<?php _e('Félicitations'); ?>', '<?php _e('Ce cours est terminé'); ?>'];
</script>
