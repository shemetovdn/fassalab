<?php

    global $_wtfl_dr;

    if (is_object($data))
    {
        $type = (int)$data->type;
    }
    else
    {
        $type = 1;
    }

    $type = array($type => 'checked="checked"');

?>
<div class="step-editor temp" data-id="<?php echo $data->id; ?>">
    <input type="hidden" name="lesson[step][key]" value="<?php echo $data->key; ?>" />
    <input type="hidden" name="lesson[step][id]" value="<?php echo $data->id; ?>" />
    <fieldset>
        <legend>
            <?php

                if (is_object($data))
                {
                    _e('Edit step');
                }
                else
                {
                    _e('New step');
                }

            ?>
        </legend>
        <fieldset>
            <legend><?php _e('Type of step '); ?>:</legend>
            <div><label><input type="radio" name="lesson[step][type]" value="1" <?php echo $type[1]; ?> /> <?php echo $_wtfl_dr['lesson']['step']['type'][1]; ?></label></div>
<!--            <div><label><input type="radio" name="lesson[step][type]" value="2" --><?php //echo $type[2]; ?><!-- /> --><?php //echo $_wtfl_dr['lesson']['step']['type'][2]; ?><!--</label></div>-->
            <div><label><input type="radio" name="lesson[step][type]" value="7" <?php echo $type[7]; ?> /> <?php echo $_wtfl_dr['lesson']['step']['type'][7]; ?></label></div>
<!--            <div><label><input type="radio" name="lesson[step][type]" value="3" --><?php //echo $type[3]; ?><!-- /> --><?php //echo $_wtfl_dr['lesson']['step']['type'][3]; ?><!--</label></div>-->
            <div><label><input type="radio" name="lesson[step][type]" value="4" <?php echo $type[4]; ?> /> <?php echo $_wtfl_dr['lesson']['step']['type'][4]; ?></label></div>
<!--            <div><label><input type="radio" name="lesson[step][type]" value="5" --><?php //echo $type[5]; ?><!-- /> --><?php //echo $_wtfl_dr['lesson']['step']['type'][5]; ?><!--</label></div>-->
            <div><label><input type="radio" name="lesson[step][type]" value="8" <?php echo $type[8]; ?> /> <?php echo $_wtfl_dr['lesson']['step']['type'][8]; ?></label></div>
<!--            <div><label><input type="radio" name="lesson[step][type]" value="6" --><?php //echo $type[6]; ?><!-- /> --><?php //echo $_wtfl_dr['lesson']['step']['type'][6]; ?><!--</label></div>-->
            <div><label><input type="radio" name="lesson[step][type]" value="10"<?php echo $type[10];?> /> <?php echo $_wtfl_dr['lesson']['step']['type'][10];?></label></div>
            <div><label><input type="radio" name="lesson[step][type]" value="9" <?php echo $type[9]; ?> /> <?php echo $_wtfl_dr['lesson']['step']['type'][9]; ?></label></div>
        </fieldset>
        <fieldset>
            <legend><?php _e('Title of step'); ?>:</legend>
            <div><input type="text" name="lesson[step][title]" value="<?php echo $data->title; ?>" /></div>
        </fieldset>
        <fieldset>
            <legend><?php _e('Data of step'); ?>:</legend>
            <div class="step-data"></div>
        </fieldset>
        <div class="actions">
            <?php

                if (is_object($data))
                {
                    ?>
                    <input type="button" name="lesson[step][save]" class="button" value="<?php _e('Sauvegarder les changements'); ?>" />
                    <?php
                }
                else
                {
                    ?>
                    <input type="button" name="lesson[step][submit]" class="button" value="<?php _e('Créer'); ?>" />
                    <?php
                }

            ?>
            <input type="button" name="lesson[step][cancel]" class="button" value="<?php _e('Annuler'); ?>" />
        </div>
    </fieldset>
</div>
