<fieldset>
    <p><?php _e('Vous êtes en train de copier'); ?></p>
    <ul>
        <?php

            if (is_object($data['lesson-from']))
            {
                if (is_array($data['lesson-from']->steps) && count($data['lesson-from']->steps))
                {
                    foreach ($data['lesson-from']->steps as $step)
                    {
                        if (in_array($step->id, $data['steps-clone']))
                        {
                            ?>
                            <li><b><?php echo $step->title; ?></b></li>
                            <?php
                        }
                    }
                }
            }

        ?>
    </ul>
    <p><?php _e('étapes de la leçon'); ?> <b><?php echo $data['lesson-from']->title; ?></b> <?php _e('vers la leçon'); ?>:</p>
    <select name="lesson-to">
        <option value="0">- <?php _e('non sélectionnée'); ?> -</option>
        <?php

            if (is_array($data['lessons']) && count($data['lessons']))
            {
                foreach ($data['lessons'] as $lesson)
                {
                    ?>
                    <option value="<?php echo $lesson->id; ?>"><?php echo $lesson->title; ?></option>
                    <?php
                }
            }

        ?>
    </select>
</fieldset>

<div class="actions">
    <input type="button" name="clone" value="<?php _e('Copie'); ?>" />
    <input type="button" name="cancel" value="<?php _e('Annuler'); ?>" />
</div>
