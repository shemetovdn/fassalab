<!--<link type="text/css" rel="stylesheet" media="all" href="<?php /*echo $this->Attr_Get('plugin_css_url');*/ ?>main.css" />-->
<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_js_url'); ?>fancybox/jquery.fancybox.css">
<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>admin.css" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>lesson-editor.css">
<link type="text/css" rel="stylesheet" media="all" href="<?php echo $this->Attr_Get('plugin_css_url'); ?>prize-editor.css">

<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>tools.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>admin.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>lesson-editor.js"></script>
<script type="text/javascript" src="<?php echo $this->Attr_Get('plugin_js_url'); ?>prize-editor.js"></script>

<script type="text/javascript">
    window.WTFL_Admin.attr.prefix = '<?php echo $this->Attr_Get('prefix'); ?>';
    window.WTFL_Lesson_Editor.attr.prefix = '<?php echo $this->Attr_Get('prefix'); ?>';

    window.WTFL_Admin.attr.url = '<?php echo admin_url('admin.php?page=' . $this->prefix); ?>';
    window.WTFL_Admin.attr.url_plugin_core = '<?php echo $this->Attr_Get('plugin_core_url'); ?>';

    window.WTFL_Admin.attr.text.cancel = '<?php _e('Annuler'); ?>';
    window.WTFL_Admin.attr.text.lesson_remove = '<?php _e('Voulez-vous supprimer cette leçon?'); ?>';
    window.WTFL_Admin.attr.text.prize_remove = '<?php _e('Voulez-vous supprimer cette prix?'); ?>';
    window.WTFL_Lesson_Editor.attr.text.lesson_step_remove = '<?php _e('Voulez-vous supprimer cette étape de la leçon?'); ?>';
    window.WTFL_Lesson_Editor.attr.text.lesson_step_data_item_file_remove = '<?php _e('Voulez-vous supprimer ce fichier?'); ?>';
    window.WTFL_Lesson_Editor.attr.text.lesson_steps_copy_to = '<?php _e('Cocher les étapes pour les copier'); ?>';
</script>

<div class="wrap">
    <h1><?php echo get_admin_page_title(); ?></h1>
</div>
