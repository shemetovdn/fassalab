<fieldset>
    <legend><?php _e('Messages of results'); ?>:</legend>
    <div>
        <span><?php _e('Correct answer message') ?>:</span>
        <input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][result-message][true]" value="<?php if (trim($data['data']['result-message']['true']) != ''){ echo $data['data']['result-message']['true']; }else{ _e('Bonne réponse!'); } ?>" />
    </div>
    <div>
        <span><?php _e('Incorrect answer message') ?>:</span>
        <input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][result-message][false]" value="<?php if (trim($data['data']['result-message']['false']) != ''){ echo $data['data']['result-message']['false']; }else{ _e('La bonne réponse était:'); } ?>" />
        <span><?php _e('Correct answer') ?>:</span>
        <input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][result-message][correct]" value="<?php echo $data['data']['result-message']['correct']; ?>" />
    </div>
</fieldset>
