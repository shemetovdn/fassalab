<?php

    global $_wtfl_dr;

    if ($data['data']['vk'] && count($_wtfl_dr['keyboard']['arabic']))
    {
        ?>
        <div class="row signs">
            <?php

            foreach ($_wtfl_dr['keyboard']['arabic'] as $k => $v)
            {
                ?>
                <i class="signs-icon" onclick="window.WTFL.keyboard.input(jQuery('.vk-<?php echo $data['id']; ?>-input'), '<?php echo $v; ?>');"><span><?php echo $v; ?></span></i>
                <?php
            }

            ?>
        </div>
        <?php
    }

?>
