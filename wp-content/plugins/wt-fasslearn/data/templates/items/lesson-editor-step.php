<div class="step step-<?php echo $k; ?>" data-id="<?php echo $v['id']; ?>" data-key="<?php echo $k; ?>">
    <div class="ch">
        <input type="checkbox" name="ls-ch[]" value="<?php echo $v['id']; ?>" />
    </div>
    <div class="rw">
        <div class="r1"><b><?php _e('Step') ?>: <?php echo ($k + 1); ?></b> | <?php echo $v['title']; ?></div>
        <div class="r2"><b><?php _e('Type') ?></b>: <?php echo $_wtfl_dr['lesson']['step']['type'][$v['type']]; ?></div>
        <?php

            if (in_array((int)$v['type'], array(2, 3, 5)))
            {
                $ttvt = (array)$v['data'][(int)$v['type']]['ttvt'];

                ?>
                <div class="r3"><b><?php _e('Correct answers') ?></b>: <span><?php echo implode('</span> <span>', $ttvt); ?></span></div>
                <?php
            }

        ?>
    </div>
    <div class="actions">
        <a href="#" class="preview" title="<?php _e('Preview'); ?>"><?php _e('Preview'); ?></a>
        <a href="#" class="edit" title="<?php _e('Edit'); ?>"><?php _e('Edit'); ?></a>
        <a href="#" class="remove" title="<?php _e('Remove'); ?>"><?php _e('Remove'); ?></a>
    </div>
</div>
