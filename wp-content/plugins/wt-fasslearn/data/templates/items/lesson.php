<div class="lesson lesson-<?php echo $v->id; ?>" data-id="<?php echo $v->id; ?>">
    <div class="rw">
        <div class="r1"><b><?php echo $v->title; ?></b></div>
        <div class="r2"><?php _e('Lesson shortcode'); ?>: [<?php echo $this->prefix; ?>_lesson id=<?php echo $v->id; ?>]</div>
        <?php

            if ((int)$v->prize_id)
            {
                ?>
                <div class="r3">
                    <?php _e('Prize'); ?>:
                    <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_prize_editor&id=' . $v->prize_id); ?>" target="_blank">
                        <?php echo $v->prize_title; ?>
                    </a>
                </div>
                <?php
            }

        ?>
    </div>
    <div class="actions">
        <a href="#" class="preview" title="<?php _e('Preview'); ?>"><?php _e('Preview'); ?></a>
        <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_lesson_editor&id=' . $v->id); ?>" class="edit" title="<?php _e('Edit'); ?>"><?php _e('Edit'); ?></a>
        <a href="#" class="remove" title="<?php _e('Remove'); ?>"><?php _e('Remove'); ?></a>
    </div>
</div>
