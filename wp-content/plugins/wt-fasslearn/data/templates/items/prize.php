<?php

    global $_wtfl_dr;

?>
<div class="prize prize-<?php echo $v->id; ?>" data-id="<?php echo $v->id; ?>">
    <div class="rw">
        <div class="r1"><b><?php echo $v->title; ?></b></div>
        <?php

            if ($v->fs_id)
            {
                ?>
                <div class="r2"><?php _e('Prize attached to lesson'); ?>:
                    <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_lesson_editor&id=' . $v->fs_id); ?>" target="_blank">
                        <?php echo $v->fs_title; ?>
                    </a>
                </div>
                <?php
            }

        ?>
        <?php

            if ($v->category_id)
            {
                ?>
                <div class="r2"><?php _e('Catégorie'); ?>: <?php echo $_wtfl_dr['prize']['category'][$v->category_id]; ?></div>
                <?php
            }

        ?>
    </div>
    <div class="actions">
        <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_prize_editor&id=' . $v->id); ?>" class="edit" title="<?php _e('Edit'); ?>"><?php _e('Edit'); ?></a>
        <a href="#" class="remove" title="<?php _e('Remove'); ?>"><?php _e('Remove'); ?></a>
    </div>
</div>
