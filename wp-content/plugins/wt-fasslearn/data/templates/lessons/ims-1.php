<?php

    global $lesson;

?>
<div
    class="<?php echo $this->prefix; ?>-lesson-wrap lesson-step <?php echo $this->prefix; ?>-lesson-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <!--Question section starts -->
    <section class="question-box thumbnails">
        <h1 class="question"><?php _e('Сhoisis la traduction'); ?> &laquo;<?php echo $data['title']; ?>&raquo;</h1>
        <div class="clearfix">
            <input type="hidden" name="lesson[step][answer]" value="" />
            <?php

                if (count($data['data']['tia']))
                {
                    foreach ($data['data']['tia'] as $k => $v)
                    {
                        $i = $k + 1;

                        ?>
                        <div class="answer-variant-box" data-value="<?php echo $k; ?>">
                            <span>
                                <?php

                                    if (isset($data['data']['files'][$k]) && !$data['data']['files'][$k]['removed'])
                                    {
                                        $file_url = $lesson->File_Url_Fix($data['data']['files'][$k]);

                                        ?>
                                        <img src="<?php echo $file_url; ?>" alt="<?php echo $v; ?>">
                                        <?php
                                    }

                                    if (isset($data['data']['audio'][$k]) && !$data['data']['audio'][$k]['removed'])
                                    {
                                        $file_url = $lesson->File_Url_Fix($data['data']['audio'][$k]);

                                        ?>
                                        <!--<span class="audio-image-variant" onclick="window.WTFL.audio.play('<?php echo $file_url; ?>');"></span>-->
                                        <input type="hidden" class="audio-image-variant" value="<?php echo $file_url; ?>" />
                                        <?php
                                    }

                                ?>
                                <footer class="option">
                                    <span class="answer-text"><?php echo $v; ?></span>
                                    <i class="circle"></i>
                                    <!--<i class="number"><?php echo $i; ?></i>-->
                                </footer>
                            </span>
                        </div>
                        <?php
                    }
                }

            ?>
        </div>
    </section>
    <!--Question section ends -->
</div>

<!--Footer starts. Footer without classes has gray color, and it can take two more classes: .success, .fail -->
<footer
    class="footer <?php echo $this->prefix; ?>-lesson-footer-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <div class="left-col">
        <?php $this->Template_Get('areas/btn-lesson-step-skip', null, $data); ?>
        <i class="success-icon"></i>
        <i class="fail-icon"></i>
        <div class="message">
            <h4></h4>
            <div class="alt-answer">
                <p></p>
            </div>
            <a href="#" class="btn small"><?php _e('Signaler un probleme'); ?></a>
            <a href="#" class="btn small"><?php _e('Discuter de la phrase'); ?> (9)</a>
        </div>
    </div>
    <div class="right-col">
        <?php $this->Template_Get('areas/btn-lesson-video-go', null, $data); ?>
        <a href="#" class="btn check disabled"><?php _e('Valider'); ?></a>
        <?php $this->Template_Get('areas/btn-lesson-next', null, $data); ?>
    </div>
</footer>
<!--Footer ends-->
