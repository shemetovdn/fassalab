<?php

    global $lesson;

?>
<div
    class="<?php echo $this->prefix; ?>-lesson-wrap lesson-step <?php echo $this->prefix; ?>-lesson-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <!--Question section starts. -->
    <section class="question-box forms">
        <h1 class="question"><?php _e('Traduis'); ?><?php //echo $data['title']; ?></h1>
        <div class="clearfix">
            <div class="left-col">
                <form action="#" class="ask-form">
                    <?php

                        if (isset($data['data']['files']['t2t']) && !$data['data']['files']['t2t']['removed'])
                        {
                            ?>
                            <i class="fa fa-volume-down" onclick="window.WTFL.audio.play('<?php echo $lesson->File_Url_Fix($data['data']['files']['t2t']); ?>');"></i>
                            <?php
                        }

                    ?>
                    <div class="textarea" id="ask"><?php echo $data['data']['t2t']; ?></div>
                </form>
            </div>
            <div class="right-col">
                <form action="#" class="answer-form">
                    <!-- This textarea can take class '.finished' which shows its state after entering answer -->
                    <textarea class="vk-<?php echo $data['id']; ?>-input" name="lesson[step][answer]" id="answer" placeholder="<?php _e('écrire ici'); ?>..."></textarea>
                </form>
            </div>
            <div class="icon-holder"><i class="fa fa-chevron-right fa-2x"></i></div>
        </div>
        <?php $this->Template_Get('areas/virtual-keyboard-arabic', null, $data); ?>
    </section>
    <!--Question section ends -->
</div>

<!--Footer starts. Footer without classes has gray color, and it can take two more classes: .success, .fail -->
<footer
    class="footer <?php echo $this->prefix; ?>-lesson-footer-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <div class="left-col">
        <?php $this->Template_Get('areas/btn-lesson-step-skip', null, $data); ?>
        <i class="success-icon"></i>
        <i class="fail-icon"></i>
        <div class="message">
            <h4></h4>
            <div class="alt-answer">
                <p></p>
            </div>
            <a href="#" class="btn small"><?php _e('Signaler un probleme'); ?></a>
            <a href="#" class="btn small"><?php _e('Discuter de la phrase'); ?> (9)</a>
        </div>
    </div>
    <div class="right-col">
        <?php $this->Template_Get('areas/btn-lesson-video-go', null, $data); ?>
        <a href="#" class="btn check disabled"><?php _e('Valider'); ?></a>
        <?php $this->Template_Get('areas/btn-lesson-next', null, $data); ?>
    </div>
</footer>
<!--Footer ends-->
