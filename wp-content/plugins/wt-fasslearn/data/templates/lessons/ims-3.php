<?php

    global $lesson;

    if (isset($data['data']['files']['t2t']) && !$data['data']['files']['t2t']['removed'])
    {
        $image_exist = true;
    }
    else
    {
        $image_exist = false;
    }

?>
<div
    class="<?php echo $this->prefix; ?>-lesson-wrap lesson-step <?php echo $this->prefix; ?>-lesson-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <!--Question section starts. -->
    <section class="question-box images">
        <h1 class="question"><?php _e('Choisis la bonne combinaison pour traduire'); ?><br />&laquo;<?php //echo $data['title']; ?> <?php echo $data['data']['t2t']; ?>&raquo;</h1>
        <div class="clearfix">
            <?php

                if ($image_exist)
                {
                    ?>
                    <div class="left-col">
                        <div class="pic-holder">
                            <img src="<?php echo $lesson->File_Url_Fix($data['data']['files']['t2t']); ?>" alt="">
                            <?php
                            /*
                            <div class="img-holder first"><img src="<?php echo $this->Attr_Get('plugin_img_url'); ?>/img-2-1.jpg" alt="Picture-1"></div>
                            <div class="img-holder second"><img src="<?php echo $this->Attr_Get('plugin_img_url'); ?>/img-2-1.jpg" alt="Picture-2"></div>
                            <div class="img-holder third"><img src="<?php echo $this->Attr_Get('plugin_img_url'); ?>/img-2-1.jpg" alt="Picture-3"></div>
                            */
                            ?>
                        </div>
                    </div>
                    <?php
                }

            ?>
            <div class="right-col <?php if (!$image_exist){ ?>image-without<?php } ?>">
                <div class="row">
                    <input type="hidden" name="lesson[step][answer-prefix]" value="" />
                    <?php

                        if (count($data['data']['ttvtp']))
                        {
                            foreach ($data['data']['ttvtp'] as $k => $v)
                            {
                                $i = $k + 1;

                                ?>
                                <span class="btn question radio-prefix" data-value="<?php echo $k; ?>"><i class="circle"></i><?php echo $v; ?></span>
                                <?php
                            }
                        }

                    ?>
                </div>
                <div class="row">
                    <!-- This has additional class (.mistake) which indicates stake after making mistake -->
                    <input class="vk-<?php echo $data['id']; ?>-input" type="text" name="lesson[step][answer]" placeholder="<?php _e('écrire ici'); ?>..." value="">
                </div>
                <?php $this->Template_Get('areas/virtual-keyboard-arabic', null, $data); ?>
            </div>
        </div>
    </section>
    <!--Question section ends -->
</div>

<!--Footer starts. Footer without classes has gray color, and it can take two more classes: .success, .fail -->
<footer
    class="footer <?php echo $this->prefix; ?>-lesson-footer-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <div class="left-col">
        <?php $this->Template_Get('areas/btn-lesson-step-skip', null, $data); ?>
        <i class="success-icon"></i>
        <i class="fail-icon"></i>
        <div class="message">
            <h4></h4>
            <div class="alt-answer">
                <p></p>
            </div>
            <a href="#" class="btn small"><?php _e('Signaler un probleme'); ?></a>
            <a href="#" class="btn small"><?php _e('Discuter de la phrase'); ?> (9)</a>
        </div>
    </div>
    <div class="right-col">
        <?php $this->Template_Get('areas/btn-lesson-video-go', null, $data); ?>
        <a href="#" class="btn check disabled"><?php _e('Valider'); ?></a>
        <?php $this->Template_Get('areas/btn-lesson-next', null, $data); ?>
    </div>
</footer>
<!--Footer ends-->
