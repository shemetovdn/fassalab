<div
    class="<?php echo $this->prefix; ?>-lesson-wrap lesson-step <?php echo $this->prefix; ?>-lesson-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <!--Question section starts. -->
    <section class="question-box selection">
        <h1 class="question"><?php _e('Choisis le mot manquant'); ?> &laquo;<?php echo $data['title']; ?>&raquo;</h1>
        <span class="ask-element s1p"><?php echo $data['data']['s1p']; ?></span>
        <div class="ui selection dropdown">
            <input type="hidden" name="lesson[step][answer]" value="" />
            <i class="dropdown icon"></i>
            <div class="default text"><?php _e('Choisis le mot'); ?></div>
            <div class="menu">
                <?php

                    if (count($data['data']['word']))
                    {
                        foreach ($data['data']['word'] as $k => $v)
                        {
                            ?>
                            <div class="item" data-value="<?php echo $k; ?>" data-text="<?php echo $v; ?>">
                                <i class="icon"></i>
                                <?php echo $v; ?>
                            </div>
                            <?php
                        }
                    }

                ?>
            </div>
        </div>
        <span class="ask-element s2p"><?php echo $data['data']['s2p']; ?></span>
    </section>
    <!--Question section ends -->
</div>

<!--Footer starts. Footer without classes has gray color, and it can take two more classes: .success, .fail -->
<footer
    class="footer <?php echo $this->prefix; ?>-lesson-footer-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <div class="left-col">
        <?php $this->Template_Get('areas/btn-lesson-step-skip', null, $data); ?>
        <i class="success-icon"></i>
        <i class="fail-icon"></i>
        <div class="message">
            <h4></h4>
            <div class="alt-answer">
                <p></p>
            </div>
            <a href="#" class="btn small"><?php _e('Signaler un probleme'); ?></a>
            <a href="#" class="btn small"><?php _e('Discuter de la phrase'); ?> (9)</a>
        </div>
    </div>
    <div class="right-col">
        <?php $this->Template_Get('areas/btn-lesson-video-go', null, $data); ?>
        <a href="#" class="btn check disabled"><?php _e('Valider'); ?></a>
        <?php $this->Template_Get('areas/btn-lesson-next', null, $data); ?>
    </div>
</footer>
<!--Footer ends-->
