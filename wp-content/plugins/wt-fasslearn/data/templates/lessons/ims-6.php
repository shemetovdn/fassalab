<?php

    global $lesson, $_wtfl_dr;

?>
<div
    class="<?php echo $this->prefix; ?>-lesson-wrap lesson-step <?php echo $this->prefix; ?>-lesson-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <div class="h5p">
        <input type="hidden" name="lesson[step][answer]" value="" />
        <?php

            $h5p_id = (int)$data['data']['h5p'];

            if ($h5p_id)
            {
                $h5p_frame = do_shortcode('[h5p id="' . $h5p_id . '"]');

                preg_match("/.*(<iframe .*<\/iframe>)/uis", $h5p_frame, $m);

                if (isset($m[1]))
                {
                    echo $m[1];
                }
            }

        ?>
    </div>
</div>

<!--Footer starts. Footer without classes has gray color, and it can take two more classes: .success, .fail -->
<footer
    class="footer <?php echo $this->prefix; ?>-lesson-footer-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <div class="left-col">
        <?php $this->Template_Get('areas/btn-lesson-step-skip', null, $data); ?>
        <i class="success-icon"></i>
        <i class="fail-icon"></i>
        <div class="message">
            <h4></h4>
            <div class="alt-answer">
                <p></p>
            </div>
            <a href="#" class="btn small"><?php _e('Signaler un probleme'); ?></a>
            <a href="#" class="btn small"><?php _e('Discuter de la phrase'); ?> (9)</a>
        </div>
    </div>
    <div class="right-col">
        <?php //$this->Template_Get('areas/btn-lesson-video-go', null, $data); ?>
        <a href="#" class="btn check disabled"><?php _e('Valider'); ?></a>
        <?php $this->Template_Get('areas/btn-lesson-next', null, $data); ?>
    </div>
</footer>
<!--Footer ends-->
