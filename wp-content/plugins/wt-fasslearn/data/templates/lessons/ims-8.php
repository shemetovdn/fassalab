<?php

    global $lesson, $_wtfl_dr;

?>
<div
    class="<?php echo $this->prefix; ?>-lesson-wrap lesson-step <?php echo $this->prefix; ?>-lesson-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <!--Question section starts. -->
    <section class="question-box audio">
        <h1 class="question"><?php _e('Ecris ce que tu entends?'); ?><?php //echo $data['title']; ?></h1>
        <div class="clearfix">
            <div class="sound-icon-holder">
                <a href="#" class="sound-icon normal-speed" onclick="window.WTFL.audio.play('<?php echo $lesson->File_Url_Fix($data['data']['files']['at']); ?>?n'); return false;"></a>
                <span class="sound-icon-splitter"></span>
                <a href="#" class="sound-icon slow-speed" onclick="window.WTFL.audio.play('<?php echo $lesson->File_Url_Fix($data['data']['files']['at']); ?>?s', 0.6); return false;"></a>
            </div>
            <div class="row">
                <div class="textarea input kw kw-<?php echo $data['id']; ?>-input"></div>
                <input type="hidden" name="lesson[step][answer]" id="answer" />
                <?php $this->Template_Get('areas/keyboard-words', null, $data); ?>
            </div>
        </div>
    </section>
    <!--Question section ends -->
</div>

<!--Footer starts. Footer without classes has gray color, and it can take two more classes: .success, .fail -->
<footer
    class="footer <?php echo $this->prefix; ?>-lesson-footer-k-<?php echo $data['key']; ?> <?php echo $data['class']; ?>"
    data-id="<?php echo $data['id']; ?>"
    data-key="<?php echo $data['key']; ?>"
>
    <div class="left-col">
        <?php $this->Template_Get('areas/btn-lesson-step-skip', null, $data); ?>
        <i class="success-icon"></i>
        <i class="fail-icon"></i>
        <div class="message">
            <h4></h4>
            <div class="alt-answer">
                <p></p>
            </div>
            <a href="#" class="btn small"><?php _e('Signaler un probleme'); ?></a>
            <a href="#" class="btn small"><?php _e('Discuter de la phrase'); ?> (9)</a>
        </div>
    </div>
    <div class="right-col">
        <?php $this->Template_Get('areas/btn-lesson-video-go', null, $data); ?>
        <a href="#" class="btn check disabled"><?php _e('Valider'); ?></a>
        <?php $this->Template_Get('areas/btn-lesson-next', null, $data); ?>
    </div>
</footer>
<!--Footer ends-->
