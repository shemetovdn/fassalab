<?php

    global $wtfs_lesson;

?>
<div class="step-data-ims ims-<?php echo $data['type']; ?>" data-ims="<?php echo $data['type']; ?>">
    <?php /* ?>
    <fieldset>
        <legend><?php _e('Settings of step'); ?>:</legend>
        <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][rtl]" class="step-rtl" value="1" <?php if ((int)$data['data']['rtl']){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('Utiliser la direction d\'écriture "de droite à gauche"'); ?></label>
    </fieldset>
    <?php */ ?>
    <fieldset class="ifi ttvt">
        <legend><?php _e('Title for translation'); ?>:</legend>
        <div class="wi">
            <div>
                <input type="text"
                       name="lesson[step][data][<?php echo $data['type']; ?>][ttl]"
                       value="<?php echo $data['data']['ttl']; ?>">
            </div>
        </div>
    </fieldset>
    <fieldset class="items-option ifi tia">
        <legend><?php _e('Possible answers'); ?>:</legend>
        <?php

            $items_count = count($data['data']['tia']);

            if ($items_count)
            {
                foreach ($data['data']['tia'] as $k => $v)
                {
                    $class = 'item-remove';

                    if ($items_count == 1)
                    {
                        $class = 'item-add';
                    }

                    ?>
                    <div class="wi">
                        <div class="wi-l">
                            <input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][tia][<?php echo $k; ?>]" value="<?php echo $v; ?>" />
                            <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][answer-true][<?php echo $k; ?>]" value="<?php echo $k; ?>" <?php if (in_array($k, (array)$data['data']['answer-true'])){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('right answer'); ?></label>
                            <div class="file">
                                <span><?php _e('Image of variant') ?>:</span>
                                <?php

                                    if (isset($data['data']['files'][$k]) && !$data['data']['files'][$k]['removed'])
                                    {
                                        $file_url = $wtfs_lesson->File_Url_Fix($data['data']['files'][$k]);

                                        ?>
                                        <div class="file-up">
                                            <span><?php _e('Uploaded file'); ?>:</span>
                                            <a href="<?php echo $file_url; ?>" target="_blank"><?php echo $data['data']['files'][$k]['name']; ?></a>
                                            <a href="#" class="remove"><?php _e('Remove file'); ?></a>
                                        </div>
                                        <?php
                                    }

                                ?>
                                <input type="hidden" name="lesson[step][data][file][type][<?php echo $k; ?>]" value="(jpg|jpeg|png|gif)" />
                                <input type="file" name="lesson-step-data-file[<?php echo $k; ?>]" />
                            </div>
                            <div class="file">
                                <span><?php _e('Audio for variant') ?>:</span>
                                <?php

                                if (isset($data['data']['audio'][$k]) && !$data['data']['audio'][$k]['removed'])
                                {
                                    $file_url = $wtfs_lesson->File_Url_Fix($data['data']['audio'][$k]);

                                    ?>
                                    <div class="file-up">
                                        <span><?php _e('Uploaded file'); ?>:</span>
                                        <a href="<?php echo $file_url; ?>" target="_blank"><?php echo $data['data']['audio'][$k]['name']; ?></a>
                                        <a href="#" class="remove"><?php _e('Remove audio'); ?></a>
                                    </div>
                                    <?php
                                }

                                ?>
                                <input type="hidden" name="lesson[step][data][audio][type][<?php echo $k; ?>]" value="(mp3|mpeg)" />
                                <input type="file" name="lesson-step-data-audio[<?php echo $k; ?>]" />
                            </div>
                        </div>
                        <a href="#" class="<?php echo $class; ?>"></a>
                    </div>
                    <?php

                    $items_count--;
                }
            }
            else
            {
                ?>
                <div class="wi">
                    <div class="wi-l">
                        <input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][tia][0]" value="<?php echo $v; ?>" />
                        <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][answer-true][0]" value="0" />&nbsp;<?php _e('right answer'); ?></label>
                        <div class="file">
                            <span><?php _e('Image of variant') ?>:</span>
                            <input type="hidden" name="lesson[step][data][file][type][0]" value="(jpg|jpeg|png|gif)" />
                            <input type="file" name="lesson-step-data-file[0]" />
                        </div>
                        <div class="file">
                            <span><?php _e('Audio for variant') ?>:</span>
                            <input type="hidden" name="lesson[step][data][audio][type][0]" value="(mp3|mpeg)" />
                            <input type="file" name="lesson-step-data-audio[0]" />
                        </div>
                    </div>
                    <a href="#" class="item-add"></a>
                </div>
                <?php
            }

        ?>
    </fieldset>
    <?php $this->Template_Get('areas/step-data-ims-tf-messages', null, $data); ?>
</div>
