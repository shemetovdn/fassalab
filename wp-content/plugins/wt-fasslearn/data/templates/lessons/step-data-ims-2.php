<?php

    global $wtfs_lesson;

?>
<div class="step-data-ims ims-<?php echo $data['type']; ?>" data-ims="<?php echo $data['type']; ?>">
    <fieldset>
        <legend><?php _e('Settings of step'); ?>:</legend>
        <?php /* ?><label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][rtl]" class="step-rtl" value="1" <?php if ((int)$data['data']['rtl']){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('Utiliser la direction d\'écriture "de droite à gauche"'); ?></label><?php */ ?>
        <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][vk]" value="1" <?php if ((int)$data['data']['vk']){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('Afficher le clavier virtuel arabe en cette étape de la leçon"'); ?></label>
    </fieldset>
    <fieldset class="ifi ttvt">
        <legend><?php _e('Text for translation'); ?>:</legend>
        <div class="wi">
            <div><textarea type="text" name="lesson[step][data][<?php echo $data['type']; ?>][t2t]"><?php echo $data['data']['t2t']; ?></textarea></div>
            <div class="file">
                <span><?php _e('Audio version of text') ?>:</span>
                <?php

                    if (isset($data['data']['files']['t2t']) && !$data['data']['files']['t2t']['removed'])
                    {
                        $file_url = $wtfs_lesson->File_Url_Fix($data['data']['files']['t2t']);

                        ?>
                        <div class="file-up">
                            <span><?php _e('Uploaded file'); ?>:</span>
                            <a href="<?php echo $file_url; ?>" target="_blank"><?php echo $data['data']['files']['t2t']['name']; ?></a>
                            <a href="#" class="remove" data-file-key="t2t"><?php _e('Remove file'); ?></a>
                        </div>
                        <?php
                    }

                ?>
                <input type="hidden" name="lesson[step][data][file][type][t2t]" value="(mp3|mpeg)" />
                <input type="file" name="lesson-step-data-file[t2t]" />
            </div>
        </div>
    </fieldset>
    <fieldset class="items-option ifi ttvt">
        <legend><?php _e('Options correct translations'); ?>:</legend>
        <?php

            $items_count = count($data['data']['ttvt']);

            if ($items_count)
            {
                foreach ($data['data']['ttvt'] as $k => $v)
                {
                    $class = 'item-remove';

                    if ($items_count == 1)
                    {
                        $class = 'item-add';
                    }

                    ?>
                    <div class="wi">
                        <div class="wi-l">
                            <textarea name="lesson[step][data][<?php echo $data['type']; ?>][ttvt][]"><?php echo $v; ?></textarea>
                            <?php /*
                            <div class="file">
                                <span><?php _e('Audio version of text for translation') ?>:</span>
                                <?php

                                    if (isset($data['data']['files'][$k]) && !$data['data']['files'][$k]['removed'])
                                    {
                                        $file_url = $wtfs_lesson->File_Url_Fix($data['data']['files'][$k]);

                                        ?>
                                        <div class="file-up">
                                            <span><?php _e('Uploaded file'); ?>:</span>
                                            <a href="<?php echo $file_url; ?>" target="_blank"><?php echo $data['data']['files'][$k]['name']; ?></a>
                                            <a href="#" class="remove"><?php _e('Remove file'); ?></a>
                                        </div>
                                        <?php
                                    }

                                ?>
                                <input type="hidden" name="lesson[step][data][file][type][<?php echo $k; ?>]" value="(mp3|mpeg)" />
                                <input type="file" name="lesson-step-data-file[<?php echo $k; ?>]" />
                            </div>
                            */ ?>
                        </div>
                        <a href="#" class="<?php echo $class; ?>"></a>
                    </div>
                    <?php

                    $items_count--;
                }
            }
            else
            {
                ?>
                <div class="wi">
                    <div class="wi-l">
                        <textarea name="lesson[step][data][<?php echo $data['type']; ?>][ttvt][]"></textarea>
                        <?php /*
                        <div class="file">
                            <span><?php _e('Audio version of text') ?>:</span>
                            <input type="hidden" name="lesson[step][data][file][type][0]" value="(mp3|mpeg)" />
                            <input type="file" name="lesson-step-data-file[0]" />
                        </div>
                        */ ?>
                    </div>
                    <a href="#" class="item-add"></a>
                </div>
                <?php
            }

        ?>
    </fieldset>
    <?php $this->Template_Get('areas/step-data-ims-tf-messages', null, $data); ?>
</div>
