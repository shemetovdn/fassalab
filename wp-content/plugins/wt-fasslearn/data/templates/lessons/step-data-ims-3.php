<?php

    global $wtfs_lesson;

?>
<div class="step-data-ims ims-<?php echo $data['type']; ?>" data-ims="<?php echo $data['type']; ?>">
    <fieldset>
        <legend><?php _e('Settings of step'); ?>:</legend>
        <?php /* ?><label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][rtl]" class="step-rtl" value="1" <?php if ((int)$data['data']['rtl']){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('Utiliser la direction d\'écriture "de droite à gauche"'); ?></label><?php */ ?>
        <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][vk]" value="1" <?php if ((int)$data['data']['vk']){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('Afficher le clavier virtuel arabe en cette étape de la leçon"'); ?></label>
    </fieldset>
    <fieldset class="ifi ttvt">
        <legend><?php _e('Text for translation'); ?>:</legend>
        <div class="wi">
            <div><textarea type="text" name="lesson[step][data][<?php echo $data['type']; ?>][t2t]"><?php echo $data['data']['t2t']; ?></textarea></div>
            <div class="file">
                <span><?php _e('Picture of step') ?>:</span>
                <?php

                    if (isset($data['data']['files']['t2t']) && !$data['data']['files']['t2t']['removed'])
                    {
                        $file_url = $wtfs_lesson->File_Url_Fix($data['data']['files']['t2t']);

                        ?>
                        <div class="file-up">
                            <span><?php _e('Uploaded file'); ?>:</span>
                            <a href="<?php echo $file_url; ?>" target="_blank"><?php echo $data['data']['files']['t2t']['name']; ?></a>
                            <a href="#" class="remove" data-file-key="t2t"><?php _e('Remove file'); ?></a>
                        </div>
                        <?php
                    }

                ?>
                <input type="hidden" name="lesson[step][data][file][type][t2t]" value="(jpg|jpeg|png|gif)" />
                <input type="file" name="lesson-step-data-file[t2t]" />
            </div>
        </div>
    </fieldset>
    <fieldset class="items-option ifi ttvtp">
        <legend><?php _e('Options prefixes'); ?>:</legend>
        <?php

            $items_count = count($data['data']['ttvtp']);

            if ($items_count)
            {
                foreach ($data['data']['ttvtp'] as $k => $v)
                {
                    $class = 'item-remove';

                    if ($items_count == 1)
                    {
                        $class = 'item-add';
                    }

                    ?>
                    <div class="wi">
                        <div class="wi-l">
                            <input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][ttvtp][]" value="<?php echo $v; ?>" />
                        </div>
                        <a href="#" class="<?php echo $class; ?>"></a>
                    </div>
                    <?php

                    $items_count--;
                }
            }
            else
            {
                ?>
                <div class="wi">
                    <div class="wi-l">
                        <input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][ttvtp][]" value="" />
                    </div>
                    <a href="#" class="item-add"></a>
                </div>
                <?php
            }

        ?>
    </fieldset>
    <fieldset class="items-option ifi ttvt">
        <legend><?php _e('Options correct translations'); ?>:</legend>
        <?php

            $items_count = count($data['data']['ttvt']);

            if ($items_count)
            {
                foreach ($data['data']['ttvt'] as $k => $v)
                {
                    $class = 'item-remove';

                    if ($items_count == 1)
                    {
                        $class = 'item-add';
                    }

                    ?>
                    <div class="wi">
                        <div class="wi-l">
                            <textarea name="lesson[step][data][<?php echo $data['type']; ?>][ttvt][]"><?php echo $v; ?></textarea>
                        </div>
                        <a href="#" class="<?php echo $class; ?>"></a>
                    </div>
                    <?php

                    $items_count--;
                }
            }
            else
            {
                ?>
                <div class="wi">
                    <div class="wi-l">
                        <textarea name="lesson[step][data][<?php echo $data['type']; ?>][ttvt][]"></textarea>
                    </div>
                    <a href="#" class="item-add"></a>
                </div>
                <?php
            }

        ?>
    </fieldset>
    <?php $this->Template_Get('areas/step-data-ims-tf-messages', null, $data); ?>
</div>
