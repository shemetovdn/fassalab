<div class="step-data-ims ims-<?php echo $data['type']; ?>" data-ims="<?php echo $data['type']; ?>">
    <?php /* ?>
    <fieldset>
        <legend><?php _e('Settings of step'); ?>:</legend>
        <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][rtl]" class="step-rtl" value="1" <?php if ((int)$data['data']['rtl']){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('Utiliser la direction d\'écriture "de droite à gauche"'); ?></label>
    </fieldset>
    <?php */ ?>
    <fieldset>
        <legend><?php _e('The first part of the sentence'); ?>:</legend>
        <div><input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][s1p]" value="<?php echo $data['data']['s1p']; ?>" /></div>
    </fieldset>
    <fieldset class="items-option words">
        <legend><?php _e('Options for the missing words'); ?>:</legend>
        <?php

            $items_count = count($data['data']['word']);

            if ($items_count)
            {
                foreach ($data['data']['word'] as $k => $v)
                {
                    $class = 'item-remove';

                    if ($items_count == 1)
                    {
                        $class = 'item-add';
                    }

                    ?>
                    <div class="wi">
                        <div class="wi-l">
                            <input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][word][]" value="<?php echo $v; ?>" />
                            <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][answer-true][]" value="<?php echo $k; ?>" <?php if (in_array($k, (array)$data['data']['answer-true'])){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('right answer'); ?></label>
                        </div>
                        <a href="#" class="<?php echo $class; ?>"></a>
                    </div>
                    <?php

                    $items_count--;
                }
            }
            else
            {
                ?>
                <div class="wi">
                    <div class="wi-l">
                        <input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][word][]" value="" />
                        <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][answer-true][]" value="0" />&nbsp;<?php _e('right answer'); ?></label>
                    </div>
                    <a href="#" class="item-add"></a>
                </div>
                <?php
            }

        ?>
    </fieldset>
    <fieldset>
        <legend><?php _e('The second part of the sentence'); ?>:</legend>
        <div><input type="text" name="lesson[step][data][<?php echo $data['type']; ?>][s2p]" value="<?php echo $data['data']['s2p']; ?>" /></div>
    </fieldset>
    <?php $this->Template_Get('areas/step-data-ims-tf-messages', null, $data); ?>
</div>
