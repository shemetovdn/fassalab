<?php

    global $wtfs_lesson;

?>
<div class="step-data-ims ims-<?php echo $data['type']; ?>" data-ims="<?php echo $data['type']; ?>">
    <fieldset>
        <legend><?php _e('Settings of step'); ?>:</legend>
        <?php /* ?><label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][rtl]" class="step-rtl" value="1" <?php if ((int)$data['data']['rtl']){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('Utiliser la direction d\'écriture "de droite à gauche"'); ?></label><?php */ ?>
        <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][vk]" value="1" <?php if ((int)$data['data']['vk']){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('Afficher le clavier virtuel arabe en cette étape de la leçon"'); ?></label>
    </fieldset>
    <fieldset class="ifi ttvt">
        <legend><?php _e('Audio text'); ?>:</legend>
        <div class="wi">
            <div class="file">
                <?php

                    if (isset($data['data']['files']['at']) && !$data['data']['files']['at']['removed'])
                    {
                        $file_url = $wtfs_lesson->File_Url_Fix($data['data']['files']['at']);

                        ?>
                        <div class="file-up">
                            <span><?php _e('Uploaded file'); ?>:</span>
                            <a href="<?php echo $file_url; ?>" target="_blank"><?php echo $data['data']['files']['at']['name']; ?></a>
                            <a href="#" class="remove" data-file-key="at"><?php _e('Remove file'); ?></a>
                        </div>
                        <?php
                    }

                ?>
                <input type="hidden" name="lesson[step][data][file][type][at]" value="(mp3|mpeg)" />
                <input type="file" name="lesson-step-data-file[at]" />
            </div>
        </div>
    </fieldset>
    <fieldset class="items-option ifi ttvt">
        <legend><?php _e('Options correct translations'); ?>:</legend>
        <?php

            $items_count = count($data['data']['ttvt']);

            if ($items_count)
            {
                foreach ($data['data']['ttvt'] as $k => $v)
                {
                    $class = 'item-remove';

                    if ($items_count == 1)
                    {
                        $class = 'item-add';
                    }

                    ?>
                    <div class="wi">
                        <div class="wi-l">
                            <textarea name="lesson[step][data][<?php echo $data['type']; ?>][ttvt][]"><?php echo $v; ?></textarea>
                        </div>
                        <a href="#" class="<?php echo $class; ?>"></a>
                    </div>
                    <?php

                    $items_count--;
                }
            }
            else
            {
                ?>
                <div class="wi">
                    <div class="wi-l">
                        <textarea name="lesson[step][data][<?php echo $data['type']; ?>][ttvt][]"></textarea>
                    </div>
                    <a href="#" class="item-add"></a>
                </div>
                <?php
            }

        ?>
    </fieldset>
    <?php $this->Template_Get('areas/step-data-ims-tf-messages', null, $data); ?>
</div>
