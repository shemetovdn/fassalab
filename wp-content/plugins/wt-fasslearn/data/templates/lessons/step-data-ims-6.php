<?php

    global $wpdb, $wtfs_lesson;

    $h5p_video_lessons = $wpdb->get_results("
        SELECT
            c.`id`,
            c.`title`
        FROM
            `{$wpdb->prefix}h5p_contents` c
        LEFT JOIN
            `{$wpdb->prefix}h5p_libraries` l
            ON
            l.`id` = c.`library_id`
        WHERE
            l.`name` = 'H5P.InteractiveVideo'
        ORDER BY
            c.`id`
            DESC
    ");

?>
<div class="step-data-ims ims-<?php echo $data['type']; ?>" data-ims="<?php echo $data['type']; ?>">
    <?php /* ?>
    <fieldset>
        <legend><?php _e('Settings of step'); ?>:</legend>
        <label><input type="checkbox" name="lesson[step][data][<?php echo $data['type']; ?>][rtl]" class="step-rtl" value="1" <?php if ((int)$data['data']['rtl']){ ?>checked="checked"<?php } ?> />&nbsp;<?php _e('Utiliser la direction d\'écriture "de droite à gauche"'); ?></label>
    </fieldset>
    <?php */ ?>
    <fieldset class="h5p-video-lessons">
        <legend><?php _e('Sélectionnez la leçon vidéo interactive'); ?> (H5P):</legend>
        <?php

            if (count($h5p_video_lessons))
            {
                ?>
                <select name="lesson[step][data][<?php echo $data['type']; ?>][h5p]">
                    <?php

                        foreach ($h5p_video_lessons as $v)
                        {
                            ?>
                            <option value="<?php echo $v->id; ?>" <?php if ((int)$data['data']['h5p'] == (int)$v->id){ ?>selected="selected"<?php } ?>><?php echo $v->title; ?></option>
                            <?php
                        }

                    ?>
                </select>
                <?php
            }

        ?>
    </fieldset>
    <?php $this->Template_Get('areas/step-data-ims-tf-messages', null, $data); ?>
</div>
