<?php

    $this->Template_Get('areas/page-admin-head');

    global $wpdb;

    $prizes = $wpdb->get_results("
        SELECT
            p.*
        FROM
            `{$wpdb->prefix}{$this->prefix}_prizes` p
        WHERE
            p.`fl_id` IS NULL 
            OR 
            p.`fl_id` < 1
            OR 
            p.`fl_id` = '{$data->id}'
        ORDER BY
            p.`date`
            DESC
    ");

?>

<div id="lesson-editor">
    <form action="" method="post" name="lesson" enctype="multipart/form-data">
        <input type="hidden" name="_handler" value="<?php echo $this->prefix; ?>" />
        <input type="hidden" name="lesson[id]" value="<?php echo $data->id; ?>" />
        <fieldset class="lesson">
            <fieldset>
                <legend><?php _e('Title of lesson'); ?>:</legend>
                <div><input type="text" name="lesson[title]" value="<?php echo $data->title; ?>" /></div>
            </fieldset>
            <!--
            <fieldset>
                <legend><?php _e('Description of lesson'); ?>:</legend>
                <div><textarea name="lesson[description]"><?php echo $data->description; ?></textarea></div>
            </fieldset>
            -->
        </fieldset>

        <fieldset class="steps">
            <legend><?php _e('Steps'); ?>:</legend>

            <input type="hidden" name="steps" value="" />
            <div class="steps-list"></div>

            <p class="submit">
                <input type="button" name="lesson[step-new]" class="button" value="<?php _e('Add new step'); ?>" />
            </p>
        </fieldset>

        <fieldset>
            <legend><?php _e('Lier la leçon avec un cadeau'); ?>:</legend>
            <select name="lesson[prize]">
                <option value="0">- <?php _e('non sélectionnée'); ?> -</option>
                <?php

                    if (count($prizes))
                    {
                        $this->Array_Treatment_Strings($lessons, 'stripslashes');
                        $this->Array_Treatment_Strings($lessons, 'htmlspecialchars');

                        foreach ($prizes as $v)
                        {
                            ?>
                            <option value="<?php echo $v->id; ?>" <?php if ((int)$data->prize_id == (int)$v->id){ ?>selected="selected"<?php } ?>><?php echo $v->title; ?></option>
                            <?php
                        }
                    }

                ?>
            </select>
        </fieldset>

        <p class="submit">
            <?php

                if (is_object($data))
                {
                    ?>
                    <input type="submit" name="lesson[save]" class="button button-primary" value="<?php _e('Sauvegarder les changements'); ?>" />
                    <input type="submit" name="lesson[clone]" class="button clone" value="<?php _e('Faites une copie de la leçon'); ?>" />
                    <input type="submit" name="lesson[clone-steps]" class="button clone" value="&nbsp;" data-value="<?php _e('Étapes de copie dans une autre leçon'); ?>" />
                    <?php
                }
                else
                {
                    ?>
                    <input type="submit" name="lesson[submit]" class="button button-primary" value="<?php _e('Create new lesson'); ?>" />
                    <?php
                }

            ?>
        </p>
    </form>

    <?php $this->Template_Get('areas/lesson-step-editor-form'); ?>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFL_Lesson_Editor.init();

        jQuery('#lesson-editor input[name="lesson[clone]"]').unbind().bind('click', function(){
            window.WTFL_Lesson_Editor.lesson_clone(<?php echo $data->id; ?>);
            return false;
        });
    });
</script>
