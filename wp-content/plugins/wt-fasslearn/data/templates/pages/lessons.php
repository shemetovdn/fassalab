<?php $this->Template_Get('areas/page-admin-head'); ?>

<div id="<?php echo $this->prefix; ?>-lessons">
    <div class="lessons-list"></div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFL_Admin.lessons_list_get();
    });
</script>
