<?php $this->Template_Get('areas/page-admin-head'); ?>

<div id="<?php echo $this->prefix; ?>-main">
    <ul class="action-main-links lessons">
        <li class="lessons-list">
            <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_lessons'); ?>">
                <span><?php _e('La liste de leçons'); ?></span>
            </a>
        </li>
        <li class="lesson-new">
            <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_lesson_editor'); ?>">
                <span><?php _e('Créer la nouvelle leçon'); ?></span>
            </a>
        </li>
    </ul>
    <ul class="action-main-links prizes">
        <li class="prizes-list">
            <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_prizes'); ?>">
                <span><?php _e('La liste de prix'); ?></span>
            </a>
        </li>
        <li class="prize-new">
            <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_prize_editor'); ?>">
                <span><?php _e('Créer la nouvelle prix'); ?></span>
            </a>
        </li>
    </ul>
</div>
