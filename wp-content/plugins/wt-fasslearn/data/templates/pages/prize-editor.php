<?php

    $this->Template_Get('areas/page-admin-head');

    global $wpdb, $_wtfl_dr;

    $data->video_data = (array)@unserialize($data->video_data);

    if (!is_object($data))
    {
        $data = new stdClass();
    }

    if (!count($data->video_data) || !isset($data->video_data[0]['h5p']))
    {
        $data->video_data = array(
            array(
                'h5p'      => 0,
                'category' => 1,
                'popup'    => true
            )
        );
    }

    $h5p_videos = $wpdb->get_results("
        SELECT
            c.`id`,
            c.`title`
        FROM
            `{$wpdb->prefix}h5p_contents` c
        LEFT JOIN
            `{$wpdb->prefix}h5p_libraries` l
            ON
            l.`id` = c.`library_id`
        WHERE
            l.`name` = 'H5P.InteractiveVideo'
        ORDER BY
            c.`id`
            DESC
    ");

    $lessons = $wpdb->get_results("
        SELECT
            l.*
        FROM
            `{$wpdb->prefix}{$this->prefix}_lessons` l
        LEFT JOIN
            `{$wpdb->prefix}{$this->prefix}_prizes` p 
            ON 
            p.`fl_id` = l.`id`
        WHERE
            p.`fl_id` IS NULL
            OR 
            p.`id` = '{$data->id}'
        ORDER BY
            l.`date`
            DESC
    ");

?>

<div id="prize-editor">
    <form action="" method="post" name="prize" enctype="multipart/form-data">
        <input type="hidden" name="_handler" value="<?php echo $this->prefix; ?>" />
        <input type="hidden" name="prize[id]" value="<?php echo $data->id; ?>" />
        <fieldset class="prize">
            <fieldset>
                <legend><?php _e('Title of prize'); ?>:</legend>
                <div><input type="text" name="prize[title]" value="<?php echo $data->title; ?>" /></div>
            </fieldset>
            <fieldset>
                <legend><?php _e('Description of prize'); ?>:</legend>
                <div><textarea name="prize[description]"><?php echo $data->description; ?></textarea></div>
            </fieldset>
            <fieldset>
                <legend><?php _e('Message of popup'); ?>:</legend>
                <div><input type="text" name="prize[message]" value="<?php echo $data->message; ?>" /></div>
            </fieldset>
            <fieldset>
                <legend><?php _e('Lier le cadeau avec une leçon'); ?>:</legend>
                <?php

                    if (count($lessons))
                    {
                        $this->Array_Treatment_Strings($lessons, 'stripslashes');
                        $this->Array_Treatment_Strings($lessons, 'htmlspecialchars');

                        ?>
                        <select name="prize[lesson]">
                            <option value="0">- <?php _e('non sélectionnée'); ?> -</option>
                            <?php

                                foreach ($lessons as $v)
                                {
                                    ?>
                                    <option value="<?php echo $v->id; ?>" <?php if ((int)$data->fl_id == (int)$v->id){ ?>selected="selected"<?php } ?>><?php echo $v->title; ?></option>
                                    <?php
                                }

                            ?>
                        </select>
                        <?php
                    }

                ?>
            </fieldset>
            <fieldset class="h5p-video">
                <legend><?php _e('Sélectionnez la leçon vidéo interactive'); ?> (H5P):</legend>
                <?php

                    if (count($data->video_data))
                    {
                        foreach ($data->video_data as $vk => $vd)
                        {
                            ?>
                            <div class="wi">
                                <fieldset>
                                    <div class="wi-l">
                                        <?php

                                            if (count($h5p_videos))
                                            {
                                                ?>
                                                <select name="prize[video][0][h5p]">
                                                    <?php

                                                        foreach ($h5p_videos as $v)
                                                        {
                                                            ?>
                                                            <option value="<?php echo $v->id; ?>" <?php if ((int)$vd['h5p'] == (int)$v->id){ ?>selected="selected"<?php } ?>><?php echo $v->title; ?></option>
                                                            <?php
                                                        }

                                                    ?>
                                                </select>
                                                <?php
                                            }

                                        ?>
                                        <fieldset>
                                            <legend><?php _e("Title"); ?>:</legend>
                                            <?php

                                                $vd['title'] = preg_replace("/[\s]{2,}/uis", ' ', trim($vd['title']));

                                            ?>
                                            <div><input type="text" name="prize[video][0][title]" value="<?php echo $vd['title']; ?>" /></div>
                                        </fieldset>
                                        <fieldset>
                                            <legend><?php _e("Enregistrer dans la catégorie de profile de l'utilisateur"); ?>:</legend>
                                            <div><label><input type="radio" name="prize[video][0][category]" value="0" /> <?php _e('Ne pas enregistrer'); ?></label></div>
                                            <?php

                                                if (count($_wtfl_dr['prize']['category']))
                                                {
                                                    foreach ($_wtfl_dr['prize']['category'] as $drk => $drv)
                                                    {
                                                        ?>
                                                        <div><label><input type="radio" name="prize[video][0][category]" value="<?php echo $drk; ?>" <?php if ((int)$vd['category'] == (int)$drk){ ?>checked="checked"<?php } ?> /> <?php echo $drv; ?></label></div>
                                                        <?php
                                                    }
                                                }

                                            ?>
                                        </fieldset>
                                        <div class="chpp">
                                            <label><input type="radio" name="prize[video][popup]" value="" <?php if ($vd['popup']){ ?>checked="checked"<?php } ?> /> <?php _e('Afficher dans le message final'); ?></label>
                                        </div>
                                    </div>
                                </fieldset>
                                <a href="#" class="item-add item-action"></a>
                            </div>
                            <?php
                        }
                    }

                ?>
            </fieldset>
        </fieldset>

        <p class="submit">
            <?php

                if (is_object($data) && isset($data->id))
                {
                    ?>
                    <input type="submit" name="prize[save]" class="button button-primary" value="<?php _e('Sauvegarder les changements'); ?>" />
                    <?php
                }
                else
                {
                    ?>
                    <input type="submit" name="prize[submit]" class="button button-primary" value="<?php _e('Create new prize'); ?>" />
                    <?php
                }

            ?>
        </p>
    </form>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFL_Prize_Editor.init();
    });
</script>
