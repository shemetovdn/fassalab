<?php $this->Template_Get('areas/page-admin-head'); ?>

<div id="<?php echo $this->prefix; ?>-prizes">
    <div class="prizes-list"></div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFL_Admin.prizes_list_get();
    });
</script>
