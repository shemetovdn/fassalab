<?php

    $this->Template_Get('areas/page-admin-head');

    $messages = get_option();

?>
<div id="<?php echo $this->prefix; ?>-settings">
    <form action="" method="post">
        <input type="hidden" name="_handler" value="<?php echo $this->prefix; ?>" />
        <input type="hidden" name="_action" value="settings" />
        <input type="hidden" name="_subaction" value="messages-save" />

        <fieldset>
            <legend><?php _e('Message about successfully passed lesson'); ?>:</legend>
            <?php

                $id = $this->prefix . '-message-spl';

                wp_editor(get_option($id), $id, array(
                    'teeny'         => true,
                    'editor_height' => 100,
                    'media_buttons' => false
                ));

            ?>
        </fieldset>
        <fieldset>
            <legend><?php _e('Message about wholly passed lesson'); ?>:</legend>
            <?php

                $id = $this->prefix . '-message-wpl';

                wp_editor(get_option($id), $id, array(
                    'teeny'         => true,
                    'editor_height' => 100,
                    'media_buttons' => false
                ));

            ?>
        </fieldset>
        <fieldset>
            <legend><?php _e('Message about wholly passed course'); ?>:</legend>
            <?php

                $id = $this->prefix . '-message-wpc';

                wp_editor(get_option($id), $id, array(
                    'teeny'         => true,
                    'editor_height' => 100,
                    'media_buttons' => false
                ));

            ?>
        </fieldset>
        <p class="submit">
            <input type="submit" name="settings[submit]" class="button button-primary" value="<?php _e('Apply'); ?>" />
        </p>
    </form>
</div>
