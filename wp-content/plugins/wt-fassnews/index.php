<?php
/*
Plugin Name: news
Plugin URI: http://weeteam.net
Description:
Version: 1.1
Author: WeeTeam
Author URI: http://weeteam.net
*/




//  Add custom post type news
add_action( 'init', 'create_post_type_news' );
function create_post_type_news() {
    register_post_type( 'news',
        array(
            'labels' => array(
                'name' => __( 'News' ),
                'singular_name' => __( 'News' ),
                'search_items' =>  __( 'Search News' ),
                'all_items' => __( 'All News' ),
                'parent_item' => __( 'Parent News' ),
                'parent_item_colon' => __( 'Parent Category:' ),
                'edit_item' => __( 'Edit News' ),
                'update_item' => __( 'Update News' ),
                'add_new_item' => __( 'Add News' ),
                'new_item_name' => __( 'New News Name' ),
                'menu_name' => __( 'News' ),
                'query_var' => true,
                'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
                'taxonomies' => array('category'),
            ),
            'public' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'revisions'),
            'has_archive' => true,
        )
    );
    register_taxonomy('taxonomy_news',array('news'), array(
        'hierarchical' => true,
        'labels' => [],
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'taxonomy_news' ),
    ));
}
add_post_type_support( 'news', 'excerpt' );
add_post_type_support( 'news', 'custom-fields' );

// подключаем функцию активации мета блока (my_extra_fields)
add_action('add_meta_boxes', 'my_extra_fields', 1);

function my_extra_fields() {
    add_meta_box( 'deck_id', 'Decks', 'extra_fields_box_func', 'news', 'normal', 'high'  );
    add_meta_box( 'news_url', 'Url', 'url_field_box_func', 'news', 'normal', 'high'  );
}

function extra_fields_box_func( $post ){
    global $wpdb;

    $result = $wpdb->get_results("
                    SELECT
                      d.*
                    FROM
                        `learn_wtfa_decks` d where special=1  order by title");
    $sel_v = get_metadata('post', (int)$post->ID, 'deck_id', 1);

    $html = '<option value="0">----</option>';
    foreach($result as $key => $value) {
        $selected = $sel_v == $value->id ? 'selected="selected"' : "";
        $html .= "<option value='" . $value->id . "' " . $selected . ">" . $value->title . "</option>";
    }
    ?>
    <p><select name="deck_id">
            <?php echo $html;?>
        </select> Decks
    </p>
    <?php
}


function url_field_box_func( $post ){
    global $wpdb;

    $result = $wpdb->get_results("
                    SELECT
                      d.*
                    FROM
                        `learn_wtfa_decks` d where special=1  order by title");
    $news_url = get_metadata('post', (int)$post->ID, 'news_url', 1);
    ?>
    <p>
        <input type="text" value="<?php echo $news_url;?>" name="news_url"/>
    </p>
    <?php
}

add_action( 'save_post', 'deck_id_update', 0 );
add_action( 'save_post', 'news_url_update', 0 );
## Сохраняем данные, при сохранении поста
function deck_id_update( $post_id ){
    // базовая проверка
    if (
         wp_is_post_autosave( $post_id )
        || wp_is_post_revision( $post_id )
    )
        return false;
//    if( empty($_POST['deck_id']) ){
//        delete_post_meta( (int)$post_id, 'deck_id' ); // удаляем поле если значение пустое
//    }
    // Все ОК! Теперь, нужно сохранить/удалить данные
    update_metadata( 'post',(int)$post_id, 'deck_id', $_POST['deck_id'] );

    return $post_id;
}

function news_url_update( $post_id ){
    // базовая проверка
    if (
        empty( $_POST['news_url'] )
//        || ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ )
        || wp_is_post_autosave( $post_id )
        || wp_is_post_revision( $post_id )
    )
        return false;
    if( empty($_POST['news_url']) ){
        delete_post_meta( (int)$post_id, 'news_url' ); // удаляем поле если значение пустое
    }
    // Все ОК! Теперь, нужно сохранить/удалить данные
    update_metadata( 'post',(int)$post_id, 'news_url', $_POST['news_url'] );

    return $post_id;
}
