<?php

class WT_Fass_Video
{
    private $prefix = 'wtvi';
    private $attr = array();
    private $wpdb;

    public function __construct()
    {
        $this->Init();
    }

    private function Init()
    {
        global $wtvi_message;
        $wtvi_message = array();
        if(!isset($_SESSION)) {
            session_start();
        }
        $this->Attr_Set('prefix', $this->prefix);
        global $wpdb;
        $this->wpdb = $wpdb;
        $this->Plugin_Paths_Init();

        //
        global $_wtvi_dr;
//        include_once($this->Attr_Get('plugin_data_path') . 'data-relations.php');
        ////

//        $this->Mods_Load();
        $this->Plugin_Hooks_Init();
        $this->Plugin_Pages_Init();

        add_action('wp_ajax_wpvi_video_block', array($this, 'Ajax_Video_Block'));
        //        add_action('init', array($this, 'Shortcodes_Init'));
//        add_action('init', array($this, 'Actions_Init'));
//        add_action('post_updated', array($this, 'Posts_Delete_Filter'), 10, 3);
        add_action( 'admin_notices', array($this, 'Display_Notice'));
        add_action('wp_enqueue_scripts', array( $this, 'Add_Scripts' ) );

    }

    public function Get($id)
    {
        if ($id == 0) {
            return 0;
        }
        $result_video = $this->wpdb->get_results("
            SELECT 
              * 
            FROM  
              `{$this->wpdb->prefix}{$this->prefix}_videos`
            WHERE 
              `id` = {$id}");
        if (count($result_video)) {
            return $result_video[0] ;
        }
        return 0;
    }

    public function Get_Clips_By_Videos ($videos_id)
    {
        $result_videos = $this->wpdb->get_results("
            SELECT 
              * 
            FROM  
              `{$this->wpdb->prefix}{$this->prefix}_video_clips` 
            WHERE
              `videos_id` = {$videos_id}
              ORDER BY `sort` ASC
            ");

        return $this->Pretty_Clips($result_videos);
    }

    private function Pretty_Clips ($clips)
    {
        foreach ($clips as &$clip) {
            $clip->id = (int)$clip->id;
            unset($clip->videos_id);
            unset($clip->date);
        }
        return $clips;
    }

    public function getAll()
    {
        $result_videos = $this->wpdb->get_results("
            SELECT 
              * 
            FROM  
              `{$this->wpdb->prefix}{$this->prefix}_videos`");
        return $result_videos ;
    }

    public function Ajax_Video_Block ()
    {
        if ($_REQUEST['subaction'] == 'delete') {
            $this->Delete_Video_Block();
        } else if($_REQUEST['subaction'] == 'add_video_block') {
            $this->Add_Video_Block();
        } else if($_REQUEST['subaction'] == 'add_clip') {
            $this->Add_Clip();
        }
        if ($_REQUEST['dev'] == 1) {
            echo '<pre>';
            var_dump($this->wpdb->last_query);
            var_dump($this->wpdb->last_error);
            var_dump($this->wpdb->last_result);
            echo '</pre>';
        }
        die();
    }

    public function Add_Video_Block ()
    {
        $title = $_REQUEST['title'];
        $status = $_REQUEST['status'];

        $result_videos = $this->wpdb->get_results("
            INSERT INTO 
                `{$this->wpdb->prefix}{$this->prefix}_videos`
            (`title`, `status`, `date`)
            VALUES ('{$title}', '{$status}', " . time() . " )");
        $result = array('status' => $result_videos);
        $result['videos-list'] = $this -> Get_Video_List();
        echo json_encode($result);
        die();

    }

    public function Delete_Video_Block ()
    {
        $id = $_REQUEST['id'];

        $result_delete = $this->wpdb->get_results("
            DELETE FROM 
                `{$this->wpdb->prefix}{$this->prefix}_videos`
            WHERE id = ".$id."");
        $result = array('status' => $result_delete);
        $result['videos-list'] = $this -> Get_Video_List();

        echo json_encode($result);
        die();
    }

    public function Add_Clip()
    {
        $videos_id = (int)$_REQUEST['id'];
        $clip_title = $_REQUEST['clip_title'];
        $clip_link = $_REQUEST['clip_link'];
        $clip_type = $_REQUEST['clip_type'];
        $result_clips = $this->wpdb->get_results("
            INSERT INTO 
                `{$this->wpdb->prefix}{$this->prefix}_video_clips` (
                `videos_id`,
                `title`, 
                `link`, 
                `type`, 
                `date`
             ) VALUES (
                '{$videos_id}',
                '{$clip_title}',
                '{$clip_link}',
                '{$clip_type}',
                " . time() . " 
              )");
        $result = array('status' => $result_clips);
        $result['videos-list'] = $this -> Get_Video_List();
        echo json_encode($result);
        die();
    }

    public function Add_Scripts() // not work
    {
//        echo '<pre>';
//        var_dump($this->Attr_Get('plugin_js_url'));
//        var_dump($this->Attr_Get('plugin_js_url').'data/js/script.js');
//        echo '</pre>';
//        die('dev');
        wp_enqueue_script($this->prefix.'_script', $this->Attr_Get('plugin_js_url').'data/js/script.js', array( 'jQuery' ));

    }

    protected function Attr_Set($key, $value)
    {
        $this->attr[$key] = $value;
    }

    public function Attr_Get($key)
    {
        if (isset($this->attr[$key]))
        {
            return $this->attr[$key];
        }
        else
        {
            return false;
        }
    }

    public function Prefix_Get()
    {
        return $this->prefix;
    }

    public function Session_Set($key, $value)
    {
        $_SESSION[$this->prefix][$key] = $value;
    }

    public function Session_Get($key)
    {
        if (isset($_SESSION[$this->prefix]) && isset($_SESSION[$this->prefix][$key]))
        {
            return $_SESSION[$this->prefix][$key];
        }
        else
        {
            return false;
        }
    }

    public function Session_Unset($key = null)
    {
        if (is_null($key))
        {
            unset($_SESSION[$this->prefix]);
        }
        else
        {
            unset($_SESSION[$this->prefix][$key]);
        }
    }

    private function Plugin_Paths_Init()
    {
        $ps = array();

        preg_match("/^(.*[\/\\\]wp-content[\/\\\]plugins[\/\\\])(.*?)[\/\\\].*$/uis", __FILE__, $ps);

        if (is_array($ps) && count($ps) == 3)
        {
            $this->Attr_Set('plugin_dir_name', $ps[2]);
            $this->Attr_Set('plugin_path', $ps[1] . $this->Attr_Get('plugin_dir_name') . '/');
            $this->Attr_Set('plugin_index_path', $this->Attr_Get('plugin_path') . 'index.php');
            $this->Attr_Set('plugin_core_path', $this->Attr_Get('plugin_path') . 'core/');
            $this->Attr_Set('plugin_actions_path', $this->Attr_Get('plugin_path') . 'core/actions/');
            $this->Attr_Set('plugin_mods_path', $this->Attr_Get('plugin_path') . 'core/mods/');
            $this->Attr_Set('plugin_data_path', $this->Attr_Get('plugin_path') . 'data/');
            $this->Attr_Set('plugin_templates_path', $this->Attr_Get('plugin_path') . 'data/templates/');
            $this->Attr_Set('plugin_file_uploads_temp_path', wp_upload_dir()['basedir'] . '/' . $this->Attr_Get('prefix') . '-files-temp/');
            $this->Attr_Set('plugin_file_uploads_path', wp_upload_dir()['basedir'] . '/' . $this->Attr_Get('prefix') . '-files/');

            $this->Attr_Set('plugin_url', plugins_url($this->Attr_Get('plugin_dir_name')) . '/');
            $this->Attr_Set('plugin_core_url', $this->Attr_Get('plugin_url') . 'core/');
            $this->Attr_Set('plugin_css_url', $this->Attr_Get('plugin_url') . 'data/css/');
            $this->Attr_Set('plugin_js_url', $this->Attr_Get('plugin_url') . 'data/js/');
            $this->Attr_Set('plugin_img_url', $this->Attr_Get('plugin_url') . 'data/images/');
            $this->Attr_Set('plugin_file_uploads_temp_url', wp_upload_dir()['baseurl'] . '/' . $this->Attr_Get('prefix') . '-files-temp/');
            $this->Attr_Set('plugin_file_uploads_url', wp_upload_dir()['baseurl'] . '/' . $this->Attr_Get('prefix') . '-files/');
        }
    }

    private function Plugin_Hooks_Init()
    {
        register_activation_hook($this->Attr_Get('plugin_index_path'), array($this, 'Install'));
        register_uninstall_hook($this->Attr_Get('plugin_index_path'), array($this, 'Uninstall'));
    }

    public function Install()
    {
        global $wpdb;

        $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_videos`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `title` VARCHAR(255) DEFAULT NULL,
                    `status` TINYINT(1) DEFAULT NULL,
                    `date` INT(10) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

        $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_video_clips`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `videos_id` BIGINT(20) DEFAULT NULL,
                    `title` VARCHAR(255) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

        $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_video_clips_passing`
                (
                    `user_id` BIGINT(20) NOT NULL,
                    `videos_id` BIGINT(20) DEFAULT NULL,
                    `clip_id` BIGINT(20) DEFAULT NULL,
                    `passed` VARCHAR(255) DEFAULT NULL,
                    `date` INT(10) DEFAULT NULL
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");

    }

    public function Uninstall()
    {
        global $wpdb;

        $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_videos`");
        $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_video_clips`");
//        $wpdb->query("DELETE FROM `{$wpdb->prefix}options` WHERE `option_name` LIKE '{$this->prefix}%'");
//        $wpdb->query("DELETE FROM `{$wpdb->prefix}usermeta` WHERE `option_name` LIKE '{$this->prefix}%'");
    }

    public function Posts_Delete_Filter($id, $post_after, $post_before)
    {
        die('hghgdmery.ighx');
        global $wpdb;

        $pt = 'sfwd-lessons';
        $st = array('private');

        if
        (
            is_object($post_before) &&
            isset($post_before->ID) &&
            $post_before->post_type == $pt &&
            preg_match("/^{$this->prefix}\-/uis", $post_before->post_name) &&
            in_array($post_before->post_status, $st) &&
            !in_array($post_after->post_status, $st)
        )
        {
            $wpdb->query("
                    UPDATE
                        `{$wpdb->prefix}posts`
                    SET
                        `post_name` = '{$post_before->post_name}',
                        `post_status` = '{$st[0]}'
                    WHERE
                        `ID` = '{$id}'
                ");
        }
    }

    public function Template_Get($name, $path_dir = null, $data = null)
    {
        if (is_null($path_dir))
        {
            $path_dir = $this->Attr_Get('plugin_templates_path');
        }

        $path = $path_dir . $name . '.php';

        if (file_exists($path))
        {
            include($path);
        }
        else
        {
            return false;
        }
    }

    private function Mods_Load()
    {
        $mods = glob($this->Attr_Get('plugin_mods_path') . '*.php');

        if (count($mods))
        {
            foreach ($mods as $v)
            {
                include_once($v);
            }
        }
    }

    public function Page_Title_Build($title)
    {
        return $title . ' :: ' . __('Fass Video');
    }

    private function Plugin_Pages_Init()
    {
        if (is_admin())
        {
            add_action('admin_menu', function(){
                add_menu_page($this->Page_Title_Build(__('Main')), __('Fass Video'), 'manage_options', $this->prefix, function(){
                    if (isset($_REQUEST['preview']) && preg_match("/^[0-9]+$/uis", $_REQUEST['preview']))
                    {
                        echo 'pages/lesson-preview';
//                        $this->Template_Get('pages/lesson-preview');
                    }
                    else
                    {
                        echo 'pages/main';
                        $this->Template_Get('pages/main');
                    }
                }, 'dashicons-video-alt3', 200);
                add_submenu_page($this->prefix, $this->Page_Title_Build(__('Videos')), __('Videos'), 'manage_options', $this->prefix . '_videos', function(){
                    if (isset($_GET['id'])) {
                        if (isset($_GET['action']) && $_GET['action'] == 'edit') {
                            $this->Template_Get('pages/video_block_edit');
                        } else {
                            $this->Template_Get('pages/video_block');
                        }
                    } else {
                        $this->Template_Get('pages/videos');
                    }

                });
                add_submenu_page($this->prefix, $this->Page_Title_Build(__('Clips')), __('Clips'), 'manage_options', $this->prefix . '_clips', function(){
                    if (isset($_GET['id'])) {
                        if (isset($_GET['action']) && $_GET['action'] == 'edit') {
                            $this->Edit_Clip($_GET['id']);
                            $this->Template_Get('pages/clip_edit');
                        } else if (isset($_GET['action']) && $_GET['action'] == 'delete'){
                            $this->Delete_Clip($_GET['id']);
                            $this->Template_Get('pages/clips');
                        } else if (isset($_POST['action']) && $_POST['action'] == 'sorting'){
                            die("HERE$");
                            wp_die();
                        } else {
                            $this->Template_Get('pages/clip');
                        }
                    } else {
                        $this->Template_Get('pages/clips');
                    }

                });

                remove_submenu_page( $this->prefix, $this->prefix );
            });
        }
    }

    public function Delete_Clip($clip_id)
    {
//        global $wtvi_message;
        $result_delete = $this->wpdb->get_results("
            DELETE FROM 
                `{$this->wpdb->prefix}{$this->prefix}_video_clips`
            WHERE id = ".$clip_id);

       ?>
      <div class="notice notice-success is-dismissible">
            <p><?php _e('clip with id= '. $clip_id .' deleted' ); ?></p>
        </div> <?php

    }

    public function Passing($user_id, $videos_id, $clip_id, $passed)
    {
        global $wpdb;
        $result = $wpdb->get_results("
                    SELECT
                          *
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_video_clips_passing` vcp 
                    WHERE
                        vcp.`user_id` = '{$user_id}'
                    AND
                        vcp.`videos_id` = '{$videos_id}'
                    AND
                        vcp.`clip_id` = '{$clip_id}'
                ");

        if (count($result)){
            $result = $wpdb->get_results("
                UPDATE
                    `{$wpdb->prefix}{$this->prefix}_video_clips_passing` vcp 
                SET
                    vcp.`passed` = '{$passed}' 
                WHERE 
                   vcp.`user_id` = '{$user_id}'
                AND
                    vcp.`videos_id` = '{$videos_id}'
                AND
                    vcp.`clip_id` = '{$clip_id}'
                ");
        } else {
            $result = $wpdb->get_results("
                INSERT INTO 
                    `{$wpdb->prefix}{$this->prefix}_video_clips_passing` 
                (`passed`,
                 `user_id`, 
                 `videos_id`,
                  `clip_id`,
                  `date`)
                VALUES 
                ($passed, $user_id, $videos_id, $clip_id, " . time() . ");
                ");
        }

        if ($wpdb->last_error =='') {
            return true;
        } else {
            return false;
        }
    }

    public function Passing_Videos_Block($user_id, $videos_id, $deck_id)
    {
        $clips = $this->Get_Clips_By_Videos($videos_id);
        $passed_clips = $this->Get_Passed_By_User_Block($user_id, $videos_id);
        $video_block_passed = true;
        foreach ($clips as $clip) {
            if (!isset($passed_clips[$clip->id])){
                $video_block_passed = false;
            }
        }


        if ($video_block_passed) {
            $result = $this->wpdb->get_results("
                UPDATE 
                    `{$this->wpdb->prefix}wtfa_decks_words_data_users` wdwdu
                SET
                    wdwdu.`video_passed` = 1 
                WHERE 
                   wdwdu.`deck_id` = '{$deck_id}'
            ");

            if ($this->wpdb->last_error =='') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function Get_Passed_By_User_Block($user_id, $videos_id)
    {
        $result = $this->wpdb->get_results("
            SELECT
                  vcp.clip_id,
                  vcp.passed
            FROM
                `{$this->wpdb->prefix}{$this->prefix}_video_clips_passing` vcp 
            WHERE
                vcp.`user_id` = '{$user_id}'
            AND
                vcp.`videos_id` = '{$videos_id}'
            AND 
                vcp.`passed` = 1
        ");

        $list_id = array();
        foreach ($result as $row){
            $list_id[(int)$row->clip_id] = $row->passed;
        }

        return $list_id;
    }

    public function Edit_Clip($clip_id)
    {
        if (isset($_REQUEST['save'])) {
            $link  = $_REQUEST['link'];
            $title = $_REQUEST['title'];
            $type  = $_REQUEST['clip_type'];

            $result_clips = $this->wpdb->query($this->wpdb->prepare("UPDATE  
                  `{$this->wpdb->prefix}{$this->prefix}_video_clips` vc
                SET 
                  link='%s', title='%s', type='%s' 
                WHERE 
                   id = '{$clip_id}'", $link, $title, $type));
            ?>
            <div class="notice notice-success is-dismissible">
                <p><?php _e('clip with id= ' . $clip_id . ' Updated'); ?></p>
            </div>
            <?php
        }

    }

    /*function Display_Notice() {
        echo 'Display_Notice';
        global $wtvi_message;
        echo '<pre>';
        var_dump($wtvi_message);
        echo '</pre>';
        foreach ($wtvi_message as $message) {
            ?>
            <div class="notice notice-<?php echo $message->type; ?> is-dismissible">
                <p><?php _e( $message->info ); ?></p>
            </div>
            <?php
        }
    }*/

    public function Get_Video_List()
    {
         return include ($this->Attr_Get('plugin_data_path').'templates/pages/videos-list.php');
    }


//    public function Shortcodes_Init()
//    {
//        add_shortcode($this->prefix . '_lesson', function($atts){
//            $atts = shortcode_atts(array(
//                'id' => 0
//            ), $atts);
//
//            $atts['id'] = (int)$atts['id'];
//
//            global $wpdb, $_wtfl_dr;
//
//            $lms_id = (int)get_the_ID();
//
//            include_once($this->Attr_Get('plugin_core_path') . 'lesson-view.php');
//        });
//
//        add_shortcode($this->prefix . '_prizes', function($atts){
//            $atts = shortcode_atts(array(
//                'user_id' => 0
//            ), $atts);
//
//            $atts['user_id'] = (int)$atts['user_id'];
//
//            if (!$atts['user_id'] && isset($_REQUEST['user_id']))
//            {
//                $atts['user_id'] = (int)$_REQUEST['user_id'];
//            }
//
//            if (!$atts['user_id'])
//            {
//                $user = wp_get_current_user();
//                $atts['user_id'] = (int)$user->ID;
//            }
//
//            if (!$atts['user_id'])
//            {
//                return true;
//            }
//
//            global $wpdb, $_wtfl_dr;
//
//            $output = '';
//            include_once($this->Attr_Get('plugin_core_path') . 'prizes-view.php');
//            return $output;
//        });
//    }

public function changeSorting($sorting)
{
    if (is_array($sorting)) {
foreach ($sorting as $key => $value) {
    global $wpdb;
    $wpdb->query("
                    UPDATE
                        `learn_wtvi_video_clips`
                    SET
                        `sort` = '{$value}'
                        WHERE 
                        `id` = '{$key}'
                ");
}
    }

}
}
