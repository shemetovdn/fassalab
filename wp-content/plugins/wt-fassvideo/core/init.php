<?php

error_reporting(E_ERROR);
ini_set('error_reporting', E_ERROR);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include_once(dirname(__FILE__) . '/core.php');

global $_wtvi;
$_wtvi = new WT_Fass_Video();

add_action('wp_ajax_sorting_video', 'sortingVideo');
add_action('wp_ajax_nopriv_sorting_video', 'sortingVideo');
function sortingVideo(){
    global $_wtvi;
    $sorting = $_POST['data'];
    $_wtvi->changeSorting($sorting);

    wp_die();
}
