<?php
global $wpdb;
$result_clips = $this->wpdb->get_results("
        SELECT
            *
        FROM
            `{$this->wpdb->prefix}{$this->prefix}_video_clips` vc
        
    ");


?>
<div class="row">
    <table>
        <tr>
            <th></th>
            <th>id</th>
            <th>video id</th>
            <th>title</th>
            <th>type</th>
            <th>link</th>
            <th>del</th>
        </tr>
        <?php foreach ($result_clips as $clip) { ?>
            <tr>
                <td><a href="/wp-admin/admin.php?page=wtvi_clips&id=<?php echo $clip->id; ?>">edit</a></td>
                <td><?php echo $clip->id; ?></td>
                <td><?php echo $clip->videos_id; ?></td>
                <td><?php echo $clip->title; ?></td>
                <td><?php echo $clip->type; ?></td>
                <td><?php echo $clip->link; ?></td>
                <td>
                    <a href="/wp-admin/admin.php?page=wtvi_clips&action=delete&id=<?php echo $clip->id; ?>">delete</a>
                </td>
            </tr>

        <?php } ?>

    </table>
</div>
<style>
    .row table {
        border: 1px solid #ccc;
        width: 100%;
        border-collapse: collapse;
    }
    .row td,.row th {
        border: 1px solid #ccc;
    }
</style>