<?php
global $wpdb;
$result_videos = $wpdb->get_results("
    SELECT
    *
    FROM
    `{$wpdb->prefix}{$this->prefix}_videos` d ");

ob_start();
 ?>
<div class="row">
    <table style="">
        <tr>
            <th>#</th>
            <th>title</th>
            <th>type</th>
            <th>status</th>
            <th>Clip count</th>
            <th>del</th>
            <th>edit</th>
        </tr>
        <?php foreach ($result_videos as $row) {
            $result_clips = $wpdb->get_var("
                SELECT
                  COUNT(*)
                FROM
                  `{$wpdb->prefix}{$this->prefix}_video_clips` 
                WHERE
                `videos_id` = '{$row->id}'" );
            ?>
            <tr>
                <td><a href="/wp-admin/admin.php?page=wtvi_videos&id=<?php echo $row->id;  ?>"><?php echo $row->id; ?></a></td>
                <td><?php echo $row->title; ?></td>
                <td><?php echo $row->type; ?></td>
                <td><?php echo $row->status; ?></td>
                <td><?php echo $result_clips; ?></td>
                <td><a href="#" class="wtvi_action del" data-id="<?php echo $row->id; ?>">todo del</a></td>
                <td><button data-id="<?php echo $row->id; ?>" class="wtvi_button add_clip">add Clips</button></td>
            </tr>
        <?php } ?>
    </table>
</div>
<?php $returm = ob_get_contents();
ob_clean();
return $returm;