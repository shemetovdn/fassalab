<?php
global $wpdb;
//$videos_id = 1;


//$result_clips = $wpdb->get_results("
//        SELECT
//            *
//        FROM
//            `{$wpdb->prefix}{$this->prefix}_video_clips` d
//        WHERE
//            d.`id` = '{$videos_id}'
//    ");
?>
<button href="#" class="wtvi_action wpvi_add_video_block">Add new video block</button>
<form action="#">
    <label>title <br>
        <input type="text" name="title">
    </label>
    <br>
    <label>status<br>
        <input type="radio" name="status" value="1" checked> yes
        <input type="radio" name="status" value="0"> no
    </label>

</form>
<div class="videos-list">
    <?php echo include ($this->Attr_Get('plugin_data_path').'templates/pages/videos-list.php'); ?>
</div>

<div class="addClip" style="display: none;">
    <form action="#" class="addClip-form" id="addClip">
        <input type="text" name="videos_id" value="0" disabled>
        <br>
        <label>
            Clip title<br>
            <input type="text" name="clip_title">
        </label><br>
        <label>
            Clip link <br>
            <input type="text" name="clip_link">
        </label> <br>
        <label>
            type <br>
            <select name="clip_type">
                <option value="theory">theory</option>
                <option value="practice">practice</option>
            </select>
        </label> <br>
        <button class="wtvi_action wpvi_add_clip">Add new clip</button>
    </form>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery(document).on('click','.wtvi_button', function (e) {
            e.preventDefault();
            if (jQuery(this).hasClass('add_clip')) {
                addClip = jQuery('.addClip');
                video_id = jQuery(this).attr('data-id');
                addClip.find('[name="videos_id"]').val(video_id);
                addClip.show();
                window.scrollTo(0,document.body.scrollHeight);
            }

        });

        jQuery(document).on('click', '.wtvi_action',function (e) {
            e.preventDefault();
            $data = {action: 'wpvi_video_block'};
            if (jQuery(this).hasClass('wpvi_add_video_block')) {
                $data['title'] = jQuery('[name=title]').val();
                $data['status'] = jQuery('[name="status"]:checked').val();
                $data['subaction'] = 'add_video_block';
            } else if (jQuery(this).hasClass('del')) {
                $data['id'] = jQuery(this).attr('data-id');
                $data['subaction'] = 'delete';
            } else if (jQuery(this).hasClass('view')) {
                $data['id'] = jQuery(this).attr('data-id');
                $data['subaction'] = 'view';
            } else if (jQuery(this).hasClass('wpvi_add_clip')) {
                $form = jQuery(this).closest('.addClip-form')
                $data['id'] = $form.find('[name=videos_id]').val();
                $data['clip_title'] = $form.find('[name=clip_title]').val();
                $data['clip_link'] = $form.find('[name=clip_link]').val();
                $data['clip_type'] = $form.find('[name=clip_type]').val();
                $data['subaction'] = 'add_clip';
            }

            $data['dev'] = 1;
            console.log($data);

            jQuery.ajax({
                url: '<?php echo admin_url("admin-ajax.php") ?>',
                type: 'POST',
                data: $data,
                // beforeSend: fucntion(xhr){ },
                success: function (data) {
                    data = JSON.parse(data);
                    if (data['videos-list'] != undefined) {
                        jQuery('.videos-list').html(data['videos-list']);
                    }
                    jQuery('.addClip').hide();
                }
            });
        });
    })
</script>
<style>
    .row table {
        border: 1px solid #ccc;
        width: 100%;
        border-collapse: collapse;
    }
    .row td,.row th {
        border: 1px solid #ccc;

    }
</style>
