<?php

    switch ($this->subaction)
    {
        case 'table-get':
            include_once($this->core->Attr_Get('plugin_libraries_path') . 'mw-wp-pagination.php');

            $qa = array(
                'where' => '',
                'sort'  => '',
                'limit' => ''
            );

            $data['attr']['search'] = preg_replace("/[\s]{2,}/uis", ' ', trim($data['attr']['search']));

            if ($data['attr']['search'] != '')
            {
                $qa['where']  = 'WHERE ';
                $qa['where'] .= "d.`id` LIKE '%" . esc_sql($data['attr']['search']) . "%'";
                $qa['where'] .= ' OR ';
                $qa['where'] .= "d.`text_source` LIKE '%" . esc_sql($data['attr']['search']) . "%'";
                $qa['where'] .= ' OR ';
                $qa['where'] .= "d.`text_translation` LIKE '%" . esc_sql($data['attr']['search']) . "%'";
                $qa['where'] .= ' OR ';
                $qa['where'] .= "d.`text_transcription` LIKE '%" . esc_sql($data['attr']['search']) . "%'";
                $qa['where'] .= ' OR ';
                $qa['where'] .= "d.`description` LIKE '%" . esc_sql($data['attr']['search']) . "%'";
            }

            if ($data['attr']['sort']['by'] != '')
            {
                $qa['sort'] = 'ORDER BY d.`' . esc_sql($data['attr']['sort']['by']) . '` ';

                if ((int)$data['attr']['sort']['type'] < 1)
                {
                    $qa['sort'] .= 'ASC';
                }
                else
                {
                    $qa['sort'] .= 'DESC';
                }
            }

            $result = $wpdb->get_results("
                SELECT
                    d.`id`
                FROM
                    `{$wpdb->prefix}{$this->prefix}_dictionary` d   
                {$qa['where']}    
            ");

            // Pagination
            $items_on_page = 100;
            $items_count = count($result);

            $pg = MW_WP_Pagination(array(
                'show-pages-count'    => 5,
                'items-on-page'       => $items_on_page,
                'items-count'         => $items_count,
                'current-page'        => (int)$data['attr']['page'],
                'href'                => 'javascript:window.WTFV_Admin.dictionary.page_change({page});',
                'href-first-page'     => 'javascript:window.WTFV_Admin.dictionary.page_change({page});',
                'current-html'        => "<a href=\"{href}\" class=\"active\">{page}</a>",
                'page-html'           => "<a href=\"{href}\">{page}</a>",
                'next-html'           => "<a href=\"{href}\" class=\"last\">{page}</a>",
                'previous-html'       => "<a href=\"{href}\" class=\"first\">{page}</a>",
                'next-text'           => __('next') . '&nbsp;&rsaquo;',
                'previous-text'       => '&lsaquo;&nbsp;' . __('previous'),
                'group-text'          => '...',
                'show-first-and-last' => true
            ));

            if ($pg['status'])
            {
                $qa['limit'] = 'LIMIT ' . (($pg['page'] * $items_on_page) - $items_on_page) . ',' . $items_on_page;
            }
            ////

            $result = $wpdb->get_results("
                SELECT
                    d.*
                FROM
                    `{$wpdb->prefix}{$this->prefix}_dictionary` d
                {$qa['where']}    
                {$qa['sort']}    
                {$qa['limit']} 
            ");

            $this->core->Array_Treatment_Strings($result, 'stripslashes');

            ob_start();

            $this->core->Template_Get('areas/dictionary-table', null, array(
                'attr'        => $data['attr'],
                'pg'          => $pg,
                'items_count' => $items_count,
                'data'        => $result
            ));

            $this->result['html'] = ob_get_contents();
            ob_end_clean();

            $this->result['status'] = true;
        break;

        case 'table-row-form':
            $data['id'] = (int)$data['id'];
            $tpl_data = array();

            if ($data['id'])
            {
                $result = $wpdb->get_results("
                    SELECT
                        d.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                    WHERE
                        d.`id` = '{$data['id']}'
                ", ARRAY_A);
            }

            if ($data['id'] && count($result))
            {
                $result[0]['id'] = (int)$result[0]['id'];
                if ((int)$result[0]['status'] > 0){ $result[0]['status'] = 1; }else{ $result[0]['status'] = 0; }

                $this->core->Array_Treatment_Strings($result[0], 'stripslashes');

                $tpl_data['caption'] = __('Editing');
                $tpl_data['row'] = $result[0];
                $tpl_data['submit']['title'] = __('Save');
            }
            else
            {
                $tpl_data['caption'] = __('Creating');
                $tpl_data['submit']['title'] = __('Create');
            }

            ob_start();

            $this->core->Template_Get('areas/dictionary-table-row-form', null, $tpl_data);

            $this->result['html'] = ob_get_contents();
            ob_end_clean();

            $this->result['status'] = true;
        break;

        case 'row-remove':
            $data['id'] = (int)$data['id'];

            if ($data['id'])
            {
                $wpdb->query("
                    DELETE FROM
                        `{$wpdb->prefix}{$this->prefix}_dictionary`
                    WHERE
                        `id` = '{$data['id']}'
                ");
            }

            $this->result['status'] = true;
        break;

        case 'row-save':
            $data['id'] = (int)$data['id'];
            $data['text_source'] = preg_replace("/[\s]{2,}/uis", ' ', trim($data['text_source']));
            $data['text_translation'] = preg_replace("/[ \t]{2,}/uis", ' ', trim($data['text_translation']));
            $data['text_transcription'] = preg_replace("/[ \t]{2,}/uis", ' ', trim($data['text_transcription']));
            $data['description'] = preg_replace("/[ \t]{2,}/uis", ' ', trim($data['description']));
            if ((int)$data['status'] > 0){ $data['status'] = 1; }else{ $data['status'] = 0; }

            if (mb_strlen($data['text_source']) < 1){ $this->result['error']['text_source'] = __('The field must contain at least 1 characters'); }
            if (mb_strlen($data['text_translation']) < 1){ $this->result['error']['text_translation'] = __('The field must contain at least 1 characters'); }
            if (mb_strlen($data['text_transcription']) < 1){ $this->result['error']['text_transcription'] = __('The field must contain at least 1 characters'); }

            if (!count($this->result['error']) && isset($_FILES['dictionary']) && !$_FILES['dictionary']['error']['audio'])
            {
                $file_audio = wp_list_pluck($_FILES['dictionary'], 'audio');

                if (!preg_match("/\/(mp3|mpeg)$/uis", $file_audio['type'])){ $this->result['error']['audio'] = __('File is wrong type'); }
            }

            if (!count($this->result['error']))
            {
                if ($data['id'])
                {
                    $wpdb->query("
                        UPDATE
                            `{$wpdb->prefix}{$this->prefix}_dictionary`
                        SET
                            `text_source` = '" . esc_sql($data['text_source']) . "',
                            `text_translation` = '" . esc_sql($data['text_translation']) . "',
                            `text_transcription` = '" . esc_sql($data['text_transcription']) . "',
                            `description` = '" . esc_sql($data['description']) . "',
                            `status` = '{$data['status']}',
                            `date` = '" . time() . "'
                        WHERE
                            `id` = '{$data['id']}'
                    ");
                }
                else
                {
                    $wpdb->query("
                        INSERT INTO
                            `{$wpdb->prefix}{$this->prefix}_dictionary`
                            (
                                `text_source`,
                                `text_translation`,
                                `text_transcription`,
                                `description`,
                                `status`,
                                `date`
                            )
                            VALUES
                            (
                                '" . esc_sql($data['text_source']) . "',
                                '" . esc_sql($data['text_translation']) . "',
                                '" . esc_sql($data['text_transcription']) . "',
                                '" . esc_sql($data['description']) . "',
                                1,
                                '" . time() . "'
                            )
                    ");

                    $data['id'] = (int)$wpdb->insert_id;
                }

                if ((int)$data['audio_remove'])
                {
                    $result = $wpdb->get_results("
                        SELECT
                            d.`file_audio`
                        FROM
                            `{$wpdb->prefix}{$this->prefix}_dictionary` d
                        WHERE
                            d.`id` = '{$data['id']}'
                    ", ARRAY_A);

                    if (count($result))
                    {
                        @unlink($this->core->Attr_Get('plugin_file_uploads_path') . $result[0]['file_audio']);

                        $wpdb->query("
                            UPDATE
                                `{$wpdb->prefix}{$this->prefix}_dictionary`
                            SET
                                `file_audio` = NULL 
                            WHERE
                                `id` = '{$data['id']}'
                        ");
                    }
                }

                if (isset($file_audio) && is_array($file_audio))
                {
                    $file_name = md5($data['id']) . '.mp3';
                    $file_path = $this->core->Attr_Get('plugin_file_uploads_path') . $file_name;

                    @unlink($file_path);

                    if (move_uploaded_file($file_audio['tmp_name'], $file_path))
                    {
                        @chmod($file_path, 0777);

                        $wpdb->query("
                            UPDATE
                                `{$wpdb->prefix}{$this->prefix}_dictionary`
                            SET
                                `file_audio` = '" . esc_sql($file_name) . "'
                            WHERE
                                `id` = '{$data['id']}'
                        ");
                    }
                }

                $this->result['status'] = true;
            }
        break;

        case 'table-row-search-detected':
            $data['id'] = (int)$data['id'];
            $this->result['count'] = 0;

            if (class_exists($this->core->class_fasslearn) && $data['id'])
            {
                $result = $wpdb->get_results("
                    SELECT
                        d.`text_source`
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                    WHERE
                        d.`id` = '{$data['id']}'
                ", ARRAY_A);

                if (count($result))
                {
                    $text_source = stripslashes($result[0]['text_source']);

                    $result = $wpdb->get_results("
                        SELECT
                            ls.`title`,
                            lsd.`data`
                        FROM
                            `{$wpdb->prefix}wtfl_lessons_steps` ls
                        LEFT JOIN    
                            `{$wpdb->prefix}wtfl_lessons_steps_data` lsd
                            ON 
                            lsd.`step_id` = ls.`id`
                    ", ARRAY_A);

                    if (count($result))
                    {
                        $this->core->Array_Treatment_Strings($result, 'stripslashes');

                        foreach ($result as $v)
                        {
                            $this->result['count'] += mb_substr_count(mb_strtolower($v['title']), mb_strtolower($text_source));
                            $this->result['count'] += mb_substr_count(mb_strtolower($v['data']), mb_strtolower($text_source));
                        }
                    }
                }
            }

            $this->result['status'] = true;
        break;

        case 'word-details-get':
            $data['text'] = stripslashes($data['text']);
            $data['text'] = preg_replace("/[ \t]{2,}/uis", ' ', trim($data['text']));

            if ($data['text'] != '')
            {
                $result = $wpdb->get_results("
                    SELECT
                        d.*
                    FROM
                        `{$wpdb->prefix}{$this->prefix}_dictionary` d
                    WHERE
                        d.`text_source` LIKE '" . preg_replace("/([\\\][\'\"])/uis", "%$1", esc_sql($data['text'])) . "'
                        AND
                        d.`status` = 1
                ", ARRAY_A);

                $this->result['title'] = $data['text'];

                if (count($result))
                {
                    $result = $result[0];
                    $this->core->Array_Treatment_Strings($result, 'stripslashes');

                    ob_start();

                    $this->core->Template_Get('areas/dictionary-word-tooltip', null, $result);

                    $this->result['html'] = ob_get_contents();

                    ob_end_clean();
                }
                else
                {
                    $this->result['html'] = __("Il n'y pas d'information sur ce mot");
                }

                $this->result['status'] = true;
            }
        break;
    }
