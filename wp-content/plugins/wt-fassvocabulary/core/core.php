<?php

    class WT_Fass_Vocabulary
    {
        private $prefix = 'wtfv';
        public $class_fasslearn = 'WT_Fass_Learn';
        public $prefix_fasslearn = 'wtfl';
        private $attr = array();

        public function __construct()
        {
            $this->Init();
        }

        private function Init()
        {
            if(!isset($_SESSION)) {
                session_start();
            }

            $this->Attr_Set('prefix', $this->prefix);

            $this->Plugin_Paths_Init();

            $this->Plugin_Hooks_Init();
            $this->Plugin_Pages_Init();

            add_action('init', array($this, 'Actions_Init'));
            add_action('wp_enqueue_scripts', array($this, 'Enqueue_Sources'));
            add_action('wp_head', array($this, 'Head_Init'));
            add_action('wp', array($this, 'Vocabulary_Front_End_Init'));
        }

        protected function Attr_Set($key, $value)
        {
            $this->attr[$key] = $value;
        }

        public function Attr_Get($key)
        {
            if (isset($this->attr[$key]))
            {
                return $this->attr[$key];
            }
            else
            {
                return false;
            }
        }

        public function Session_Set($key, $value)
        {
            $_SESSION[$this->prefix][$key] = $value;
        }

        public function Session_Get($key)
        {
            if (isset($_SESSION[$this->prefix]) && isset($_SESSION[$this->prefix][$key]))
            {
                return $_SESSION[$this->prefix][$key];
            }
            else
            {
                return false;
            }
        }

        public function Session_Unset($key = null)
        {
            if (is_null($key))
            {
                unset($_SESSION[$this->prefix]);
            }
            else
            {
                unset($_SESSION[$this->prefix][$key]);
            }
        }

        private function Plugin_Paths_Init()
        {
            $ps = array();

            preg_match("/^(.*[\/\\\]wp-content[\/\\\]plugins[\/\\\])(.*?)[\/\\\].*$/uis", __FILE__, $ps);

            if (is_array($ps) && count($ps) == 3)
            {
                $this->Attr_Set('plugin_dir_name', $ps[2]);
                $this->Attr_Set('plugin_path', $ps[1] . $this->Attr_Get('plugin_dir_name') . '/');
                $this->Attr_Set('plugin_index_path', $this->Attr_Get('plugin_path') . 'index.php');
                $this->Attr_Set('plugin_core_path', $this->Attr_Get('plugin_path') . 'core/');
                $this->Attr_Set('plugin_actions_path', $this->Attr_Get('plugin_path') . 'core/actions/');
                $this->Attr_Set('plugin_libraries_path', $this->Attr_Get('plugin_path') . 'core/libraries/');
                $this->Attr_Set('plugin_data_path', $this->Attr_Get('plugin_path') . 'data/');
                $this->Attr_Set('plugin_templates_path', $this->Attr_Get('plugin_path') . 'data/templates/');
                $this->Attr_Set('plugin_file_uploads_path', wp_upload_dir()['basedir'] . '/' . $this->Attr_Get('prefix') . '-files/');

                $this->Attr_Set('plugin_url', plugins_url($this->Attr_Get('plugin_dir_name')) . '/');
                $this->Attr_Set('plugin_css_url', $this->Attr_Get('plugin_url') . 'data/css/');
                $this->Attr_Set('plugin_js_url', $this->Attr_Get('plugin_url') . 'data/js/');
                $this->Attr_Set('plugin_img_url', $this->Attr_Get('plugin_url') . 'data/images/');
                $this->Attr_Set('plugin_file_uploads_url', wp_upload_dir()['baseurl'] . '/' . $this->Attr_Get('prefix') . '-files/');
            }
        }

        private function Plugin_Hooks_Init()
        {
            register_activation_hook($this->Attr_Get('plugin_index_path'), array($this, 'Install'));
            //register_deactivation_hook();
            register_uninstall_hook($this->Attr_Get('plugin_index_path'), array($this, 'Uninstall'));
        }

        public function Install()
        {
            global $wpdb;

            $wpdb->query("
                CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}{$this->prefix}_dictionary`
                (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `text_source` LONGTEXT DEFAULT NULL,
                    `text_translation` LONGTEXT DEFAULT NULL,
                    `description` LONGTEXT DEFAULT NULL,
                    `status` TINYINT(1) DEFAULT NULL,
                    `file_audio` VARCHAR(255) DEFAULT NULL,
                    `date` INT(10) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                ENGINE=MyISAM DEFAULT CHARSET=utf8
            ");
        }

        public function Uninstall()
        {
            global $wpdb;

            $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->prefix}_dictionary`");
            //$wpdb->query("DELETE FROM `{$wpdb->prefix}options` WHERE `option_name` LIKE '{$this->prefix}%'");
        }

        public function Template_Get($name, $path_dir = null, $data = null)
        {
            if (is_null($path_dir))
            {
                $path_dir = $this->Attr_Get('plugin_templates_path');
            }

            $path = $path_dir . $name . '.php';

            if (file_exists($path))
            {
                include($path);
            }
            else
            {
                return false;
            }
        }

        public function Page_Title_Build($title)
        {
            return $title . ' :: ' . __('Fass Vocabulary');
        }

        private function Plugin_Pages_Init()
        {
            if (is_admin())
            {
                add_action('admin_menu', function(){
                    add_menu_page($this->Page_Title_Build(__('Main')), __('Fass Vocabulary'), 'manage_options', $this->prefix, function(){
                        $this->Template_Get('pages/main');
                    }, '', 201);

                    add_submenu_page('wtfv', $this->Page_Title_Build(__('Dictionary')), __('Dictionary'), 'manage_options', $this->prefix . '_dictionary', function(){
                        $this->Template_Get('pages/dictionary');
                    });

                    add_submenu_page('wtfv', $this->Page_Title_Build(__('Import')), __('Import'), 'manage_options', $this->prefix . '_import', function(){
                        $this->Template_Get('pages/import');
                    });
                });
            }
        }

        public function Actions_Init()
        {
            if (isset($_REQUEST['_handler']) && $_REQUEST['_handler'] == $this->prefix)
            {
                include_once($this->Attr_Get('plugin_core_path') . 'action.php');

                $action = new WT_Fass_Vocabulary_Action($this);
                $action->name = $_REQUEST['_action'];
                $action->subaction = $_REQUEST['_subaction'];
                $action->Action_Load($_REQUEST[$_REQUEST['_action']]);
            }
        }

        public function Enqueue_Sources()
        {
            if ($this->Is_FassLearn_Single_View())
            {
                wp_enqueue_style('qtip', $this->Attr_Get('plugin_js_url') . 'qtip/jquery.qtip.min.css', array(), '1.0', 'all');
                wp_enqueue_script('qtip', $this->Attr_Get('plugin_js_url') . 'qtip/jquery.qtip.min.js', array('jquery'), '1.0');

                wp_enqueue_style($this->prefix, $this->Attr_Get('plugin_css_url') . 'main.css', array(), '1.0', 'all');
                wp_enqueue_script($this->prefix, $this->Attr_Get('plugin_js_url') . 'main.js', array('jquery'), '1.0');
            }
        }

        public function Head_Init()
        {
            ?>
            <script type="text/javascript">
                if (typeof(WTFV) == 'object')
                {
                    window.WTFV.attr.prefix = '<?php echo $this->Attr_Get('prefix'); ?>';
                }
            </script>
            <?php
        }

        public function Vocabulary_Front_End_Init()
        {
            add_filter('body_class', function($classes){
                if ($this->Is_FassLearn_Single_View())
                {
                    $classes[] = $this->prefix . '-vocabulary';
                }

                return $classes;
            });
        }

        public function Is_FassLearn_Single_View()
        {
            global $post;

            if (class_exists($this->class_fasslearn) && is_single() && preg_match("/\[{$this->prefix_fasslearn}_lesson[\s\]]?/uis", $post->post_content))
            {
                return true;
            }
        }

        public function Array_Treatment_Strings(&$array, $procedure)
        {
            $type = null;

            if (is_array($array))
            {
                $type = 'array';
            }
            else
            if (is_object($array))
            {
                $type = 'object';
            }

            if (!is_null($type) && count($array))
            {
                foreach ($array as $k => $v)
                {
                    if (is_string($v) && !preg_match("/^a:[0-9]+:\{/uis", $v))
                    {
                        switch (strtolower($procedure))
                        {
                            case 'addslashes':
                                $v = addslashes($v);
                            break;
                            case 'stripslashes':
                                $v = stripslashes($v);
                            break;
                            case 'htmlspecialchars':
                                $v = htmlspecialchars($v);
                            break;
                            case 'htmlspecialchars_decode':
                                $v = htmlspecialchars_decode($v);
                            break;
                        }
                    }
                    else
                    {
                        $v = $this->Array_Treatment_Strings($v, $procedure);
                    }

                    switch ($type)
                    {
                        case 'array':
                            $array[$k] = $v;
                        break;
                        case 'object':
                            $array->{$k} = $v;
                        break;
                    }
                }
            }

            return $array;
        }
    }
