<?php

    error_reporting(E_ERROR);
    ini_set('error_reporting', E_ERROR);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    include_once(dirname(__FILE__) . '/core.php');

    global $_wtfv;
    $_wtfv = new WT_Fass_Vocabulary();
