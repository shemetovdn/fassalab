<?php

    /**
    * Function name: MW WP Pagination
    * Author: Modeewine
    * Author URI: http://modeewine.com
    * Version: 1.10.4
    * Date release: 2013.07.13
    */

    if (!function_exists('MW_WP_Pagination'))
    {
        function MW_WP_Pagination($args = array())
        {
            $res = array();
            $res['status'] = false;

            $args['items-on-page'] = intval($args['items-on-page']);
            $args['items-count'] = intval($args['items-count']);
            $args['current-page'] = intval($args['current-page']);
            $args['show-pages-count'] = intval($args['show-pages-count']);

            if ($args['items-count'] > $args['items-on-page'])
            {
                $res['status'] = true;

                $pages_count = ceil($args['items-count'] / $args['items-on-page']);
                if ($args['current-page'] < 1){ $args['current-page'] = 1; }elseif ($args['current-page'] > $pages_count){ $args['current-page'] = $pages_count; }
                if ($args['last-page']){ $args['current-page'] = $pages_count; }

                $group_pages = ceil($pages_count / $args['show-pages-count']);
                $group_pages_current = ceil($args['current-page'] / $args['show-pages-count']);
                $group_pages_start = $group_pages_current * $args['show-pages-count'] - $args['show-pages-count'] + 1;
                $group_pages_end = $group_pages_start + $args['show-pages-count'] - 1;

                $res['page'] = $args['current-page'];

                if ($args['current-page'] > 1)
                {
                    if (isset($args['href-first-page'])){ $href = $args['href-first-page']; }else{ $href = $args['href']; }
                    $res['html'] .= str_replace(array('{href}', '{page}'), array(str_replace('{page}', ($args['current-page'] - 1), $href), $args['previous-text']), $args['previous-html']);
                }

                if ($group_pages_current > 1)
                {
                    if ($args['show-first-and-last']){ $res['html'] = $res['html'] .= str_replace(array('{href}', '{page}'), array(str_replace('{page}', 1, $args['href']), 1), $args['page-html']); }
                    $res['html'] .= str_replace(array('{href}', '{page}'), array(str_replace('{page}', ($group_pages_start - 1), $args['href']), $args['group-text']), $args['page-html']);
                }

                for ($i1 = $group_pages_start; $i1 <= $group_pages_end; $i1++)
                {
                    if (isset($args['href-first-page']) && $i1 == 1){ $href = $args['href-first-page']; }else{ $href = $args['href']; }
                    if ($i1 == $args['current-page']){ $html = $args['current-html']; }else{ $html = $args['page-html']; }
                    $res['html'] .= str_replace(array('{href}', '{page}'), array(str_replace('{page}', $i1, $href), $i1), $html);
                    if ($i1 >= $pages_count){ break; }
                }

                if ($group_pages_current < $group_pages)
                {
                   $res['html'] .= str_replace(array('{href}', '{page}'), array(str_replace('{page}', ($group_pages_end + 1), $args['href']), $args['group-text']), $args['page-html']);
                   if ($args['show-first-and-last']){ $res['html'] = $res['html'] .= str_replace(array('{href}', '{page}'), array(str_replace('{page}', $pages_count, $args['href']), $pages_count), $args['page-html']); }
                }

                if ($args['current-page'] < $pages_count)
                {
                    $res['html'] .= str_replace(array('{href}', '{page}'), array(str_replace('{page}', ($args['current-page'] + 1), $args['href']), $args['next-text']), $args['next-html']);
                }
            }

            return $res;
        }
    }

?>
