jQuery(document).ready(function(){

});

var WTFV_Admin = {
    attr: {
        p: null,
        text: {},
        t: null,
        tsd: false
    },

    preloader: function(show){
        var id = window.WTFV_Admin.attr.prefix + '-preloader';
        var speed = 200;
        var preloader = jQuery('#' + id);

        if (show)
        {
            if (preloader.length)
            {
                preloader.stop().fadeIn(speed).unbind();
            }
            else
            {
                preloader = jQuery('<div>').attr({id: id}).hide();

                jQuery('body').append(preloader);

                preloader = jQuery('#' + id);
                preloader.fadeIn(speed).unbind();
            }

            setTimeout(function(){
                preloader.click(function(){
                    window.WTFV_Admin.preloader(false);
                });
            }, 5000);
        }
        else
        {
            preloader.fadeOut(speed, function(){
                jQuery(this).remove();
            });
        }
    },

    init: {
        pre_import: function(){
            var p = jQuery('#' + window.WTFV_Admin.attr.prefix + '-import');
            var form = jQuery('form[name="import"]', p);
            var res_area = jQuery('.response', form);

            jQuery('input[name="import[file]"]').unbind().bind('change', function(){
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {
                        _handler: window.WTFV_Admin.attr.prefix,
                        _action: 'import',
                        _subaction: 'pre-import'
                    },
                    beforeSubmit: function(){
                        res_area.hide().removeClass('error success');
                        jQuery('*', form).attr({disabled: true});
                        window.WTFV_Admin.preloader(true);
                    },
                    success: function(responseText, statusText, xhr){
                        if (responseText.status)
                        {
                            res_area.html(responseText.doc.message).addClass('success');
                            window.WTFV_Admin.init.import({
                                doc: {
                                    tmp_path: responseText.doc.tmp_path
                                }
                            });
                        }
                        else
                        {
                            res_area.html(responseText.error).addClass('error');
                        }

                        res_area.slideDown();

                        window.WTFV_Admin.preloader(false);
                        jQuery('*', form).attr({disabled: false});
                    }
                });
            });
        },

        import: function(data){
            var p = jQuery('#' + window.WTFV_Admin.attr.prefix + '-import');
            var form = jQuery('form[name="import"]', p);
            var res_area = jQuery('.response', form);

            jQuery('input[name="import[do]"]', form).unbind().bind('click', function(){
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {
                        _handler: window.WTFV_Admin.attr.prefix,
                        _action: 'import',
                        _subaction: 'import',

                        import: data
                    },
                    beforeSubmit: function(){
                        res_area.hide().removeClass('error success');
                        jQuery('*', form).attr({disabled: true});
                        window.WTFV_Admin.preloader(true);
                    },
                    success: function(responseText, statusText, xhr){
                        if (responseText.status)
                        {
                            res_area.html(responseText.doc.message).addClass('success');
                        }
                        else
                        {
                            res_area.html(responseText.error).addClass('error');
                        }

                        res_area.slideDown();

                        window.WTFV_Admin.preloader(false);
                        jQuery('*', form).attr({disabled: false});
                    }
                });
            });
        },

        table_dictionary: function(){
            var p = jQuery('#' + window.WTFV_Admin.attr.prefix + '-dictionary');
            var form = jQuery('form[name="dictionary"]', p);
            var table = jQuery('.table', form);

            jQuery('thead th', table).removeClass('sorted');
            jQuery('thead th[data-key="' + window.WTFV_Admin.dictionary.table_attr.sort.by + '"]', table).addClass('sorted');

            var sorted_index = jQuery('thead th.sorted', table).index();

            if (sorted_index >= 0)
            {
                jQuery('tr td:nth-child(' + (sorted_index + 1) + ')', table).addClass('sorted');
            }

            jQuery('thead th[data-key!=""]', table).unbind().bind('click', function(){
                var d = jQuery(this).data();

                if (typeof(d.st) == 'undefined' || parseInt(d.st) > 0)
                {
                    var st = 0;
                }
                else
                {
                    var st = 1;
                }

                window.WTFV_Admin.dictionary.table_attr.sort.by = d.key;
                window.WTFV_Admin.dictionary.table_attr.sort.type = st;

                window.WTFV_Admin.dictionary.table_get();
            });

            jQuery('input[name="search"]', form).focus();

            window.WTFV_Admin.init.table_rows_dictionary();

            jQuery('input[name="dictionary[row][create]"]', form).unbind().bind('click', function(){
                window.WTFV_Admin.dictionary.table_row_form();
            });
        },

        search_dictionary: function(){
            var p = jQuery('#' + window.WTFV_Admin.attr.prefix + '-dictionary');
            var form = jQuery('form[name="dictionary"]', p);
            var s = jQuery('input[name="search"]', form);

            s.unbind().bind('keyup', function(){
                clearTimeout(window.WTFV_Admin.attr.t);
                var _this = jQuery(this);

                window.WTFV_Admin.attr.t = setTimeout(function(){
                    window.WTFV_Admin.dictionary.table_attr.search = _this.val();
                    window.WTFV_Admin.dictionary.table_attr.page = 1;

                    window.WTFV_Admin.dictionary.table_get();
                }, 500);
            });
        },

        table_rows_dictionary: function(){
            var p = jQuery('#' + window.WTFV_Admin.attr.prefix + '-dictionary');
            var form = jQuery('form[name="dictionary"]', p);
            var table = jQuery('.table', form);

            jQuery('td', table).unbind();

            jQuery('td', table).bind('hover', function(){
                jQuery('tr', table).removeClass('hover');
                jQuery(this).closest('tr').addClass('hover');
            });

            jQuery('td', table).bind('click', function(){
                window.WTFV_Admin.dictionary.table_row_form(jQuery(this).closest('tr').data('id'));
            });

            jQuery('a', table).unbind().bind('click', function(e){
                e.stopPropagation();
            });
        },

        table_row_form_dictionary: function(){
            var fb = jQuery('.' + window.WTFV_Admin.attr.prefix + '-table-row-form');
            var form = jQuery('form[name="dictionary"]', fb);

            var form_submit = function(action){
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {
                        _handler: window.WTFV_Admin.attr.prefix,
                        _action: 'dictionary',
                        _subaction: 'row-' + action
                    },
                    beforeSubmit: function(){
                        jQuery('*', form).attr({disabled: true});
                        window.WTFV_Admin.preloader(true);
                    },
                    success: function(responseText, statusText, xhr){
                        if (!responseText.status)
                        {
                            jQuery('.error', form).remove();

                            jQuery.each(responseText.error, function(k, v){
                                var input = jQuery('[name="dictionary[' + k + ']"]');

                                if (input.length)
                                {
                                    var error = jQuery('<div class="error">' + v + '</div>');
                                    error.insertAfter(input);
                                }
                            });

                            jQuery('.error', form).stop().slideDown();
                        }
                        else
                        {
                            jQuery.fancybox.close();
                            window.WTFV_Admin.dictionary.table_get();
                        }

                        window.WTFV_Admin.preloader(false);
                        jQuery('*', form).attr({disabled: false});
                    }
                });
            }

            jQuery('input[name="dictionary[cancel]"]', form).unbind().bind('click', function(){
                jQuery.fancybox.close();
            });

            jQuery('input[name="dictionary[remove]"]', form).unbind().bind('click', function(){
                if (!confirm(window.WTFV_Admin.attr.text.dictionary_row_remove)) return;
                form_submit('remove');
            });

            jQuery('input[name="dictionary[submit]"]', form).unbind().bind('click', function(){
                form_submit('save');
            });
        }
    },

    dictionary: {
        table_attr: {
            sort: {
                by: 'date',
                type: 1
            },
            page: 1,
            search: ''
        },

        table_get: function(){
            var p = jQuery('#' + window.WTFV_Admin.attr.prefix + '-dictionary');
            var form = jQuery('form[name="dictionary"]', p);

            jQuery.ajax({
                url: window.ajaxUrl,
                global: false,
                type: 'post',
                dataType: 'json',
                async: true,
                cache: false,
                data: {
                    _handler: window.WTFV_Admin.attr.prefix,
                    _action: 'dictionary',
                    _subaction: 'table-get',

                    dictionary: {
                        attr: window.WTFV_Admin.dictionary.table_attr
                    }
                },
                beforeSend: function(){
                    jQuery('*', form).attr({disabled: true});
                    window.WTFV_Admin.preloader(true);
                },
                success: function(response){
                    jQuery('*', form).attr({disabled: false});

                    if (response.status)
                    {
                        jQuery('.table', form).html(response.html);

                        window.WTFV_Admin.init.table_dictionary();
                        window.WTFV_Admin.dictionary.table_row_search_detected();
                    }
                },
                complete: function(){
                    window.WTFV_Admin.preloader(false);
                }
            });
        },

        page_change: function(page){
            window.WTFV_Admin.dictionary.table_attr.page = parseInt(page);
            window.WTFV_Admin.dictionary.table_get();

            return false;
        },

        table_row_form: function(id){
            jQuery.ajax({
                url: window.ajaxUrl,
                global: false,
                type: 'post',
                dataType: 'json',
                async: true,
                cache: false,
                data: {
                    _handler: window.WTFV_Admin.attr.prefix,
                    _action: 'dictionary',
                    _subaction: 'table-row-form',

                    dictionary: {
                        id: id
                    }
                },
                beforeSend: function(){
                    window.WTFV_Admin.preloader(true);
                },
                success: function(response){
                    if (response.status)
                    {
                        jQuery.fancybox.open({
                            wrapCSS: window.WTFV_Admin.attr.prefix + '-table-row-form',
                            content: response.html,
                            afterShow: function(){
                                window.WTFV_Admin.init.table_row_form_dictionary();
                            }
                        });
                    }
                },
                complete: function(){
                    window.WTFV_Admin.preloader(false);
                }
            });
        },

        table_row_search_detected: function(){
            if (window.WTFV_Admin.attr.tsd) return;

            var p = jQuery('#' + window.WTFV_Admin.attr.prefix + '-dictionary');
            var form = jQuery('form[name="dictionary"]', p);
            var d = jQuery('.table td.d', form).not('.completed');

            if (d.length)
            {
                window.WTFV_Admin.attr.tsd = true;
                
                var _this = d.eq(0);
                var id = _this.closest('tr').data('id');

                jQuery.ajax({
                    url: window.ajaxUrl,
                    global: false,
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    cache: false,
                    data: {
                        _handler: window.WTFV_Admin.attr.prefix,
                        _action: 'dictionary',
                        _subaction: 'table-row-search-detected',

                        dictionary: {
                            id: id
                        }
                    },
                    beforeSend: function(){
                        _this.html('<div class="preloader"></div>');
                    },
                    error: function(){
                        window.WTFV_Admin.dictionary.table_row_search_detected();
                    },
                    success: function(response){
                        _this.html(response.count).addClass('completed');
                    },
                    complete: function(){
                        window.WTFV_Admin.attr.tsd = false;
                        window.WTFV_Admin.dictionary.table_row_search_detected();
                    }
                });
            }
        }
    }
}
