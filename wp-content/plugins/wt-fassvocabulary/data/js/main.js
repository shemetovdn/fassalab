jQuery(document).ready(function(){
    window.WTFV.init();
});

var WTFV = {
    attr: {
        whto: null
    },

    init: function(){
        if (jQuery('body').hasClass(window.WTFV.attr.prefix + '-vocabulary'))
        {
            var p = jQuery('.lesson-steps-wrapper');
            var wtfvc = window.WTFV.attr.prefix + '-word';
            var wp = ['<span class="' + wtfvc + '">', '</span>'];

            jQuery('.lesson-step', p).each(function(){
                var q = jQuery('.question-box', this);

                /*
                jQuery('*', q).each(function(){
                    var _this = jQuery(this);
                    var text = _this.html();

                    if (text.match(/^([^<>]|([<]br.*?[\/]?[>]))+$/gi))
                    {
                        console.log(text);
                        text = text.split(/[<]br.*?[\/]?[>]/gi);

                        if (text.length > 1)
                        {
                            text = wp[0] + text.join(wp[1] + '<br />' + wp[0]) + wp[1];
                        }
                        else
                        if (text.length == 1)
                        {
                            text = wp[0] + text[0] + wp[1];
                        }

                        _this.html(text);
                    }
                });
                */

                jQuery('*', q).each(function(){
                    var flag = 0;
                    var flagt = [];
                    var text = jQuery(this).html();

                    if (jQuery(this).hasClass('signs') || jQuery(this).hasClass('signs-icon'))
                    {

                    }
                    else
                    if (!text.match(/<.*?[>]?.*?[<]?[\/]?.*?>/gi))
                    {
                        flag = 1;
                    }
                    else
                    if (flagt = text.match(/[\/]?.*?>([^<>]+)/i))
                    {
                        if (typeof(flagt[1]) == 'string')
                        {
                            text = flagt[1];
                            flag = 2;
                        }
                    }

                    if (flag > 0)
                    {
                        text = text.replace(/^[\s]+/gi, '').replace(/[\s]+$/gi, '').replace(/[\s]{2,}/gi, ' ');

                        if (text != '')
                        {
                            text = text.split(' ');
                            text = wp[0] + text.join(wp[1] + ' ' + wp[0]) + wp[1];

                            if (flag == 2)
                            {
                                if (flagt[0].match(/<br.*?[\/]?>/gi))
                                {
                                    var tmp = flagt[0].split(/<br.*?[\/]?>/gi);

                                    tmp = wp[0] + tmp.join(wp[1] + '<br />' + wp[0]) + wp[1];

                                    text = jQuery(this).html().replace(flagt[0], tmp);
                                }
                                else
                                {
                                    text = jQuery(this).html().replace(flagt[1], text);
                                }
                            }

                            jQuery(this).html(text);
                        }
                    }
                });
            });

            var words = jQuery('.' + window.WTFV.attr.prefix + '-word', p);

            words.unbind();

            words.qtip({
                content: {
                    text: ''
                },
                show: false,
                hide: {
                    fixed: true,
                    delay: 300
                },
                style: {
                    classes: window.WTFV.attr.prefix + '-word-tooltip'
                },
                events: {
                    show: function(event, api){
                        window.WTFV.init_word_tooltip_audio();
                    }
                }
            });

            words.hover(function(){
                var _this = jQuery(this);
                var text = _this.text();

                clearTimeout(window.WTFV.attr.whto);

                window.WTFV.attr.whto = setTimeout(function(){
                    jQuery.ajax({
                        url: window.ajaxUrl,
                        global: false,
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        data: {
                            _handler: window.WTFV.attr.prefix,
                            _action: 'dictionary',
                            _subaction: 'word-details-get',

                            dictionary: {
                                text: text
                            }
                        },
                        beforeSend: function(){},
                        success: function(response){
                            if (response.status)
                            {
                                var api = _this.qtip('api');

                                api.set('content.title', response.title);
                                api.set('content.text', response.html);
                                api.toggle(true);
                            }
                        },
                        complete: function(){}
                    });
                }, 1000);
            }, function(){
                clearTimeout(window.WTFV.attr.whto);
            });
        }
    },

    init_word_tooltip_audio: function(){
        var p = jQuery('.' + window.WTFV.attr.prefix + '-word-tooltip');

        jQuery('.audio > div a', p).unbind().bind('click', function(){
            var _this = jQuery(this);
            var audio = jQuery('audio', _this.closest('.audio'))[0];

            if (_this.hasClass('pause'))
            {
                audio.currentTime = 0;
                audio.play();

                _this.removeClass('pause').addClass('play');
            }
            else
            {
                audio.pause();

                _this.removeClass('play').addClass('pause');
            }

            return false;
        });
    }
}
