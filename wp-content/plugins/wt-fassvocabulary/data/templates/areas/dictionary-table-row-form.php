<h4><?php echo $data['caption']; ?></h4>
<form action="" method="post" name="dictionary" enctype="multipart/form-data">
    <input type="hidden" name="dictionary[id]" value="<?php echo $data['row']['id']; ?>" />
    <fieldset>
        <legend><?php _e('Source text'); ?>:</legend>
        <input type="text" name="dictionary[text_source]" value="<?php echo $data['row']['text_source']; ?>" />
    </fieldset>
    <fieldset>
        <legend><?php _e('Translation text'); ?>:</legend>
        <textarea name="dictionary[text_translation]"><?php echo $data['row']['text_translation']; ?></textarea>
    </fieldset>
    <fieldset>
        <legend><?php _e('Transcription text'); ?>:</legend>
        <textarea name="dictionary[text_transcription]"><?php echo $data['row']['text_transcription']; ?></textarea>
    </fieldset>
    <fieldset>
        <legend><?php _e('Description'); ?>:</legend>
        <textarea name="dictionary[description]"><?php echo $data['row']['description']; ?></textarea>
    </fieldset>
    <fieldset>
        <legend><?php _e('Audio'); ?>:</legend>
        <input type="file" name="dictionary[audio]" />
        <?php

            if ($data['row']['file_audio'] != '')
            {
                ?>
                <div class="audio">
                    <a href="<?php echo $this->Attr_Get('plugin_file_uploads_url') . $data['row']['file_audio']; ?>" class="play" target="_blank" title="<?php _e('Open audio'); ?>"><?php _e('File attached'); ?></a>
                    <div><label><input type="checkbox" name="dictionary[audio_remove]" value="1" /> <?php _e('Remove audio file'); ?></label></div>
                </div>
                <?php
            }

        ?>
    </fieldset>
    <fieldset>
        <legend><?php _e('Status'); ?>:</legend>
        <div><label><input type="radio" name="dictionary[status]" value="1" <?php if (!$data['row']['id'] || $data['row']['status'] == 1) echo 'checked="checked"'; ?> /> <?php _e('Active'); ?></label></div>
        <div><label><input type="radio" name="dictionary[status]" value="0" <?php if ($data['row']['id'] && $data['row']['status'] == 0) echo 'checked="checked"'; ?> /> <?php _e('Deactivate'); ?></label></div>
    </fieldset>
    <div class="btns">
        <input type="button" name="dictionary[submit]" value="<?php echo $data['submit']['title']; ?>" />
        <?php if ($data['row']['id']) { ?><input type="button" name="dictionary[remove]" value="<?php _e('Remove'); ?>" /><?php } ?>
        <input type="button" name="dictionary[cancel]" value="<?php _e('Cancel'); ?>" />
    </div>
</form>
