<div class="info top">
    <?php

        if ($data['pg']['status'])
        {
            ?>
            <div class="pg">
                <?php echo $data['pg']['html']; ?>
            </div>
            <?php
        }

    ?>
    <div class="count">
        <span><?php _e('Rows found'); ?>: <b><?php echo $data['items_count']; ?></b></span>
    </div>
    <div class="btns">
        <input type="button" name="dictionary[row][create]" value="<?php _e('Create row'); ?>" />
    </div>
</div>

<?php

    if (count($data['data']))
    {
        ?>
        <table>
            <thead>
                <?php $k = 'id'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('ID'); ?></th>
                <?php $k = 'text_source'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Source text'); ?></th>
                <?php $k = 'text_translation'; ?>
                <th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Translation text'); ?></th>
                </th>
                <?php $k = 'text_transcription'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Transcription text'); ?>
                </th>
                <?php $k = 'description'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Description'); ?></th>
                <th data-key=""><?php _e('Audio'); ?></th>
                <?php $k = 'status'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Status'); ?></th>
                <?php $k = 'date'; ?><th data-key="<?php echo $k; ?>" data-st="<?php echo ($k == $data['attr']['sort']['by'] ? $data['attr']['sort']['type'] : 1); ?>"><?php _e('Updating date'); ?></th>
                <th data-key=""><?php _e('D'); ?></th>
            </thead>
            <tbody>
            <?php

                foreach ($data['data'] as $v)
                {
                    $v->text_source = htmlspecialchars($v->text_source);
                    $v->text_translation = htmlspecialchars($v->text_translation);
                    $v->text_translation = preg_replace("/[\r\n]+/uis", '<br>', $v->text_translation);
                    $v->description = preg_replace("/[\r\n]+/uis", '<br>', $v->description);

                    ?>
                    <tr data-id="<?php echo $v->id; ?>">
                        <td class="id"><?php echo $v->id; ?></td>
                        <td><?php echo $v->text_source; ?></td>
                        <td><?php echo $v->text_translation; ?></td>
                        <td><?php echo $v->text_transcription; ?></td>
                        <td><?php echo $v->description; ?></td>
                        <td class="audio">
                            <?php

                                if ($v->file_audio != '')
                                {
                                    ?>
                                    <a href="<?php echo $this->Attr_Get('plugin_file_uploads_url') . $v->file_audio; ?>" class="play" target="_blank" title="<?php _e('Open audio'); ?>"></a>
                                    <?php
                                }

                            ?>
                        </td>
                        <td class="status"><?php ((int)$v->status ? _e('Active') : _e('Disabled')); ?></td>
                        <td class="date"><?php echo date('d.m.Y / H:i', $v->date); ?></td>
                        <td class="d">?</td>
                    </tr>
                    <?php
                }

            ?>
            </tbody>
        </table>
        <?php
    }
    else
    {
        ?>
        <p><?php _e('List is empty'); ?></p>
        <?php
    }

?>


<div class="info bottom">
    <?php

        if ($data['pg']['status'])
        {
            ?>
            <div class="pg">
                <?php echo $data['pg']['html']; ?>
            </div>
            <?php
        }

    ?>
    <div class="count">
        <span><?php _e('Rows found'); ?>: <b><?php echo $data['items_count']; ?></b></span>
    </div>
    <div class="btns">
        <input type="button" name="dictionary[row][create]" value="<?php _e('Create row'); ?>" />
    </div>
</div>
