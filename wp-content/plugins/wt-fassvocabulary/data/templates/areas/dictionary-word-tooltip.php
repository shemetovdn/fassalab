<div>
    <?php
    
        $data['text_translation'] = htmlspecialchars($data['text_translation']);
        $data['text_translation'] = preg_split("/[\r\n]+/uis", $data['text_translation']);

        $data['description'] = preg_replace("/[\r\n]+/uis", '<br>', $data['description']);

        if (count($data['text_translation']))
        {
            ?>
            <ul>
                <?php

                    foreach ($data['text_translation'] as $v)
                    {
                        ?>
                        <li><?php echo $v; ?></li>
                        <?php
                    }

                ?>
            </ul>
            <?php
        }

        if ($data['file_audio'] != '')
        {
            ?>
            <hr />
            <div class="audio">
                <audio>
                    <source src="<?php echo $this->Attr_Get('plugin_file_uploads_url') . $data['file_audio']; ?>" type="audio/mpeg">
                </audio>
                <div>
                    <span><?php _e("Accompagnement audio"); ?></span>
                    <a href="#" class="pause"></a>
                </div>
            </div>
            <?php
        }

    ?>
    <hr />
    <div class="description">
        <?php echo $data['description']; ?>
    </div>
</div>
