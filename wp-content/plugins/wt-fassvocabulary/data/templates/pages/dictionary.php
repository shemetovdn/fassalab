<?php

    $this->Template_Get('areas/page-admin-head');

?>
<div id="<?php echo $this->prefix; ?>-dictionary">
    <form action="" method="post" name="dictionary">
        <div class="search">
            <input type="text" name="search" value="" />
        </div>
        <div class="table"></div>
    </form>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFV_Admin.init.search_dictionary();
        window.WTFV_Admin.dictionary.table_get();
    });
</script>
