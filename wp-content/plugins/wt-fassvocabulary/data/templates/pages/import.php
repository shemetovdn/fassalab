<?php

    $this->Template_Get('areas/page-admin-head');

?>
<div id="<?php echo $this->prefix; ?>-import">
    <form action="" method="post" name="import" enctype="multipart/form-data">
        <fieldset>
            <legend><?php _e('File for import'); ?>:</legend>
            <div><input type="file" name="import[file]" /></div>
            <div class="response"></div>
        </fieldset>
    </form>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        window.WTFV_Admin.init.pre_import();
    });
</script>
