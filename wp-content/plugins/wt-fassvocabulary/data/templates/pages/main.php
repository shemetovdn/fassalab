<?php $this->Template_Get('areas/page-admin-head'); ?>

<div id="<?php echo $this->prefix; ?>-main">
    <ul class="action-main-links">
        <li class="lesson-new">
            <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_dictionary'); ?>">
                <span><?php _e('Gestion de vocabulaire'); ?></span>
            </a>
        </li>
        <li class="lessons-list">
            <a href="<?php echo admin_url('admin.php?page=' . $this->prefix . '_import'); ?>">
                <span><?php _e('Importation de données'); ?></span>
            </a>
        </li>
    </ul>
</div>
